<?php
/**  
 * Controller : cVendas
 * Registro de novas vendas realizadas através do aplicativo, etapa fundamental para geração dos pontos para o cliente
 * será enviado uma mensagem de confirmação no app do cliente para que a venda seja aprovada.
 *
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, LBN Sistemas
 * @version 1.2.0
 */
class cVendas extends Controller {
	private $table='vendas';
	private $colID='VND_ID';			// PK/ID da tabela principal
	private $keyword='Registro de venda';			// Palavra a ser utilizada nas mensagens de retorno
	private $qdUploadID='';
	private $uploadDestination='../etc/uploads/parceiros/';
	private $model;
	private $mysqli;
	/**
	 * Construtor
	 * 
	 */
	public function __construct() 
	{
		// Inicia model
		//	$this->model = new mUsers();
		$this->mysqli = Controller::getMysqli();
	}
		// Escapa SQL Injection
	private function escape($string){
		return($this->mysqli->escape_string($string));	
	}
	// Recupera valor do objeto envelope->fields
	private function get($fields,$column_name) {
		return(Controller::getValue($fields,$column_name));
	}
	// Seta uma valor ao campo do objeto envelope->fields	
	private function set($fields,$column_name,$value) {
		return(Controller::setValue($fields,$column_name,$value));
	}
	/**
	 * Busca os dados do cliente
	 * @param  stdClass envelope, dados possíveis : {filters:[{columns:['CLI_CPF'],value1:'000.000.000-00',rule:'='}]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function buscaDadosCliente($envelope){
		$obj = new stdClass();
		$obj->init = 0;
		$obj->limit = 1;
		$obj->filters = $envelope->filters;	
		$obj->view = $_POST['view'];
		$obj->webService = true;
		$obj->table = 'clientes';
		$obj->columns = 'CLI_ID,CLI_CPF,CLI_NOME, CLI_TOTALPONTOS';
		$obj->msgFail = "CPF não encontrado.";
		Controller::azView($obj);
	}
	 
	/**
	 * Traz os cadastros para serem visualizados e editados(com a devida permissao) na view
	 * @param  stdClass envelope, dados possíveis : inicio, limite, orderBy, filtros[{}]
	 * @return string json mensagem sucesso  ou falha
	 */
	public function requestData($envelope) {
		$obj = new stdClass();
		$obj->init = $envelope->init;
		$obj->limit = $envelope->limit;
		$obj->orderBy = $envelope->orderBy;
		$obj->filters = $envelope->filters;	
		$obj->view = $_POST['view'];
		$obj->webService = false;
		$obj->table = $this->table;
		$obj->complSQL = 'INNER JOIN parceiros ON VND_PAR_ID = PAR_ID ';
		// Administrador vê todas as vendas
		if($_SESSION[SES_USER_LEVEL]=='ADMINISTRATOR') {
			$obj->complSQL .= 'INNER JOIN clientes ON VND_CLI_ID = CLI_ID';
		
		// O parceiro vê apenas as suas vendas
		}else {
			$obj->complSQL .= 'INNER JOIN clientes ON VND_CLI_ID = CLI_ID WHERE VND_PAR_ID='.$_SESSION['PAR_ID'];	
		}
		
		// Caso esteja na view "Aprovação de vendas", visualiza apenas as vendas ainda carentes de aprovação
		if(_view=='aprovacao-vendas') {
			$obj->complSQL .= ' AND VND_STATUS="REGISTRADO"';
		}	
		
		// Colunas reservadas do sistema 
		array_push($envelope->columns,'VND_STATUS');	
		array_push($envelope->columns,'VND_MSGEXTRA');
		array_push($envelope->columns,'CLI_CPF');
		
		array_push($envelope->columns,'vendas.AZ_USARCHIVED USUARIO');
		array_push($envelope->columns,'vendas.AZ_DHARCHIVED DATAHORA');				
	
		// Adiciona as colunas adicionais + as solicitadas pela View
		$obj->columns = $envelope->columns;
		
		// Executa a ação pelo Controller e traz o objeto de retorno 
		$ret = Controller::azView($obj);
		
		// Verifica se processou com sucesso
		if($ret->type=='win') {
			$dataMainTable = $ret->data;
		
		// Falha ou alerta
		}else {
			$dataMainTable = array();
		}
		
		// Imprimi o retorno no formato JSON
		Controller::azReturn($ret->message, $ret->type, $ws=true,$dataMainTable);
			
	}
	/**
	 * Reserva ou busca (caso já tenha sido reservada) uma PK(ID) para o próximo cadastro
	 * Verifica e exclui os arquivos de upload de uma tentativa fracassada de cadastro anterior (com o mesmo ID)
	 * @return string json com a PK(ID) reservada ou mensagem de falha
	 */
	public function prepareContainer() {
		// Array para o conteúdo dos frames de upload
		$uploadFramesContent = array();
		
		// Busca uma possível PK(ID) já reservada ou reserva uma nova
		$r = Controller::findReserveID($this->table,$this->colID,$_POST['view']);
		
		// Verifica se processou com sucesso
		if( $r->type=='win' ) {
			// ID reservado/encontrado
			$ID = $r->data->ID;
		

			// Define o objeto de retorno
			$objRet = array('ID'=>$ID);
			
		// Falha:
		}else {
			$objRet = array();
			
		}
		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($r->message, $r->type, true, $objRet);
	}

	/**
	 * Inclui um novo cadastro do pacote principal (clientes)
	 * @param  stdClass envelope, dados possíveis :  {acao:"insert_Main", campos : [{coluna, value}]}
	 * @return string json com a PK (ID) do cadastro recem criado ou mensagem de falha
	 */
	public function insert_Main($envelope) {
		
		// Ação negada para usuarios nivel USUARIO
		if($_SESSION[SES_USER_LEVEL]=='USER') {
			Controller::azReturn("Permissão negada, inclusão cancelada.","fail",true);
		
		// Para todos outros níveis acima, permite inclusões :
		}else {
			// Busca o ID do cliente
			$sql = "SELECT CLI_ID FROM clientes WHERE CLI_CPF='".Controller::getValue($envelope,'CLI_CPF')."'";
			$r = $this->mysqli->query($sql);
			if($r->num_rows==0) {
				Controller::azReturn('CPF não localizado','fail',true);
			}
			$row = $r->fetch_assoc();
			$cli_id = $row['CLI_ID'];
			
			// Busca dados do Parceiro
			$sql = "SELECT PAR_FANTASIA, CID_NOME FROM parceiros 
			INNER JOIN az_users ON USE_PAR_ID = PAR_ID
			INNER JOIN cidades ON PAR_CID_ID = CID
			WHERE USE_ID='".$_SESSION[SES_USER_ID]."'";
			$r = $this->mysqli->query($sql);
			if($r->num_rows==0) {
				Controller::azReturn('Dados do parceiro não localizados','fail',true);
			}
			$row = $r->fetch_assoc();
			
			// Cria notificação para que o usuário confirme a venda registrada
			$obj = new stdClass();
			$cp = new Fields($envelope->fields);
			$cp->add('NOT_VND_ID',$envelope->ID);
			$cp->add('NOT_TITULO','Nova Compra');
			$cp->add('NOT_CLI_ID', $cli_id);
			$cp->add('NOT_DATAHORA',date('Y-m-d H:i:s'));
			$vrCompra = Controller::convertNumber(Controller::getValue($envelope,'VND_TOTALBRUTO'));
			$cp->add('NOT_MSG','Confirma compra de R$ '.$vrCompra.' na empresa <b>'.$row['PAR_FANTASIA'].'</b> em '.$row['CID_NOME'].'?');
			$obj->fields = $cp->ret();
			$obj->table = 'notificacoes';
			$obj->view = $_POST['view'];
			$obj->webService = false;
			$ret = Controller::azInsert($obj);
			// Verifica se inseriu a notificação
			if($ret->type=='win') {
				// Cria objeto com os campos recebidos, adicionando outros campos do sistema
				$cp = new Fields($envelope->fields);
				$cp->add($this->colID,$envelope->ID);
				$cp->add('AZ_DHREGISTER',date('Y-m-d H:i:s'));
				$cp->add('AZ_USREGISTER',$_SESSION[SES_USER_NAME]);
				// Prossegue com a inclusão do cadastro principal
				$obj->fields = $cp->ret();
				$obj->table = $this->table;
				$obj->view = $_POST['view'];
				$obj->msgWin = $this->keyword.' incluído com sucesso';
				$obj->msgFail =  $this->keyword.' não incluído, tente novamente.';
				// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
				$obj->webService = true;
				// Registra ação no BD do sistema
				Controller::registerAction();
				// Executa a ação pela classe Controller
				Controller::azInsert($obj);
			}else {
				$sql = "DELETE * FROM notificacoes WHERE NOT_VND_ID=".$envelope->ID;
				$this->mysqli->query($sql);
				Controller::azReturn('Problema para registrar a notificação do cliente, tente novamente.', 'fail', true);
			}
		}
	}
	/**
	 * Arquivo um dado cadastro
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"archive_Main", ID:[do cadastro principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function archive_Main($envelope) {
		// Ação negada para usuarios nivel USER
		if($_SESSION[SES_USER_LEVEL]=='USER') {
			Controller::azReturn("Permissão negada, arquivamento cancelado.","fail",true);
			
		// Para qualquer outro nível acima :
		}else {	
			$obj = new stdClass();
			$obj->table = $this->table;
			$obj->colID = $this->colID;
			$obj->ID = $envelope->ID;// Array com o(s) ID(s) selecionado(s)
			// Cria os campos do framework  para arquivamento de cadastro
			$cp = new Fields();
			$cp->add('AZ_ARCHIVED',1);
			$cp->add('AZ_DHARCHIVED',date('Y-m-d H:i:s'));
			$cp->add('AZ_USARCHIVED',$_SESSION[SES_USER_NAME]);
			// Adiciona os campos criados
			$obj->fields = $cp->ret();
			$obj->view = $_POST['view'];
			$s = count($envelope->ID) > 1 ? 's' : '';// Variavel para complemento da frase de erro ou sucesso
			$obj->msgWin =   "Cadastro$s de ".$this->keyword."$s arquivado$s com sucesso";
			$obj->msgFail =  "Cadastro$s de ".$this->keyword."$s não arquivado$s, tente novamente.";
			// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
			$obj->webService = true;
			// Registra ação no BD do sistema
			Controller::registerAction();
			// Executa a ação pela classe Controller
			Controller::azUpdate($obj);
		}
	}
	
	/**
	 * Altera um cadastro existente do pacote principal (clientes)
	 * @param  stdClass envelope, dados obrigatorios :  
	   {acao:"update_Main", campos : [{coluna, value, default}], ID:[_ID_RESERVADO tabela principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function update_Main($envelope) {
		$obj = new stdClass();
		$obj->table = $this->table;
		$obj->colID = $this->colID;
		$obj->ID = $envelope->ID; 			
		$obj->fields = $envelope->fields;
		$obj->view = $_POST['view'];
		$obj->msgWin = $this->keyword.' alterado com sucesso';
		$obj->msgFail = $this->keyword.' não alterado, tente novamente.';
		
		// Se estiver na tela de aprovação do pedido e o status for de aprovado, quer dizer que está sendo aprovado agora
		// Então: gera os pontos do usuário(1%) e debitada os pontos do parceiro (1,5%)
		if(_view=='aprovacao-vendas' && $this->get($obj->fields,"VND_STATUS")=='APROVADO') {
			// Busca os dados mínimos da venda
			$sql = "SELECT * FROM vendas WHERE VND_ID=".$envelope->ID;
			$r = $this->mysqli->query($sql);
			$rowv = $r->fetch_assoc();
			#
			# Verifica se é fornecedor avulso
			#
			if($rowv['VND_PAR_ID']>0) {
				// Verifica se o lojista está pontuando dobrado
				$sql = "SELECT * FROM campanhas_parceiros WHERE 
							CAP_PAR_ID=".$_SESSION['PAR_ID']." AND
							( 
							 (CAP_DIASEMANA>0 AND CAP_DIASEMANA=DAYOFWEEK(NOW()) OR (CAP_DIAMES=DAY(NOW()) AND CAP_DIAMES>0)) 
							)
							AND CAP_STATUS='ATIVO' AND CAP_DHINICIAL>=NOW() AND CAP_DHFIM<=NOW()";
				$r = $this->mysqli->query($sql);
				// Pontos dobrados:
				if($r->num_rows>0) {
					$rowc = $r->fetch_assoc();
					$cap_id = $rowc['EXP_CAP_ID'];
					// Calcula o total de pontos dobrados (5%)
					$totalPontosDeb = ceil($rowv["VND_TOTALBRUTO"]*0.05);
					$totalPontosCred = ceil($rowv["VND_TOTALBRUTO"]*0.02);
				// Pontos normais:	
				}else {
					$cap_id = '0';
					// Calcula o total de pontos (2,5%)
					$totalPontosDeb = ceil($rowv["VND_TOTALBRUTO"]*0.025);
					$totalPontosCred = ceil($rowv["VND_TOTALBRUTO"]*0.01);		
				}
				// Calcula os pontos
				$pontosCliente = 45 * $totalPontosCred;
				$pontosParceiro = 45 * $totalPontosDeb;
				// String SQL para gerar o lançamento de bônus para o parceiro
				$sql = "INSERT INTO extrato_pontos SET EXP_PAR_ID=".$rowv['VND_PAR_ID'].", EXP_PONTOS='".$pontosParceiro."',
				EXP_DEBCRED='D', EXP_DESCRICAO='Nova venda confirmada',EXP_CAP_ID='".$cap_id."', AZ_DHREGISTER=NOW(), AZ_USREGISTER='".$_SESSION[SES_USER_LOGIN]."', EXP_VND_ID=".$envelope->ID;
				$this->mysqli->query($sql);	 
				// String SQL para gerar o débito para o cliente
				$sql = "INSERT INTO extrato_pontos SET EXP_CLI_ID=".$rowv['VND_CLI_ID'].", EXP_PONTOS='".$pontosCliente."',
				EXP_DEBCRED='C', EXP_DESCRICAO='Venda aprovada',EXP_CAP_ID='".$cap_id."', AZ_DHREGISTER=NOW(), AZ_USREGISTER='".$_SESSION[SES_USER_LOGIN]."', EXP_VND_ID=".$envelope->ID;
				$this->mysqli->query($sql);
				// String SQL para atualizar o saldo de pontos do cliente
				$sql = "UPDATE clientes SET CLI_TOTALPONTOS=CLI_TOTALPONTOS+".$pontosCliente." WHERE CLI_ID=".$rowv['VND_CLI_ID'];
				$this->mysqli->query($sql);
			}
		}
		
		// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
		$obj->webService = true;
		// Registra ação no BD do sistema
		Controller::registerAction();
		// Executa a ação pela classe Controller utilizando a model mMeusdados
		$ret = Controller::azUpdate($obj); 
	
	}
	
	
	/**
	 * Traz todos os dados para carregamento de todos formularios da view (principal e dinamicos)
	 * @param  stdClass envelope, dados possíveis : acao : 'dadosFormularios', 
	 *         filtros : { value1 : ID, colunas : ['<?=_columnID?>'], regra : '=' 
	 * @return string json mensagem sucesso  ou falha
	 */
	public function formsData($envelope) {
		# Array para retornar os conteudos dos frames de upload
		$uploadFramesContent = array();
		
		# ID do cadastro principal
		$ID = $envelope->ID;
		
		# Busca os dados da tabela principal 
		$obj = new stdClass();
		$obj->view = $_POST['view'];
		$obj->filters = $envelope->filters;
		$obj->webService = false; 
		$obj->table = $this->table;
		$obj->complSQL = 'INNER JOIN clientes ON VND_CLI_ID = CLI_ID';
		
		// Executa a ação pela classe Controller e salva a resposta
		$ret = Controller::azView($obj);
		
		// Verifica se foi executado com sucesso
		if($ret->type=='win') {
			// Define objeto com os dados da tabela Principal "Main"
			$dataMainTable = $ret->data->ret;
						
			// Define o objeto de retorno
			$objRet = array( $this->table => $dataMainTable);
			
		// Falha:
		}else {
			$objRet = array();
		}
		
		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($ret->message, $ret->type, true, $objRet);
	}
	
	 
	 /**
	 * Recepciona os arquivos do upload e seta as regras para processamento do(s) arquivo(s)
	 * @param stdClass envelope, dados possíveis : acao, qdUploadID, ID
	 * @return string json mensagem sucesso  ou falha
	 */
	public function receiveFile($envelope) {
		// Upload do logo do parceiro
		if($envelope->qdUploadID==$this->qdUploadID) {
			// Pasta destino para armazenamento do(s) arquivo(s) do upload
			$envelope->destination = SYS_SRC_APP.$this->uploadDestination.$envelope->ID."/logo/";
			// Array com o(s) arquivo(s) do upload
			$envelope->files = $_FILES['files'];
			// Se true : cria uma miniatura da imagem na mesma pasta de upload para rapida exibição na view
			$envelope->thumbnail = true;
			// Tamanho máximo em MB
			$envelope->maxSizeMB = 2;
			// Quantidade máxima
			$envelope->maxAmount = 1;
			// Altera nome do arquivo original ?
			$envelope->updateFileName = false;
			// Extensoes permitidas (argumento para preg_match)
			$envelope->allowedExtensions = "jp?g|png";
			// Url dos arquivos, para retorno das imagens após upload efetudo
			$envelope->urlFiles = SYS_APP_URL.$this->uploadDestination.$envelope->ID."/logo/";
			
			// Recepciona o(s) arquivo(s) e imprimi o resultado
			Controller::uploadFile($envelope);
		// qdUploadID não reconhecido 
		}else {
			Controller::azReturn('qdUploadID não reconhecido.', 'fail', true);	
		}
	}
	
	/**
	 * Baixar de formar dinamica um determinado arquivo
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"baixarArquivo", fileName, qdUploadID, ID}
	 * @return string json com a URL para baixar o arquivo ou mensagem de falha
	 */
	public function downloadFile($envelope) {
		$fileName = Controller::getValue($envelope->fields,'fileName');
		$qdUploadID = Controller::getValue($envelope->fields,'qdUploadID');
		$src	 = $this->uploadDestination.$envelope->ID."/logo/".$fileName ;	
		$srcFile = realpath(SYS_SRC_APP.$src); // Caminho real do arquivo
		$urlFile = SYS_APP_URL.$src; // Endereço web do arquivo
		
		// Verifica se é um arquivo válido 
		if(is_file($srcFile)){
			$tipo = 'win';
			$msg = '';
			// Sessões utilizadas no arquivo php/downloader.php
			$_SESSION['urlFileToDownload'] = $urlFile;
			$_SESSION['srcFileToDownload'] = $srcFile;
			
		// Arquivo inválido
		}else {
			$tipo = 'fail';
			$msg = 'Arquivo não encontrado.';
			// Resetas as sessões de download
			$_SESSION['urlFileToDownload'] = null;
			$_SESSION['srcFileToDownload'] = null;
			
			
		}
		
		// De qualquer forma, retorna a URL  e o caminho fonte do arquivo
		$objRet = array('url'=>$urlFile,'src'=>$srcFile);

		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($msg, $tipo, true, $objRet);
	}
	/**
	 * Deleta um arquivo do servidor (deletar também o thumbnail se houver)
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"deletarArquivo", fileName, qdUploadID, ID}
	 * @return string json com mensagem de sucesso ou falha
	 */
	public function deleteFile($envelope) {
		$obj = new stdClass();
		$fileName = Controller::getValue($envelope->fields,'fileName');
		$qdUploadID = Controller::getValue($envelope->fields,'qdUploadID');
		// Deleta imagem da categoria principal
		if($qdUploadID==$this->qdUploadID){
			$obj = new stdClass();
			$obj->view = $_POST['view'];
			$obj->fileName = $fileName;
			$obj->folder = $this->uploadDestination.$envelope->ID."/";
			$obj->webService = true;
		}
		
		// Executa a ação pela classe Controller
		Controller::delete_file($obj);	
	}
	
	
}
