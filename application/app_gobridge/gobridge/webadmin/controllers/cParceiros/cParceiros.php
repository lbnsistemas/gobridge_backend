<?php
// Incluí model especifica
//require_once(SYS_SRC_MODELS."/mUsers/mUsers.php");

/**  
 * Controller : cUsuarios
 * Inclusão e alteração no cadastro de usuários (principal)
 * criação de perfis de permissões
 *
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, LBN Sistemas
 * @version 1.2.0
 */
class cParceiros extends Controller {
	private $table='parceiros';
	private $colID='PAR_ID';			// PK/ID da tabela principal
	private $keyword='Cadastro de parceiro';			// Palavra a ser utilizada nas mensagens de retorno
	private $qdUploadID='uploadLogoParceiro';
	private $uploadDestination='../etc/uploads/parceiros/';
	private $model;
	private $mysqli;
	/**
	 * Construtor
	 * 
	 */
	public function __construct() 
	{
		// Inicia model
		//	$this->model = new mUsers();
		$this->mysqli = Controller::getMysqli();
	}
	
	/**
	 * Traz os cadastros para serem visualizados e editados(com a devida permissao) na view
	 * @param  stdClass envelope, dados possíveis : inicio, limite, orderBy, filtros[{}]
	 * @return string json mensagem sucesso  ou falha
	 */
	public function requestData($envelope) {
		$obj = new stdClass();
		$obj->init = $envelope->init;
		$obj->limit = $envelope->limit;
		$obj->orderBy = $envelope->orderBy;
		$obj->filters = $envelope->filters;	
		$obj->view = $_POST['view'];
		$obj->webService = false;
		$obj->table = $this->table;
		
		// Colunas reservadas do sistema 
		array_push($envelope->columns,$this->table.'.AZ_ARCHIVED'); 	// Informa se o cadastro está arquivado
		array_push($envelope->columns,$this->table.'.AZ_USARCHIVED');	// Informa o nome do usuário que arquivou
		array_push($envelope->columns,$this->table.'.AZ_DHARCHIVED');	// Informa a data que foi arquivado
		array_push($envelope->columns,$this->table.'.PAR_LOGO');		// Logo
	
		// Adiciona as colunas adicionais + as solicitadas pela View
		$obj->columns = $envelope->columns;
		
		// Executa a ação pelo Controller e traz o objeto de retorno 
		$ret = Controller::azView($obj);
		
		// Verifica se processou com sucesso
		if($ret->type=='win') {
			$dataMainTable = $ret->data;
			$rows = $ret->data->ret;
			// Analisa todas categorias e verifica se alguma possuí thumbnail a ser apresentado na listagem
			foreach($rows as $row) {
				$srcFiles = $this->uploadDestination.$row->PAR_ID."/logo/";
				$arFile = Controller::hasThumbnail($row->PAR_LOGO,$srcFiles);
				if($arFile !==false) {
					$row->THUMBNAIL = $arFile['thumbnail'];
					$row->URL = $arFile['url'];
				}
				
			}
		
		// Falha ou alerta
		}else {
			$dataMainTable = array();
		}
		
		// Imprimi o retorno no formato JSON
		Controller::azReturn($ret->message, $ret->type, $ws=true,$dataMainTable);
			
	}
	/**
	 * Reserva ou busca (caso já tenha sido reservada) uma PK(ID) para o próximo cadastro
	 * Verifica e exclui os arquivos de upload de uma tentativa fracassada de cadastro anterior (com o mesmo ID)
	 * @return string json com a PK(ID) reservada ou mensagem de falha
	 */
	public function prepareContainer() {
		// Array para o conteúdo dos frames de upload
		$uploadFramesContent = array();
		
		// Busca uma possível PK(ID) já reservada ou reserva uma nova
		$r = Controller::findReserveID($this->table,$this->colID,$_POST['view']);
		
		// Verifica se processou com sucesso
		if( $r->type=='win' ) {
			// ID reservado/encontrado
			$ID = $r->data->ID;
			
			// Busca as cidades
			$obj = new stdClass();
			$obj->view = $_POST['view'];
			$obj->webService = false; 
			$obj->orderBy = 'EST_SIGLA ASC';
			$obj->table = 'estados';
			$estados = Controller::azView($obj)->data->ret;
			
			// Busca as categorias 
			$obj = new stdClass();
			$obj->view = $_POST['view'];
			$obj->webService = false; 
			$obj->orderBy = 'PAC_TITULO ASC';
			$obj->table = 'categorias_parceiros';
			$categorias = Controller::azView($obj)->data->ret;

			
			// Faz leitura da pasta de arquivos do cadastro principal
			$files = Controller::readFiles($this->uploadDestination.$ID."/logo/");
			
			// Se houver uma imagem da capa
			if(count($files)>0) {
				// Arrays com os conteúdos de cada frame de upload de arquivos
				$uploadFramesContent = array('nameID'=>$this->qdUploadID, 'files'=>$files);
			}

			// Define o objeto de retorno
			$objRet = array('ID'=>$ID,'uploadFramesContent'=>$uploadFramesContent,'estados'=>$estados,"categorias_parceiros"=>$categorias);
			
		// Falha:
		}else {
			$objRet = array();
			
		}
		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($r->message, $r->type, true, $objRet);
	}

	/**
	 * Inclui um novo relacionamento entre parceiro e segmento
	 * @param  stdClass envelope, dados possíveis :  {acao:"insert_formMainSegmentos", campos : [{coluna, value}]}
	 * @return string json com a PK (ID) do cadastro recem criado ou mensagem de falha
	 */
	public function insert_formMainSegmentos($envelope) {
		$obj = new stdClass();
		$obj->table = 'parceiros_segmentos';
		
		// Index de relacionamento com a tabela principal
		$cp = new Fields($envelope->fields);
		$cp->add('PSE_PAR_ID',$envelope->ID);
		$obj->fields = $cp->ret();
		$obj->view = $_POST['view'];
		$obj->msgWin = '';
		$obj->msgFail = 'Segmento não incluído, tente novamente.';
		// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
		$obj->webService = true;
		// Registra ação no BD do sistema
		Controller::registerAction();
		// Executa a ação pela classe Controller
		Controller::azInsert($obj);
	}
	/**
	 * Altera um relacionamento de segmento 
	 * @param  stdClass envelope, dados possíveis :  
	 	{acao:"update_formMainSegmentos", campos : [{coluna, value}], ID:[da tabela principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function update_formMainSegmentos($envelope) {
		$obj = new stdClass();
		$obj->table = 'parceiros_segmentos';
		$obj->colID = 'PSE_ID';
		$obj->ID = $envelope->ID;
		$obj->fields = $envelope->fields;
		$obj->view = $_POST['view'];
		$obj->msgWin = 'Segmento alterado com sucesso';
		$obj->msgFail = 'Segmento não alterado, tente novamente.';
		// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
		$obj->webService = true;
		// Registra ação no BD do sistema
		Controller::registerAction();
		// Executa a ação pela classe Controller
		Controller::azUpdate($obj);
	}
	/**
	 * Deleta permanentemente um relacionamento de segmento
	 * @param  stdClass envelope, dados obrigatórios :  {acao:"archive_formMainSegmentos", ID}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function archive_formMainSegmentos($envelope) {
		$obj = new StdClass();
		$obj->table='parceiros_segmentos';
		$obj->colID='PSE_ID';
		$obj->ID=$envelope->ID;
		$obj->keyword='Segmento';
		$obj->webService=true;
		// Executa ação de arquivamento principal
		Controller::azDelete($obj);
	}

	/**
	 * Inclui um novo cadastro do pacote principal (clientes)
	 * @param  stdClass envelope, dados possíveis :  {acao:"insert_Main", campos : [{coluna, value}]}
	 * @return string json com a PK (ID) do cadastro recem criado ou mensagem de falha
	 */
	public function insert_Main($envelope) {
		
		// Ação negada para usuarios nivel USUARIO
		if($_SESSION[SES_USER_LEVEL]=='USER') {
			Controller::azReturn("Permissão negada, inclusão cancelada.","fail",true);
		
		// Para todos outros níveis acima, permite inclusões :
		}else {
			$obj = new stdClass();			
			// Cria objeto com os campos recebidos, adicionando outros campos do sistema
			$cp = new Fields($envelope->fields);
			$cp->add($this->colID,$envelope->ID);
			$cp->add('AZ_DHREGISTER',date('Y-m-d H:i:s'));
			$cp->add('AZ_USREGISTER',$_SESSION[SES_USER_NAME]);
			// Prossegue com a inclusão do cadastro principal
			$obj->fields = $cp->ret();
			$obj->table = $this->table;
			$obj->view = $_POST['view'];
			$obj->msgWin = $this->keyword.' incluído com sucesso';
			$obj->msgFail =  $this->keyword.' não incluído, tente novamente.';
			// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
			$obj->webService = true;
			// Registra ação no BD do sistema
			Controller::registerAction();
			// Executa a ação pela classe Controller
			Controller::azInsert($obj);
		}
	}
	/**
	 * Arquivo um dado cadastro
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"archive_Main", ID:[do cadastro principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function archive_Main($envelope) {
		// Ação negada para usuarios nivel USER
		if($_SESSION[SES_USER_LEVEL]=='USER') {
			Controller::azReturn("Permissão negada, arquivamento cancelado.","fail",true);
			
		// Para qualquer outro nível acima :
		}else {	
			$obj = new stdClass();
			$obj->table = $this->table;
			$obj->colID = $this->colID;
			$obj->ID = $envelope->ID;// Array com o(s) ID(s) selecionado(s)
			// Cria os campos do framework  para arquivamento de cadastro
			$cp = new Fields();
			$cp->add('AZ_ARCHIVED',1);
			$cp->add('AZ_DHARCHIVED',date('Y-m-d H:i:s'));
			$cp->add('AZ_USARCHIVED',$_SESSION[SES_USER_NAME]);
			// Adiciona os campos criados
			$obj->fields = $cp->ret();
			$obj->view = $_POST['view'];
			$s = count($envelope->ID) > 1 ? 's' : '';// Variavel para complemento da frase de erro ou sucesso
			$obj->msgWin =   "Cadastro$s de ".$this->keyword."$s arquivado$s com sucesso";
			$obj->msgFail =  "Cadastro$s de ".$this->keyword."$s não arquivado$s, tente novamente.";
			// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
			$obj->webService = true;
			// Registra ação no BD do sistema
			Controller::registerAction();
			// Executa a ação pela classe Controller
			Controller::azUpdate($obj);
		}
	}
	
	/**
	 * Altera um cadastro existente do pacote principal (clientes)
	 * @param  stdClass envelope, dados obrigatorios :  
	   {acao:"update_Main", campos : [{coluna, value, default}], ID:[_ID_RESERVADO tabela principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function update_Main($envelope) {
		$obj = new stdClass();
		$obj->table = $this->table;
		$obj->colID = $this->colID;
		$obj->ID = $envelope->ID; 		
		//Busca a latitude e longitude utilizando API do Google
		if(trim(Controller::getValue($envelope->fields,'PAR_LATITUDE'))==0 || trim(Controller::getValue($envelope->fields,'PAR_LONGITUDE'))==0) {
			$xAdress = Controller::getValue($envelope->fields,'PAR_LOGRADOURO');
			$xAdress .= ", ".Controller::getValue($envelope->fields,'PAR_NUMERO');
			//Busca o nome da cidade
			$sql = "SELECT CID_NOME FROM cidades WHERE CID_ID = ".Controller::getValue($envelope->fields,'PAR_CID_ID');
			$r = $this->mysqli->query($sql);
			$row = $r->fetch_assoc();
			$xAdress .= ", ".$row['CID_NOME'];
			//Consulta a API geocoding do google
			$geoData = Controller::loadExternal('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($xAdress).'&key=AIzaSyDFBtU-m944g8eI373JVAk6gWEr0s2oQ1E');
			$geoData = json_decode($geoData);
			$latitude = $geoData->results[0]->geometry->location->lat;
			$longitude = $geoData->results[0]->geometry->location->lng;
		}

		Controller::setValue($envelope->fields,'PAR_LATITUDE',$latitude);
		Controller::setValue($envelope->fields,'PAR_LONGITUDE',$longitude);
		
		$obj->fields = $envelope->fields;
		$obj->view = $_POST['view'];
		$obj->msgWin = $this->keyword.' alterado com sucesso';
		$obj->msgFail = $this->keyword.' não alterado, tente novamente.';
		// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
		$obj->webService = true;
		// Registra ação no BD do sistema
		Controller::registerAction();
		// Executa a ação pela classe Controller utilizando a model mMeusdados
		$ret = Controller::azUpdate($obj); 
	
	}
	
	
	/**
	 * Traz todos os dados para carregamento de todos formularios da view (principal e dinamicos)
	 * @param  stdClass envelope, dados possíveis : acao : 'dadosFormularios', 
	 *         filtros : { value1 : ID, colunas : ['<?=_columnID?>'], regra : '=' 
	 * @return string json mensagem sucesso  ou falha
	 */
	public function formsData($envelope) {
		# Array para retornar os conteudos dos frames de upload
		$uploadFramesContent = array();
		
		# ID do cadastro principal
		$ID = $envelope->ID;
		
		# Busca os dados da tabela principal 
		$obj = new stdClass();
		$obj->view = $_POST['view'];
		$obj->filters = $envelope->filters;
		$obj->webService = false; 
		$obj->table = $this->table;
	
		
		// Executa a ação pela classe Controller e salva a resposta
		$ret = Controller::azView($obj);
		
		// Verifica se foi executado com sucesso
		if($ret->type=='win') {
			// Define objeto com os dados da tabela Principal "Main"
			$dataMainTable = $ret->data->ret;
			
			// Faz leitura da pasta de arquivos do diretorio principal
			$files = Controller::readFiles($this->uploadDestination.$ID."/logo/");
			
			// Se houver uma imagem da capa
			if(count($files)>0) {
				// Arrays com os conteúdos de cada frame de upload de arquivos
				array_push($uploadFramesContent, array('nameID'=>$this->qdUploadID, 'files'=>$files));
			}

			
			// Busca os estados, com o estado cadastrado no topo
			$obj = new stdClass();
			$obj->table = 'estados';
			$obj->view = $_POST['view'];
			$obj->webService = false;
			$obj->orderBy = 'EST_ID='.$dataMainTable[0]->PAR_EST_ID.' DESC, EST_SIGLA ASC';
			$resp = Controller::azView($obj);
			$estados = $resp->data->ret;
			
			// Busca as cidades do mesmo estado, com a cidade cadastrada no topo
			$obj = new stdClass();
			$obj->table = 'cidades';
			$obj->view = $_POST['view'];
			$obj->webService = false;
			$obj->complSQL = 'WHERE CID_ID_ESTADO="'.$dataMainTable[0]->PAR_EST_ID.'"';
			if($dataMainTable[0]->PAR_CID_ID>0) {
				$obj->orderBy = 'CID_ID='.$dataMainTable[0]->PAR_CID_ID.' DESC, CID_NOME ASC';
			}else {
				$obj->orderBy = 'CID_NOME ASC';
			}
			$resp = Controller::azView($obj);
			$cidades = $resp->data->ret;
			
			// Busca as categorias 
			$obj = new stdClass();
			$obj->view = $_POST['view'];
			$obj->webService = false; 
			$obj->table = 'categorias_parceiros';
			$obj->orderBy = 'PAC_ID='.$dataMainTable[0]->PAR_PAC_ID.' DESC, PAC_TITULO ASC';
			$categorias = Controller::azView($obj)->data->ret;
			
			// Busca os segmentos 
			$obj = new stdClass();
			$obj->view = $_POST['view'];
			$obj->webService = false; 
			$obj->orderBy = 'SEG_TITULO ASC';
			$obj->table = 'segmentos';
			$segmentos = Controller::azView($obj)->data->ret;
			
			// Busca as etapas/fases de obra 
			$obj = new stdClass();
			$obj->view = $_POST['view'];
			$obj->webService = false; 
			$obj->orderBy = 'ETA_TITULO ASC';
			$obj->table = 'etapas_obras';
			$etapas = Controller::azView($obj)->data->ret;
			
			
			// Relação de segmentos com o parceiro
			$obj = new stdClass();
			$obj->view = $_POST['view'];
			$obj->webService = false; 
			$obj->complSQL = '
			INNER JOIN segmentos ON SEG_ID = PSE_SEG_ID
			WHERE PSE_PAR_ID='.$dataMainTable[0]->PAR_ID;
			$obj->orderBy = 'SEG_TITULO ASC';
			$obj->table = 'parceiros_segmentos';
			$parceiros_segmentos = Controller::azView($obj)->data->ret;

			
			// Define o objeto de retorno
			$objRet = array( $this->table => $dataMainTable,'uploadFramesContent'=>$uploadFramesContent,
			'estados'=>$estados,'cidades'=>$cidades, 'categorias_parceiros'=>$categorias,'segmentos'=>$segmentos,
			'etapas'=>$etapas,'parceiros_segmentos'=>$parceiros_segmentos);
			
		// Falha:
		}else {
			$objRet = array();
		}
		
		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($ret->message, $ret->type, true, $objRet);
	}
	
	 
	 /**
	 * Recepciona os arquivos do upload e seta as regras para processamento do(s) arquivo(s)
	 * @param stdClass envelope, dados possíveis : acao, qdUploadID, ID
	 * @return string json mensagem sucesso  ou falha
	 */
	public function receiveFile($envelope) {
		// Upload do logo do parceiro
		if($envelope->qdUploadID==$this->qdUploadID) {
			// Pasta destino para armazenamento do(s) arquivo(s) do upload
			$envelope->destination = SYS_SRC_APP.$this->uploadDestination.$envelope->ID."/logo/";
			// Array com o(s) arquivo(s) do upload
			$envelope->files = $_FILES['files'];
			// Se true : cria uma miniatura da imagem na mesma pasta de upload para rapida exibição na view
			$envelope->thumbnail = false;
			// Tamanho máximo em MB
			$envelope->maxSizeMB = 2;
			// Quantidade máxima
			$envelope->maxAmount = 1;
			// Altera nome do arquivo original ?
			$envelope->updateFileName = false;
			// Extensoes permitidas (argumento para preg_match)
			$envelope->allowedExtensions = "jp?g|png";
			// Url dos arquivos, para retorno das imagens após upload efetudo
			$envelope->urlFiles = SYS_APP_URL.$this->uploadDestination.$envelope->ID."/logo/";
			
			// Recepciona o(s) arquivo(s) e imprimi o resultado
			Controller::uploadFile($envelope);
		// qdUploadID não reconhecido 
		}else {
			Controller::azReturn('qdUploadID não reconhecido.', 'fail', true);	
		}
	}
	
	/**
	 * Baixar de formar dinamica um determinado arquivo
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"baixarArquivo", fileName, qdUploadID, ID}
	 * @return string json com a URL para baixar o arquivo ou mensagem de falha
	 */
	public function downloadFile($envelope) {
		$fileName = Controller::getValue($envelope->fields,'fileName');
		$qdUploadID = Controller::getValue($envelope->fields,'qdUploadID');
		$src	 = $this->uploadDestination.$envelope->ID."/logo/".$fileName ;	
		$srcFile = realpath(SYS_SRC_APP.$src); // Caminho real do arquivo
		$urlFile = SYS_APP_URL.$src; // Endereço web do arquivo
		
		// Verifica se é um arquivo válido 
		if(is_file($srcFile)){
			$tipo = 'win';
			$msg = '';
			// Sessões utilizadas no arquivo php/downloader.php
			$_SESSION['urlFileToDownload'] = $urlFile;
			$_SESSION['srcFileToDownload'] = $srcFile;
			
		// Arquivo inválido
		}else {
			$tipo = 'fail';
			$msg = 'Arquivo não encontrado.';
			// Resetas as sessões de download
			$_SESSION['urlFileToDownload'] = null;
			$_SESSION['srcFileToDownload'] = null;
			
			
		}
		
		// De qualquer forma, retorna a URL  e o caminho fonte do arquivo
		$objRet = array('url'=>$urlFile,'src'=>$srcFile);

		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($msg, $tipo, true, $objRet);
	}
	/**
	 * Deleta um arquivo do servidor (deletar também o thumbnail se houver)
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"deletarArquivo", fileName, qdUploadID, ID}
	 * @return string json com mensagem de sucesso ou falha
	 */
	public function deleteFile($envelope) {
		$obj = new stdClass();
		$fileName = Controller::getValue($envelope->fields,'fileName');
		$qdUploadID = Controller::getValue($envelope->fields,'qdUploadID');
		// Deleta imagem da categoria principal
		if($qdUploadID==$this->qdUploadID){
			$obj = new stdClass();
			$obj->view = $_POST['view'];
			$obj->fileName = $fileName;
			$obj->folder = $this->uploadDestination.$envelope->ID."/logo/";
			$obj->webService = true;
		}
		
		// Executa a ação pela classe Controller
		Controller::delete_file($obj);	
	}
	
	
}
