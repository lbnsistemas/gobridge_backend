<?php
// Incluí model especifica
require_once(SYS_SRC_MODELS."/mAppLevelsRules/mAppLevelsRules.php");
/**  
 * Controller : cConfigAsterisk
 * Edição dos valores no arquivo de configuração application/config.xml que alimenta : _azul/php/config/config.php
 * tag pai <asterisk>
 *
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, LBN Sistemas
 * @version 1.2.0
 */
class cAppLevelsRules extends Controller {
	private $table = 'applevels_rules';
	private $colID = ''; 				// PK/ID da tabela principal
	private $keyword = 'Regra';	// Palavra a ser utilizada nas mensagens de retorno
	private $model;
	private $mysql='';
	/**
	 * Construtor
	 * 
	 */
	public function __construct() 
	{
		// Inicia model
		$this->model = new mAppLevelsRules();
		// Pega a uma instância de mysqli (DEPRECATED)
		$this->mysqli = Controller::getMysqli();
	}
	/**
	 * Atualiza todos os levels dos associados, aumentando ou reduzindo seus níveis
	 * com base nas regras de levels definidas em config.xml, tag 'applevels_rules'
	 * @param  stdClass envelope, dados obrigatorios :  {action:"updateAppLevels"}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function updateAppLevels($envelope){
		$cont_update_users=0;
		try{
		  	// Pega as regras de levels atuais, definidas pelo usuário
		    $args = new StdClass();
		    $args->table = $this->table;
			$rules = $this->model->mSelect($args)->ret;
			// Busca todos os associados com cadastro ativo 
			$sql = "SELECT * FROM az_users WHERE USE_LEVEL='ASSOCIATED' AND USE_STATUS='ATIVO' AND AZ_ARCHIVED=FALSE";
			$r = $this->mysqli->query($sql);
			while($user = $r->fetch_assoc()) {
				$total = 0;
				// Soma todas indicações efetuadas 
				$sql = "SELECT * FROM indications WHERE AZ_ARCHIVED=FALSE AND IND_INDICATOR=".$user['USE_ID'];
				$rt = $this->mysqli->query($sql);
				$total += ($rt->num_rows*$rules['indicationPoints']);
		
				// Soma todas visitas 
				$sql = "SELECT * FROM visits WHERE AZ_ARCHIVED=FALSE AND VIS_VISITOR=".$user['USE_ID'];
				$rt = $this->mysqli->query($sql);
				$total += ($rt->num_rows*$rules['visitPoints']);
				
				// Soma todos negócios (compra ou venda)
				$sql = "SELECT * FROM business WHERE AZ_ARCHIVED=FALSE AND BUS_USE_ID=".$user['USE_ID'];
				$rt = $this->mysqli->query($sql);
				$total += ($rt->num_rows*$rules['businessPoints']);
				
				
				
				// Atualiza o total de pontos e o level do usuário 
				$total = (int)$total;
				$sql = "UPDATE az_users SET USE_TOTALPOINTS='".$total."', 
				USE_LVL_ID=(SELECT LVL_ID FROM applevels WHERE LVL_NECESSARYPOINTS<=".$total." 
				ORDER BY LVL_NECESSARYPOINTS DESC LIMIT 1) 
				WHERE USE_ID=".$user['USE_ID'];
				$this->mysqli->query($sql);
				$cont_update_users += $this->mysqli->affected_rows;
			}
			Controller::azReturn('Quantidade de usuários atualizados: '.$cont_update_users, 'win', true);
		// Pega os erros e retorna uma mensagem de falha
		}catch(Exception $e){
		 	Controller::azReturn($e->getMessage(), 'fail', true);
		}
	}
	/**
	 * Altera um cadastro existente do pacote principal (clientes)
	 * @param  stdClass envelope, dados obrigatorios :  
	   {action:"alterarPrincipal", fields : [{column, value, default}], ID:[_ID_RESERVADO tabela principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function update_Main($envelope) {
		$obj = new stdClass();
		$obj->table = $this->table;
		$obj->colID = $this->colID;
		$obj->ID = $envelope->ID;
		$obj->fields = $envelope->fields;
		$obj->view = $_POST['view'];
		$obj->msgWin = $this->keyword.' alterada com sucesso';
		$obj->msgFail = $this->keyword.' não alterada, tente novamente.';
		// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
		$obj->webService = true;
		// Executa a ação pela classe Controller utilizando a model mMeusdados
		Controller::azUpdate($obj,$this->model);
	}
	
	
	/**
	 * Traz todos os dados para carregamento de todos formularios da view (principal e dinamicos)
	 * @param  stdClass envelope, dados possíveis : acao : 'dadosFormularios', 
	 *         filtros : { value1 : ID, colunas : ['<?=_columnID?>'], regra : '=' 
	 * @return string json mensagem sucesso  ou falha
	 */
	public function formsData($envelope,$webservice=true) {
		# Busca os dados da tabela principal ----------------------------------------------------------------------//
		$obj = new stdClass();
		$obj->table = $this->table;
		$obj->view = $_POST['view'];
		$obj->filters = $envelope=='' ? '' : $envelope->filters;
		$obj->webService = false; 
		
		// Executa a ação pela classe Controller e salva a resposta
		$ret =  Controller::azView($obj,$this->model);
		$tbMainData = $ret->data->ret;
	
		// Unifica os resultados das duas consultas
		$objRet = array( $this->table => $tbMainData);
		
		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($ret->message, $ret->type, $webservice, $objRet);
	}
	

	
	
}
