<?php
/**  
 * Controller : cRelatorioAcoes
 * Exibição das ações realizadas no sistema
 *
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, LBN Sistemas
 * @version 1.2.0
 */
class cRelatorioAcessos extends Controller {
	private $table = 'az_access';
	/**
	 * Construtor
	 * 
	 */
	public function __construct() 
	{
	}
	/**
	 * Traz os cadastros para serem visualizados e editados(com a devida permissao) na view
	 * @param  stdClass envelope, dados possíveis : inicio, limite, orderBy, filtros[{}]
	 * @return string json mensagem sucesso  ou falha
	 */
	public function requestData($envelope) {
		$obj = new stdClass();
		$obj->init = $envelope->init;
		$obj->limit = $envelope->limit;
		$obj->orderBy = $envelope->orderBy;
		$obj->filters = $envelope->filters;
		$obj->view = $_POST['view'];
		$obj->webService = true;
		$obj->table = $this->table;
		$obj->columns = $envelope->columns;
		
		// Executa a ação pela classe Controller 
		Controller::azView($obj);	
	}
}
