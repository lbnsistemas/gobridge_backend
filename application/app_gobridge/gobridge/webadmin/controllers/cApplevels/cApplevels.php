<?php
/**
 * @description: CRUD, envio de arquivo e formularios dinamicos 
 *
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, LBN Sistemas
 * @version 1.0.0
 */
class cApplevels extends Controller {
	private $table='applevels';
	private $colID='LVL_ID';
	private $keyword='Level';
	private $qdUploadID='uploadImagemAvatar';
	private $uploadDestination='../etc/uploads/applevels/';
	/**
	 * Construtor
	 * @param string json envelope 
	 */
	public function __construct() 
	{	
	}
	/**
	 * Traz os cadastros para serem visualizados e editados(com a devida permissao) na view
	 * @param  stdClass envelope, dados possíveis : inicio, limite, orderBy, filtros[{}]
	 * @return string json mensagem sucesso  ou falha
	 */
	public function requestData($envelope) {
		$obj = new stdClass();
		$obj->init = $envelope->init;
		$obj->limit = $envelope->limit;
		$obj->orderBy = $envelope->orderBy;
		$obj->filters = $envelope->filters;	
		$obj->view = $_POST['view'];
		$obj->webService = false;
		$obj->table = $this->table;
		$obj->columns = $envelope->columns;

		// Colunas reservadas do sistema 
		array_push($envelope->columns,'AZ_ARCHIVED'); 	// Informa se o cadastro está arquivado
		array_push($envelope->columns,'AZ_USARCHIVED');	// Informa o nome do usuário que arquivou
		array_push($envelope->columns,'AZ_DHARCHIVED');	// Informa a data que foi arquivado
		// Adiciona as colunas adicionais + as solicitadas pela View
		$obj->columns = $envelope->columns;
		
		// Executa a ação pela classe Controller 
		$ret = Controller::azView($obj);
		
		// Se executou com sucesso:
		if($ret->type=='win') {
			$retDataTable = $ret->data;
			$applevels = $retDataTable->ret;
			
			// Analisa todos os levels, checando se algum possuí icone válido p/ ser exibida
			foreach($applevels as $lvl) {
				$srcFile = SYS_SRC_APP.$this->uploadDestination.$lvl->LVL_ID."/".$lvl->LVL_IMAGE;
				if(is_file($srcFile)) {
					$lvl->IMAGE = SYS_APP_URL.$this->uploadDestination.$lvl->LVL_ID."/".$lvl->LVL_IMAGE;
				}
			}
			
		// Se houve falha ou algum alerta:	
		}else {
			$retDataTable= array();
		}
		
		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($ret->message, $ret->type, true, $retDataTable);
	}
	
	/**
	 * Traz todos os dados para carregamento de todos formularios da view (principal e dinamicos)
	 * @param  stdClass envelope, dados possíveis : acao : 'dadosFormularios', 
	 *         filtros : { value1 : ID, colunas : ['<?=_columnID?>'], regra : '=' 
	 * @return string json mensagem sucesso  ou falha
	 */
	public function formsData($envelope) {
		# Array para retornar os conteudos dos frames de upload
		$uploadFramesContent = array();
		
		# ID do cadastro principal
		$ID = $envelope->ID;
		
		# Busca os dados da tabela principal 
		$obj = new stdClass();
		$obj->view = $_POST['view'];
		$obj->filters = $envelope->filters;
		$obj->webService = false; 
		$obj->table = $this->table;
		
		// Executa a ação pela classe Controller e salva a resposta
		$mainTbData = Controller::azView($obj)->data->ret;
		
		# Prepara o retorno em JSON para a View
		if(count($mainTbData)==0) {
			$type = 'fail';
			$msg = '';
		}else {
			// Faz leitura da pasta de imagens do post (capa)
			$files = Controller::readFiles($this->uploadDestination.$ID."/");
			// Se houver uma imagem da capa
			if(count($files)>0) {
				// Arrays com os conteúdos de cada frame de upload de arquivos
				array_push($uploadFramesContent, array('nameID'=>$this->qdUploadID, 'files'=>$files));
			}
			$type = 'win';	
			$msg =  '';
			
			// Define o objeto de retorno
			$objRet = array( $this->table => $mainTbData,'uploadFramesContent'=>$uploadFramesContent);
		}
			
		
		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($msg, $type, true, $objRet);
	}
	/**
	 * Inclui um novo cadastro do pacote principal (clientes)
	 * @param  stdClass envelope, dados possíveis :  {acao:"incluir_Principal", campos : [{coluna, value}]}
	 * @return string json com a PK (ID) do cadastro recem criado ou mensagem de falha
	 */
	public function insert_Main($envelope) {
		$obj = new stdClass();
		
		// Cria objeto com os campos recebidos, adicionando outros campos do sistema
		$cp = new Fields($envelope->fields);
		$cp->add($this->colID,$envelope->ID);
		$cp->add('AZ_DHREGISTER',date('Y-m-d H:i:s'));
		$cp->add('AZ_USREGISTER',$_SESSION[SES_USER_NAME]);
		
		// Seta as propriedades do obj para o insert
		$obj->fields = $cp->ret();
		$obj->table = $this->table;
		$obj->view = $_POST['view'];
		$obj->msgWin = "Cadastro de ".$this->keyword.' incluído com sucesso';
		$obj->msgFail =  "Cadastro de ".$this->keyword.' não incluído, tente novamente.';
		$obj->webService = true;
		
		// Registra a ação do usuário
		Controller::registerAction();
		
		// Executa a ação pela classe Controller
		Controller::azInsert($obj);
	}
	/**
	 * Altera um cadastro existente do pacote principal 
	 * @param  stdClass envelope, dados obrigatorios :  
	   {acao:"alterarPrincipal", campos : [{coluna, value, default}], ID:[_RESERVED_ID tabela principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function update_Main($envelope) {
		$obj = new stdClass();
		$obj->ID = $envelope->ID;
		$obj->colID = $this->colID;
		$obj->table = $this->table;
		$obj->fields = $envelope->fields;
		$obj->view = $_POST['view'];
		$obj->msgWin = "Cadastro(s) de ".$this->keyword.'(s) alterado(s) com sucesso';
		$obj->msgFail = "Cadastro(s) de ".$this->keyword.'(s) não alterado(s), tente novamente.';
		// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
		$obj->webService = true;
			
		Controller::registerAction();
		// Executa a ação pela classe Controller utilizando a model mMeusdados
		Controller::azUpdate($obj);
	}
	/**
	 * Deleta um cadastro do pacote principal (clientes)
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"archive_Main", ID:[do cadastro principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function archive_Main($envelope) {
		$obj = new stdClass();
		$obj->ID = $envelope->ID;// Array com o(s) ID(s) selecionado(s)
		$obj->table = $this->table;
		$obj->colID = $this->colID;
		// Cria os campos do framework  para arquivamento de cadastro
		$cp = new Fields();
		$cp->add('AZ_ARCHIVED',1);
		$cp->add('AZ_DHARCHIVED',date('Y-m-d H:i:s'));
		$cp->add('AZ_USARCHIVED',$_SESSION[SES_USER_NAME]);
		// Adiciona os campos criados
		$obj->fields = $cp->ret();
		$obj->view = $_POST['view'];
		$s = count($envelope->ID) > 1 ? 's' : '';// Variavel para complemento da frase de erro ou sucesso
		$obj->msgWin =   "Cadastro$s de ".$this->keyword."$s arquivado$s com sucesso";
		$obj->msgFail = "Cadastro$s de ".$this->keyword."$s não arquivado$s, tente novamente.";
		// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
		$obj->webService = true;
		
		Controller::registerAction();
		
		// Executa a ação pela classe Controller
		Controller::azUpdate($obj);
	}
	
	/**
	 * Reserva ou busca (caso já tenha sido reservada) uma PK(ID) para o próximo cadastro
	 * Verifica e exclui os arquivos de upload de uma tentativa fracassada de cadastro anterior (com o mesmo ID)
	 * @return string json com a PK(ID) reservada ou mensagem de falha
	 */
	public function prepareContainer() {
		$ID = '';
		$objRet = false;
		$uploadFramesContent = array();
		// Busca uma possível PK(ID) já reservada ou reserva uma nova
		$r = Controller::findReserveID($this->table,$this->colID,$_POST['view'], $webservice=false);
		
		// Caso esteja deslogado ou não tenha permissão:
		if($r->data==-1 || $r->data===false) {
			$type = $r->type;
			$msg = $r->message;
			// Se estiver apenas deslogado, envia instrução de redirecionamento para tela de login do app
			$objRet = ($r->data==-1 ? array('redirect'=>SYS_DOMAIN._client.'/login') : '');
		
		// Verifica se existe ou foi reservado um ID com SUCESSO:
		}else if( $r->data->ID > 0 ) {
			// ID reservado/encontrado
			$ID = $r->data->ID;
			
			// Faz leitura da pasta de imagens 
			$files = Controller::readFiles($this->uploadDestination.$ID."/");
			// Se houver uma imagem da capa
			if(count($files)>0) {
				// Arrays com os conteúdos de cada frame de upload de arquivos
				array_push($uploadFramesContent, array('nameID'=>$this->qdUploadID, 'files'=>$files));
			}
			
			//Sucesso : Padroniza o retorno  com as informações encontradas
			$objRet = array('ID'=>$r->data->ID,'uploadFramesContent'=>$uploadFramesContent);
			$type = 'win';
			$msg = '';
			
		// Falha:	
		}else {
			$type = 'fail';
			$msg = 'Falha ao reservar/buscar ID para o próximo cadastro.';
		}
		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($msg, $type, true, $objRet);
	}
	
	/**
	 * Recepciona os arquivos do upload e seta as regras para processamento do(s) arquivo(s)
	 * @param stdClass envelope, dados possíveis : acao, qdUploadID, ID
	 * @return string json mensagem sucesso  ou falha
	 */
	public function receiveFile($envelope) {
		// Upload da imagem avatar
		if($envelope->qdUploadID==$this->qdUploadID) {
			// Pasta destino para armazenamento do(s) arquivo(s) do upload
			$envelope->destination = SYS_SRC_APP.$this->uploadDestination.$envelope->ID."/";
			// Array com o(s) arquivo(s) do upload
			$envelope->files = $_FILES['files'];
			// Se true : cria uma miniatura da imagem na mesma pasta de upload para rapida exibição na view
			$envelope->thumbnail = false;
			// Tamanho máximo em MB
			$envelope->maxSizeMB = 0.1;
			// Quantidade máxima
			$envelope->maxAmount = 1;
			// Altera nome do arquivo original ?
			$envelope->updateFileName = false;
			// Extensoes permitidas (argumento para preg_match)
			$envelope->allowedExtensions = "jp?g";
			// Url dos arquivos, para retorno das imagens após upload efetudo
			$envelope->urlFiles = SYS_APP_URL.$this->uploadDestination.$envelope->ID."/";
			
			// Recepciona o(s) arquivo(s) e imprimi o resultado
			Controller::uploadFile($envelope);
		// qdUploadID não reconhecido 
		}else {
			Controller::azReturn('qdUploadID não reconhecido.', 'fail', true);	
		}
	}
	
	/**
	 * Baixar de formar dinamica um determinado arquivo
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"baixarArquivo", fileName, qdUploadID, ID}
	 * @return string json com a URL para baixar o arquivo ou mensagem de falha
	 */
	public function downloadFile($envelope) {
		$fileName = Controller::getValue($envelope->fields,'fileName');
		$qdUploadID = Controller::getValue($envelope->fields,'qdUploadID');
		$src	 = $this->uploadDestination.$envelope->ID."/".$fileName ;	
		$srcFile = realpath(SYS_SRC_APP.$src); // Caminho real do arquivo
		$urlFile = SYS_APP_URL.$src; // Endereço web do arquivo
		
		// Verifica se é um arquivo válido 
		if(is_file($srcFile)){
			$tipo = 'win';
			$msg = '';
			// Sessões utilizadas no arquivo php/downloader.php
			$_SESSION['urlFileToDownload'] = $urlFile;
			$_SESSION['srcFileToDownload'] = $srcFile;
			
		// Arquivo inválido
		}else {
			$tipo = 'fail';
			$msg = 'Arquivo não encontrado.';
			// Resetas as sessões de download
			$_SESSION['urlFileToDownload'] = null;
			$_SESSION['srcFileToDownload'] = null;
			
			
		}
		
		// De qualquer forma, retorna a URL  e o caminho fonte do arquivo
		$objRet = array('url'=>$urlFile,'src'=>$srcFile);

		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($msg, $tipo, true, $objRet);
	}
	/**
	 * Deleta um arquivo do servidor (deletar também o thumbnail se houver)
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"deletarArquivo", fileName, qdUploadID, ID}
	 * @return string json com mensagem de sucesso ou falha
	 */
	public function deleteFile($envelope) {
		$obj = new stdClass();
		$fileName = Controller::getValue($envelope->fields,'fileName');
		$qdUploadID = Controller::getValue($envelope->fields,'qdUploadID');
		// Deleta imagem da categoria principal
		if($qdUploadID==$this->qdUploadID){
			$obj = new stdClass();
			$obj->view = $_POST['view'];
			$obj->fileName = $fileName;
			$obj->folder = $this->uploadDestination.$envelope->ID."/";
			$obj->webService = true;
		}
		
		// Executa a ação pela classe Controller
		Controller::delete_file($obj);	
	}
	
	
	
}