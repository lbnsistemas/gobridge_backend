<?php
/**
 * @description: CRUD, envio de arquivo e formularios dinamicos 
 *
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, LBN Sistemas
 * @version 1.0.0
 */
class cEtapasObras extends Controller {
	private $table='etapas_obras';
	private $colID='ETA_ID';
	private $keyword='Etapa de obra';
	private $qdUploadID='uploadImagemEtapas';
	private $uploadDestination='../etc/uploads/etapas-obra/';
	
	/**
	 * Construtor
	 * @param string json envelope 
	 */
	public function __construct() 
	{	
	}
	/**
	 * Traz os cadastros para serem visualizados e editados(com a devida permissao) na view
	 * @param  stdClass envelope, dados possíveis : inicio, limite, orderBy, filtros[{}]
	 * @return string json mensagem sucesso  ou falha
	 */
	public function requestData($envelope) {
		$obj = new stdClass();
		$obj->init = $envelope->init;
		$obj->limit = $envelope->limit;
		$obj->orderBy = $envelope->orderBy;
		$obj->filters = $envelope->filters;	
		$obj->view = $_POST['view'];
		$obj->webService = false;
		$obj->table = $this->table;
		$obj->columns = $envelope->columns;

		/* Colunas reservadas do sistema 
		array_push($envelope->columns,'AZ_ARCHIVED'); 	// Informa se o cadastro está arquivado
		array_push($envelope->columns,'AZ_USARCHIVED');	// Informa o nome do usuário que arquivou
		array_push($envelope->columns,'AZ_DHARCHIVED');	// Informa a data que foi arquivado*/
		// Adiciona as colunas adicionais + as solicitadas pela View
		$obj->columns = $envelope->columns;
		
		// Executa a ação pela classe Controller 
		$ret = Controller::azView($obj);
		
		// Se executou com sucesso:
		if($ret->type=='win') {
			$retDataTable = $ret->data;
			$rows = $retDataTable->ret;
			/* Analisa todos os levels, checando se algum possuí icone válido p/ ser exibida
			foreach($rows as $row) {
				$sufix = $row->CAT_ID."/".$row->CAT_IMAGE;
				$srcFile = SYS_SRC_APP.$this->uploadDestination.$sufix;
				if(is_file($srcFile)) {
					$row->THUMBNAIL = SYS_APP_URL.$this->uploadDestination.$sufix;
					$row->URL = SYS_APP_URL.$this->uploadDestination.$sufix;
				}
			}*/
			
		// Se houve falha ou algum alerta:	
		}else {
			$retDataTable = array();
			
		}
		
		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($ret->message, $ret->type, true, $retDataTable);
	}
	
	/**
	 * Traz todos os dados para carregamento de todos formularios da view (principal e dinamicos)
	 * @param  stdClass envelope, dados possíveis : acao : 'dadosFormularios', 
	 *         filtros : { value1 : ID, colunas : ['<?=_columnID?>'], regra : '=' 
	 * @return string json mensagem sucesso  ou falha
	 */
	public function formsData($envelope) {
		# Array para retornar os conteudos dos frames de upload
		$uploadFramesContent = array();
		
		# ID do cadastro principal
		$ID = $envelope->ID;
		
		# Busca os dados da tabela principal ----------------------------------------------------------------------//
		$obj = new stdClass();
		$obj->view = $_POST['view'];
		$obj->filters = $envelope->filters;
		$obj->webService = false; 
		$obj->table = $this->table;
		
		
		// Executa a ação pela classe Controller e salva a resposta
		$ret = Controller::azView($obj);
		
		// Verifica se processou com sucesso
		if($ret->type=='win') {
			$mainTbData = $ret->data->ret;
			// Busca as subcategorias (serão armazenadas em um array  global, para preenchimento dinamico na View)
			$obj = new stdClass();
			$obj->view = $_POST['view'];
			$obj->webService = false; 
			$obj->complSQL = 'WHERE SUB_ETA_ID='.$ID; 
			$obj->table = 'subetapas_obras';
			$subetapas_obras = Controller::azView($obj)->data->ret;
		
			/* Faz leitura da pasta de imagens do post (capa)
			$files = Controller::readFiles( $this->uploadDestination.$ID."/");
			// Se houver uma imagem da capa
			if(count($files)>0) {
				// Arrays com os conteúdos de cada frame de upload de arquivos
				array_push($uploadFramesContent, array('nameID'=>$this->qdUploadID, 'files'=>$files));
			}*/
			
			// Define o objeto de retorno
			$objRet = array( $this->table => $mainTbData,'uploadFramesContent'=>$uploadFramesContent,'subetapas_obras'=>$subetapas_obras);
		
		// Falha:
		}else {
			$objRet = array();
			
		}
		//var_dump($objRet);
		//exit();
		
		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($ret->message, $ret->type, true, $objRet);
	}
	/**
	 * Inclui um novo cadastro de subcategoria
	 * @param  stdClass envelope, dados possíveis :  {acao:"insert_formMainSubCategorias", campos : [{coluna, value}]}
	 * @return string json com a PK (ID) do cadastro recem criado ou mensagem de falha
	 */
	public function insert_formMainSubetapas($envelope) {
		$this->table='subetapas_obras';
		$this->colID='SUB_ID';
		$this->keyword='Subetapa';
		// Executa a função de inserção principal
		$this->insert_Main($envelope);
	}
	/**
	 * Inclui um novo cadastro do pacote principal (clientes)
	 * @param  stdClass envelope, dados possíveis :  {acao:"incluir_Principal", campos : [{coluna, value}]}
	 * @return string json com a PK (ID) do cadastro recem criado ou mensagem de falha
	 */
	public function insert_Main($envelope) {
		$obj = new stdClass();
		
		// Cria objeto com os campos recebidos, adicionando outros campos do sistema
		$cp = new Fields($envelope->fields);
		if($this->table=='etapas_obras') {
			$cp->add($this->colID,$envelope->ID);
		}else if($this->table=='subetapas_obras') {
			$cp->add('SUB_ETA_ID',$envelope->ID);
		}
		//$cp->add('AZ_DHREGISTER',date('Y-m-d H:i:s'));
		//$cp->add('AZ_USREGISTER',$_SESSION[SES_USER_NAME]);
		
		// Seta as propriedades do obj para o insert
		$obj->fields = $cp->ret();
		$obj->table = $this->table;
		$obj->view = $_POST['view'];
		$obj->msgWin = "Cadastro de ".$this->keyword.' incluído com sucesso';
		$obj->msgFail =  "Cadastro de ".$this->keyword.' não incluído, tente novamente.';
		$obj->webService = true;
		
		// Registra a ação do usuário
		Controller::registerAction();
		
		// Executa a ação pela classe Controller
		Controller::azInsert($obj);
	}
	/**
	 * Altera um det. cadastro de subetapa
	 * @param  stdClass envelope, dados obrigatorios :  
	   {acao:"alterarPrincipal", campos : [{coluna, value, default}], ID:[_RESERVED_ID tabela principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function update_formMainSubetapas($envelope) {
		$this->table='subetapas_obras';
		$this->colID='SUB_ID';
		$this->keyword='Subetapa';
		// Executa a função de inserção principal
		$this->update_Main($envelope);
	}
	/**
	 * Altera um cadastro existente do pacote principal 
	 * @param  stdClass envelope, dados obrigatorios :  
	   {acao:"alterarPrincipal", campos : [{coluna, value, default}], ID:[_RESERVED_ID tabela principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function update_Main($envelope) {
		$obj = new stdClass();
		$obj->ID = $envelope->ID;
		$obj->colID = $this->colID;
		$obj->table = $this->table;
		$obj->fields = $envelope->fields;
		$obj->view = $_POST['view'];
		$obj->msgWin = "Cadastro(s) de ".$this->keyword.'(s) alterado(s) com sucesso';
		$obj->msgFail = "Cadastro(s) de ".$this->keyword.'(s) não alterado(s), tente novamente.';
		// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
		$obj->webService = true;
			
		Controller::registerAction();
		// Executa a ação pela classe Controller utilizando a model mMeusdados
		Controller::azUpdate($obj);
	}
	/**
	 * Deleta um determinado cadastro de subetapa
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"archive_formMainSubetapa", ID:[do cadastro principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function archive_formMainSubetapas($envelope) {
		$this->table='subetapas_obras';
		$this->colID='SUB_ID';
		$this->keyword='Subetapa';
		// Executa a função de inserção principal
		$this->delete_Main($envelope);
	}
	
	/**
	 * Arquiva um cadastro do pacote principal 
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"archive_Main", ID:[do cadastro principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function archive_Main($envelope) {
		return($this->delete_Main($envelope));
	}
	
	/**
	 * Deleta um cadastro do pacote principal 
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"delete_Main", ID:[do cadastro principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function delete_Main($envelope) {
		$obj = new stdClass();
		$obj->ID = $envelope->ID;// Array com o(s) ID(s) selecionado(s)
		$obj->table = $this->table;
		$obj->colID = $this->colID;
		$obj->msgWin =  "Cadastro$s de ".$this->keyword."$s deletado$s com sucesso";
		$obj->msgFail = "Cadastro$s de ".$this->keyword."$s não deletado$s, tente novamente.";
		// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
		$obj->webService = true;
		
		Controller::registerAction();
		
		// Executa a ação pela classe Controller
		Controller::azDelete($obj);
	}
	
	/**
	 * Reserva ou busca (caso já tenha sido reservada) uma PK(ID) para o próximo cadastro
	 * Verifica e exclui os arquivos de upload de uma tentativa fracassada de cadastro anterior (com o mesmo ID)
	 * @return string json com a PK(ID) reservada ou mensagem de falha
	 */
	public function prepareContainer() {
		$ID = '';
		$objRet = false;
		$uploadFramesContent = array();
		// Busca uma possível PK(ID) já reservada ou reserva uma nova
		$r = Controller::findReserveID($this->table,$this->colID,$_POST['view'], $webservice=false);
		
		// Caso esteja deslogado ou não tenha permissão:
		if($r->data==-1 || $r->data===false) {
			$type = $r->type;
			$msg = $r->message;
			// Se estiver apenas deslogado, envia instrução de redirecionamento para tela de login do app
			$objRet = ($r->data==-1 ? array('redirect'=>SYS_DOMAIN._client.'/login') : '');
		
		// Verifica se existe ou foi reservado um ID com SUCESSO:
		}else if( $r->data->ID > 0 ) {
			// ID reservado/encontrado
			$ID = $r->dados->ID;
			
			/* Faz leitura da pasta de imagens do post (capa)
			$files = Controller::readFiles( $this->uploadDestination.$ID."/");
			// Se houver uma imagem da capa
			if(count($files)>0) {
				// Arrays com os conteúdos de cada frame de upload de arquivos
				array_push($uploadFramesContent, array('nameID'=>$this->qdUploadID, 'files'=>$files));
			}
			*/
			
			//Sucesso : Padroniza o retorno  com as informações encontradas
			$objRet = array('ID'=>$r->data->ID,'uploadFramesContent'=>$uploadFramesContent);
			$type = 'win';
			$msg = '';
			
		// Falha:	
		}else {
			$type = 'fail';
			$msg = 'Falha ao reservar/buscar ID para o próximo cadastro.';
		}
		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($msg, $type, true, $objRet);
	}
	
	/**
	 * Recepciona os arquivos do upload e seta as regras para processamento do(s) arquivo(s)
	 * @param stdClass envelope, dados possíveis : acao, qdUploadID, ID
	 * @return string json mensagem sucesso  ou falha
	 */
	public function receiveFile($envelope) {
		// Upload da imagem avatar
		if($envelope->qdUploadID==$this->qdUploadID) {
			// Pasta destino para armazenamento do(s) arquivo(s) do upload
			$envelope->destination = SYS_SRC_APP.$this->uploadDestination.$envelope->ID."/";
			// Array com o(s) arquivo(s) do upload
			$envelope->files = $_FILES['files'];
			// Se true : cria uma miniatura da imagem na mesma pasta de upload para rapida exibição na view
			$envelope->thumbnail = false;
			// Tamanho máximo em MB
			$envelope->maxSizeMB = 0.25;
			// Quantidade máxima
			$envelope->maxAmount = 1;
			// Altera nome do arquivo original ?
			$envelope->updateFileName = false;
			// Extensoes permitidas (argumento para preg_match)
			$envelope->allowedExtensions = "jp?g";
			// Url dos arquivos, para retorno das imagens após upload efetudo
			$envelope->urlFiles = SYS_APP_URL.$this->uploadDestination.$envelope->ID."/";
			
			// Recepciona o(s) arquivo(s) e imprimi o resultado
			Controller::uploadFile($envelope);
		// qdUploadID não reconhecido 
		}else {
			Controller::azReturn('qdUploadID não reconhecido.', 'fail', true);	
		}
	}
	
	/**
	 * Baixar de formar dinamica um determinado arquivo
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"baixarArquivo", fileName, qdUploadID, ID}
	 * @return string json com a URL para baixar o arquivo ou mensagem de falha
	 */
	public function downloadFile($envelope) {
		$fileName = Controller::getValue($envelope->fields,'fileName');
		$qdUploadID = Controller::getValue($envelope->fields,'qdUploadID');
		$src	 = $this->uploadDestination.$envelope->ID."/".$fileName ;	
		$srcFile = realpath(SYS_SRC_APP.$src); // Caminho real do arquivo
		$urlFile = SYS_APP_URL.$src; // Endereço web do arquivo
		
		// Verifica se é um arquivo válido 
		if(is_file($srcFile)){
			$tipo = 'win';
			$msg = '';
			// Sessões utilizadas no arquivo php/downloader.php
			$_SESSION['urlFileToDownload'] = $urlFile;
			$_SESSION['srcFileToDownload'] = $srcFile;
			
		// Arquivo inválido
		}else {
			$tipo = 'fail';
			$msg = 'Arquivo não encontrado.';
			// Resetas as sessões de download
			$_SESSION['urlFileToDownload'] = null;
			$_SESSION['srcFileToDownload'] = null;
			
			
		}
		
		// De qualquer forma, retorna a URL  e o caminho fonte do arquivo
		$objRet = array('url'=>$urlFile,'src'=>$srcFile);

		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($msg, $tipo, true, $objRet);
	}
	/**
	 * Deleta um arquivo do servidor (deletar também o thumbnail se houver)
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"deletarArquivo", fileName, qdUploadID, ID}
	 * @return string json com mensagem de sucesso ou falha
	 */
	public function deleteFile($envelope) {
		$obj = new stdClass();
		$fileName = Controller::getValue($envelope->fields,'fileName');
		$qdUploadID = Controller::getValue($envelope->fields,'qdUploadID');
		// Deleta imagem da categoria principal
		if($qdUploadID==$this->qdUploadID){
			$obj = new stdClass();
			$obj->view = $_POST['view'];
			$obj->fileName = $fileName;
			$obj->folder = $this->uploadDestination.$envelope->ID."/";
			$obj->webService = true;
		}
		
		// Executa a ação pela classe Controller
		Controller::delete_file($obj);	
	}
	
	
	
}