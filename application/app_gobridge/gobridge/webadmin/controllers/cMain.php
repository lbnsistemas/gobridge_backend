<?php
//Requisita controller cEmail para gerenciar as notificações por email, se habilitado
require(SYS_SRC_APP.'controllers/cEmail/cEmail.php');
	
/**
 * Controller cPrincipal : Ações comuns a várias views
 *
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, LBN Sistemas
 * @version 1.0.0
 */
class cMain extends Controller {
	private $table;
	private $colID;
	private $keyword;
	private $uploadDestination;
	private $qdUploadID;
	private $mysqli;
	public $cEmail; //Objeto cEmail
	/**
	 * Construtor
	 * @param string json envelope 
	 */
	public function __construct() 
	{	
		
		// Regras especificas para cada view
		switch(_view) {
			// View para manipular os cadastros de negócios
			case 'dashboard' :
				$this->table = 'demandas';
				$this->complSQL = 'INNER JOIN clientes ON clientes.CLI_ID=demandas.CLI_ID';
				$this->colID = 'DEM_ID';
				$this->keyword = 'Solicitação';
				$this->uploadDestination = '';
				$this->qdUploadID = '';
				break;
		}
		//Inicia cEmail
		$this->cEmail = new cEmail();
		//Pega a instacia do Mysqli
		$this->mysqli = Controller::getMysqli();
	}
	/**
	 * Aprova ou rejeita um determinado documento já enviado
	 * @param  stdClass envelope, dados possíveis : {action:"aprovarRejeitarDocumento", extra : {ENV_ID, Status, Motivo}}
	 * @return string json mensagem win ou fail 
	 */
	public function aprovarRejeitarDocumento($envelope) {
		$objRet = array();
		$q = array();
		// Recebe e valida os dados
		$envID = $envelope->extra->ENV_ID;
		$status = $envelope->extra->Status;
		$motivo = $envelope->extra->Motivo;
		
		// SQL para aprovar ou rejeitar um documento
		$sql = "UPDATE envios SET Status='".$status."', Motivo='".$motivo."' WHERE ENV_ID=".$envID;
		$r = $this->mysqli->query($sql);
		if($this->mysqli->affected_rows>0) {
			$msg = 'Status "'.$status.'" registrado com sucesso';
			$type = 'win';	
		}else {
			$msg = 'Erro no servidor, tente novamente ou contate o suporte técnico.';
			$type = 'fail';	
		}
		return(Controller::azReturn($msg, $type,true,$link));
	}
	/**
	 * Traz todas as cidades relacionadas a um determinado estado
	 * @param  stdClass envelope, dados possíveis : {action:"criaNovaDemanda", extra : {}}
	 * @return string json mensagem win ou fail 
	 */
	public function criaNovaDemanda($envelope) {
		$objRet = array();
		$q = array();
		// Recebe e valida os dados
		$area = $envelope->extra->area;
		$tipo = $envelope->extra->tipo;
		$cliente = $envelope->extra->cliente;
		$email = $envelope->extra->email;
		$documentos = $envelope->extra->documentos;
		
		// Valida os dados
		if(trim($area)=='' || trim($tipo)=='' || trim($cliente)=='' || count($documentos)==0) {
			return(Controller::azReturn('Preencha todos os campos corretamente.', 'false',true,array()));
		}
		
		// Busca uma possível PK(ID) já reservada ou reserva uma nova
		$sql = "SELECT * FROM demandas";
		$r = $this->mysqli->query($sql);
		$dem_id_pk = $r->num_rows+1;
		
		// Gera a HashID para envio do link
		$HashID = md5($cliente.$dem_id_pk);
		
		// Gera link 
		$link = 'https://link.gobridge.com.br/'.$HashID;
		
		// SQL para gerar nova demanda por documentos
		$sql = "INSERT INTO demandas SET DEM_ID=".$dem_id_pk.", Titulo='".$tipo."', Status='ABERTO',
		DataHora_criacao=now(),CLI_ID=".$cliente.", Qtd_requisicao='".count($documentos)."', 
		ESC_ID=".$_SESSION[SES_USER_ID].",HashID='".$HashID."'";
		$q[] = $sql;
		
		// Registra os documentos solicitados para envio
		foreach($documentos as $doc) {
			$sql = "INSERT INTO envios SET DEM_ID=".$dem_id_pk.",  Status='ABERTO', DOC_ID=".$doc;
			$q[] = $sql;
		}
		
		
		
		// Executa transaction
		$ret = mMain::mTransaction($q);
		if($ret->ret==false) {
			$msg = 'Houve uma falha na criação da solicitação. Tente novamente';
			$type = 'fail';	
		}else {
			$msg = '';
			$type = 'win';	
		}
		return(Controller::azReturn($msg, $type,true,$link));
	}
	/**
	 * Envia email com o link para o cliente
	 * @param  stdClass envelope, dados possíveis : {action:"enviarLinkEmail", extra : {}}
	 * @return string json mensagem win ou fail 
	 */
	public function enviaLinkEmail($envelope) {
		// Recebe os dados
		$link = $envelope->extra->link;
		$mensagem = $envelope->extra->mensagem;
		$email = $envelope->extra->email;
		$nome = $envelope->extra->nome;
		
		// Corpo email
		$corpo = $mensagem." <br /><a href='".$link."' target='_blank'><button style='background-color:green; padding: 10px'>Enviar documentos</button></a>";
		
		$ret = $this->cEmail->envioDireto($email ,$nome,  'Link para envio de documentos', $corpo);
		if($r->type=='win') {
			$r->message = 'Link enviado com sucesso';
		}
		
		// Retorno do WS
		return(Controller::azReturn($ret->message, $ret->type,true,$ret->data));
	}
	/**
	 * Traz todas as cidades relacionadas a um determinado estado
	 * @param  stdClass envelope, dados possíveis :  {action:"carregarCidades", filters : [{value1,rule, [columns]}], orderBy}
	 * @return string json mensagem win ou fail 
	 */
	public function carregarCidades($envelope){
		$obj = new stdClass();
		$obj->filters = $envelope->filters;
		$obj->orderBy = $envelope->orderBy;
		$obj->view = $_POST['view'];
		$obj->webService = false; 
		$obj->table = 'cidades';
		// Executa a ação pela classe Controller 
		$ret = Controller::azView($obj);
		// Retorno do WS
		return(Controller::azReturn($ret->message, $ret->type,true,$ret->data));
	}
	/**
	 * Efetua login no sistema
	 * @param  stdClass envelope, dados possíveis :  {acao:"login", campos : [{coluna, value}]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function doLogin($envelope) {
		// Pega o valor do campo usuario
		$user = Controller::getValue($envelope->fields,'USER');
		// Pega o valor do campo senha
		$pass = Controller::getValue($envelope->fields,'PASS');
		// Tenta efetuar o login
		$ret = mMain::verifyLogin($user,$pass);
		// Sucesso 
		if(count($ret)>0 && $ret!==false ) {
			$row = $ret[0];
			$_SESSION[SES_LOGIN]=true;
			$_SESSION[SES_USER_NAME]=$row['Nome_Razao'];
			$_SESSION[SES_USER_ID]=$row['ESC_ID'];
			$_SESSION[SES_USER_LEVEL]='USER';
			
			// Cria e salva Token de acesso
			$_SESSION[SES_TOKEN] = Controller::getAccessToken(true); 
		
			// Salva hora e ip de acesso ao sistema
			/*$obj = new stdClass();
			$cp = new Fields();
			$cp->add('ACC_MODULE', $_POST['module']);
			$cp->add('ACC_URL',$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
			$cp->add('ACC_NAVEGATOR', $_SERVER['HTTP_USER_AGENT']);
			$cp->add('ACC_IP', Controller::IP());
			$cp->add('AZ_DHREGISTER', date('Y-m-d H:i:s'));
			$cp->add('AZ_USREGISTER', $user);
			$obj->fields = $cp->ret();
			$obj->table = DB_NAME.'.az_access';
			$obj->view = $_POST['view'];
			$obj->webService = false;
			$ret = Controller::azInsert($obj);

			// Salva em sessão o ID de acesso
			$_SESSION[SES_ACC_ID] = $ret->data->ret->ID;*/
			// Retorna mensagem tipo sucesso
			Controller::azReturn('','win',true);
			
		// Falha
		}else {
			Controller::azReturn('Usuário ou senha inválidos.','fail',true);	
		}
	}
	
	/**
	 * Efetua logout no sistema
	 * @return string json true ou mensagem falha 
	 */
	public function doLogout() {
		if(isset($_SESSION[SES_LOGIN])) {
			$ret = session_destroy();
			$_SESSION=false;
			// Retorna confirmação de logout (true)
			if($ret) {
				Controller::azReturn('','win',true, array('redirect'=>''));
			}else {
				// Retorna mensagem de falha
				Controller::azReturn('Logout não pode ser efetuado, contato o suporte do Sige Azul.','fail',true, array('redirect'=>''));		
			}
			
		}else {
			// Retorna mensagem de falha
			Controller::azReturn('Sessão de login não encontrada','fail',true, array());	
		}
	}
	
	
	/**
	 * Traz os cadastros para serem visualizados e editados(com a devida permissao) na view
	 * @param  stdClass envelope, dados possíveis : inicio, limite, orderBy, filtros[{}]
	 * @return string json mensagem sucesso  ou falha
	 */
	public function requestData($envelope) {
		$dataRet = array();
		$obj = new stdClass();
		$obj->init = $envelope->init;
		$obj->limit = $envelope->limit;
		$obj->orderBy = $envelope->orderBy;
		$obj->filters = $envelope->filters;	
		$obj->view = $_POST['view'];
		$obj->webService = false;
		$obj->table = $this->table;
		$obj->complSQL = $this->complSQL;
		// Colunas reservadas do sistema 
		//array_push($envelope->columns,$this->table.'.AZ_ARCHIVED'); 	// Informa se o cadastro está arquivado
		//array_push($envelope->columns,$this->table.'.AZ_USARCHIVED');	// Informa o nome do usuário que arquivou
		//array_push($envelope->columns,$this->table.'.AZ_DHARCHIVED');	// Informa a data que foi arquivado
		// Adiciona as colunas adicionais + as solicitadas pela View
		$obj->columns = $this->columns!='' ? $this->columns : $envelope->columns;
		
		// Busca os dados principais
		$objRet = Controller::azView($obj,'',false);
		
		
		// Para dashboard, trás os documentos requisitados em cada demanda
		if(_view=='dashboard') {
			foreach($objRet->data->ret as $key=>$demanda) {
				$sql = "SELECT * FROM envios 
				INNER JOIN documentos ON envios.DOC_ID=documentos.DOC_ID 
				WHERE DEM_ID=".$demanda->DEM_ID;
				$r = $this->mysqli->query($sql);
				$documentos = [];
				while($row = $r->fetch_assoc()) {
					$documentos[] = $row;	
				}
				$objRet->data->ret[$key]->solicitacoes = $documentos;
			}
		}
			
		
		// Retorno do WS
		return(Controller::azReturn($objRet->message,$objRet->type, true, $objRet->data));
	}
	
	/**
	 * Traz todos os dados para carregamento de todos formularios da view (principal e dinamicos)
	 * @param  stdClass envelope, dados possíveis : acao : 'dadosFormularios', 
	 *         filtros : { value1 : ID, colunas : ['<?=_columnID?>'], regra : '=' 
	 * @return string json mensagem sucesso  ou falha
	 */
	public function formsData($envelope) {
		# Array para retornar os conteudos dos frames de upload
		$uploadFramesContent = array();
		$estados = array();
		
		# ID do cadastro principal
		$ID = $envelope->ID;
		
		# Busca os dados da tabela principal ----------------------------------------------------------------------//
		$obj = new stdClass();
		$obj->view = $_POST['view'];
		$obj->filters = $envelope->filters;
		$obj->webService = false; 
		$obj->table = $this->table;
	
		// Executa a ação pela classe Controller e salva a resposta
		$mainTbData = Controller::azView($obj)->data->ret;
		
		// Dados da tabela principal
		$objRet = array( $this->table => $mainTbData);
		
		// Se não encontrou os dados da tabela princial, gera erro:
		if(count($mainTbData)==0) {
			$type = 'fail';
			$msg = '';
			
		}else {
			$type = 'win';	
			$msg =  '';
			// Se estiver na view cadastro-clientes, relaciona a foto de perfil e demais possiveis arquivos:
			if($this->table=='clientes' && _view=='cadastro-clientes') {
				//Relaciona os estados
				// Busca as categorias (a primeira é a atual no cadastro)
				$obj = new stdClass();
				$obj->view = $_POST['view'];
				$obj->webService = false; 
				$obj->orderBy = 'EST_SIGLA ASC';
				$obj->table = 'estados';
				$estados = Controller::azView($obj)->data->ret;
				
				// Faz leitura da pasta de imagens do post (capa)
				$files = Controller::readFiles($this->uploadDestination.$ID."/");
				// Se houver uma imagem da capa
				if(count($files)>0) {
					// Arrays com os conteúdos de cada frame de upload de arquivos
					array_push($uploadFramesContent, array('nameID'=>$this->qdUploadID, 'files'=>$files));
				}
				// Incrementa as tabelas adicionais para o retorno
				array_merge($objRet,array('estados'=>$estados,'uploadFramesContent'=>$uploadFramesContent));
				
			// Se estiver na view acompanhar-orcamentos
			}else if(_view=='acompanhar-orcamentos'){
				// Busca os itens
				$sql = "SELECT * FROM  orcamentos_itens WHERE ORI_ORC_ID=".$ID;
				$r = $this->mysqli->query($sql);
				$orcamentos_itens = array();
				if($r->num_rows>0) {
					while($row = $r->fetch_assoc()) {
						$orcamentos_itens[] = $row;	
					}
				}
				// Busca os envios
				$sql = "SELECT * FROM orcamentos_envios WHERE ORE_ORC_ID=".$ID;
				$r = $this->mysqli->query($sql);
				$orcamentos_envios = array();
				if($r->num_rows>0) {
					while($row = $r->fetch_assoc()) {
						$row['ORE_EMAIL'] = substr($row['ORE_EMAIL'], 0, 3).'****'.substr($row['ORE_EMAIL'], strpos($row['ORE_EMAIL'], "@"));
						$orcamentos_envios[] = $row;	
					}
				}
				// Busca as respostas
				$sql = "SELECT  * FROM orcamentos_respostas 
				INNER JOIN orcamentos_envios ON ORE_ID = ORR_ORE_ID
				WHERE ORR_ORC_ID=".$ID;
				$r = $this->mysqli->query($sql);
				$orcamentos_respostas = array();
				if($r->num_rows>0) {
					while($row = $r->fetch_assoc()) {
						$orcamentos_respostas[] = $row;	
					}
				}
				// Incrementa as tabelas adicionais para o retorno
				$objRet = array_merge($objRet,array('orcamentos_itens'=>$orcamentos_itens, 
				'orcamentos_envios'=>$orcamentos_envios, 'orcamentos_respostas'=>$orcamentos_respostas));
				
			}
		}
		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($msg, $type, true, $objRet);
	}
	/**
	 * Inclui um novo cadastro do pacote principal (clientes)
	 * @param  stdClass envelope, dados possíveis :  {acao:"incluir_Principal", campos : [{coluna, value}]}
	 * @return string json com a PK (ID) do cadastro recem criado ou mensagem de falha
	 */
	public function insert_Main($envelope) {
		$obj = new stdClass();
		
		// Cria objeto com os campos recebidos, adicionando outros campos do sistema
		$cp = new Fields($envelope->fields);
		$cp->add($this->colID,$envelope->ID);
		$cp->add('AZ_DHREGISTER',date('Y-m-d H:i:s'));
		$cp->add('AZ_USREGISTER',$_SESSION[SES_USER_NAME]);
		
		// Seta as propriedades do obj para o insert
		$obj->fields = $cp->ret();
		$obj->table = $this->table;
		$obj->view = $_POST['view'];
		$obj->msgWin = "Cadastro de ".$this->keyword.' incluído com sucesso';
		$obj->msgFail =  "Cadastro de ".$this->keyword.' não incluído, tente novamente.';
		$obj->webService = true;
		
		// Registra a ação do usuário
		Controller::registerAction();
		
		
		// Executa a ação pela classe Controller
		Controller::azInsert($obj);
	}
	
	/**
	 * Altera um cadastro existente do pacote principal 
	 * @param  stdClass envelope, dados obrigatorios :  
	   {acao:"alterarPrincipal", campos : [{coluna, value, default}], ID:[_RESERVED_ID tabela principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function update_Main($envelope) {
		$obj = new stdClass();
		$obj->ID = $envelope->ID;
		$obj->colID = $this->colID;
		$obj->table = $this->table;
		$obj->fields = $envelope->fields;
		$obj->view = $_POST['view'];
		$obj->msgWin = "Cadastro(s) de ".$this->keyword.'(s) alterado(s) com sucesso';
		$obj->msgFail = "Cadastro(s) de ".$this->keyword.'(s) não alterado(s), tente novamente.';
		// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
		$obj->webService = true;
			
		Controller::registerAction();
		// Executa a ação pela classe Controller utilizando a model mMeusdados
		Controller::azUpdate($obj);
	}
	
	/**
	 * Deleta um cadastro do pacote principal (clientes)
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"deletarPrincipal", ID:[do cadastro principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function archive_Main($envelope) {
		$obj = new stdClass();
		$obj->ID = $envelope->ID;// Array com o(s) ID(s) selecionado(s)
		$obj->table = $this->table;
		$obj->colID = $this->colID;
		// Cria os campos do framework  para arquivamento de cadastro
		$cp = new Fields();
		$cp->add('AZ_ARCHIVED',1);
		$cp->add('AZ_DHARCHIVED',date('Y-m-d H:i:s'));
		$cp->add('AZ_USARCHIVED',$_SESSION[SES_USER_NAME]);
		// Adiciona os campos criados
		$obj->fields = $cp->ret();
		$obj->view = $_POST['view'];
		$s = count($envelope->ID) > 1 ? 's' : '';// Variavel para complemento da frase de erro ou sucesso
		$obj->msgWin =   "Cadastro$s de ".$this->keyword."$s arquivado$s com sucesso";
		$obj->msgFail = "Cadastro$s de ".$this->keyword."$s não arquivado$s, tente novamente.";
		Controller::registerAction();
		// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
		$obj->webService = true;
		// Executa a ação pela classe Controller
		Controller::azUpdate($obj);
	}
	
	/**
	 * Reserva ou busca (caso já tenha sido reservada) uma PK(ID) para o próximo cadastro
	 * Verifica e exclui os arquivos de upload de uma tentativa fracassada de cadastro anterior (com o mesmo ID)
	 * @return string json com a PK(ID) reservada ou mensagem de falha
	 */
	public function prepareContainer() {
		// Array para o conteúdo dos frames de upload
		$uploadFramesContent = array();
		
		// Busca uma possível PK(ID) já reservada ou reserva uma nova
		$r = Controller::findReserveID($this->table,$this->colID,$_POST['view']);
		
		// Verifica se processou com sucesso
		if( $r->type=='win' ) {
			// ID reservado/encontrado
			$ID = $r->data->ID;	
			if($this->table=='clientes') {
				// Busca as categorias (a primeira é a atual no cadastro)
				$obj = new stdClass();
				$obj->view = $_POST['view'];
				$obj->webService = false; 
				$obj->orderBy = 'EST_SIGLA ASC';
				$obj->table = 'estados';
				$estados = Controller::azView($obj)->data->ret;
				
				// Faz leitura da pasta de arquivos do cadastro principal
				$files = Controller::readFiles($this->uploadDestination.$ID."/");
				
				// Se houver uma imagem da capa
				if(count($files)>0) {
					// Arrays com os conteúdos de cada frame de upload de arquivos
					array_push($uploadFramesContent, array('nameID'=>$this->qdUploadID, 'files'=>$files));
				}
				// Define o objeto de retorno
				$objRet = array('ID'=>$ID,'uploadFramesContent'=>$uploadFramesContent,'estados'=>$estados);
			}else {
				// Define o objeto de retorno
				$objRet = array('ID'=>$ID);	
			}
		// Falha:
		}else {
			// Se o login expirou, redireciona para tela de login
			if($r->message=='Seu acesso expirou, realize o login novamente.'){
				$objRet = array('redirect'=>'/webadmin/login/');
			}else {
				$objRet = array();
			}
		}
		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($r->message, $r->type, true, $objRet);
	}
	
	/**
	 * Recepciona os arquivos do upload e seta as regras para processamento do(s) arquivo(s)
	 * @param stdClass envelope, dados possíveis : acao, qdUploadID, ID
	 * @return string json mensagem sucesso  ou falha
	 */
	public function receiveFile($envelope) {
		// Upload da foto da visita
		if($envelope->qdUploadID!='') {
			// Pasta destino para armazenamento do(s) arquivo(s) do upload
			$envelope->destination = SYS_SRC_APP.$this->uploadDestination.$envelope->ID."/";
			// Array com o(s) arquivo(s) do upload
			$envelope->files = $_FILES['files'];
			// Se true : cria uma miniatura da imagem na mesma pasta de upload para rapida exibição na view
			$envelope->thumbnail = true;
			// Tamanho máximo em MB
			$envelope->maxSizeMB = 2;
			// Quantidade máxima
			$envelope->maxAmount = 1;
			// Altera nome do arquivo original ?
			$envelope->updateFileName = false;
			// Extensoes permitidas (argumento para preg_match)
			$envelope->allowedExtensions = "jp?g|webp|pdf";
			// Url dos arquivos, para retorno das imagens após upload efetudo
			$envelope->urlFiles = SYS_APP_URL.$this->uploadDestination.$envelope->ID."/";
			
			// Recepciona o(s) arquivo(s) e imprimi o resultado
			echo json_encode(Controller::uploadFile($envelope));
	
		// qdUploadID não reconhecido 
		}else {
			Controller::azReturn('qdUploadID não reconhecido.', 'fail', true);	
		}
	}
	
	/**
	 * Baixar de formar dinamica um determinado arquivo
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"baixarArquivo", fileName, qdUploadID, ID}
	 * @return string json com a URL para baixar o arquivo ou mensagem de falha
	 */
	public function downloadFile($envelope) {
		$fileName = Controller::getValue($envelope->fields,'fileName');
		//die($fileName);
		$qdUploadID = Controller::getValue($envelope->fields,'qdUploadID');
		// Verifica se é download da foto capa da obra
		if($qdUploadID=='uploadCapaObra' && _view=='cadastro-obras'){
			$src = '../etc/uploads/obras/capas/'.$envelope->ID.'/'.$fileName;
			
		// Verifica se é download de documento anexado ao custo da obra
		}else if($qdUploadID=='uploadDocumentoCompra' && _view=='lancar-custos-obra'){
			$src = '../etc/uploads/compras/documentos/'.$envelope->ID.'/'.$fileName;
		// Erro
		}else {
			Controller::azReturn('qdUploadID não reconhecido.', 'fail', true,$qdUploadID);	
		}
		$srcFile = realpath(SYS_SRC_APP.$src); // Caminho real do arquivo
		$urlFile = SYS_APP_URL.$src; // Endereço web do arquivo
		
		// Verifica se é um arquivo válido 
		if(is_file($srcFile)){
			$tipo = 'win';
			$msg = '';
			// Sessões utilizadas no arquivo php/downloader.php
			$_SESSION['urlFileToDownload'] = $urlFile;
			$_SESSION['srcFileToDownload'] = $srcFile;
			
		// Arquivo inválido
		}else {
			$tipo = 'fail';
			$msg = 'Arquivo não encontrado.';
			// Resetas as sessões de download
			$_SESSION['urlFileToDownload'] = null;
			$_SESSION['srcFileToDownload'] = null;
			
			
		}
		
		// De qualquer forma, retorna a URL  e o caminho fonte do arquivo
		$objRet = array('url'=>$urlFile,'src'=>$srcFile);

		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($msg, $tipo, true, $objRet);
	}
	/**
	 * Deleta um arquivo do servidor (deletar também o thumbnail se houver)
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"deletarArquivo", fileName, qdUploadID, ID}
	 * @return string json com mensagem de sucesso ou falha
	 */
	public function deleteFile($envelope) {
		$obj = new stdClass();
		$fileName = Controller::getValue($envelope->fields,'fileName');
		$qdUploadID = Controller::getValue($envelope->fields,'qdUploadID');
		// Deletar foto de perfil do cliente
		//if($qdUploadID=='uploadFotoPerfil' && $this->table=='clientes'){}
		$obj = new stdClass();
		$obj->view = $_POST['view'];
		$obj->fileName = $fileName;
		$obj->folder = $this->uploadDestination.$envelope->ID."/";
		$obj->webService = true;
		
		// Executa a ação pela classe Controller
		Controller::delete_file($obj);	
	}
	
	/**
	 * Action para rodar testes
	 * @param  stdClass envelope
	 * @return string json com mensagem de sucesso ou falha
	 */
	public function azTeste($envelope) {
		
		$pot = $envelope->ID*$envelope->ID;
		die('<table bgcolor="#009900"><tr><td>O resultado é: '.$pot.'</td></tr></table>');
		// Teste de envio do convite(pelo arquiteto) para o cliente se cadastrar no app
		$conversionData = array(
			'nome_dest'=>'lucas Nunes', 
			'email_dest'=>'lucasnunes@lbnsistemas.com.br', 
			'titulo_rem'=>'arquiteto', 
			'nome_rem'=>'lucas', 
			'assunto'=>'Nova Solicitação de Orçamento', 
			'codigo'=>'123',
			'url_action'=>'https://brickbe.com.br/solicitacao/?id=1255&codigo=123'
		);
		$ret = $this->cEmail->enviaEmail('avisar_fornecedor_orcamento',0,0, false, false,$conversionData);
		var_dump($ret);
		//Controller::azReturn('oi baby!','fail',true,'');
	}
	
	
	
	
}