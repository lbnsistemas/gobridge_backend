<?php
/**  
 * Controller : cMeusDados
 * Alteração dos dados do parceiro
 *
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, LBN Sistemas
 * @version 1.2.0
 */
class cMeusDados extends Controller {
	private $table='parceiros';
	private $colID='PAR_ID';			// PK/ID da tabela principal
	private $keyword='Parceiro';			// Palavra a ser utilizada nas mensagens de retorno
	private $qdUploadID='uploadFotoPerfil';
	private $uploadDestination='../etc/uploads/parceiros/';
	private $mysqli;
	/**
	 * Construtor
	 * 
	 */
	public function __construct() 
	{
		// Intância do mysqli
		$this->mysqli = Controller::getMysqli();
	}
	/**
	 * Reserva ou busca (caso já tenha sido reservada) uma PK(ID) para o próximo cadastro
	 * Verifica e exclui os arquivos de upload de uma tentativa fracassada de cadastro anterior (com o mesmo ID)
	 * @return string json com a PK(ID) reservada ou mensagem de falha
	 */
	public function prepareContainer() {
		// Array para o conteúdo dos frames de upload
		$uploadFramesContent = array();
		
		// Busca uma possível PK(ID) já reservada ou reserva uma nova
		$r = Controller::findReserveID($this->table,$this->colID,$_POST['view']);
		
		// Verifica se processou com sucesso
		if( $r->type=='win' ) {
			// ID reservado/encontrado
			$ID = $r->data->ID;

			// Faz leitura da pasta de arquivos do cadastro principal
			$files = Controller::readFiles($this->uploadDestination.$ID."/");
			// Se houver uma imagem da capa
			if(count($files)>0) {
				// Arrays com os conteúdos de cada frame de upload de arquivos
				array_push($uploadFramesContent, array('nameID'=>$this->qdUploadID, 'files'=>$files));
			}
			// Define o objeto de retorno
			$objRet = array('ID'=>$ID,'uploadFramesContent'=>$uploadFramesContent);
			
		// Falha:
		}else {
			$objRet = array();
			
		}
		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($r->message, $r->type, true, $objRet);
	}	
	/**
	 * Altera um cadastro existente do pacote principal (clientes)
	 * @param  stdClass envelope, dados obrigatorios :  
	   {acao:"update_Main", campos : [{coluna, value, default}], ID:[_ID_RESERVADO tabela principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function update_Main($envelope) {
		// Ação negada para usuarios nivel USUARIO
		if($_SESSION[SES_USER_LEVEL]=='USER') {
			Controller::azReturn("Permissão negada, inclusão cancelada.","fail",true);
		
		// Para todos outros níveis acima, permite inclusões :
		}else {
			$q = array();
			//Receber os dados
			$responsavel = Controller::getValue($envelope->fields,'PAR_RESPONSAVEL');
			$email = Controller::getValue($envelope->fields,'PAR_EMAIL');
			$celular = Controller::getValue($envelope->fields,'PAR_CELULAR');
			$senha1 = Controller::getValue($envelope->fields,'senha1');
			$senha2 = Controller::getValue($envelope->fields,'senha2');
			
			//Verifica se as senhas são identicas
			if($senha1!=$senha2) {
				Controller::azReturn("As senhas não conferem, digite a mesma senha no dois campos.","fail",true);	
			}
			
			/* Criptografa a senha para salvar no BD
			if(Controller::getValue($envelope->fields,'USE_PASSWORD')!='') {
				$senhaCript = md5(Controller::getValue($envelope->fields,'USE_PASSWORD').Controller::getValue($envelope->fields,'USE_USERNAME'));
				Controller::setValue($envelope->fields,'USE_PASSWORD',$senhaCript);	
			}*/
			
			
			// SQL string para atualizar os dados do parceiro
			array_push($q,
			'UPDATE parceiros SET PAR_RESPONSAVEL="'.$responsavel.'", PAR_EMAIL="'.$email.'", PAR_CELULAR="'.$celular.'" 
			WHERE PAR_ID='.$envelope->ID);
			
			
			
			// SQL string para atualizar a senha do parceiro (se está foi alterada)
			if($senha1!='') {
				array_push($q,
				'UPDATE  az_users SET USE_PASSWORD="'.$senha1.'", USE_EMAIL="'.$email.'"  
				WHERE USE_PAR_ID='.$envelope->ID);
			}else {
				// SQL string para trocar o email de acesso
				array_push($q,
				'UPDATE  az_users SET USE_EMAIL="'.$email.'" WHERE USE_PAR_ID='.$envelope->ID);
			}
			
			
			
			// Executa transaction
			if(Database::transaction($q)==false) {
				$objRet = array('stringSQL'=>Database::get_stringSQL(),'errorSQL'=>Database::get_errorSQL());
				Controller::azReturn('Erro na atualização do cadastro, tente novamente ou contato o suporte BrickBe.','fail',true,$objRet);
				exit();
			}
			Database::disconnect();
			// Registra ação no BD do sistema
			Controller::registerAction();
			// Retorno de sucesso
			Controller::azReturn('Dados atualizados com sucesso!','win',true, '');
		}
	}
	
	
	/**
	 * Traz todos os dados para carregamento de todos formularios da view (principal e dinamicos)
	 * @param  stdClass envelope, dados possíveis : acao : 'dadosFormularios', 
	 *         filtros : { value1 : ID, colunas : ['<?=_columnID?>'], regra : '=' 
	 * @return string json mensagem sucesso  ou falha
	 */
	public function formsData($envelope) {
		# Array para retornar os conteudos dos frames de upload
		$uploadFramesContent = array();
		
		# ID do cadastro principal
		$ID = $envelope->ID;
		
		# Busca os dados da tabela principal 
		$obj = new stdClass();
		$obj->view = $_POST['view'];
		$obj->filters = $envelope->filters;
		$obj->webService = false; 
		$obj->table = $this->table;
		
		// Executa a ação pela classe Controller e salva a resposta
		$ret = Controller::azView($obj);
		
		// Verifica se foi executado com sucesso
		if($ret->type=='win') {
			// Define objeto com os dados da tabela Principal "Main"
			$dataMainTable = $ret->data->ret;
			
			// Faz leitura da pasta de arquivos do diretorio principal
			$files = Controller::readFiles($this->uploadDestination.$ID."/");
			// Se houver uma imagem da capa
			if(count($files)>0) {
				// Arrays com os conteúdos de cada frame de upload de arquivos
				array_push($uploadFramesContent, array('nameID'=>$this->qdUploadID, 'files'=>$files));
			}
			
			// Define o objeto de retorno
			$objRet = array( $this->table => $dataMainTable,'uploadFramesContent'=>$uploadFramesContent);
			
		// Falha:
		}else {
			$objRet = array();
		}
		
		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($ret->message, $ret->type, true, $objRet);
	}
	 
	 /**
	 * Recepciona os arquivos do upload e seta as regras para processamento do(s) arquivo(s)
	 * @param stdClass envelope, dados possíveis : acao, qdUploadID, ID
	 * @return string json mensagem sucesso  ou falha
	 */
	public function receiveFile($envelope) {
		// Upload da imagem avatar
		if($envelope->qdUploadID==$this->qdUploadID) {
			// Pasta destino para armazenamento do(s) arquivo(s) do upload
			$envelope->destination = SYS_SRC_APP.$this->uploadDestination.$envelope->ID."/";
			// Array com o(s) arquivo(s) do upload
			$envelope->files = $_FILES['files'];
			// Se true : cria uma miniatura da imagem na mesma pasta de upload para rapida exibição na view
			$envelope->thumbnail = true;
			// Tamanho máximo em MB
			$envelope->maxSizeMB = 2;
			// Quantidade máxima
			$envelope->maxAmount = 1;
			// Altera nome do arquivo original ?
			$envelope->updateFileName = false;
			// Extensoes permitidas (argumento para preg_match)
			$envelope->allowedExtensions = "jp?g|webp";
			// Url dos arquivos, para retorno das imagens após upload efetudo
			$envelope->urlFiles = SYS_APP_URL.$this->uploadDestination.$envelope->ID."/";
			
			// Recepciona o(s) arquivo(s) e imprimi o resultado
			$ret = Controller::uploadFile($envelope);
			echo(json_encode($ret));
		// qdUploadID não reconhecido 
		}else {
			Controller::azReturn('qdUploadID não reconhecido.', 'fail', true);	
		}
	}
	
	/**
	 * Baixar de formar dinamica um determinado arquivo
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"baixarArquivo", fileName, qdUploadID, ID}
	 * @return string json com a URL para baixar o arquivo ou mensagem de falha
	 */
	public function downloadFile($envelope) {
		$fileName = Controller::getValue($envelope->fields,'fileName');
		$qdUploadID = Controller::getValue($envelope->fields,'qdUploadID');
		$src	 = $this->uploadDestination.$envelope->ID."/".$fileName ;	
		$srcFile = realpath(SYS_SRC_APP.$src); // Caminho real do arquivo
		$urlFile = SYS_APP_URL.$src; // Endereço web do arquivo
		
		// Verifica se é um arquivo válido 
		if(is_file($srcFile)){
			$tipo = 'win';
			$msg = '';
			// Sessões utilizadas no arquivo php/downloader.php
			$_SESSION['urlFileToDownload'] = $urlFile;
			$_SESSION['srcFileToDownload'] = $srcFile;
			
		// Arquivo inválido
		}else {
			$tipo = 'fail';
			$msg = 'Arquivo não encontrado.';
			// Resetas as sessões de download
			$_SESSION['urlFileToDownload'] = null;
			$_SESSION['srcFileToDownload'] = null;
			
			
		}
		
		// De qualquer forma, retorna a URL  e o caminho fonte do arquivo
		$objRet = array('url'=>$urlFile,'src'=>$srcFile);

		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($msg, $tipo, true, $objRet);
	}
	/**
	 * Deleta um arquivo do servidor (deletar também o thumbnail se houver)
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"deletarArquivo", fileName, qdUploadID, ID}
	 * @return string json com mensagem de sucesso ou falha
	 */
	public function deleteFile($envelope) {
		$obj = new stdClass();
		$fileName = Controller::getValue($envelope->fields,'fileName');
		$qdUploadID = Controller::getValue($envelope->fields,'qdUploadID');
		// Deleta imagem da categoria principal
		if($qdUploadID==$this->qdUploadID){
			$obj = new stdClass();
			$obj->view = $_POST['view'];
			$obj->fileName = $fileName;
			$obj->folder = $this->uploadDestination.$envelope->ID."/";
			$obj->webService = true;
		}
		
		// Executa a ação pela classe Controller
		Controller::delete_file($obj);	
	}
	
	
}
