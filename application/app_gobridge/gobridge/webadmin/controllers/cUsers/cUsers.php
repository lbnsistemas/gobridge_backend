<?php
// Incluí model especifica
require_once(SYS_SRC_MODELS."/mUsers/mUsers.php");

/**  
 * Controller : cUsuarios
 * Inclusão e alteração no cadastro de usuários (principal)
 * criação de perfis de permissões
 *
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, LBN Sistemas
 * @version 1.2.0
 */
class cUsers extends Controller {
	private $table='az_users';
	private $colID='USE_ID';			// PK/ID da tabela principal
	private $keyword='Usuário';			// Palavra a ser utilizada nas mensagens de retorno
	private $qdUploadID='uploadFotoPerfil';
	private $uploadDestination='../etc/uploads/users/';
	private $model;
	/**
	 * Construtor
	 * 
	 */
	public function __construct() 
	{
		// Inicia model
		$this->model = new mUsers();
	}
	
	/**
	 * Traz os cadastros para serem visualizados e editados(com a devida permissao) na view
	 * @param  stdClass envelope, dados possíveis : inicio, limite, orderBy, filtros[{}]
	 * @return string json mensagem sucesso  ou falha
	 */
	public function requestData($envelope) {
		$obj = new stdClass();
		$obj->init = $envelope->init;
		$obj->limit = $envelope->limit;
		$obj->orderBy = $envelope->orderBy;
		$obj->filters = $envelope->filters;	
		$obj->view = $_POST['view'];
		$obj->webService = false;
		$obj->table = $this->table;
		// Complemento SQL para relacionar os usuriarios com seus levels
		$complSQL = " LEFT JOIN applevels ON LVL_ID = USE_LVL_ID ";
		// Verifica nível do usuario
		switch($_SESSION[SES_USER_LEVEL]) {
			// usuário nivel MANTENEDOR, vê todos PROPRIETARIOS e abaixo (incluindo seu proprio cadastro)
			case "MAINTENANCE":
				$complSQL .= 'WHERE (USE_LEVEL<>"MAINTENANCE" OR USE_ID="'.$_SESSION[SES_USER_ID].'")';
				break;
			// usuário nivel PROPRIETARIOS, vê todos ADMINISTRADOR e abaixo (incluindo seu proprio cadastro)
			case "OWNER" :
				$complSQL .= 'WHERE (USE_LEVEL<>"MAINTENANCE" AND USE_LEVEL<>"OWNER" 
				OR USE_ID="'.$_SESSION[SES_USER_ID].'")';
				break;
			// usuário nivel ADMINISTRADOR, vê todos USUARIOS (incluindo seu proprio cadastro)
			case "ADMINISTRATOR" :
				$complSQL .= 'WHERE (USE_LEVEL<>"MAINTENANCE" AND USE_LEVEL<>"OWNER" AND 
				USE_LEVEL<>"ADMINISTRATOR" OR USE_ID="'.$_SESSION[SES_USER_ID].'")';
				break;
			// usuário nivel USUARIO, vê apenas o próprio cadastro
			case "USER" :
				$complSQL .= 'WHERE USE_ID="'.$_SESSION[SES_USER_ID];
				break;
		}
		$obj->complSQL = $complSQL;
		// Colunas reservadas do sistema 
		array_push($envelope->columns,$this->table.'.AZ_ARCHIVED'); 	// Informa se o cadastro está arquivado
		array_push($envelope->columns,$this->table.'.AZ_USARCHIVED');	// Informa o nome do usuário que arquivou
		array_push($envelope->columns,$this->table.'.AZ_DHARCHIVED');	// Informa a data que foi arquivado
		array_push($envelope->columns,'USE_STATUS');	// Informa o status atual do usuário
		array_push($envelope->columns,'LVL_TITLE');		// Informa o titulo do level de aplicativo
		array_push($envelope->columns,'LVL_NUMBER');	// Informa o número do level de aplicativo
		array_push($envelope->columns,'LVL_IMAGE');		// Informa o número do level de aplicativo
		// Adiciona as colunas adicionais + as solicitadas pela View
		$obj->columns = $envelope->columns;
		
		// Executa a ação pelo Controller e traz o objeto de retorno 
		$ret = Controller::azView($obj);
		
		// Verifica se processou com sucesso
		if($ret->type=='win') {
			$dataMainTable = $ret->data;
			$rows = $ret->data->ret;
			// Analisa todas categorias e verifica se alguma possuí thumbnail a ser apresentado na listagem
			foreach($rows as $row) {
				$srcFiles = $this->uploadDestination.$row->USE_ID."/";
				$arFile = Controller::hasThumbnail($row->USE_PROFILEPIC,$srcFiles);
				if($arFile !==false) {
					$row->THUMBNAIL = $arFile['thumbnail'];
					$row->URL = $arFile['url'];
				}
				
				// Verifica se o corrente usuário possui um icone de level valido
				$srcIcone = SYS_SRC_APP.'../etc/uploads/applevels/'.$row->USE_LVL_ID.'/'.$row->LVL_IMAGE;
				if(is_file($srcIcone) && $row->USE_LVL_ID>0) {
					$row->ICONE = SYS_APP_URL.'../etc/uploads/applevels/'.$row->USE_LVL_ID.'/'.$row->LVL_IMAGE;
				}
				
			}
		
		// Falha ou alerta
		}else {
			$dataMainTable = array();
		}
		
		// Imprimi o retorno no formato JSON
		Controller::azReturn($ret->message, $ret->type, $ws=true,$dataMainTable);
			
	}
	/**
	 * Reserva ou busca (caso já tenha sido reservada) uma PK(ID) para o próximo cadastro
	 * Verifica e exclui os arquivos de upload de uma tentativa fracassada de cadastro anterior (com o mesmo ID)
	 * @return string json com a PK(ID) reservada ou mensagem de falha
	 */
	public function prepareContainer() {
		// Array para o conteúdo dos frames de upload
		$uploadFramesContent = array();
		
		// Busca uma possível PK(ID) já reservada ou reserva uma nova
		$r = Controller::findReserveID($this->table,$this->colID,$_POST['view']);
		
		// Verifica se processou com sucesso
		if( $r->type=='win' ) {
			// ID reservado/encontrado
			$ID = $r->data->ID;
			
			// Busca as categorias (a primeira é a atual no cadastro)
			$obj = new stdClass();
			$obj->view = $_POST['view'];
			$obj->webService = false; 
			$obj->orderBy = 'CAT_TITLE ASC';
			$obj->complSQL = 'WHERE AZ_ARCHIVED=FALSE '; 
			$obj->table = 'categories';
			$categories = Controller::azView($obj)->data->ret;
			
			// Busca as subcategorias (a primeira é a atual no cadastro)
			$obj = new stdClass();
			$obj->view = $_POST['view'];
			$obj->webService = false; 
			$obj->orderBy = 'SUB_TITLE ASC';
			$obj->complSQL = 'WHERE AZ_ARCHIVED=FALSE'; 
			$obj->table = 'subcategories';
			$subcategories = Controller::azView($obj)->data->ret;
			
			// Busca as permissões salvas na ultima tentativa de cadastro
			$obj = new stdClass();
			$obj->view = $_POST['view'];
			$obj->webService = false; 
			$obj->complSQL = 'WHERE AZ_ARCHIVED=FALSE AND PER_USE_ID='.$ID; 
			$obj->table = 'az_permissions';
			$permissions = Controller::azView($obj)->data->ret;
		
			// Faz leitura da pasta de arquivos do cadastro principal
			$files = Controller::readFiles($this->uploadDestination.$ID."/");
			// Se houver uma imagem da capa
			if(count($files)>0) {
				// Arrays com os conteúdos de cada frame de upload de arquivos
				array_push($uploadFramesContent, array('nameID'=>$this->qdUploadID, 'files'=>$files));
			}
			// Define o objeto de retorno
			$objRet = array('ID'=>$ID,'uploadFramesContent'=>$uploadFramesContent,
			'categories'=>$categories, 'subcategories'=>$subcategories,'az_permissions'=>$permissions);
			
		// Falha:
		}else {
			$objRet = array();
			
		}
		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($r->message, $r->type, true, $objRet);
	}

	/**
	 * Inclui um novo cadastro de permissao para uma determinada View
	 * @param  stdClass envelope, dados possíveis :  {acao:"incluir_formConfiguracoesPermissoes", campos : [{coluna, value}]}
	 * @return string json com a PK (ID) do cadastro recem criado ou mensagem de falha
	 */
	public function insert_formPermissoesPrincipal($envelope) {
		// Ação negada para usuarios nivel USER
		if($_SESSION[SES_USER_LEVEL]=='USER') {
			Controller::azReturn("Permissão negada, inclusão não concluída.","fail",true);
			
		// Para qualquer outro nível acima :
		}else {
			$obj = new stdClass();
			$obj->table = 'az_permissions';
			
			// Index de relacionamento com a tabela principal
			$cp = new Fields($envelope->fields);
			$cp->add('PER_USE_ID',$envelope->ID);
			$obj->fields = $cp->ret();
			$obj->view = $_POST['view'];
			$obj->msgWin = 'Permissão incluída com sucesso';
			$obj->msgFail = 'Permissão não incluída, tente novamente.';
			// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
			$obj->webService = true;
			// Registra ação no BD do sistema
			Controller::registerAction();
			// Executa a ação pela classe Controller
			Controller::azInsert($obj);
		}
	}
	/**
	 * Altera uma permissão
	 * @param  stdClass envelope, dados possíveis :  
	 {acao:"alterar_formPermissoesPrincipal", campos : [{coluna, value}], ID:[da tabela principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function update_formPermissoesPrincipal($envelope) {
		// Ação negada para usuarios nivel USER
		if($_SESSION[SES_USER_LEVEL]=='USER') {
			Controller::azReturn("Permissão negada, alteração cancelada.","fail",true);
			
		// Para qualquer outro nível acima :
		}else {
			$obj = new stdClass();
			$obj->table = 'az_permissions';
			$obj->colID = 'PER_ID';
			$obj->ID = $envelope->ID;
			$obj->fields = $envelope->fields;
			$obj->view = $_POST['view'];
			$obj->msgWin = 'Permissão alterada com sucesso';
			$obj->msgFail = 'Permissão não alterada, tente novamente.';
			// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
			$obj->webService = true;
			// Registra ação no BD do sistema
			Controller::registerAction();
			// Executa a ação pela classe Controller
			Controller::azUpdate($obj);
		}
	}
	/**
	 * Deleta permanentemente uma permissão
	 * @param  stdClass envelope, dados obrigatórios :  {acao:"deletar_formPermissoesPrincipal", ID}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function archive_formPermissoesPrincipal($envelope) {
		// Altera as propriedades do controller
		$this->table='az_permissions';
		$this->colID='PER_ID';
		$this->keyword='Permissão';
		// Executa ação de arquivamento principal
		$this->archive_Main($envelope);
	}

	/**
	 * Inclui um novo cadastro do pacote principal (clientes)
	 * @param  stdClass envelope, dados possíveis :  {acao:"insert_Main", campos : [{coluna, value}]}
	 * @return string json com a PK (ID) do cadastro recem criado ou mensagem de falha
	 */
	public function insert_Main($envelope) {
		
		// Ação negada para usuarios nivel USUARIO
		if($_SESSION[SES_USER_LEVEL]=='USER') {
			Controller::azReturn("Permissão negada, inclusão cancelada.","fail",true);
		
		// Para todos outros níveis acima, permite inclusões :
		}else {
			$obj = new stdClass();
			// Criptografa a senha para salvar no BD
			if(Controller::getValue($envelope->fields,'USE_PASSWORD')!='') {
				$senhaCript = md5(Controller::getValue($envelope->fields,'USE_PASSWORD').Controller::getValue($envelope->fields,'USE_USERNAME'));
				Controller::setValue($envelope->fields,'USE_PASSWORD',$senhaCript);	
				
			}
			
			// Verifica nível do usuario
			switch($_SESSION[SES_USER_LEVEL]) {
				// usuário nivel MANTENEDOR, não pode criar outros MANTENEDORs
				case "MAINTENANCE":
					if(Controller::getValue($envelope->fields,'USE_LEVEL')=='MAINTENANCE') {
						Controller::azReturn("Você não pode criar usuários  nível 'MANTENEDOR'.","fail",true);
						exit();
					}
					break;
				// usuário nivel PROPRIETARIO, não pode criar outros PROPRIETARIOs
				case "OWNER" :
					if(Controller::getValue($envelope->fields,'USE_LEVEL')=='OWNER'){
						Controller::azReturn("Você não pode criar usuários nível 'PROPRIETARIO'.","fail",true);
						exit();
					}
					break;
				// usuário nivel ADMINISTRADOR, não pode criar outros ADMINISTRADORs
				case "ADMINISTRATOR" :
					if(Controller::getValue($envelope->fields,'USE_LEVEL')=='ADMINISTRATOR'){
						Controller::azReturn("Você não pode criar usuários nível 'ADMINISTRADOR'.","fail",true);
						exit();
					}
					break;
				// usuário nivel USUARIO não pode realizar ação de incluir novos usuários
				case "USER" :
					Controller::azReturn("Você não pode criar usuários.","fail",true);
					exit();
			}
			
			// Se for um novo associado, cria uma lista de permissões para utitilização do aplicativo
			if(Controller::getValue($envelope->fields,'USE_LEVEL')=='ASSOCIATED') {
				Database::connect();
				// Dados do novo usuario
				$use_id = $envelope->ID;
				$arSQLStrings = array();
				# Permissões: V-> Visualização, I-> Inclusão, A-> Alteração, D-> Exclusão
				// Permissão de V na tela 'login' do webadmin
				array_push($arSQLStrings,'INSERT INTO az_permissions VALUES (NULL,'.$use_id.',"V","login",0,NULL,NULL)');
				// Permissão de VI na tela 'meusdados' do webadmin
				array_push($arSQLStrings,'INSERT INTO az_permissions VALUES (NULL,'.$use_id.',"VAD","meusdados",0,NULL,NULL)');
				// Permissão de VI na tela 'login.html' do app
				array_push($arSQLStrings,'INSERT INTO az_permissions VALUES (NULL,'.$use_id.',"VI","login.html",0,NULL,NULL)');
				// Permissão VIDA na tela 'favorites.html' do app
				array_push($arSQLStrings,'INSERT INTO az_permissions VALUES (NULL,'.$use_id.',"VI","favorites.html",0,NULL,NULL)');
				// Permissão de V na tela 'index.html' do app
				array_push($arSQLStrings,'INSERT INTO az_permissions VALUES (NULL,'.$use_id.',"V","index.html",0,NULL,NULL)');
				// Permissão de VIDA na tela 'business.html' do app 
				array_push($arSQLStrings,'INSERT INTO az_permissions VALUES (NULL,'.$use_id.',"VIDA","business.html",0,NULL,NULL)');
				// Permissão de VIDA na tela 'visits.html' do app 
				array_push($arSQLStrings,'INSERT INTO az_permissions VALUES (NULL,'.$use_id.',"VIDA","visits.html",0,NULL,NULL)');
				// Permissão de VIDA na tela 'indications.html' do app  
				array_push($arSQLStrings,'INSERT INTO az_permissions VALUES (NULL,'.$use_id.',"VIDA","indications.html",0,NULL,NULL)');
				// Permissão de VIDA na tela 'profile.html' do app 
				array_push($arSQLStrings,'INSERT INTO az_permissions VALUES (NULL,'.$use_id.',"VIDA","profile.html",0,NULL,NULL)');
				// Permissão de VIDA na tela 'profile_visits.html' do app 
				array_push($arSQLStrings,'INSERT INTO az_permissions VALUES (NULL,'.$use_id.',"VIDA","profile_visits.html",0,NULL,NULL)');
				// Permissão de V na tela 'associateds.html' do app 
				array_push($arSQLStrings,'INSERT INTO az_permissions VALUES (NULL,'.$use_id.',"V","associateds.html",0,NULL,NULL)');
				// Permissão de V na tela 'promotions.html' do app 
				array_push($arSQLStrings,'INSERT INTO az_permissions VALUES (NULL,'.$use_id.',"V","promotions.html",0,NULL,NULL)');
				// Permissão de V na tela 'promotions_user.html' do app 
				array_push($arSQLStrings,'INSERT INTO az_permissions VALUES (NULL,'.$use_id.',"VIDA","promotions_user.html",0,NULL,NULL)');
				if(Database::transaction($arSQLStrings)==false) {
					$objRet = array('stringSQL'=>Database::get_stringSQL(),'errorSQL'=>Database::get_errorSQL());
					Controller::azReturn('Erro no cadastro das permissões','fail',true,$objRet);
					exit();
				}
				Database::disconnect();
				
			// Se for usuário comum, concede as permissões básicas:
			}else if(Controller::getValue($envelope->fields,'USE_LEVEL')=='USER') {
				Database::connect();
				// Dados do novo usuario
				$use_id = $envelope->ID;
				$arSQLStrings = array();
				# Permissões: V-> Visualização, I-> Inclusão, A-> Alteração, D-> Exclusão
				// Permissão de VI na tela 'login' do webadmin
				array_push($arSQLStrings,'INSERT INTO az_permissions VALUES (NULL,'.$use_id.',"VI","login",0,NULL,NULL)');
				// Permissão de V na tela 'dashboard' do webadmin 
				array_push($arSQLStrings,'INSERT INTO az_permissions VALUES (NULL,'.$use_id.',"V","dashboard",0,NULL,NULL)');
				// Permissão de VA na tela 'meusdados' do webadmin 
				array_push($arSQLStrings,'INSERT INTO az_permissions VALUES (NULL,'.$use_id.',"VA","meusdados",0,NULL,NULL)');
				if(Database::transaction($arSQLStrings)==false) {
					$objRet = array('stringSQL'=>Database::get_stringSQL(),'errorSQL'=>Database::get_errorSQL());
					Controller::azReturn('Erro no cadastro das permissões','fail',true,$objRet);
					exit();
				}
				Database::disconnect();
			}
			
			// Cria objeto com os campos recebidos, adicionando outros campos do sistema
			$cp = new Fields($envelope->fields);
			$cp->add($this->colID,$envelope->ID);
			$cp->add('AZ_DHREGISTER',date('Y-m-d H:i:s'));
			$cp->add('AZ_USREGISTER',$_SESSION[SES_USER_NAME]);
			// Prossegue com a inclusão do cadastro principal
			$obj->fields = $cp->ret();
			$obj->table = $this->table;
			$obj->view = $_POST['view'];
			$obj->msgWin = $this->keyword.' incluído com sucesso';
			$obj->msgFail =  $this->keyword.' não incluído, tente novamente.';
			// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
			$obj->webService = true;
			// Registra ação no BD do sistema
			Controller::registerAction();
			// Executa a ação pela classe Controller
			Controller::azInsert($obj);
		}
	}
	/**
	 * Arquivo um dado cadastro
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"archive_Main", ID:[do cadastro principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function archive_Main($envelope) {
		// Ação negada para usuarios nivel USER
		if($_SESSION[SES_USER_LEVEL]=='USER') {
			Controller::azReturn("Permissão negada, arquivamento cancelado.","fail",true);
			
		// Para qualquer outro nível acima :
		}else {	
			$obj = new stdClass();
			$obj->table = $this->table;
			$obj->colID = $this->colID;
			$obj->ID = $envelope->ID;// Array com o(s) ID(s) selecionado(s)
			// Cria os campos do framework  para arquivamento de cadastro
			$cp = new Fields();
			$cp->add('AZ_ARCHIVED',1);
			$cp->add('AZ_DHARCHIVED',date('Y-m-d H:i:s'));
			$cp->add('AZ_USARCHIVED',$_SESSION[SES_USER_NAME]);
			// Adiciona os campos criados
			$obj->fields = $cp->ret();
			$obj->view = $_POST['view'];
			$s = count($envelope->ID) > 1 ? 's' : '';// Variavel para complemento da frase de erro ou sucesso
			$obj->msgWin =   "Cadastro$s de ".$this->keyword."$s arquivado$s com sucesso";
			$obj->msgFail =  "Cadastro$s de ".$this->keyword."$s não arquivado$s, tente novamente.";
			// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
			$obj->webService = true;
			// Registra ação no BD do sistema
			Controller::registerAction();
			// Executa a ação pela classe Controller
			Controller::azUpdate($obj);
		}
	}
	
	/**
	 * Altera um cadastro existente do pacote principal (clientes)
	 * @param  stdClass envelope, dados obrigatorios :  
	   {acao:"update_Main", campos : [{coluna, value, default}], ID:[_ID_RESERVADO tabela principal]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function update_Main($envelope) {
		$obj = new stdClass();
		$obj->table = $this->table;
		$obj->colID = $this->colID;
		$obj->ID = $envelope->ID;
		 
		// Cada usuario que não seja um simples 'USER', pode criar outros usuários, concedendo a eles 
		// nivéis/privilegios de controle inferiores, nunca o mesmo privilegio que o seu próprio.
		$levelToCreate = Controller::getValue($envelope->fields,'USE_LEVEL');
		
		// Verifica se o usuário está tentando criar um usuário do mesmo ou nível superior
		if(	$this->compareLevel1BiggerOrEqualThanLevel2($levelToCreate,$_SESSION[SES_USER_LEVEL])) {
			Controller::azReturn("Você não pode conceder um nível superior ou igual ao seu.","fail",true);
			exit();
		}
		
		// Se for usuario nivel USUARIO, permite apenas edição de alguns campos do cadastro
		if($_SESSION[SES_USER_LEVEL]=='USER' || $_SESSION[SES_USER_LEVEL]=='ASSOCIATED') {
			$cp = new Fields(); 
			// Primeira linha: Nome, empresa, categoria, subcategoria
			$cp->add('USE_NAME',Controller::getValue($envelope->fields,'USE_NAME'));
			$cp->add('USE_COMPANY',Controller::getValue($envelope->fields,'USE_COMPANY'));
			$cp->add('USE_CATEGORY',Controller::getValue($envelope->fields,'USE_CATEGORY'));
			$cp->add('USE_SUBCATEGORY',Controller::getValue($envelope->fields,'USE_SUBCATEGORY'));
			
			// Segunda linha: Level App., Pontos App., Usuário, Senha
			$cp->add('USE_USERNAME',Controller::getValue($envelope->fields,'USE_USERNAME'));
			// Se a senha foi alterada, pega o novo valor e criptografa para salvar no BD
			if(Controller::getValue($envelope->fields,'USE_PASSWORD')!='') {
				//die(Controller::getValue($envelope->campos,'SUS_SENHA').Controller::getValue($envelope->campos,'SUS_USUARIO'));
				$senhaCript = md5(Controller::getValue($envelope->fields,'USE_PASSWORD').Controller::getValue($envelope->fields,'USE_USERNAME'));
				Controller::setValue($envelope->fields,'USE_PASSWORD',$senhaCript);	
				$cp->add('USE_PASSWORD',Controller::getValue($envelope->fields,'USE_PASSWORD'));		
				
			}
			// Terceira linha: Cidade, Fone, WhatsApp, Email
			$cp->add('USE_CITY',Controller::getValue($envelope->fields,'USE_CITY'));
			$cp->add('USE_PHONE',Controller::getValue($envelope->fields,'USE_PHONE'));
			$cp->add('USE_WHATSAPP',Controller::getValue($envelope->fields,'USE_WHATSAPP'));
			$cp->add('USE_EMAIL',Controller::getValue($envelope->fields,'USE_EMAIL'));
			
			// Quarta linha: URL google maps, URL Facebook
			$cp->add('USE_URLGOOGLEMAPS',Controller::getValue($envelope->fields,'USE_URLGOOGLEMAPS'));
			$cp->add('USE_FACEBOOK',Controller::getValue($envelope->fields,'USE_FACEBOOK'));
			
			// Foto de perfil
			$cp->add('USE_PROFILEPIC',Controller::getValue($envelope->fields,'USE_PROFILEPIC'));
		
			$obj->fields = $cp->ret();

		
		// Para qualquer outro nivel acima, permite a edição de todos os campos
		}else {
			// Se a senha foi alterada, pega o novo valor e criptografa para salvar no BD
			if(Controller::getValue($envelope->fields,'USE_PASSWORD')!='') {
				//die(Controller::getValue($envelope->campos,'SUS_SENHA').Controller::getValue($envelope->campos,'SUS_USUARIO'));
				$senhaCript = md5(Controller::getValue($envelope->fields,'USE_PASSWORD').Controller::getValue($envelope->fields,'USE_USERNAME'));
				Controller::setValue($envelope->fields,'USE_PASSWORD',$senhaCript);	
				
			}
			$obj->fields = $envelope->fields;
		}
		$obj->view = $_POST['view'];
		$obj->msgWin = $this->keyword.' alterado com sucesso';
		$obj->msgFail = $this->keyword.' não alterado, tente novamente.';
		// Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
		$obj->webService = true;
		// Registra ação no BD do sistema
		Controller::registerAction();
		// Executa a ação pela classe Controller utilizando a model mMeusdados
		$ret = Controller::azUpdate($obj); 
	
	}
	
	
	/**
	 * Traz todos os dados para carregamento de todos formularios da view (principal e dinamicos)
	 * @param  stdClass envelope, dados possíveis : acao : 'dadosFormularios', 
	 *         filtros : { value1 : ID, colunas : ['<?=_columnID?>'], regra : '=' 
	 * @return string json mensagem sucesso  ou falha
	 */
	public function formsData($envelope) {
		# Array para retornar os conteudos dos frames de upload
		$uploadFramesContent = array();
		
		# ID do cadastro principal
		$ID = $envelope->ID;
		
		# Busca os dados da tabela principal 
		$obj = new stdClass();
		$obj->view = $_POST['view'];
		$obj->filters = $envelope->filters;
		$obj->webService = false; 
		$obj->table = $this->table;
		// Traz a informação do level do usuario
		$obj->complSQL = " LEFT JOIN applevels ON USE_LVL_ID=LVL_ID ";
		
		// Executa a ação pela classe Controller e salva a resposta
		$ret = Controller::azView($obj);
		
		// Verifica se foi executado com sucesso
		if($ret->type=='win') {
			// Define objeto com os dados da tabela Principal "Main"
			$dataMainTable = $ret->data->ret;
			
			// Busca as categorias (a primeira é a atual no cadastro)
			$obj = new stdClass();
			$obj->view = $_POST['view'];
			$obj->webService = false; 
			$obj->orderBy = 'CAT_ID='.$dataMainTable[0]->USE_CATEGORY.' DESC, CAT_TITLE ASC';
			$obj->complSQL = 'WHERE AZ_ARCHIVED=FALSE '; 
			$obj->table = 'categories';
			$categories = Controller::azView($obj)->data->ret;
			
			// Busca as subcategorias (a primeira é a atual no cadastro)
			$obj = new stdClass();
			$obj->view = $_POST['view'];
			$obj->webService = false; 
			$obj->orderBy = 'SUB_ID='.$dataMainTable[0]->USE_SUBCATEGORY.' DESC, SUB_TITLE ASC';
			$obj->complSQL = 'WHERE AZ_ARCHIVED=FALSE'; 
			$obj->table = 'subcategories';
			$subcategories = Controller::azView($obj)->data->ret;
		
			// Faz leitura da pasta de arquivos do diretorio principal
			$files = Controller::readFiles($this->uploadDestination.$ID."/");
			// Se houver uma imagem da capa
			if(count($files)>0) {
				// Arrays com os conteúdos de cada frame de upload de arquivos
				array_push($uploadFramesContent, array('nameID'=>$this->qdUploadID, 'files'=>$files));
			}
			
			// Busca as permissoes relacionadas ao usuario
			$obj = new stdClass();
			$obj->table = 'az_permissions';
			$obj->view = $_POST['view'];
			$obj->complSQL = "WHERE AZ_ARCHIVED=FALSE AND PER_USE_ID = '".$dataMainTable[0]->USE_ID."'  ORDER BY PER_VIEW";
			$obj->webService = false; // Incluencia no retorno da função, true: print($retorno) | false: return($retorno)
			// Executa a ação pela classe Controller e salva a resposta
			$resp = Controller::azView($obj);
			$permissions = $resp->data->ret;
			
			// Define o objeto de retorno
			$objRet = array( $this->table => $dataMainTable,'uploadFramesContent'=>$uploadFramesContent,
			'categories'=>$categories, 'subcategories'=>$subcategories,'az_permissions'=>$permissions);
			
		// Falha:
		}else {
			$objRet = array();
		}
		
		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($ret->message, $ret->type, true, $objRet);
	}
	

	/**
	 * Verifica se usuario informado no cadastro já existe
	 * @param  stdClass envelope, dados possíveis : acao : 'checarUsuario', campos : { coluna : 'USUARIO', value : (usuario) }
	 * @return string json mensagem sucesso + bool true ou false(exite ou não) ou mensagem de falha
	 */
	public function checksUserName($envelope) {
		if(Controller::checksPermission('V',$_POST['view'])){
			$ret = $this->model->checksUserName(Controller::getValue($envelope->fields,'USERNAME'));
			if($ret->ret!==false) {
				// Retorna o resultado da pesquisa no padrão JSON
				Controller::azReturn('','win',true,$ret);	
			}else {
				Controller::azReturn('Problema para checar usuário. Informe o suporte técnico.','fail',true);	
			}
		}else {
			Controller::azReturn('Permissão para visualizar não cadastrada, verifique o cadastro de usuários.','falha',true);
		}	
	}
	
	/**
	 * Compara o peso de dois níveis de usuários
	 * @param  string $level1, string $level2
	 * @return bool
	 */
	 public function compareLevel1BiggerOrEqualThanLevel2($level1,$level2) {
		$arrayWeights = array('MAINTENANCE'=>99,'OWNER'=>98,'ADMINISTRATOR'=>97,'USER'=>96);
		if($level1!='' && $level2!='') {
			return($arrayWeights[$level1] >= $arrayWeights[$level2]);	
		}else {
			return(false);
		}
	 }
	 
	 /**
	 * Recepciona os arquivos do upload e seta as regras para processamento do(s) arquivo(s)
	 * @param stdClass envelope, dados possíveis : acao, qdUploadID, ID
	 * @return string json mensagem sucesso  ou falha
	 */
	public function receiveFile($envelope) {
		// Upload da imagem avatar
		if($envelope->qdUploadID==$this->qdUploadID) {
			// Pasta destino para armazenamento do(s) arquivo(s) do upload
			$envelope->destination = SYS_SRC_APP.$this->uploadDestination.$envelope->ID."/";
			// Array com o(s) arquivo(s) do upload
			$envelope->files = $_FILES['files'];
			// Se true : cria uma miniatura da imagem na mesma pasta de upload para rapida exibição na view
			$envelope->thumbnail = true;
			// Tamanho máximo em MB
			$envelope->maxSizeMB = 2;
			// Quantidade máxima
			$envelope->maxAmount = 1;
			// Altera nome do arquivo original ?
			$envelope->updateFileName = false;
			// Extensoes permitidas (argumento para preg_match)
			$envelope->allowedExtensions = "jp?g";
			// Url dos arquivos, para retorno das imagens após upload efetudo
			$envelope->urlFiles = SYS_APP_URL.$this->uploadDestination.$envelope->ID."/";
			
			// Recepciona o(s) arquivo(s) e imprimi o resultado
			Controller::uploadFile($envelope);
		// qdUploadID não reconhecido 
		}else {
			Controller::azReturn('qdUploadID não reconhecido.', 'fail', true);	
		}
	}
	
	/**
	 * Baixar de formar dinamica um determinado arquivo
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"baixarArquivo", fileName, qdUploadID, ID}
	 * @return string json com a URL para baixar o arquivo ou mensagem de falha
	 */
	public function downloadFile($envelope) {
		$fileName = Controller::getValue($envelope->fields,'fileName');
		$qdUploadID = Controller::getValue($envelope->fields,'qdUploadID');
		$src	 = $this->uploadDestination.$envelope->ID."/".$fileName ;	
		$srcFile = realpath(SYS_SRC_APP.$src); // Caminho real do arquivo
		$urlFile = SYS_APP_URL.$src; // Endereço web do arquivo
		
		// Verifica se é um arquivo válido 
		if(is_file($srcFile)){
			$tipo = 'win';
			$msg = '';
			// Sessões utilizadas no arquivo php/downloader.php
			$_SESSION['urlFileToDownload'] = $urlFile;
			$_SESSION['srcFileToDownload'] = $srcFile;
			
		// Arquivo inválido
		}else {
			$tipo = 'fail';
			$msg = 'Arquivo não encontrado.';
			// Resetas as sessões de download
			$_SESSION['urlFileToDownload'] = null;
			$_SESSION['srcFileToDownload'] = null;
			
			
		}
		
		// De qualquer forma, retorna a URL  e o caminho fonte do arquivo
		$objRet = array('url'=>$urlFile,'src'=>$srcFile);

		// Imprimi o retorno no padrão JSON 
		Controller::azReturn($msg, $tipo, true, $objRet);
	}
	/**
	 * Deleta um arquivo do servidor (deletar também o thumbnail se houver)
	 * @param  stdClass envelope, dados obrigatorios :  {acao:"deletarArquivo", fileName, qdUploadID, ID}
	 * @return string json com mensagem de sucesso ou falha
	 */
	public function deleteFile($envelope) {
		$obj = new stdClass();
		$fileName = Controller::getValue($envelope->fields,'fileName');
		$qdUploadID = Controller::getValue($envelope->fields,'qdUploadID');
		// Deleta imagem da categoria principal
		if($qdUploadID==$this->qdUploadID){
			$obj = new stdClass();
			$obj->view = $_POST['view'];
			$obj->fileName = $fileName;
			$obj->folder = $this->uploadDestination.$envelope->ID."/";
			$obj->webService = true;
		}
		
		// Executa a ação pela classe Controller
		Controller::delete_file($obj);	
	}
	
	
}
