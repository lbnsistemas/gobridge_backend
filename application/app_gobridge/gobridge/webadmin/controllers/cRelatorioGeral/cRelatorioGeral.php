<?php
// Incluí classe para criar e manipular planilhas Excel
include  SYS_SRC_LIBS.'PHPExcel.php';

/**  
 * Controller : cRelatorioGeral
 * Exibição do relatório principal do sistema de chamadas
 *
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, LBN Sistemas
 * @version 1.2.0
 */
class cRelatorioGeral extends Controller {
	private $tabela = 'v_relatorio_geral';
	private $view;
	/**
	 * Construtor
	 * 
	 */
	public function __construct() 
	{
		$this->view = new View();
	}
	/**
	 * Traz os cadastros para serem visualizados e editados(com a devida permissao) na view
	 * @param  stdClass envelope, dados possíveis : inicio, limite, orderBy, filtros[{}]
	 * @return string json mensagem sucesso  ou falha
	 */
	public function visualizar($envelope) {
		$obj = new stdClass();
		$obj->inicio = $envelope->inicio;
		$obj->limite = $envelope->limite;
		$obj->orderBy = $envelope->orderBy;
		$obj->filtros = $envelope->filtros;
		$obj->view = $_POST['view'];
		$obj->webService = true;
		$obj->tabela = $this->tabela;

		// Une novas colunas com as solicitadas pela View
		$envelope->colunas = array_merge($envelope->colunas,array('V_RETURN'));
		$obj->colunas = $envelope->colunas;
		
		// Executa a ação pela classe Controller 
		Controller::toView($obj);	
	}
	/**
	 * Carrega o list de agentes do filtro do relatorio geral
	 * @return string json mensagem sucesso  ou falha
	 */
	public function loadListAgents() {
		$obj = new stdClass();
		$obj->orderBy = 'USE_USERNAME';
		$obj->complSQL = "WHERE SA_ARQUIVED=FALSE";
		$obj->view = $_POST['view'];
		$obj->webService = true;
		$obj->tabela ='sa_users';
		$obj->colunas = array('USE_USERNAME');

		// Executa a ação pela classe Controller 
		Controller::toView($obj);	
	}
	/**
	 * Exportar dados filtrados e ordenados em formato excel 
	 * disponilizando o arquivo temporario para download pelo downloader.php
	 *
	 * @param  stdClass envelope, dados possíveis :  {acao:"exportarParaExcel", inicio, limite, orderBy, filtros[{}]}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function exportarParaExcel($envelope) {
		$obj = new stdClass();
		$obj->orderBy = $envelope->orderBy;
		$obj->filtros = $envelope->filtros;
		$obj->view = $_POST['view'];
		$obj->webService = false;
		$obj->tabela = $this->tabela;
			
		// Une novas colunas com as solicitadas pela View
		$obj->colunas = $envelope->colunas;
		$obj->colunas = array_merge($obj->colunas,array('V_POSITION','V_ORIGINALPOSITION','V_HOLDTIME'));
		$colunasCpy = $obj->colunas; 
		// Executa a ação pela classe Controller
		$pacoteRet = Controller::toView($obj)->dados;
			
			
		// Se encontrou contatos, cria a planilha e salva na pasta temporária
		if(count($pacoteRet->ret)>0 && $pacoteRet->ret!==false) {
			// Instanciamos a classe
			$objPHPExcel = new PHPExcel();
			
			
			// Definição do cabeçalho do arquivo
			$alfabeto = array('A','B','C','D','E','F','G','H','I','J','K','L','M');
			for($i=0; $i<count($colunasCpy);$i++)  {
				
				// Definimos o estilo da fonte
				$objPHPExcel->getActiveSheet()->getStyle($alfabeto[$i].'1')->getFont()->setBold(true);
				
				// Define a coluna
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($alfabeto[$i].'1', $colunasCpy[$i]);
			}
			// Podemos configurar diferentes larguras paras as colunas como padrão
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);// Fila
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);// V_DHCALL
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);// V_DHABANDON
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8); // V_TIME
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);// V_CONTACT
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);// V_STATUS
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);// V_DHRETURN
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);// V_AGENT
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);// V_RETURN
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);// V_POSITION<strong></strong>
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);// V_ORIGINALPOSITION
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);// V_HOLDTIME
			
			// Adiciona as linhas encontradas, respeitando as colunas já criadas
			for($l=0; $l<count($pacoteRet->ret); $l++) {
				// stdCLass to Array
				$linha = (array)$pacoteRet->ret[$l];
				
				// Lê todas as colunas criadas para o select do BD
				for($c=0; $c<count($colunasCpy); $c++) {
					// Pega valor da coluna (eliminando os caracteres html)
					$colValue =htmlspecialchars_decode(strip_tags($linha[$colunasCpy[$c]]));
					// Também podemos escolher a posição exata aonde o dado será inserido (coluna, linha, dado);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($c, $l+2, $colValue);
					
				}
			}
			// Podemos renomear o nome das planilha atual, lembrando que um único arquivo pode ter várias planilhas
			$objPHPExcel->getActiveSheet()->setTitle('Relatório Geral - Callsystem');
		
			// Acessamos o 'Writer' para poder salvar o arquivo
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			
			// Cria um nome aleatório iniciado com a data de hoje, o sistema irá apagar todos arquivos temporários do dia anterior
			$nomeArquivo = date('Y-m-d_')."_relatorio_geral_chamadas_".rand().".xls";
			
			// Salva o arquivo na pasta de arquivos temporarios : 'downlaods/temp'
			// Se quiser jogar na tela : 'php://output'
			$objWriter->save(SYS_SRC_APP.'downloads/temp/'.$nomeArquivo); 
			
			// Cria as sessões para download do arquivo 
			$_SESSION['urlFileToDownload'] = SYS_SRC_APP."downloads/temp/".$nomeArquivo;
			$_SESSION['srcFileToDownload'] = SYS_SRC_APP."downloads/temp/".$nomeArquivo;
			
			// Variáveis para o retorno
			$tipo = 'sucesso';	
			$msg =  '';	
		// Nenhum contato encontrado
		}else {
			$tipo = 'falha';
			$msg = 'Nenhum contato foi encontrado com os filtros informados.';
		}	
		
		// Registra ação no BD do sistema
		Controller::registerAction();
		
		// Imprimi o retorno no padrão JSON 
		Controller::retorno($msg, $tipo, true);
		
	}
	
}
