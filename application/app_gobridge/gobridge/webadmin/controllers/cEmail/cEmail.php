<?php
// Incluí model especifica
//require_once(SYS_SRC_MODELS."/mEmail/mEmail.php");

/**
 * Controller : cEmail
 * Controle, preparo e envio de emails
 *
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, LBN Sistemas
 * @version 1.1.0
 */
class cEmail extends Controller {
	private $model;
	private $httpResponse;
	private $mysqli;
	
	/**
	 * Construtor
	 *
	 */
	public function __construct($httpResponse=false)
	{
		//Escreve a responde para retorno via HTTP ou retorna uma string para ser tratada
		$this->httpResponse = $httpResponse;

		// Intância do mysqli
		$this->mysqli = Controller::getMysqli();
	}
	/**
	 * Envia um email diretamente ao destinatario informado, sem utilização de modelos
	 * @param $dest_email: email do destinatario a receber a mensagem
	 * @param $dest_nome: nome do destinatario a receber a mensagem
	 * @param $assunto: assunto do email
	 * @param $mensagem: mensagem HTML do corpo do email
	 */
	public function envioDireto($dest_email ,$dest_nome,  $assunto, $mensagem) {
		// Inicia a classe PHPMailer
		$mail = new PHPMailer();
		// Define os dados do servidor e tipo de conexão
		// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		$mail->IsSMTP(); // Define que a mensagem será SMTP
		$mail->Host = "mail.".SYS_HOST; // Endereço do servidor SMTP
		//$mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
		//$mail->Username = 'seumail@dominio.net'; // Usuário do servidor SMTP
		//$mail->Password = 'senha'; // Senha do servidor SMTP
		// Define o remetente
		// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		$mail->From = "suporte@".SYS_HOST; // Seu e-mail
		$mail->FromName = EMP_FANTASIA; // Seu nome
		// Define os destinatário(s)
		// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		$mail->AddAddress($dest_email, $dest_nome);
		//$mail->AddAddress('suporte@lbnsistemas.com.br');
		//$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
		//$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta
		// Define os dados técnicos da Mensagem
		// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
		$mail->CharSet = 'utf-8'; // Charset da mensagem (opcional)
		// Define a mensagem (Texto e Assunto)
		// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		$mail->Subject  = $assunto; // Assunto da mensagem
		$mail->Body = $mensagem;
		$mail->AltBody = strip_tags(preg_replace('/\<br(\s*)?\/?\>/i', "\n", $mensagem));
		// Define os anexos (opcional)
		/* =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		if(is_file($anexo)) {
			$mail->AddAttachment($anexo,  basename($anexo));  // Insere um anexo
		}*/
		// Envia o e-mail
		$enviado = $mail->Send();
		// Limpa os destinatários e os anexos
		$mail->ClearAllRecipients();
		$mail->ClearAttachments();
		// Exibe uma mensagem de resultado
		if ($enviado) {
			$ret_tipo = 'win';
			$ret_msg =  "E-mail enviado com sucesso!";
		} else {
			$ret_tipo = 'fail';
			$ret_msg =  "Não foi possível enviar o e-mail. Erro:".$mail->ErrorInfo;
		}
		return(Controller::azReturn($ret_msg, $ret_tipo, $this->httpResponse));
	}
	/**
	 * Envia um email informativo ao cliente
	 * Situação em que o cliente deve/pode receber um email:
	 * Boas vindas, Newsletter, Pedido aprovado,Pedido enviado, Pedido em produção, Pedido devolvido, Compra finalizada
	 *
	 * @param string $finalidade: {'Boas vindas','Pedido aprovado','Pedido em produção','Pedido enviado', 'Pedido devolvido','Compra finalizada'}
	 * @param int $vndID: ID da venda/pedido,
	 * @param int $cliID: ID do cliente
	 * @param int $parID: ID do parceiro
	 * @param int $vocID: ID da ocorrencia de venda (vendas_ocorrencias)
	 * @return stdClass {status['win','fail'],email}
	 */
	public function enviaEmail($finalidade, $conversionData,  $anexo=false) {
		//Busca o primeiro modelo ativo referente a finalidade informada
		$modelo = mMain::email_model($finalidade);
		if($modelo->ret!=false) {
			$modelo = $modelo->ret[0];
			// Verifica se existe um token/codigo de envio, se não tiver, cria um
			if($conversionData['codigo']=='') {
				$conversionData = array_merge($conversionData, array("codigo"=>Controller::generateCode(10,true)));
			}
			$modeloCustomizado = $this->preparaEmailHTML($modelo, $finalidade,$conversionData);
			if($modeloCustomizado->type=='win') {
				$modelo = $modeloCustomizado->data;
				// Inicia a classe PHPMailer
				$mail = new PHPMailer();
				// Define os dados do servidor e tipo de conexão
				// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				$mail->IsSMTP(); // Define que a mensagem será SMTP
				$mail->Host = "mail.".SYS_HOST; // Endereço do servidor SMTP
				//$mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
				//$mail->Username = 'seumail@dominio.net'; // Usuário do servidor SMTP
				//$mail->Password = 'senha'; // Senha do servidor SMTP
				// Define o remetente
				// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				$mail->From = "suporte@".SYS_HOST; // Seu e-mail
				$mail->FromName = EMP_FANTASIA; // Seu nome
				// Define os destinatário(s)
				// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				$mail->AddAddress($modelo->email, $modelo->nome);
				//$mail->AddAddress('suporte@lbnsistemas.com.br');
				//$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
				//$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta
				// Define os dados técnicos da Mensagem
				// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
				$mail->CharSet = 'utf-8'; // Charset da mensagem (opcional)
				// Define a mensagem (Texto e Assunto)
				// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				$mail->Subject  = $modelo->MOE_TITULO; // Assunto da mensagem
				$mail->Body = $modelo->MOE_HTML;
				$mail->AltBody = strip_tags(preg_replace('/\<br(\s*)?\/?\>/i', "\n", $modelo->MOE_HTML));
				// Define os anexos (opcional)
				// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				if(is_file($anexo)) {
					$mail->AddAttachment($anexo,  basename($anexo));  // Insere um anexo
				}
				// Envia o e-mail
				$enviado = $mail->Send();
				// Limpa os destinatários e os anexos
				$mail->ClearAllRecipients();
				$mail->ClearAttachments();
				// Exibe uma mensagem de resultado
				if ($enviado) {
					// Registra o envio do email em az_email_sending
					$sql = "INSERT INTO az_email_sending SET AZS_MOE_ID=".$modelo->MOE_ID.", 
					AZS_CONVERSIONDATA='".json_encode($conversionData)."', AZS_EMAIL='".$modelo->email."',
					AZS_CODE='".$conversionData['codigo']."', AZS_USE_ID='".$_SESSION[SES_USER_ID]."',
					AZS_IP_ORIGIN='".Controller::IP()."', AZS_DATETIME=NOW()";
					$this->mysqli->query($sql);
					
					// Prepara retorno informando sucesso no envio
					$ret_tipo = 'win';
				  	$ret_msg =  "E-mail enviado com sucesso!";
				} else {
					$ret_tipo = 'fail';
					$ret_msg =  "Não foi possível enviar o e-mail. Erro:".$mail->ErrorInfo;
				}
			}else {
				$ret_tipo = 'fail';
				$ret_msg = $modeloCustomizado->message;
			}
		//ERRO: Modelo não encontrado
		}else {
			$ret_tipo = 'fail';
			$ret_msg = 'Modelo de email não encontrado para a finalidade: '.$finalidade.'.';
		}
		return(Controller::azReturn($ret_msg, $ret_tipo, $this->httpResponse));
	}
	/**
	 * Prepara o modelo do email a ser enviado, substituindo as palavras mágicas "{{}}" pelos seus valores correspondentes
	 *
	 * @return string json mensagem tipo win/fail
	 */
	private function preparaEmailHTML($modelo, $finalidade, $conversionData=''){
		
		// Verifica se foi informado um array de conversão
		if(is_array($conversionData)) {
			foreach($conversionData as $key=>$val) {
				// Reconhecimento dos nomes (cliente e do arquiteto)
				if($key=='nome' || $key=='nome_rem' || $key=='nome_dest') {
					if(strpos($val, " ")!==false) {
						$val = substr($val,0,strpos($val," "));
					}
					$val = ucfirst($val);
					if($key=='nome' || $key=='nome_dest' ) 
						$modelo['nome'] = $val;
				}
				
				// Reconhecimento do email
				if($key=='email' || $key=='email_dest') {
					$modelo['email'] = $val;
				}
				
				// Reconhecimento do assunto (se não for informado, usa o que esta definido em az_email_models (MOE_TITULO))
				if($key=='assunto'){
					$modelo['MOE_TITULO'] = $val;
				}
				
				$modelo['MOE_HTML'] = str_replace("{{".$key."}}",$val,$modelo['MOE_HTML']);	
			}
		}
		// Verifica se o email foi decadastrado pelo usuário (para está finalidade)
		$complSQL = "WHERE AEB_EMAIL='".$modelo['email']."' AND (AEB_SUBJECTS LIKE '%,".$modelo['MOE_FINALIDADE']."%' || AEB_SUBJECTS LIKE '%*%') ";
		Database::connect();
		$r = Database::select('az_email_blocked', $complSQL);
		if(count($r)>0) {
			$ret_tipo = 'info';
			$ret_msg = 'O email "'.$modelo['email'].'" foi decadastrado para a finalidade "'.$modelo['MOE_FINALIDADE'].'"';
		}else {
			$ret_tipo = 'win';
			$ret_msg = '';
		}
		Database::disconnect();
		return(Controller::azReturn($ret_msg, $ret_tipo, $this->httpResponse,$modelo));
	}
	

}
