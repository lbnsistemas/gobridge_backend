<?php
//Requisita controller Principal 
require(SYS_SRC_CONTROLLERS.'/cMain.php');
/**  
 * Controller : cWebServices
 * Controle, preparo e envio de emails
 *
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, LBN Sistemas
 * @version 1.1.0
 */
class cWebServices extends Controller {
	private $model;  //Model
	private $cMain;  //Objeto cMain (Controller Principal)
	private $cEmail; //Objeto cEmail
	private $mysqli; //Objeto Mysqli
	private $dataTempo; //Objeto DataTempo
	private $httpResponse;
	public $serviceName;
	private $arPublicList = array('documentRequest','documentUpload','documentURL');
	/**
	 * Construtor
	 * 
	 */
	public function __construct($httpResponse=true) {
		//Pega o nome do serviço
		$pURL = parse_url($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		preg_match("/.*\/ws\/([a-zA-Z\-]+)\/?/",$pURL['path'],$matches);
		$this->serviceName = $matches[1];
		
		//Alguns serviços só são permitidos para usuário logados no sistema de retaguarda do ecommerce
		//Para isso consultamos se o serviço está na lista de serviços publicos 'arPublicList'
		if(in_array($this->serviceName,$this->arPublicList)==false) {
			if($_SESSION['USER_ID']=='' || $_SESSION['HORA_LOGIN']=='')
				die('Acesso não autorizado.');
		}
		
		//Escreve a responde para retorno via HTTP ou retorna uma string para ser tratada
		$this->httpResponse = $httpResponse;
		
		//Inicia cMain
		$this->cMain = new cMain($httpResponse);
		
		//Inicia cEmail
		$this->cEmail = new cEmail($httpResponse);
		
		// Intância de mysqli 
		$this->mysqli = Controller::getMysqli();
		
		// Inicia DataTempo
		$this->dataTempo = new DataTempo();
	}
	/**
	 * @name documentUpload
	 * @fields ENV_ID, files
	*/
	public function documentUpload(){
		$envelope = new StdClass();
		// Busca os dados da demanda e valida a hash recebida
		$sql = "SELECT *, envios.Status StatusEnv FROM envios
		INNER JOIN demandas ON demandas.DEM_ID=envios.DEM_ID
		WHERE  ENV_ID='".$_GET['ENV_ID']."'";
		$r = $this->mysqli->query($sql);
		if($r->num_rows==0) {
			// Retorno do WS
			return(Controller::azReturn('Demanda expirada ou link inválido.', 'fail',true,array()));	
		}
		$demanda = $r->fetch_assoc();
		// Pasta destino para armazenamento do(s) arquivo(s) do upload
		$envelope->destination = SYS_SRC_APP.'etc/uploads/escritorios/solicitacoes/'.$demanda['DEM_ID']."/";
		// Array com o(s) arquivo(s) do upload
		$envelope->files = $_FILES['files'];
		// Se true : cria uma miniatura da imagem na mesma pasta de upload para rapida exibição na view
		$envelope->thumbnail = false;
		// Tamanho máximo em MB
		$envelope->maxSizeMB = 3;
		// Quantidade máxima
		$envelope->maxAmount = 15;
		// Altera nome do arquivo original ?
		$envelope->updateFileName = true;
		// Funcionalidade para retorno de webservice (json)
		$envelope->webService = false;
		// Extensoes permitidas (argumento para preg_match)
		$envelope->allowedExtensions = "jp?g|pdf|cad|png|pdf|doc|docx|xls|xlsx|xlsm|rar|zip|gzip";
		// Url dos arquivos, para retorno das imagens após upload efetudo
		$envelope->urlFiles = SYS_APP_URL.'etc/uploads/escritorios/solicitacoes/'.$demanda['DEM_ID']."/";
		
		// Recepciona o(s) arquivo(s) e imprimi o resultado
		$objRetUpl = Controller::uploadFile($envelope);
		
		// Verifica se ocorreu tudo bem com o upload
		if($objRetUpl->type=='win') {
			$qtdSolicitacao = $demanda['Qtd_requisicao']; 
			$qtdEnviada = $demanda['Qtd_resposta']+1;
			// Atualiza a qtd de documentos recebidos da demanda
			// Se a nova quantidade for igual ou maior que a quantidade solicitada, atualiza o status 
			if($qtdSolicitacao<=$qtdEnviada) {
				$complSQL = ', Status="ATENDIDA"';	
			}else {
				$complSQL = '';		
			}
			if($demanda['StatusEnv']=='ABERTO') {
				$sql = "UPDATE demandas SET Qtd_resposta=".$qtdEnviada.$complSQL." WHERE DEM_ID=".$demanda['DEM_ID'];
				$this->mysqli->query($sql);
			}
			// Informa o link e altera o status do envio 
			$sql = "UPDATE envios SET Status='RECEBIDO', Link_Download='".$objRetUpl->data->files[0]->url."' WHERE ENV_ID=".$_GET['ENV_ID'];
			$this->mysqli->query($sql);
		}
		
		
		// Prossegue o retorno do webservice
		echo(json_encode($objRetUpl));	
	}	
	/**
	 * @name documentURL
	 * @fields ENV_ID, url
	*/
	public function documentURL(){
		$envelope = new StdClass();
		// Busca os dados da demanda e valida a hash recebida
		$sql = "SELECT *, envios.Status StatusEnv FROM envios
		INNER JOIN demandas ON demandas.DEM_ID=envios.DEM_ID
		WHERE ENV_ID='".$_GET['ENV_ID']."'";
		$r = $this->mysqli->query($sql);
		if($r->num_rows==0) {
			// Retorno do WS
			return(Controller::azReturn('Demanda expirada ou link inválido.', 'fail',true,array()));	
		}
		$demanda = $r->fetch_assoc();
		$qtdSolicitacao = $demanda['Qtd_requisicao']; 
		$qtdEnviada = $demanda['Qtd_resposta']+1;
		// Atualiza a qtd de documentos recebidos da demanda
		// Se a nova quantidade for igual ou maior que a quantidade solicitada, atualiza o status 
		if($qtdSolicitacao<=$qtdEnviada) {
			$complSQL = ', Status="ATENDIDA"';	
		}
		if($demanda['StatusEnv']=='ABERTO') {
			$sql = "UPDATE demandas SET Qtd_resposta=".$qtdEnviada.$complSQL." WHERE DEM_ID=".$demanda['DEM_ID'];
			$this->mysqli->query($sql);
		}
		// Informa o link e altera o status do envio 
		$sql = "UPDATE envios SET Status='RECEBIDO', Link_Download='".$_GET['URL']."' WHERE ENV_ID=".$_GET['ENV_ID'];
		$this->mysqli->query($sql);
		
		// Retorno sucesso
		return(Controller::azReturn('URL informado com sucesso.', 'win',true,array()));	
		
	}
	/**
	 * @name documentRequest
	 * @fields HashIID
	*/
	public function documentRequest(){
		$objRet = array();
		
		// SQL string para trazer os dados da demanda
		$sql = "SELECT * FROM demandas 
		WHERE HashID = '".$_GET['HashID']."'";
		//die($sql);
		$ret = $this->mysqli->query($sql);
		$totalEncontrado = $ret->num_rows;
				
		// Verifica se processou com sucesso
		if($totalEncontrado>0) {
			$objRet['demanda'] = array();
			while($row = $ret->fetch_assoc()) {
				// Campos da demanda
				$objRet['demanda'] = $row;
				
				// Busca os dados do cliente
				$sql = "SELECT CLI_ID, Nome_Razao FROM clientes WHERE CLI_ID=".$row['CLI_ID'];
				$retc = $this->mysqli->query($sql);
				$rowc = $retc->fetch_assoc();
				$objRet['demanda']['cliente'] = $rowc;
				
				// Busca os dados do escritorio
				$sql = "SELECT ESC_ID, Nome_Razao, CPF_CNPJ, Email, Logo 
				FROM escritorios WHERE ESC_ID=".$row['ESC_ID'];
				$rete = $this->mysqli->query($sql);
				$rowe = $rete->fetch_assoc();
				$objRet['demanda']['escritorio'] = $rowe;
				
				// Logo do escritorio
				$srcFiles = "../etc/uploads/escritorios/logos/".$rowe['ESC_ID']."/";
				$urlFiles = SYS_APP_URL.$srcFiles;
				// Verifica se existe o thumbnail da capa da obra
				$tb = Controller::hasThumbnail($rowe['Logo'],$srcFiles);	
				$objRet['demanda']['escritorio']['thumbnail'] = $tb['thumbnail'];
				if(empty($tb['thumbnail'])==true)
					$objRet['demanda']['escritorio']['thumbnail'] = $urlFiles.$rowe['Logo'];
					
				
				// Busca os documentos solicitados
				$sql = "SELECT 
				ENV_ID, envios.Status Status, Data_Hora_Envio,  Link_Download, documentos.DOC_ID, 
				documentos.Titulo Titulo, documentos.Descricao, documentos.Imagem
				FROM envios 
				INNER JOIN documentos ON documentos.DOC_ID = envios.DOC_ID
				LEFT JOIN tipos_documentos ON tipos_documentos.TDO_ID = documentos.TDO_ID
				WHERE DEM_ID=".$row['DEM_ID'];
				
				$rete = $this->mysqli->query($sql);
				$rowen = [];
				while($re = $rete->fetch_assoc()){
					$rowen[] = $re;	
				}
				$objRet['demanda']['documentos'] = $rowen;				
			}
			$message='';
			$type='win';
		}else {
			$message='Link inválido. Contate o escritório e solicite um novo link.';
			$type='fail';
		}
		// Retorno do WS
		return(Controller::azReturn($message, $type,true,$objRet));
	}
	/** 
	 * @description: Decadastrar-se dos emails BrickBe
	 * @method: GET
	 * @fields: {codigo,titulo,cliID}
	 * @return: stdClass Azul Return Message {type, message, data}
	**/
	public function decadastrarEmail() {
		//ERRO: postFields inválidos
		if($_GET['email']=='' || $_GET['codigo']=='') {
			$ret_tipo='fail';
			$ret_msg = 'Parâmetros incorretos.';
		//Prosse com o processamento da requisição
		}else{
			$arSQL = array();
			// Verifica se o código é condizente com algum envio feito e se o email a ser decadastrado é o mesmo
			$sql = "SELECT * FROM az_email_sending WHERE AZS_EMAIL='".$_GET['email']."' AND AZS_ACTION='' 
			AND AZS_CODE='".$_GET['codigo']."'";
			$r = $this->mysqli->query($sql);
			if($r->num_rows>0) {
				
				// SQL para incluir o email na lista de bloqueio, para não receber mais nenhum email
				$sql = "INSERT INTO az_email_blocked SET AEB_SUBJECTS='*', AEB_EMAIL='".$_GET['email']."', 
				AZ_DHREGISTER=NOW(), AZ_USREGISTER='*site'";
				$arSQL[] = $sql;
				
				// SQL para registrar a calltoaction (chamada de ação do usuário com o email recebido)
				$sql = "UPDATE az_email_sending SET AZS_ACTION='".$this->serviceName."',
				AZS_IP_CALLTOACTION='".Controller::IP()."', AZS_DHCALLTOACTION=NOW(), AZS_REASON='...'
				WHERE AZS_CODE='".$_GET['codigo']."' AND AZS_EMAIL='".$_GET['email']."'"; 
				$arSQL[] = $sql;
				
				// Tenta executar a transaction 
				$ret = mMain::mTransaction($arSQL);
				if($ret->ret==true) {
					$ret_tipo = 'win';
					$ret_msg = 'Email descadastrado com sucesso.';
				}else {
					$ret_tipo = 'fail';
					$ret_msg = "Erro ao executar as instruções. ".(DEBUG_MODE==true ? $ret->errorSQL." - SQL: ".$ret->stringSQL:'');
				}	
			}else{
				$ret_tipo = 'fail';
				$ret_msg = 'Falha para registrar o descadastramento, solicite via email: suporte@brickbe.com.br';
			}
		}
		return(Controller::azReturn($ret_msg, $ret_tipo, $this->httpResponse));
	}
	/** 
	 * @description: Somente registra as calltoactions vindas dos emails
	 * @method: GET
	 * @fields: {codigo,titulo,cliID}
	 * @return: stdClass Azul Return Message {type, message, data}
	**/
	public function registrar_callToAction($reason='') {
		//ERRO: postFields inválidos
		if($_GET['email']=='' || $_GET['codigo']=='') {
			$ret_tipo='fail';
			$ret_msg = 'Parâmetros incorretos.';
		//Prosse com o processamento da requisição
		}else{
			$arSQL = array();
			// Verifica se o código é condizente com algum envio feito e se o email a ser decadastrado é o mesmo
			$sql = "SELECT * FROM az_email_sending WHERE AZS_EMAIL='".$_GET['email']."' AND AZS_ACTION='' 
			AND AZS_CODE='".$_GET['codigo']."'";
			$r = $this->mysqli->query($sql);
			if($r->num_rows>0) {
				// SQL para registrar a calltoaction (chamada de ação do usuário com o email recebido)
				$sql = "UPDATE az_email_sending SET AZS_ACTION='".$this->serviceName."',
				AZS_IP_CALLTOACTION='".Controller::IP()."', AZS_DHCALLTOACTION=NOW(), AZS_REASON='".$reason."'
				WHERE AZS_CODE='".$_GET['codigo']."' AND AZS_EMAIL='".$_GET['email']."'"; 
				$arSQL[] = $sql;
				// Tenta executar a transaction 
				$ret = mMain::mTransaction($arSQL);
				if($ret->ret==true) {
					$ret_tipo = 'win';
					$ret_msg = 'CallToAction registrado com sucesso!';
				}else {
					$ret_tipo = 'fail';
					$ret_msg = "Erro ao executar as instruções. Contate o suporte através do  email: suporte@brickbe.com.br ".(DEBUG_MODE==true ? $ret->errorSQL." - SQL: ".$ret->stringSQL:'');
				}	
			}else{
				$ret_tipo = 'info';
				$ret_msg = 'Ação já registrada anteriorment.';
			}
		}
		return(Controller::azReturn($ret_msg, $ret_tipo, $this->httpResponse));
	}
	/**
	 * Consulta o status da transação na Cielo de todas as vendas que sejam de cartão e estejam Aguardando pgto
	 *
	 * @postFields {}
	 * @return string 
	 */
	public function atualizarPagamentosCielo() {
		$aprovados = [];
		$naoAutorizados = [];
		$erros =[];
		$processando = [];
		$totalPedidos = [];
		//Seleciona todas as vendas no cartão e no status 'Aguardando pagamento'
		$sql = "SELECT * FROM vendas WHERE VND_STATUS='Aguardando pagamento' 
		AND (VND_FRM_ID=".CARTAO_CREDITO." OR VND_FRM_ID=".CARTAO_DEBITO." )";
		$r = $this->mysqli->query($sql);
		$totalPedidos = $r->num_rows;
		while($venda = $r->fetch_assoc()) {
			$ret = $this->cMain->getDadosTransacaoCielo('Pedido'.$venda['VND_ID']);
			//Verifica se recuperou os dados da transação:
			if($ret->status=='win') {
				$objTrans = json_decode($ret->message);
				//Verifica o status do pagamento: 2-pago, 7-autorizado
				if($objTrans->payment_status==2 || $objTrans->payment_status==7){
					/* Somente se o status da venda estiver como "Aguardando pagamento":
					 *
					 */
					if($venda['VND_STATUS']=='Aguardando pagamento') {
						//Monta a string para o detalhe do pagamento (mascara do cartão + tid)
						$detalheTransacao = 'Cartão: '.$objTrans->payment_maskedcreditcard.' - TID: '.$objTrans->tid;
						
						//Registra ocorrência: Pagamento confirmado
						$vocID = mMain::reserveID('vendas_ocorrencias','VOC_ID')->ID;
						$sql = "INSERT INTO `vendas_ocorrencias` SET VOC_ID=".$vocID.", VOC_TITULO='Pagamento confirmado',
						 VOC_DETALHE='".$detalheTransacao."',VOC_DATAHORA=NOW(),VOC_USUARIO='sistema', VOC_VND_ID=".$vnd_id;
						$arSQLs[]=$sql;
							
						//Altera o status do pedido: Aprovado
						$sql = "UPDATE  `vendas` SET VND_STATUS='Aprovado',VND_NAOAUTORIZADO=FALSE WHERE VND_ID=".$vnd_id;
						$arSQLs[]=$sql;
				
						//Busca todas as parcelas relacionadas a este pedido de venda
						$sql = "SELECT * FROM financeiro_fluxodecaixa 
						INNER JOIN financeiro_parcelas ON PAR_FLUX_ID = FLUX_ID
						INNER JOIN vendas ON VND_ID = FLUX_VND_ID
						WHERE VND_ID=".$venda['VND_ID'];
						$rpar = $this->mysqli->query($sql);
						
						//Quita parcela por parcela da venda
						while($parcela = $rpar->fetch_assoc()) {
							//Reserva PK para a quitação
							$quiID = mMain::reserveID('financeiro_quitacao','QUI_ID')->ID;
							
							//Cadastra os dados do pagamento da parcela com cartão
							$sql = "INSERT financeiro_quitacao SET 
							QUI_ID = ".$quiID.",QUI_DATAHORA=NOW(), QUI_OBS='Pagamento via checkout cielo. ".$detalheTransacao."',
							QUI_VALORTOTAL='".$parcela['PAR_VALOR']."', QUI_FRMQUI='Cartão ".
							($objTrans->payment_status==7 ? 'de crédito' : 'de débito')."', QUI_USUARIO='sistema'";
							$arSQLs[]=$sql;
							
							//Cadastra o movimento
							$dtPrevista =  $this->dataTempo->addDayIntoDate(date('Y-m-d'),($objTrans->payment_status==7 ? 30 : 2));
							$sql = "INSERT INTO financeiro_movimentos SET  MOV_BAN_ID='".ECOM_BANCO_CARTOES."',MOV_FRMQUI='Cartão ".($objTrans->payment_status==7 ? 'Crédito' : 'Débito')."',MOV_DATAHORA=NOW(),MOV_USUARIO='sistema',MOV_ENTSAI='E', MOV_PARC='".$parcela['PAR_PARCELA']."', MOV_VALOR='".$valorParcela."', MOV_DATA='".date('Y-m-d')."',MOV_DATAPREV='".$dtPrevista."',MOV_FLUX_ID='".$parcela['FLUX_ID']."',MOV_PAR_ID='".$parcela['PAR_ID']."',MOV_CONCILIAR=TRUE";		
							$arSQLs[]=$sql;
							
							//Altera a parcela informando o ID de quitação
							$sql = "UPDATE  `financeiro_parcelas` SET PAR_QUI_ID='".$quiID."' WHERE PAR_ID=".$parcela['PAR_ID'];
							$arSQLs[]=$sql;
						}
						$ret = mMain::mTransaction($arSQLs);
						if($ret->ret==true) {
							$aprovados[] = $venda['VND_ID'];
							//Verifica se já foi enviado o primeiro email (do pedido) para o cliente
							if($venda['VND_EMAILENVIADO']) {
								//Seleciona o modelo de email 'Nova ocorrência'
								$finalidadeEmail = 'Nova ocorrência';
							}else {
								//Seleciona o modelo de email 'Pedido aprovado'
								$finalidadeEmail = 'Pedido aprovado';
							}
							//Envia o email para o cliente
							$retEmail = $con->cEmail->enviaEmailParaCliente($finalidadeEmail, $venda['VND_CLI_ID'], $venda['VND_ID'],$vocID);
						}else {
							$erros[] = $venda['VND_ID'];
						}
					}
				
				//Pagamento em processamento
				}else if($objTrans->payment_status==1) {
					$processando[] = $venda['VND_ID'];
					
				//FALHA: Pagamento não autorizado 
				}else {
					//Altera o status do pedido: Aprovado
					$sql = "UPDATE  `vendas` SET VND_STATUS='Aguardando pagamento',VND_NAOAUTORIZADO=TRUE WHERE VND_ID=".$vndID;
					$ret = $this->mysqli->query($sql);
					$naoAutorizados[] = $venda['VND_ID'];
					
				}	
				
			//ERRO: dados da transação indisponíveis
			}else {
				$erros[] = $venda['VND_ID'];
			}
		}
		$objRet = array('aprovados'=>$aprovados,'processando'=>$processando,'naoAutorizados'=>$naoAutorizados,'erros'=>$erros,
		'totalPedidos'=>$totalPedidos);
		return(Controller::azReturn('', 'win', $this->httpResponse,$objRet));
	}
	/**
	 * Busca o JSON de checkout cielo (salvo na tabela vendas) e faz uma requisição de URL de pagamento para o WS da cielo
	 *
	 * @postFields {vndID}
	 * @return string 
	 */
	public function urlCielo() {
		$objRet = array();
		//ERRO: postFields inválidos
		if($_POST['vndID']==''  ) {
			$ret_tipo='fail';
			$ret_msg = 'Parâmetros incorretos.';
			
		//Prosse com o processamento da requisição
		}else{
			//Busca os dados da venda
			$venda = mMain::venda($_POST['vndID']);
			if($venda->ret!=false) {
				$venda = $venda->ret[0];
				$ret = $this->cMain->getURLCielo($venda['VND_JSON_CHKCIELO']);
				$ret_tipo = $ret->status;
				$ret_msg = $ret->message;//Erro ou URL do checkout cielo
			}else {
				$ret_tipo='fail';
				$ret_msg = 'Dados da venda não encontrados.';
			}
		}
		return(Controller::azReturn($ret_msg, $ret_tipo, $this->httpResponse,$objRet));
	}
	/**
	 * Registra nova ocorrência, e se estiver habilitado, envia notificação por email
	 *
	 * @postFields {vndID,titulo,cliID}
	 * @return string 
	 */
	public function cadastraNovaOcorrencia() {
		//ERRO: postFields inválidos
		if($_POST['vndID']=='' || $_POST['titulo']==''  || $_POST['cliID']=='' ) {
			$ret_tipo='fail';
			$ret_msg = 'Parâmetros incorretos.';
			
		//Prosse com o processamento da requisição
		}else{
			//Busca os dados da venda
			$venda = mMain::venda($_POST['vndID'], $_POST['cliID'])->ret[0];
			$enviarEmail = $_POST['enviarEmail']!='' ? $_POST['enviarEmail'] : ($venda['VND_ENVIAREMAIL']?'true':'false');
			
			//SQL string para cadastrar nova ocorrencia
			$vocID = mMain::reserveID('vendas_ocorrencias','VOC_ID')->ID;
			$sql = "INSERT vendas_ocorrencias SET VOC_ID='".$vocID."',VOC_TITULO='".$_POST['titulo']."',
			VOC_DETALHE='".$_POST['detalhe']."', VOC_DATAHORA=NOW(), VOC_USUARIO='".$_SESSION['USER_LOGIN']."', 
			VOC_VND_ID=".$_POST['vndID'].",VOC_ENVIOUEMAIL=".$enviarEmail;
			$arSQLs[] = $sql;
			
			//Efetua transaction
			$objRet = mMain::mTransaction($arSQLs);
			if ($objRet->ret) { 
				//Se estiver habilitado, envia notificação por email ao cliente:
				if($enviarEmail=='true') {
					$finalidadeEmail = $_POST['titulo']=='Pedido enviado' ? 'Entrega iniciada' : 'Nova ocorrência';
					$ret = $this->cEmail->enviaEmailParaCliente($finalidadeEmail, $_POST['cliID'], $_POST['vndID'], $vocID);
					if($ret->type=='win') {
						$complRet = ' (email enviado ao cliente)';	
					}else {
						$complRet = ' (email não enviado ao cliente. Erro: '.$ret->message.')';	
					}
				}
				$ret_tipo = 'win';
				$ret_msg = 'Nova ocorrência cadastrada com sucesso.'.$complRet;
			}else {
				$ret_tipo = 'fail';
				$ret_msg = "Erro ao executar as instruções. ".(DEBUG_MODE==true ? $objRet->errorSQL." - SQL: ".$objRet->stringSQL:'');
			}	
		}
		return(Controller::azReturn($ret_msg, $ret_tipo, $this->httpResponse));
	}
	/**
	 * Inicia uma etapa atual de um determinada venda, enviando notificação por email caso esteja habilitado
	 *
	  * @postFields {vndID,cliID, eplID,eplTitulo}
	  * @return string 
	 */
	public function iniciaEtapa() {
		//ERRO: postFields inválidos
		if($_POST['vndID']=='' || $_POST['cliID']=='' || $_POST['eplID']=='' || $_POST['eplTitulo']=='' ) {
			$ret_tipo='fail';
			$ret_msg = 'Parâmetros incorretos.';
		//Prosse com o processamento da requisição
		}else{
			//Busca os dados da venda
			$venda = mMain::venda($_POST['vndID'], $_POST['cliID'])->ret[0];
			$enviarEmail = $_POST['enviarEmail']!='' ? $_POST['enviarEmail'] : ($venda['VND_ENVIAREMAIL']?'true':'false');
			
			//SQL string para iniciar a etpa
			$sql = "UPDATE vendas_etapas SET EPL_DHINICIO=NOW(),EPL_USUARIO_INICIO='".$_SESSION['USER_LOGIN']."' WHERE EPL_ID=".$_POST['eplID'];
			$arSQLs[] = $sql;
			
			
			$finalidadeEmail = 'Nova ocorrência';
			//Gera uma nova ocorrência (Pedido em produção)
			if($_POST['eplTitulo']=='Produção') {
				$tituloOcorrencia = 'Pedido em produção';
				
			//Gera uma nova ocorrência  (Pedido na expedição)
			}else if($_POST['eplTitulo']=='Expedição') {
				$tituloOcorrencia = 'Pedido na expedição';
				
			//Gera uma nova ocorrência (Pedido enviado, Pedido pronto p/ retirada)	
			}else if($_POST['eplTitulo']=='Entrega') {
				//Se for venda com frete
				if($venda['VND_OPCAOFRETE']=='calculado') {
					$tituloOcorrencia = 'Pedido enviado';
					$finalidadeEmail = 'Entrega iniciada';
				//Retirar na loja	
				}else {
					$tituloOcorrencia = 'Pedido pronto p/ entrega';
				}
				
			//Etapa Pós-venda não gera ocorrências
			}else if($_POST['eplTitulo']=='Pós-venda') {
				$tituloOcorrencia = '';
				$finalidadeEmail = '';
			}
			if($tituloOcorrencia!='') {
				$vocID = mMain::reserveID('vendas_ocorrencias','VOC_ID')->ID;
				$sql = "INSERT vendas_ocorrencias SET VOC_ID='".$vocID."',VOC_TITULO='".$tituloOcorrencia."',
				VOC_DETALHE='', VOC_DATAHORA=NOW(), VOC_USUARIO='".$_SESSION['USER_LOGIN']."', 
				VOC_VND_ID=".$_POST['vndID'].",VOC_ENVIOUEMAIL=".$enviarEmail;
				$arSQLs[] = $sql;	
			}
			
			//Efetua transaction
			$objRet = mMain::mTransaction($arSQLs);
			if ($objRet->ret) { 
				//Se estiver habilitado, envia notificação por email ao cliente:
				if($enviarEmail='true' && $finalidadeEmail!='') {
					$ret = $this->cEmail->enviaEmailParaCliente($finalidadeEmail, $_POST['cliID'], $_POST['vndID'], $vocID);
					if($ret->type=='win') {
						$complRet = ' (email enviado ao cliente)';	
					}else {
						$complRet = ' (email não enviado ao cliente. Erro: '.$ret->message.')';	
					}
				}
				$ret_tipo = 'win';
				$ret_msg = "Etapa iniciada com sucesso.".$complRet;
			}else {
				$ret_tipo = 'fail';
				$ret_msg = "Erro ao executar as instruções. ".(DEBUG_MODE==true ? $objRet->errorSQL." - SQL: ".$objRet->stringSQL:'');
			}
		}
		return(Controller::azReturn($ret_msg, $ret_tipo, $this->httpResponse));
	}
	/**
	 *  Conclui uma etapa de um determinada venda, enviando notificação por email caso esteja habilitado
	 *
	 * @postFields {vndID,cliID, eplID,eplTitulo}
	 * @return string 
	 */
	 public function concluiEtapa() {
		//ERRO: postFields inválidos
		if($_POST['vndID']=='' || $_POST['cliID']=='' || $_POST['eplID']=='' || $_POST['eplTitulo']=='' ) {
			$ret_tipo='fail';
			$ret_msg = 'Parâmetros incorretos.';
		//Prosse com o processamento da requisição
		}else{
			//Busca os dados da venda
			$venda = mMain::venda($_POST['vndID'], $_POST['cliID'])->ret[0];
			$enviarEmail = $_POST['enviarEmail']!='' ? $_POST['enviarEmail'] : ($venda['VND_ENVIAREMAIL']?'true':'false');
			
			//SQL string para iniciar a etpa
			$sql = "UPDATE vendas_etapas SET EPL_DHFIM=NOW(),EPL_USUARIO_FIM='".$_SESSION['USER_LOGIN']."', EPL_CONCLUIDA=TRUE
			 WHERE EPL_ID=".$_POST['eplID'];
			$arSQLs[] = $sql;
			
	
			//Gera uma nova ocorrência (Entrega finalizada)	
			if($_POST['eplTitulo']=='Entrega') {
				$tituloOcorrencia = 'Entrega finalizada';
				$finalidadeEmail = 'Entrega finalizada';
				
				//SQL string para registro da ocorrencia
				$vocID = mMain::reserveID('vendas_ocorrencias','VOC_ID')->ID;
				$sql = "INSERT vendas_ocorrencias SET VOC_ID='".$vocID."',VOC_TITULO='".$tituloOcorrencia."',
				VOC_DETALHE='', VOC_DATAHORA=NOW(), VOC_USUARIO='".$_SESSION['USER_LOGIN']."', 
				VOC_VND_ID=".$_POST['vndID'].",VOC_ENVIOUEMAIL=".$enviarEmail;
				$arSQLs[] = $sql;			
			}
			
			//Efetua transaction
			$objRet = mMain::mTransaction($arSQLs);
			if ($objRet->ret) { 
				//Se estiver habilitado, envia notificação por email ao cliente:
				if($enviarEmail='true' && $finalidadeEmail=='Entrega finalizada') {
					$ret = $this->cEmail->enviaEmailParaCliente($finalidadeEmail, $_POST['cliID'], $_POST['vndID'], $vocID);
					if($ret->type=='win') {
						$complRet = ' (email enviado ao cliente)';	
					}else {
						$complRet = ' (email não enviado ao cliente. Erro: '.$ret->message.')';	
					}
				}
				$ret_tipo='win';
				$ret_msg = "Etapa concluída com sucesso.".$complRet;
			}else {
				$ret_tipo='fail';
				$ret_msg = "Erro ao executar as instruções. ".(DEBUG_MODE==true ? $objRet->errorSQL." - SQL: ".$objRet->stringSQL:'');
			}
		}
		return(Controller::azReturn($ret_msg, $ret_tipo, $this->httpResponse));
	 }
	 /**
	 * Cancela uma determinada venda, enviando notificação por email caso esteja habilitado
	 * Retorna os produtos ao estoque. (Encomenda/Produção)
	 * @postFields {vndID,cliID, eplID,eplTitulo}
	 * @return string 
	 */
	 public function cancelaVenda() {
		//ERRO: Postfields inválidos
		if($_POST['vndID']=='' || $_POST['cliID']=='' || $_POST['motivo']=='' || $_POST['enviarEmail']=='') {
			$ret_tipo='fail';
			$ret_msg = 'Parâmetros incorretos.';
		//Prosse com o processamento da requisição
		}else{
			//Verifica se o usuário logado tem permissão para cancelar venda
			$sql = "SELECT * FROM permissoes WHERE PER_USER_ID=".$_SESSION['USER_ID']." AND PER_PAGE='pdv_telavenda.php'
			 AND PER_PERMISSAO LIKE '%D%'";
			$r = $this->mysqli->query($sql);
			//ERRO: permissão negada
			if ($r->num_rows==0 && $_SESSION['USER_NIVEL']!='ADMINISTRADOR') {
				$ret_tipo='fail';
				$ret_msg = 'Você não tem permissão para cancelar esta venda.';
			//Prosse com o processamento da requisição
			}else {
				//Busca os dados da venda
				$venda = mMain::venda($_POST['vndID'], $_POST['cliID'])->ret[0];
				$enviarEmail = $_POST['enviarEmail']!='' ? $_POST['enviarEmail'] : ($venda['VND_ENVIAREMAIL']?'true':'false');
				
				//SQL String para cancelar a venda
				$sql = "UPDATE vendas SET VND_STATUS='Cancelado', VND_MOTIVO_CANC='".$_POST['motivo']."', 
				VND_DATA_CANC=NOW(),VND_USER_CANC='".$_SESSION['USER_LOGIN']."' WHERE VND_ID=".$_POST['vndID'];
				$arSQLs[] = $sql;
				
				//SQLs string para retornar produtos ao estoque
				$sql = "SELECT * FROM vendas_itens 
				INNER JOIN vendas ON ITV_VND_ID = VND_ID
				INNER JOIN produtos ON ITV_PRO_ID = PRO_ID
				WHERE ITV_VND_ID=".$_POST['vndID'];
				$r = $this->mysqli->query($sql);
				while($row = $r->fetch_assoc()) {
					// Se for PRODUTO, retorna ao estoque e atualiza historico
					if($row['PRO_TIPO']=='Produto') {
						//SQL PARA RETORNAR O PRODUTO AO ESTOQUE
						$sql = "UPDATE  status_estoque SET 
						".$_SESSION['COL_ESTOQUE']."=".$_SESSION['COL_ESTOQUE']."+".$row['ITV_QUANTIDADE']."  
						WHERE STA_PRO_ID=".$row['ITV_PRO_ID'];
						$arSQLs[] = $sql;
						
						//String SQL para registrar entrada no estoque
						$sql = "INSERT INTO historico_estoque SET 
						HIE_PRO_ID=".$row['ITV_PRO_ID'].", HIE_QUANTIDADE=".$row['ITV_QUANTIDADE'].
						", HIE_USUARIO='".$_SESSION['USER_LOGIN']."', HIE_DATAHORA=NOW(), 
						HIE_DESTINO='ESTOQUE',HIE_OBS='VENDA CANCELADA, PRODUTO DEVOLVIDO AO ESTOQUE. COD.: ".$_POST['vndID']."';;";
						$arSQLs[] = $sql;
					}
				}
				//SQL string para cancelar o lançamento financeiro
				$sql = "UPDATE financeiro_fluxodecaixa SET FLUX_STATUS='CANCELADO', FLUX_MOTIVO_CANC='".$_POST['motivo']."', 
				FLUX_DATA_CANC=NOW(),FLUX_USER_CANC='".$_SESSION['USER_LOGIN']."' WHERE FLUX_VND_ID=".$_POST['vndID'];
				$arSQLs[] = $sql;
				//SQL string para registrar a ocorrência "Pedido cancelado"
				$vocID = mMain::reserveID('vendas_ocorrencias','VOC_ID')->ID;
				$sql = "INSERT INTO vendas_ocorrencias SET VOC_ID='".$vocID."',VOC_TITULO='Pedido cancelado',
				VOC_DETALHE='".$_POST['motivo']."', VOC_DATAHORA=NOW(),VOC_USUARIO='".$_SESSION['USER_LOGIN']."', 
				VOC_VND_ID=".$_POST['vndID'];
				$arSQLs[] = $sql;
				
				
				//Efetua transaction
				$objRet = mMain::mTransaction($arSQLs);
				if ($objRet->ret) { 
					//Se estiver habilitado, envia notificação por email ao cliente:
					if($enviarEmail='true') {
						$ret = $this->cEmail->enviaEmailParaCliente('Nova ocorrência', $_POST['cliID'], $_POST['vndID'], $vocID);
						if($ret->type=='win') {
							$complRet = ' (email enviado ao cliente)';	
						}else {
							$complRet = ' (email não enviado ao cliente. Erro: '.$ret->message.')';	
						}
					}
					$ret_tipo='win';
					$ret_msg = "Pedido cancelado com sucesso.".$complRet;
				}else {
					$ret_tipo='fail';
					$ret_msg = "Erro ao cancelar o pedido, contate o desenvolvedor do sistema.".
					(DEBUG_MODE==true ? $objRet->errorSQL." - SQL: ".$objRet->stringSQL:'');
				}
			}
		}
		return(Controller::azReturn($ret_msg, $ret_tipo, $this->httpResponse));
	 }
	 
}
