<?php
	// Seta as configurações da view
	$view->config(array("normalMode"=>true));
	
	// Incorpora todas as abas/formularios/campos pelo arquivo {nome_view}.xml na pasta /templates
	$view->incorporateXML(true);
	
	// Define o título da página
	define("_title", "Parceiros");
	
	// Define o título da página
	define("_columnID", "PAR_ID");
	
	// Define o controller
	define("_controller", "cParceiros");

	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php require SYS_SRC_APP.'views/pieces/common_header.php' ?>
</head>
<body class="theme-blue">
<script>
/**
 * Paginação no navegador(JS) até limite máximo estabelecido ($maxRows), acima disso é por ajax(PHP)
 * @param int init indice de inicio para mostrar x linhas a partir deste ponto
 */
function showData(init){
	// Altera a global que armazena a pagina atual
	//_PAGINA_ATUAL = init;
	// Limpa as linhas já existentes na listagem
	$('#tbData tbody').html('');
	// Loop master
	for ( i=init; i < ( init + _azListingObject.maxRowsPage ) & i < _azListingObject.data.length; i++ ) {
		// Linha com todas as colunas disponibilizadas em objListagem.data
		objRow = _azListingObject.data[i];
		ID = objRow['<?=_columnID?>'];
		
		// Se for cadastro arquivado, exibe nome e hora do arquivamento
		if(objRow['AZ_ARCHIVED']==true) {
			var infoAdc = "Usuário : " + objRow['AZ_USARCHIVED']+" \r\n Data/Hora : "+objRow['AZ_DHARCHIVED'];
			var linha  = '<tr  data-toggle="tooltip" data-placement="top" title="'+infoAdc+'" az-ID="'+ID+'">';
		// Cadastro ativo :
		}else {
			var linha  = "<tr onclick=\"$(this).toggleClass('row_select')\" id='rowList"+ID+"' az-ID='"+ID+"'>";
		}
		// Preenche todas as colunas visiveis da linha retornada pelo WS
		var contColVis = 0;// Contagem de colunas vísiveis exibidas
		for (c in _azListingColumnsArray) {
			var infoAdc = '';
			coluna = _azListingColumnsArray[c];
			// Se for  dispositivo portabil : (REVER ESTA LOGICA)
			colspan = (_IS_PORTABLE==true ? '2' : '');
			
			// Exibi coluna com possiveis informações adc. na primeira coluna (caso seja movel) ou na segunda coluna em PC/Notebook
			if(((contColVis==1 && _IS_PORTABLE==false) || (contColVis==0 && _IS_PORTABLE==true)) && objRow['AZ_ARCHIVED']==true) {
				infoAdc = objRow['AZ_USARCHIVED']+" - "+objRow['AZ_DHARCHIVED'];
				linha += "<td colspan='"+colspan+"'>"+objRow[coluna]+" <span class='label label-default'>Arquivado por "+infoAdc+"</span></td>";		
				
			// Exibe o conteudo da coluna
			} else {
				// Se a coluna da foto do perfil, mostra o thumbnail:
				if(coluna=='USE_PROFILEPIC' && objRow['THUMBNAIL']!='' && objRow['THUMBNAIL']!=undefined) {
					conteudoColuna = '<a href="'+objRow['URL']+'" target="_blank"><img src="'+objRow['THUMBNAIL']+'" height=50 /></a>';
				
				// Se for a coluna do level, mostra o icone:
				}else if(coluna=='USE_LVL_ID' && objRow['ICONE']!='' && objRow['ICONE']!=undefined) {
					conteudoColuna = '<a href="'+objRow['ICONE']+'" target="_blank" title="'+objRow['LVL_TITLE']+'" data-toggle="tooltip" data-placement="top"><img src="'+objRow['ICONE']+'" height=50 /></a>';
				
				// Conteúdo padrão (apenas texto)	
				}else {
					conteudoColuna=objRow[coluna];
				}
				
				// Se estiver bloqueado, exibe botão laranja
				if(contColVis==1 && objRow['USE_STATUS']==="0") {
					infoAdc = " <span class='label label-danger'>BLOQUEADO</span>";
				}
	
				// Incrementa uma coluna na linha
				linha += "<td colspan='"+colspan+"'>"+conteudoColuna+infoAdc+"</td>";
			}
			
			// Incrementa contador de colunas visiveis
			contColVis++;
		}
		
		// Botao alterar
		btAlterar = "<span class='glyphicon glyphicon-pencil handCursor'  data-toggle='tooltip' data-placement='top'"
					+" title='Alterar cadastro' onclick='fillOutForms("+ID+")'></span> &nbsp; &nbsp;";
		// Botao Deletar
		btExcluir = " <span class='glyphicon glyphicon-remove handCursor'  data-toggle='tooltip' data-placement='top'"
					+" title='Excluir cadastro' onclick=\"archiveSelectedRegisters()\"></span> ";		
					
		// Se estiver arquivado, não permite nenhuma edição
		if(objRow['AZ_ARCHIVED']==true) {
			btAlterar='';	btExcluir='';
		}
		
		// Coluna funções (alterar e remover cadastro) (apenas pc e notebooks)
		<?php if(!$view->isPortable()) :?>
		linha += "<td align='right'>"+btAlterar+" "+btExcluir+"</td>";
		<?php else :?>
		linha += "<td align='right'>"+btAlterar+" </td>";
		<?php endif; ?>
		
		// Fecha linha
		linha += "</tr>";
		// Incrementa linha no corpo da tabela
		$('#tbData tbody').append(linha);
		
		
		// Inicia todas as tooltips (pop-up informativo)
		$('[data-toggle="tooltip"]').tooltip(); 

	}
	
	// Atualiza paginação
	nrPaginas = Math.round((_azListingObject.data.length/_azListingObject.maxRowsPage));
	html = '';
	for(i=0; i < nrPaginas;i++ ) {
		html += '<li><a href="#" onclick="showData('+(i*_azListingObject.maxRowsPage)+')">'+(i+1)+'</a></li>';	
	}
	$('ul.pagination').html(html);
	
	
}
/**
 * Preenche o list de categorias, conservando a ordem retornada pelo WS (cadastro)
 * Obs: está função é executada quando fillOutForms() for chamada (preenchimento dos formularios)
 * @param categorias:array -> array com os objetos de cada categoria retornada pelo WS
 * @param subcategorias:array -> array com os objetos de cada subcategoria retornada pelo WS
 */
var _GL_SUBCATEGORIAS=[];
function preencheCategorias(categorias,subcategorias){
	// Armazena as subcategorias encontradas na global
	_GL_SUBCATEGORIAS = subcategorias;
	// Reseta todas options do list  categorias
	$('#listcategoria').html('');
	// Utiliza a primeira categoria do array para preencher o list das subcategorias
	var cat_id_sel = categorias[0].CAT_ID;
	preencheSubcategorias(cat_id_sel);
	// Exibe as categorias encontradas
	for(var c in categorias) {
		cat = categorias[c];
		var htmlOption = "<option value='"+cat.CAT_ID+"' extra=''>"+cat.CAT_TITLE+"</option>";	
		$('#listcategoria').append(htmlOption);
	}
	
}
/**
 * Preenche o list de subcategorias, conservando a ordem retornada pelo WS (cadastro)
 * Obs: exibi somente as subcategorias da categoria selecionada
 * @param catID:string -> PK da categoria selecionada
 */
function preencheSubcategorias(catID){
	// Reseta todas options do list subcategorias
	$('#listsubcategoria').html('');
	// Exibe as subcategorias relacionadas com a categoria selecionada
	for(var s in _GL_SUBCATEGORIAS) {
		subcat = _GL_SUBCATEGORIAS[s];
		if(subcat.SUB_CAT_ID == catID) {
			var htmlOption = "<option value='"+subcat.SUB_ID+"' extra=''>"+subcat.SUB_TITLE+"</option>";	
			$('#listsubcategoria').append(htmlOption);
		}
	}
}
 
/**
 * Realiza validação remota do nome de usuário informado
 * @param objField:Domelement -> campo do username
 */
function checksUserName(objField){
	// Verifica apenas quando o valor atual é diferente do default
	if($(objField).val()!=$(objField).attr('az-default')) {
		// Seta payload:
		_azSender7845.setPayload({
		  action:'checksUserName',
		  view:_SYS_VIEW
		});
		// Seta envelope:
		_azSender7845.setEnvelope({
			fields:[{column:'USERNAME',value:objField.value}]
		});
		// Seta callback
		_azSender7845.callback = function(objRet){
			// Se estiver em modo de debugação salva e imprime o retorno no console
			if(_DEBUG_MODE) _azGeneral.debug(objRet);
			
			// Verifica se o usuário existe
			if(objRet.ret.exists===true) {
				$(objField).attr('az-custom',false);
				$(objField).attr('az-remoteMessage','Este usuário já existe.');
			}else {
				$(objField).attr('az-custom',true);
				$(objField).attr('az-remoteMessage','');	
			}
			
			// Realiza validação do formulário novamente
			_azValidation.exec('Main');
		}
		// Envia o pacote
		_azSender7845.send();
	}
}
</script>
<?php
	//var_dump($Abas->abas); exit();
	
?>
<!--TOPO-->
<?php include(SYS_SRC_APP.'views/pieces/common_top.php')?>
<!--MENU de navegação do sistema-->
<?php include(SYS_SRC_APP.'views/pieces/common_menu.php')?>
<!--CONTEUDO DA VIEW -->
<div class="content" style="margin-left:0;">
	<div class="main-content"> 
    	<!-- TITULO DA VIEW -->
        <?=$view->printTitleForm(_title,$view->isPortable())?>
        <!------------------------------------------------------------------
        --- Container ctPrincipal: Formulários de cadastro/informação
        ------------------------------------------------------------------->
        <div class="destaqueShadow bordaCinza hide" id="ctMain">
            <!-- ABAS -->
            <ul class="nav nav-tabs" role="tablist">
            	<?php $view->printTabs($Tabs->tabs); ?>
            </ul>
            <!-- FORMULARIOS (DE CADA ABA) -->
            <div class="tab-content" style="margin-top:15px;">
           		<?php $view->printForms($Tabs->tabs)?>
            </div>
        </div>
        <?php
			// Se for Modo de edição, não mostra a listagem, apenas um cadastro especifico
			if($view->config()->editMode==false) {
				$view->printListing($view->config());
			}
		?>
     </div>
</div>
<!--Janelas Modals-->
<?php 		
//Modal filtros de busca
include(SYS_SRC_APP.'views/pieces/common_modalSearch.php');

//Modal funções de listagem
include(SYS_SRC_APP.'views/pieces/common_modalFunctions.php');

// Common piece bottom
include(SYS_SRC_APP.'views/pieces/common_bottom.php')
?>
</body>
</html>