<?php
	// Incorpora todas as abas/formularios/campos pelo arquivo {nome_view}.xml na pasta /templates
	$view->incorporateXML(true);
	
	// Define o título da página
	define("_title", "Relatório Ações");
	
	// Define o título da página
	//define("_columnID", "QUE_ID");
	
	// Define o controller
	define("_controller", "cRelatorioAcoes");
	
	// Configurações da View
	$view->config(array(
		'ID'			=> 0, 		// ID inicial para buscar os dados dos formularios
		'editMode'	=> false,	// Prepara o container principal apenas para editar determinado cadastro
		'normalMode'	=> false,	// Carrega todos containers e funções da View para operações CRUD
		'listingMode'  => true		// Carrega apenas as funções para listagem/relatórios
	));
	

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php require SYS_SRC_APP.'views/pieces/common_header.php' ?>
</head>
<body class="theme-blue">
<style>
.sa-form {
	margin-top:15px;
	padding:10px;
	border-radius: 3px;
}
 * {
	font-family: calibri;        
}
</style>
<script>

/**
 * Paginação no navegador(JS) até limite máximo estabelecido ($maxRows), acima disso é por ajax(PHP)
 * @param int init indice de inicio para mostrar x linhas a partir deste ponto
 */
function showData(init){
	// Altera a global que armazena a pagina atual
	//_PAGINA_ATUAL = init;
	// Limpa as linhas já existentes na listagem
	$('#tbData tbody').html('');
	// Loop master
	for ( i=init; i < ( init + _azListingObject.maxRowsPage ) & i < _azListingObject.data.length; i++ ) {
		// Linha com todas as colunas disponibilizadas em objListagem.data
		objRow = _azListingObject.data[i];
		ID = objRow['<?=_columnID?>'];
		
		var linha  = "<tr onclick=\"$(this).toggleClass('row_select')\" id='rowList"+ID+"' az-ID='"+ID+"'>";
		
		// Preenche todas as colunas visiveis da linha retornada pelo WS
		for (c in _azListingColumnsArray) {
			coluna = _azListingColumnsArray[c];
			
			// Conteúdo padrão (apenas texto)	
			conteudoColuna=objRow[coluna];

			// Incrementa uma coluna na linha
			linha += "<td >"+conteudoColuna+"</td>";
			
		}
		
		// Adiciona botão para maiores informações
		var htmlInfo = "<b> IP : </b> </b>"+objRow['ACC_IP']+"<br/>";
		htmlInfo += "<b> Navegador : </b> </b>"+objRow['ACC_NAVEGATOR']+"<br/>";
		htmlInfo += "<b> Dt./hr. acesso : </b></b> "+objRow['DH_ACESSO']+"<br/>";	
		var infoAdc = " <button onclick=\"sa.alert('Informações adicionais','"+htmlInfo+"')\" class='btn btn-info'>Info</button>";
		linha += "<td >"+infoAdc+"</td>";
		
		// Fecha linha
		linha += "</tr>";
		// Incrementa linha no corpo da tabela
		$('#tbData tbody').append(linha);
		
		
		// Inicia todas as tooltips (pop-up informativo)
		$('[data-toggle="tooltip"]').tooltip(); 

	}
	
	// Atualiza paginação
	nrPaginas = Math.round((_azListingObject.data.length/_azListingObject.maxRowsPage));
	html = '';
	for(i=0; i < nrPaginas;i++ ) {
		html += '<li><a href="#" onclick="showData('+(i*_azListingObject.maxRowsPage)+')">'+(i+1)+'</a></li>';	
	}
	$('ul.pagination').html(html);
	
	
}
</script>
<!--TOPO-->
<?php include(SYS_SRC_APP.'views/pieces/common_top.php')?>
<!--MENU de navegação do sistema-->
<?php include(SYS_SRC_APP.'views/pieces/common_menu.php')?>
<!--CONTEUDO DA VIEW -->
<div class="content" style="margin-left:0;">
	<div class="main-content"> 
    	<!-- TITULO DA VIEW -->
        <?=$view->printTitleForm(_title,$view->isPortable())?>
        <!------------------------------------------------------------------
        --- Container ctPrincipal: Formulários de cadastro/informação
        ------------------------------------------------------------------->
        <div class="destaqueShadow bordaCinza hide" id="ctMain">
            <!-- ABAS -->
            <ul class="nav nav-tabs" role="tablist">
            	<?php $view->printTabs($Tabs->tabs); ?>
            </ul>
            <!-- FORMULARIOS (DE CADA ABA) -->
            <div class="tab-content" style="margin-top:15px;">
           		<?php $view->printForms($Tabs->tabs)?>
            </div>
        </div>
        <?php
			// Se for Modo de edição, não mostra a listagem, apenas um cadastro especifico
			if($view->config()->editMode==false) {
				$view->printListing($view->config());
			}
		?>
     </div>
</div>
<!--Janelas Modals-->
<?php 		
//Modal filtros de busca
include(SYS_SRC_APP.'views/pieces/common_modalSearch.php');

//Modal funções de listagem
include(SYS_SRC_APP.'views/pieces/common_modalFunctions.php');

// Common piece bottom
include(SYS_SRC_APP.'views/pieces/common_bottom.php')
?>
</body>
</html>