<?php
	// Define o título da página
	define("_title", "Privacidade - MasterBusiness");
	
	// Configurações da View
	$view->config(array(
		'ID'			=> 0, 		// ID inicial para buscar os dados dos formularios
		'editMode'	=> false,	// Prepara o container principal apenas para editar determinado cadastro
		'normalMode'	=> false,	// Carrega todos containers e funções da View (CRUD)
	));
?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<?php require SYS_SRC_APP.'views/pieces/common_header.php' ?>
</head>  
<body >
<style>
  .dialog {
	  float: none;
	  margin: 1.5em auto 0;
	  width: 70%;
  }
</style>
    <!--TOPO-->
	<?php include(SYS_SRC_APP.'views/pieces/common_top.php')?>
    
    
    <div class="dialog">
    <div class="panel panel-default" >
            <p class="panel-heading no-collapse">&nbsp;</p>
            <div class="panel-body">
            <h1> POLÍTICA DE PRIVACIDADE</h1>
              <p align="center">
                Esta política de privacidade governa  o uso do aplicativo&nbsp;&quot;MasterBusiness&quot; para dispositivos móveis criado pela  LBN Sistemas.&nbsp;<br>
                <br>
  <h2>Que informações o aplicativo obtém e como é usado?</h2><br>
  <strong>Informações fornecidas pelo usuário</strong>&nbsp;<br>
                O aplicativo obtém as informações  fornecidas quando você baixa e registra o aplicativo.&nbsp;O registro com a  gente é opcional.&nbsp;No entanto, tenha em mente que talvez você não consiga  usar alguns dos recursos oferecidos pelo aplicativo, a menos que você se  registre com a gente.<br>
                Quando você se registra conosco e usa  o aplicativo, geralmente fornece (a) seu nome, endereço de e-mail, idade, nome  de usuário, senha e outras informações de registro;&nbsp;(B) informações  relacionadas a transações, como quando faz compras, respondem a qualquer oferta  ou baixem ou usam aplicativos de nós;&nbsp;(C) informações que você nos fornece  quando você nos contata para obter ajuda;&nbsp;(D) informações do cartão de  crédito para compra e uso da Aplicação, e;&nbsp;(E) informações que você entra  no nosso sistema ao usar o aplicativo, como informações de contato e informações  de gerenciamento de projetos.<br>
                Nós também podemos usar as  informações que você nos forneceu para entrar em contato com você de tempos em  tempos para fornecer informações importantes, avisos exigidos e promoções de  marketing.<br>
                <br>
  <strong>Informações coletadas automaticamente</strong>&nbsp;<br>
                Além disso, o aplicativo pode coletar  certas informações automaticamente, incluindo, mas não limitado a, o tipo de  dispositivo móvel que você usa, o ID do dispositivo exclusivo de seus  dispositivos móveis, o endereço IP do seu dispositivo móvel, seu sistema  operacional móvel, o tipo de dispositivo móvel Navegadores de Internet que você  usa e informações sobre a forma como você usa o aplicativo.&nbsp;&nbsp;<br>
                <br>
  <h2>Os terceiros vêem e / ou têm acesso a informações obtidas pelo  aplicativo?</h2><br>
                Somente agregados, dados anônimos são  periodicamente transmitidos para serviços externos para nos ajudar a melhorar a  Aplicação e nosso serviço.&nbsp;Nós compartilharemos suas informações com  terceiros somente nas formas descritas nesta declaração de privacidade.<br>
              Podemos divulgar informações  fornecidas pelo usuário e coletadas automaticamente:</p>
              <ul>
                <li>Conforme exigido por lei, de modo a cumprir uma intimação ou processo  legal similar;</li>
                <li>Quando acreditamos de boa fé que a divulgação é necessária para proteger  nossos direitos, proteger sua segurança ou a segurança de terceiros, investigar  fraudes ou responder a um pedido do governo;</li>
                <li>Com os nossos provedores de serviços confiáveis ​​que trabalham em nosso  nome, não têm um uso independente das informações que divulgamos, e concordamos  em aderir às regras estabelecidas nesta declaração de privacidade.&nbsp;</li>
              </ul>
              <p><strong>Quais são os meus direitos de exclusão?</strong> <br>
                Você pode interromper toda a coleção  de informações pelo aplicativo facilmente, desinstalando o  aplicativo.&nbsp;Você pode usar os processos de desinstalação padrão que possam  estar disponíveis como parte de seu dispositivo móvel ou através do mercado ou  da rede de aplicativos móveis.&nbsp;Você também pode solicitar a exclusão via  email, em&nbsp;suporte@lbnsistemas.com.br&nbsp;.<br>
                <br>
  <strong>Política de Retenção de Dados, Gerenciando Suas Informações</strong><strong> </strong><br>
                Retardaremos os dados fornecidos pelo  usuário durante o uso do aplicativo e por um período razoável depois  disso.&nbsp;Retardaremos as informações coletadas automaticamente por até&nbsp;24 meses&nbsp;&nbsp;e,  posteriormente,&nbsp;podemos&nbsp;armazená-la em conjunto.&nbsp;Se você quiser  que elimine dados fornecidos pelo usuário que você forneceu através do  aplicativo, entre em contato conosco em&nbsp;&nbsp;suporte@ lbnsistemas.com.br&nbsp;&nbsp;e nós responderemos em um prazo  razoável.&nbsp;Observe que alguns ou todos os dados fornecidos pelo usuário  podem ser necessários para que o aplicativo funcione corretamente.<br>
                <br>
  <h2>Crianças</h2> 
                Nós não usamos o Aplicativo para  solicitar dados ou mercado para crianças com idade inferior a 13 anos. Se um  pai ou responsável tomar conhecimento de que seu filho nos forneceu informações  sem o seu consentimento, ele ou ela deve entrar em contato conosco suporte@lbnsistemas.com.br&nbsp;.&nbsp;Vamos  excluir essas informações dos nossos arquivos dentro de um prazo razoável.</p>
              <p><h2>Segurança</h2> 
                Estamos preocupados com a salvaguarda  da confidencialidade de suas informações.&nbsp;Nós fornecemos salvaguardas  físicas, eletrônicas e processuais para proteger a informação que processamos e  mantemos, tais como: backup periódico, certificados de segurança, criptografia;  limitamos o acesso a essas informações aos funcionários e contratados  autorizados que precisam saber essas informações para operar, desenvolver ou  melhorar nossa Aplicação.&nbsp;Tenha em mente que, apesar de nos esforçarmos de  fornecer segurança razoável para as informações que processamos e mantemos,  nenhum sistema de segurança pode prevenir todas as possíveis violações da  mesma.<br>
                <br>
<br>
  <h2>Alterações</h2>
                Esta Política de Privacidade pode ser  atualizada periodicamente por qualquer motivo.&nbsp;Nós o notificaremos sobre  quaisquer alterações à nossa Política de Privacidade, publicando a nova  Política de Privacidade&nbsp;&nbsp;aqui&nbsp;&nbsp;e&nbsp;&nbsp;informando você por e-mail ou  mensagem de texto.&nbsp;É aconselhável consultar regularmente esta Política de Privacidade  para quaisquer alterações, uma vez que o uso continuado é considerado aprovação  de todas as alterações.&nbsp;</p>
            </div>
        </div>
        <p class="pull-right small" style="">versão do sistema <?=SYS_VERSION?></p>
    </div>
<?php
	// Common piece bottom
	include(SYS_SRC_APP.'views/pieces/common_bottom.php')
?>
</body>
</html>