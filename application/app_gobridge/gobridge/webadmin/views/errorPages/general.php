<!DOCTYPE html>
<html lang="en">
<head>
 	<?php require SYS_SRC_APP.'views/pieces/common_header.php' ?>
<body class="theme-blue">

<!--TOPO-->
<?php include(SYS_SRC_APP.'views/pieces/common_top.php')?>

<!--CONTEUDO DA VIEW-->
<div class="content" style="margin-left:0">
    <div class="main-content"> 
        <div class="destaqueShadow bordaCinza" id="ctPrincipal" style="padding-left:20px; padding-top:0">
             <div class="row ">
             	 <div class="col-xs-12 col-md-6">
                    <img src="<?=SYS_FRAMEWORK_URL?>images/master/erro.png" />
                 </div>
                 <div class="col-xs-12 col-md-6">
                 <h1> Ops !</h1>
                    <h2><?=$msgErro?></h2>
                    <small>Tente novamente ou contate o suporte técnico.</small>
                </div>
             </div>
        </div>
    </div>
 </div>

</body>
</head>
</html>