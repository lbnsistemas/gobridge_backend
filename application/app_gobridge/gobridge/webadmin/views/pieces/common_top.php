<div class="navbar navbar-default teste" role="navigation" style="border-radius:0;background-image: linear-gradient(#484e55, #14171b 60%, #0d0f10); ">
      
      <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
             <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <a class="" href="<?=SYS_APP_URL?>webadmin"><span class="navbar-brand" style="padding:10px; ">
          <!--<img src="<?=SYS_APP_URL?>../etc/images/icone-brickbe.png" height="100%" />-->
          <i class="fa fa-asterisk"></i> goBridge
          </span></a>
      </div>
      
      
		
        <?php
			// Observar o método de segurança avançado na hora de efetuar o login no controller
			// isto irá interferir na validação da sessão
			if($controller->validSession($secureAdvanced=true)) {
				$primeiroNome = explode(" ",$_SESSION['PAR_RESPONSAVEL']);
				$primeiroNome = $primeiroNome[0];
				
		?>	
        <div class="navbar-collapse collapse" style="height: 1px;">
          <ul id="main-menu" class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-user padding-right-small" style="position:relative;top: 3px;"></span> Olá, <?=$primeiroNome?>
                    <i class="fa fa-caret-down"></i>
                </a>

              <ul class="dropdown-menu">
                <li><a tabindex="-1" href="#" onclick="doLogout()">Sair</a></li>
                <li><a  href="<?=SYS_DOMAIN.'webadmin/'?>meu-cadastr" >Meu cadastro</a></li>
              </ul>
            </li>
          </ul>
        </div>
        <?php } ?>
      
</div>