﻿<!--Modal para os filtros -->
<div class="modal fade" id="mdFiltroBusca" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- Corpo-->
      <div class="modal-body">
      	  	<div class="row">
          	<?php
				//Imprime os filtros, setados pela view...
				$contador = -1;
				if(is_array($prop->arFilters)) {
					foreach($prop->arFilters as $filter) {
						$contador++;
						//Pula o primeiro campo que obrigatóriamente é o filtro genérico
						if($contador==0) continue;
						
						//Verefica se existem dois campos na mesma linha, ex: data1 à data2, valor1 e valor2,..
						if(is_array($filter)) {
							// dataHora ou data
							if($filter[0]->dataType=='dateTime' || $filtro[0]->dataType=='date') {
								$formato = $filter[0]->dataType=='dateTime' ? 'DD/MM/YYYY HH:mm' : 'DD/MM/YYYY';
								print '
										<row>
											<div class="form-group col-sm-6" >
												<label for=" '.$filter[0]->nameID.'">'.$filter[0]->title.':</label>
												 <div class="input-group date" id="dtp'.$filter[0]->nameID.'">
													<input type="text" data-format="'.$formato.'" id="'.$filter[0]->nameID.'" class="form-control" az-default="" az-dataType="date" size="25" >
													<span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
													</span>
												 </div>
											 </div>
											 <div class="form-group col-sm-6">
												<label for="'.$filter[1]->nameID.'">&nbsp;</label>
												<div class="input-group date" id="dtp'.$filter[1]->nameID.'">
													<input data-format="'.$formato.'"  az-default=""  type="text" id="'.$filter[1]->nameID.'" class="form-control"  size="25" az-dataType="date" >
													<span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
													</span>
												</div>
											 </div>
										 </row>
										 <script type="text/javascript">
											// Campo data hora 1
											$("#dtp'.$filter[0]->nameID.'").datetimepicker({
												locale : "pt-br",
												format : "'.$formato.'"
												
											});
											
											// Campo data hora 2
											$("#dtp'.$filter[1]->nameID.'").datetimepicker({
												locale : "pt-br",
												format : "'.$formato.'"
											});	
											
											// Formata o campo para datas
											_azAzul.afterLoad.push(function() {
												_azDataManipulation.setFormatting(\'#'.$filter[0]->nameID.'\');
												_azDataManipulation.setFormatting(\'#'.$filter[1]->nameID.'\');
												
											});
										 </script>
									  ';
							
							// moeda
							}else  {
								print '
										<row>
											<div class="form-group col-sm-6">
												<label for="'.$filter[0]->nameID.'">'.$filter[0]->title.':</label>
												<input type="text"  az-default="" az-dataType="currency" id="'.$filter[0]->nameID.'" class="form-control"  size="25" >
											 </div>
											 <div class="form-group col-sm-6">
												<label for="'.$filter[1]->nameID.'">&nbsp;</label>
												<input type="text"  az-default="" az-dataType="currency" id="'.$filter[1]->nameID.'" class="form-control"  size="25" >
											 </div>
										 </row>
										
									  ';
							}
						//Apenas um campo por linha		
						}else {
							print '
									<row>
										 <div class="form-group col-sm-12">
											<label for="'.$filter->nameID.'">'.$filter->title.':</label>';
											
											// input
											if($filter->fieldType=='input') {
												print '<input type="text" id="'.$filter->nameID.'" az-default="'.$filter->nameID.'" class="form-control"  az-dataType="text" size="25" >';
												
											// select
											}else if($filter->fieldType=='select') {
												
												//Imprime as opções do select
												$cont = 0;
												foreach ($filter->options as $opt) {
													if($cont++==0) {
														print '<select id="'.$filter->nameID.'" az-default="'.$opt->attributes->value.'" class="form-control" az-dataType="text" >'."\r\n";
													}
													print '<option value="'.$opt->attributes->value.'" '.$opt->attributes->extra.'>'.$opt->text.'</option>'."\r\n";
												}
												print '</select>';
											}
											
							print '				
										 </div>
									</row>
								  ';
						}
						
					}
				}
			?>
          </div>
      </div>
      <!--Rodape-->
      <div class="modal-footer">
      <!-- $('#mdFiltroBusca').modal('hide')-->
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary" onClick="requestData(0)">Buscar</button>
      </div>
    </div>
  </div>
</div>