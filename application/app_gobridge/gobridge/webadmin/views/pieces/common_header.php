<!-- CODIFICAÇÃO UTF8-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- DEFINIÇÃO DE WIDTH PARA MOBILES-->
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no"/>
<!-- IMPEDIR CACHE -->
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Expires" content="0" />
<!-- TITULO DA PAGINA -->
<title><?=(defined("_title")==true ? _title : '')?> | <?=SYS_NAME?></title>
<!-- PLUGINS (js/css) DA PLATAFORMA-->
<?=$view->printHead($CONFIG_PLUGIN)?>
<!-- Compilador das funções JS da View (Azul Framework) -->
<?php include(SYS_SRC_FRAMEWORK."Azul.php")?>

<script type="application/javascript">
// GLOBAIS
var _GL_LATITUDE = -23.506269;
var _GL_LONGITUDE = -47.455631; // Latitude e longitude padrão para: Sorocaba
var _GL_GOOGLE_KEY = '';

	// Função para atualizar os levels do app
	function updateLevels() {
		var _azSenderUser = new AzulSender();
		_azSenderUser.setPayload({
			controller:"cAppLevelsRules",
			action: "updateAppLevels"
		});
		_azSenderUser.callback = function(objRet){_azSenderUser.reset()};
		_azSenderUser.send();
	}
</script>
<style>
body {
	color: #5a5a5a !important;	
}
.bg-white{
	background-color: white;	
}
#ctListing, #ctMain {
	margin-top: 30px;	
	border-top: none;
}
.modal-content {
	background-color: white;	
}
table, #ctListing, #ctMain {
	background-color: white !important;	
}
#tbData tbody tr:hover{
	background-color: #999 !important;	
	color: white !important;
}
.select2-container{
	display:block !important;
	width: auto !important;	
}
.sidebar-nav {
	top: 50px !important;
	position:fixed !important;	
}
.progress {
	margin-top: 15px !important;
	background-color: #EEE !important;
	
}
.progress-bar{
	color: black !important;	
}
.content{
	margin-left:0px !important;	
}
.laranja {
	color:#f05223 !important;
}
@media screen and (max-width:600px) {
	.content{
		margin-left:0px !important;	
		border-left: none !important;
	}
	
	.sidebar-nav{
		display:none !important;	
	}
}
</style>