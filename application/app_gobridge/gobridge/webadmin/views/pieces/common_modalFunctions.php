﻿<!--Modal para os filtros -->
<div class="modal fade" id="mdListingFunctions" tabindex="-1" role="dialog" aria-labelledby="mdListingFunctions">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- Corpo-->
      <div class="modal-body">
      	  <div class="row">
          	<div class="form-group col-md-12" >
            	<button class="btn btn-default" style="width:100%" id="btnSelection" onclick="doSelection()"  >selecionar todos</button>
            </div>	
            <div class="form-group col-md-12" >
            	 <button class="btn btn-danger" style="width:100%" onclick="archiveSelectedRegisters()" id="btnSelectedFiles" disabled > arquivar selecionado(s) </button> 
            </div>	
          </div>
      </div>
    </div>
  </div>
</div>