<div class="sidebar-nav " >
    <ul>        
        <!-- Funcionalidades comuns -->
        <!-- Dashboard -->
        <li>
        	<a href="<?=SYS_DOMAIN.'webadmin/'?>" class="nav-header" >
            <i class="fa fa-fw fa-home"></i> Início </a>
        </li>       
 
         <li>
        	<a href="<?=SYS_DOMAIN.'webadmin/'?>obras" class="nav-header" >
            <i class="fa fa-fw fa-caret-right"></i> Obras </a>
        </li>
        <!-- Registro de despesa de obra -->
        <li>
        	<a href="<?=SYS_DOMAIN.'webadmin/'?>acompanhar-orcamentos" class="nav-header" >
            <i class="fa fa-fw fa-caret-right"></i> Acompanhar orçamentos</a>
        </li>
        <li>
        	<a href="<?=SYS_DOMAIN.'webadmin/'?>solicitar-orcamento" class="nav-header" >
            <i class="fa fa-fw fa-caret-right"></i> Solicitar Orçamento</a>
        </li>
        <li>
        	<a href="<?=SYS_DOMAIN.'webadmin/'?>lancar-custos-obra" class="nav-header" >
            <i class="fa fa-fw fa-caret-right"></i> Lançar Custos de Obra</a>
        </li>
        <li>
        	<a href="<?=SYS_DOMAIN.'webadmin/'?>arquivos-fotos" class="nav-header" >
            <i class="fa fa-fw fa-caret-right"></i> Arquivos e Fotos </a>
        </li>
        
     </ul>
</div>