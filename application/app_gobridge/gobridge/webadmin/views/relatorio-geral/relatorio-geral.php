<?php
	// Incorpora todas as abas/formularios/campos pelo arquivo {nome_view}.xml na pasta /templates
	$view->incorporateXML(true);
	
	// Define o título da página
	define("_title", "Relatório Chamadas");
	
	// Define o título da página
	//define("_columnID", "QUE_ID");
	
	// Ordenação inicial
	//define("_orderBy","V_DHCALL DESC");
	
	// Define o controller
	define("_controller", "cRelatorioGeral");
	
	// Configurações da View
	$view->config(array(
		'ID'			=> 0, 		// ID inicial para buscar os dados dos formularios
		'modoEdicao'	=> false,	// Prepara o container principal apenas para editar determinado cadastro
		'modoNormal'	=> false,	// Carrega todos containers e funções da View para operações CRUD
		'modoListagem'  => true		// Carrega apenas as funções para listagem/relatórios
	));
	
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php require SYS_SRC_APP.'views/'._module.'/pieces/common_header.php' ?>
</head>
<body class="theme-blue">
<style>
.sa-form {
	margin-top:15px;
	padding:10px;
	border-radius: 3px;
}
 * {
	font-family: calibri;        
}
</style>
<script>
/**
 * Paginação no navegador(JS) até limite máximo estabelecido ($maxRows), acima disso é por ajax(PHP)
 * @param int init indice de inicio para mostrar x linhas a partir deste ponto
 */
function mostrarLinhas(init){
	// Altera a global que armazena a pagina atual
	_PAGINA_ATUAL = init;
	// Limpa as linhas já existentes na listagem
	$('#tbDados tbody').html('');
	// Loop master
	for ( i=init; i < ( init + objListagem.maxRowsPage ) & i < objListagem.data.length; i++ ) {
		// Linha com todas as colunas disponibilizadas em objListagem.data
		objRow = objListagem.data[i];
		ID = objRow['<?=_columnID?>'];
		var retorno = objRow['V_RETURN']=='-' ? '' : objRow['V_RETURN'];
		// Abre a linha
		var linha  = "<tr onclick=\"$(this).toggleClass('row_select')\" data-toggle=\"tooltip\"  title='"+retorno+"' id='rowList"+ID+"' sa-ID='"+ID+"'>";
		
		// Preenche todas as colunas visiveis da linha retornada pelo WS
		for (c in arColunasListagem) {
			var infoAdc = '';
			coluna = arColunasListagem[c];
			// Se for  dispositivo portabil : (REVER ESTA LOGICA)
			colspan = (_IS_PORTABLE==true ? '2' : '');
			
			
			if(coluna=='V_STATUS') {
				// Tradução para ABANDONED
				if(objRow[coluna]=='ABANDONED') {
					objRow[coluna] = 'ABANDONADO'
			
				// para UNSUCCESSFUL
				} else if(objRow[coluna]=='UNSUCCESSFUL') {
					objRow[coluna] = 'INSUCESSO';
				
				// para RETORNED
				} else if(objRow[coluna]=='RETURNED') {
					objRow[coluna] = 'RETORNADO';
					
				}
				
			}
			// Conteúdo padrão (apenas texto)	
			conteudoColuna=objRow[coluna];

			// Incrementa uma coluna na linha
			linha += "<td colspan='"+colspan+"'>"+conteudoColuna+infoAdc+"</td>";
			
		}
		
		// Fecha linha
		linha += "</tr>";
		// Incrementa linha no corpo da tabela
		$('#tbDados tbody').append(linha);
		
		
		// Inicia todas as tooltips (pop-up informativo)
		$('[data-toggle="tooltip"]').tooltip(); 

	}
	
	// Atualiza paginação
	nrPaginas = Math.round((objListagem.data.length/objListagem.maxRowsPage));
	html = '';
	for(i=0; i < nrPaginas;i++ ) {
		html += '<li><a href="#" onclick="mostrarLinhas('+(i*objListagem.maxRowsPage)+')">'+(i+1)+'</a></li>';	
	}
	$('ul.pagination').html(html);
	
	
}

// Função para carregar o list de agentes dinamicamente
function loadListAgents () {
	sa.sender.pacote.action = 'loadListAgents';
	// Seta a função pós-processamento com sucesso
	sa.sender.callback = function(objRet) { 
	    // Se estiver em modo de debugação salva e imprime o retorno no console
		if(_DEBUG_MODE) sa.debug(objRet);
		var age_value;
		var age_text;
		for(var i=0; i<objRet.ret.length; i++) {
			age_value = objRet.ret[i].USE_USERNAME;
			age_text = objRet.ret[i].USE_USERNAME;
			var htmlOption = '<option value="'+age_value+'">'+age_text+"</option>";
			$('#listagents').append(htmlOption);	
		}
		// Reseta sa.sender
		sa.sender.reseta();
	}
	// Envia pacote (método oculto)
	sa.sender.send(true);
}

// Função para exportar os dados paginados e filtrados em formado xlsx(excel)
function exportarParaExcel() {
	sa.sender.pacote.action = 'exportarParaExcel';
	sa.sender.setEnvelope(
		{
			orderBy : 'V_DHABANDON DESC', 
			colunas : [<?php $view->imprimiArrayColunasView($view->config())?>],
			filtros : [<?php $view->imprimiObjetosFiltros($view->config())?>]
		}
	)
	// Seta a função pós-processamento com sucesso
	sa.sender.callback = function(objRet) { 
	    // Se estiver em modo de debugação salva e imprime o retorno no console
		if(_DEBUG_MODE) sa.debug(objRet);
		
		// Url do arquivo downloader, que retornará o arquivo propriamente dito
		var urlDOwnloader = _SYS_FRAMEWORK_URL+'php/downloader.php?deleteFile';
		
		// Abri a janela de download do navegador já com o arquivo em stream
		var iframe = $("<iframe/>").attr({
								id: 'iFrameDownloader',
								src: urlDOwnloader,
								style: "visibility:hidden;display:none"
							}).appendTo('body');
					   
		// Reseta sa.sender
		sa.sender.reseta();
	}
	// Envia pacote (método explícito)
	sa.sender.send();
}

</script>
<?php
	//var_dump($Abas->abas); exit();
	
?>
<!--TOPO-->
<?php include(SYS_SRC_APP.'views/'._module.'/pieces/common_top.php')?>
<!--MENU de navegação do sistema-->
<?php include(SYS_SRC_APP.'views/'._module.'/pieces/common_menu.php')?>
<!--CONTEUDO DA VIEW -->
<div class="content" style="margin-left:0;">
	<div class="main-content"> 
    	<!-- TITULO DA VIEW -->
        <?=$view->printTitleForm(_title,$view->isPortable())?>
        <!------------------------------------------------------------------
        --- Container ctPrincipal: Formulários de cadastro/informação
        ------------------------------------------------------------------->
        <div class="destaqueShadow bordaCinza hide" id="ctPrincipal">
            <!-- ABAS -->
            <ul class="nav nav-tabs" role="tablist">
            	<?php $view->imprimiAbas($Abas->abas); ?>
            </ul>
            <!-- FORMULARIOS (DE CADA ABA) -->
            <div class="tab-content" style="margin-top:15px;">
           		<?php $view->imprimiFormularios($Abas->abas)?>
            </div>
        </div>
        <?php
			// Se for Modo de edição, não mostra a listagem, apenas um cadastro especifico
			if($view->config()->modoEdicao==false) {
				$view->imprimiListagem($view->config());
			}
		?>
     </div>
</div>
<!--Janelas Modals-->
<?php 		
//Modal filtros de busca
include(SYS_SRC_APP.'views/'._module.'/pieces/common_modalSearch.php');

//Modal funções de listagem
include(SYS_SRC_APP.'views/'._module.'/pieces/common_modalFunctions.php');

// Common piece bottom
include(SYS_SRC_APP.'views/'._module.'/pieces/common_bottom.php')
?>
<script>
// Funções após tudo estar carregado :
$(document).ready(function() {
	setTimeout('loadListAgents()',3000);
	
	// Adiciona botão para gerar arquivo de exporatação dos dados paginados (filtrados)
	$($('#ctListagem').find('div')[0]).append('<button class="btn btn-success" onClick="exportarParaExcel()">Exportar para Excel</button>');
	
});
</script>
</body>
</html>