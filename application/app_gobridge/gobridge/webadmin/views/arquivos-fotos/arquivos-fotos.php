<?php	
	// Incorpora todas as abas/formularios/campos pelo arquivo {nome_view}.xml na pasta /templates
	$view->incorporateXML(false);
	
	// Configurações da View
	$view->config(array(
		'ID'			=> 0, 		// ID inicial para buscar os dados dos formularios
		'editMode'	=> false,	// Prepara o container principal apenas para editar determinado cadastro
		'normalMode'	=> false,	// Carrega todos containers e funções da View (CRUD)
	));
	
	// Define o título da página
	define("_title", "Arquivos e Fotos");
	
	// Define o título da página
	define("_columnID", "ANE_ID");
	
	// Define o controller
	define("_controller", "cArquivosObra");

	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php require SYS_SRC_APP.'views/pieces/common_header.php' ?>
</head>
<body class="theme-blue">
<script>


function buscaDadosCliente(cpf) {
	if(cpf!='') {
		azSender = new AzulSender();
		// Preapra os os cabeçalhos de 'payload':
		azSender.setPayload({
			action:'buscaDadosCliente'
		});
		// Prepara o envelope:
		azSender.setEnvelope(
			{
				filters : [{
					value1	:cpf,
					rule	:'=',
					columns	:['CLI_CPF']
				}]
			}
		);
		// Seta a função pós-processamento com sucesso
		azSender.callback = function(objRet) {
			// Se estiver em modo de debugação salva e imprime o retorno no console
			if(_DEBUG_MODE) _azGeneral.debug(objRet);
	
			//Preenche os campos com os dados do cliente
			$('#txtnomecliente').val(objRet.ret[0].CLI_NOME);
			$('#txtpontoscliente').val(objRet.ret[0].CLI_TOTALPONTOS);
			
			// Reseta sa.sender
			azSender.reset();
		}
		// Envia pacote
		azSender.send();	
	}
}


// Abre popups dinamicamente com ajax
function abrePopup(popupName, extra) {
	switch(popupName) {
		// Popup detalhe do orçamento
		case "popup-detalhe":
			// Dados do orçamento
			$('#popup-detalhe .numero').html(extra.orcamento.DHEMISSAO);
			$('#popup-detalhe .cliente').html('@'+extra.orcamento.USE_NICKNAME);
			$('#popup-detalhe .parceiro').html(extra.orcamento.PAR_FANTASIA);
			$('#popup-detalhe .emissao').html(extra.orcamento.ORC_DHEMISSAO);
			$('#popup-detalhe .qtdGeral').html(extra.orcamento.ORC_QTD);
			// Itens do orçamento
			var html = '';
			for(var i=0; i<extra.itens.length; i++) {
				var it = extra.itens[i];
				html = '<strong>Qtd:</strong> '+it.ORI_QTD+' <B>Desc.:</b> '+it.ORI_DESCRICAO+'<BR />';
			}
			$('#popup-detalhe .itens').html(html);
			break;
	}
	$('#'+popupName).modal('show');
}

</script>
<style>
.table-striped > tbody > tr:nth-of-type(odd) {
    background-color: #f5f5f5 !important;
	color: black !important;
}
</style>
<!--TOPO-->
<?php include(SYS_SRC_APP.'views/pieces/common_top.php')?>
<!--MENU de navegação do sistema-->
<?php include(SYS_SRC_APP.'views/pieces/common_menu.php')?>

<style>
.container_foto{
	background-color:white;
	padding:7px;
	border-radius: 5px;
	height:200px;
	width:100%;
	box-shadow: 1px 1px 5px 0.5px #555;
	margin-bottom:20px;
	overflow:hidden;

	background-repeat: no-repeat !important;
	background-position: center !important;
	background-size: cover !important;
	background-image: url(https://3dwarehouse.sketchup.com/warehouse/v1.0/publiccontent/6ce983b0-9e19-45f2-a722-c6d146bd5b82);
}
.thumbnail{
	background-color:white;
	padding:7px;
	border-radius: 5px;
	height:100px;
	width:16%;
	margin-bottom:20px;
	overflow:hidden;
	float:left;
	margin-right:5px;
	background-repeat: no-repeat !important;
	background-position: center !important;
	background-size: cover !important;
	background-image: url(https://3dwarehouse.sketchup.com/warehouse/v1.0/publiccontent/6ce983b0-9e19-45f2-a722-c6d146bd5b82);
}
.upload-thumbnail{
	background-color:white;
	padding:7px;
	height:350px;
	width:100%;
	margin-bottom:20px;
	background-repeat: no-repeat !important;
	background-position: center !important;
	background-size: cover !important;
}
.container_foto .legenda{
    position: absolute;
    bottom: 26px;
    color: #999;
	z-index:99;
}
.container_foto .fundo_legenda{
	width: 110%;
    height: 50px;
    background-color: black;
    margin-top: -10px;
    margin-left: -10px;
    margin-right: 0;
    margin-top: 144px;	
	opacity: 0.8;
}
.thumbnail .legenda{
	position: absolute;
    bottom: 23px;
    color: #999;
	z-index:99;	
}
.thumbnail .fundo_legenda{
	width: 120%;
    height: 45px;
    background-color: black;
    margin-top: -10px;
    margin-left: -10px;
    margin-right: 0;
    margin-top: 50px;	
	opacity: 0.8;
}
</style>

<!--CONTEUDO DA VIEW -->
<div class="content" style="margin-left:240px;">
	<div class="main-content" id="dropbox"> 
    
    	<!-- Campo para receber o arquivo a ser enviado -->
    	<input type="file" id="uploader" style="display:none">
        
        <button class="btn btn-default hide" id="btn_voltar" onClick="voltar_obras()">Voltar</button>
        
        <div class="row" id="ret_obras" ></div>
        
        
        <!-- container das fotos -->
        <div class="row hide" id="container_fotos" >
        	<div class="col-xs-12 col-md-12" >
            	<h3>Fotos</h3>
            </div>
        	<div class="col-xs-12 col-md-12" id="ret_fotos" last_ane_id="" >
            	
            </div>
             <div class="col-xs-12 col-md-12" >
            	<button class="btn btn-default" onClick="abreJanelaDeArquivos('fotos')" style="float:right; margin-right:15px"><i class="fa fa-upload"></i> Envia foto</button>
            </div>
        </div>
        
        <!-- container dos arquivos -->
        <div class="row hide" id="container_arquivos" style="margin-top:70px;">
        	<div class="col-xs-12 col-md-12" >
            	<h3>Arquivos</h3>
            </div>
        	<div class="col-xs-12 col-md-12" id="ret_arquivos" last_ane_id="" >
            	
            </div>
            <div class="col-xs-12 col-md-12" >
            	<button class="btn btn-default"  onClick="abreJanelaDeArquivos('arquivos')"  style="float:right; margin-right:15px"><i class="fa fa-upload"></i> Enviar arquivo</button>
            </div>
        </div>     
        
    </div>
</div>


<!--Popup para cadastro/visualização/download/edição dos dados/exclusão do arquivo-->
<div class="modal fade" id="popup-manage-file" tabindex="-1" role="dialog" >
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="height: 520px;">
      <div class="modal-body">
      	  <!-- Thumbnail -->
          <div class="upload-thumbnail"></div>
          <!-- Comentario -->
          <input type="text" class="form-control comentario" maxlength="150" placeholder="Insira aqui a descrição...">
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-danger hide" onClick="deletarArquivoFoto(this)">Deletar</button>
        <button type="button" class="btn btn-info hide" onClick="downloadFile(this)" >Baixar</button>
        <button type="button" class="btn btn-success" onClick="salvarArquivoFoto(this)" >Salvar</button>
      </div>
    </div>
  </div>
</div>

<!--Janelas Modals-->
<?php 		
//Modal funções de listagem
include(SYS_SRC_APP.'views/pieces/common_modalSearch.php');

// Common piece bottom
include(SYS_SRC_APP.'views/pieces/common_bottom.php')
?>

<script>
// Função para ocultar container das obras e mostra arquivos e fotos e vice-versa
function toggle_arquivos_obras() {
	$('#btn_voltar').toggleClass('hide');
	$('#ret_obras').toggleClass('hide');
	$('#container_fotos').toggleClass('hide');
	$('#container_arquivos').toggleClass('hide');
	
}
// Função para exibir as obras
function buscarMinhasObras() {
	az = new AzulSender();
	// Payload
	az.setPayload({
		controller: 'cMinhasObras',
		module: 'app',
		action: 'verificaMinhasObras',
		view: 'arquivos-fotos'
	});
	// Função retorno do servidor
	az.callback = function(objRet){
		for(var i =0; i < objRet.obras.length; i++) {
			var obra = objRet.obras[i];
			var html = '<div class="col-xs-12 col-md-3 " >'
			+'<a href="javascript: carregaArquivosFotos('+obra.OBR_ID+');">'
			+'<div class="container_foto" style="background-image: url('+obra.thumbnail+')">'
			+'	<div class="legenda">'
			+'	  <strong style="color:white">'+obra.OBR_ID+'-'+obra.OBR_TITULO+'</strong><br />'
			+'	  <small>'+obra.OBR_PROPRIETARIO_NOME+'</small>'
			+'	</div>'
			+'	<div class="fundo_legenda"></div>'
			+'</div>'
			+'</a></div>';
			$('#ret_obras').append(html);
		}
	}
	// Envio
	az.send();	
}
// Função para exibir as fotos e aquivos da obra selecionada
function carregaArquivosFotos(obrID){
	var last_id_foto = parseInt($('#container_fotos').attr('last_ane_id'));
	last_id_foto = isNaN(last_id_foto) ? 0 : last_id_foto;
	var last_id_arquivo = parseInt($('#container_arquivos').attr('last_ane_id'));
	last_id_arquivo = isNaN(last_id_arquivo) ? 0 : last_id_arquivo;
	// Inicia AzulSender
	az = new AzulSender();
	// Payload
	az.setPayload({
		controller : 'cArquivosObra',
		module	   : 'app',
		action	   : 'buscarFotosArquivosObra',
		view       : 'arquivos-fotos'
	});
	// Envelope
	az.setEnvelope({
		extra : {
			obrID: obrID,
			last_id_foto: last_id_foto,	
			last_id_arquivo:  last_id_arquivo,
		}
	});
	// Função retorno do servidor
	az.callback = function(objRet){
		// Salva o ID da obra no atributo obrID do botao "salvar
		$('#popup-manage-file').find('.btn-success').attr('obrID',obrID);
		// Mostra ou oculta os containeres (obras x arquivos da obra selecionada)
		if(!$('#ret_obras').hasClass('hide')){
			toggle_arquivos_obras();	
		}
		if(objRet.anexos != undefined) {
			for(var i =0; i < objRet.anexos.length; i++) {
				var anexo = objRet.anexos[i];
				var html = '<a href="javascript: return(false);" tipo="'+anexo.ANE_TIPO+'" fileName="'+anexo.ANE_NOMEARQUIVO+'" obrID="'+obrID+'" comentario="'+anexo.ANE_COMENTARIO+'" id="thumbnail'+anexo.ANE_ID+'" aneID="'+anexo.ANE_ID+'" onclick="manipularAnexo(this)">'
				+'<div class="thumbnail" style="background-image: url('+anexo.thumbnail+')">'
				+'	<div class="legenda">'
				+'	  <strong style="color:white" class="comentario">'+anexo.COMENTARIO+'</strong><br />'
				+'	  <small>'+_azDataManipulation.invertDate(anexo.ANE_DHENVIO)+'</small>'
				+'	</div>'
				+'	<div class="fundo_legenda"></div>'
				+'</div>'
				+'</a>';
				// Se for foto
				if(anexo.ANE_TIPO=='foto') {
					$('#ret_fotos').append(html);
					if(anexo.ANE_ID>last_id_foto) {
						last_id_foto = anexo.ANE_ID
						$('#container_fotos').attr('last_ane_id',anexo.ANE_ID);
					}
				// Arquivos
				}else {
					$('#ret_arquivos').append(html);
					if(anexo.ANE_ID>last_id_arquivo) {
						last_id_arquivo = anexo.ANE_ID
						$('#container_arquivos').attr('last_ane_id',anexo.ANE_ID);
					}
				}
			}
		}
	}
	// Envio
	az.send();	
}
// Função para abrir o popup para baixar/visualizar/editar[descrição]/excluir o anexo
function manipularAnexo(obj) {
	// Pega os dados contidos no thumbnail
	var aneID = $(obj).attr('aneID');
	var comentario = $(obj).attr('comentario');
	var fileName = $(obj).attr('fileName');
	var background_image = $(obj).find('.thumbnail').css('background-image');
	var tipo = $(obj).attr('tipo');
	// Mostra os botões ocultos
	$('#popup-manage-file').find('.btn-info').removeClass('hide');
	$('#popup-manage-file').find('.btn-danger').removeClass('hide');
	// Prepara o popup antes de exibir
	$('#popup-manage-file').find('.comentario').val(comentario);
	$('#popup-manage-file').find('.upload-thumbnail').css('background-image',background_image);
	$('#popup-manage-file').find('.btn-success').attr('action','alterarDescricao');
	$('#popup-manage-file').find('.btn-success').attr('aneID',aneID);
	$('#popup-manage-file').find('.btn-success').attr('tipo',tipo);
	$('#popup-manage-file').find('.btn-success').attr('fileName',fileName);
	// Exibe os popup já preparado para exibir a foto/arquivo e alterar a descrição,
	// fazer download ou deletar
	$('#popup-manage-file').modal('show');
}
/*
 @description Função para enviar e cadastrar um novo arquivo/foto
 			  ou alterar os dados de um arquivo/salvo já salvo anteriormente
*/
function salvarArquivoFoto(obj) {
	var action = $(obj).attr('action');
	var obrID = $(obj).attr('obrID');
	var aneID = $(obj).attr('aneID');
	// Instancia novo objeto AzulSender
	var azulsender = new AzulSender();
	// Instancia objeto FormData
	var formData = new FormData();
	// Payload
	formData.append('client', _SYS_CLIENT);
	formData.append('application', _SYS_APPLICATION);
	formData.append('module', 'app');
	formData.append('controller', _SYS_CONTROLLER);
	formData.append('view', _SYS_VIEW);
	formData.append('action', action);
	if(action=='receiveFile')
		formData.append('files', $('#uploader')[0].files[0]);
	// Envelope
	envelope = {
		extra : {
			aneID : aneID,
			obrID : obrID,
			comentario : $('#popup-manage-file .comentario').val()
		}
	};
	formData.append('envelopeJSON',JSON.stringify(envelope));
	// Estabelece a função de retorno
	azulsender.callback = function(data) {
		// Reseta o formulario
		$('#uploader').val('');
		$('#popup-manage-file .comentario').val();
		// Fecha o modal
		$('#popup-manage-file').modal('hide');
		// Exibe a miniatura(thumbnail) do arquivo recém enviado
		carregaArquivosFotos(obrID);
	}
	// Envia ao WS
	azulsender.send(false,formData);
}

// Função para deletar o arquivo
function deletarArquivoFoto(obj) {
	// Confirmação
	bootbox.confirm("Tem certeza que deseja deletar este arquivo?", function(result) {
	  if(result) {
		  	var _azSender7845 = new AzulSender;
		  	// Seta a ação em Payload
			_azSender7845.setPayload({
				module:'app',
				action:'deleteFile'
			});
			// Prepara o envelope
			_azSender7845.setEnvelope({
				extra : {
					obrID : $(obj).closest('div').find('.btn-success').attr('obrID'),
					aneID : $(obj).closest('div').find('.btn-success').attr('aneID'),
					fileName : $(obj).closest('div').find('.btn-success').attr('fileName'),
					tipo : $(obj).closest('div').find('.btn-success').attr('tipo')
				}
			});
			// Seta a função pós-processamento com sucesso
			_azSender7845.callback = function(objRet) {
				// Apaga coluna do arquivo deletado
				$('#thumbnail'+$(obj).closest('div').find('.btn-success').attr('aneID')).remove();
				// Fecha o modal
				$('#popup-manage-file').modal('hide');
				// Reseta sa.sender
				_azSender7845.reset();
			}
			// Envia pacote
			_azSender7845.send();
	  }else {
		   return(-1);
	  }
	});
}


/**
 *  @desc Solicita ação para download de arquivo na mesma view
 *	@param string fileName : nome do arquivo com extensao a ser baixado
 *	@param string qdUploadID : ID do quadro de Upload do arquivo a ser baixado
 *
 */
function downloadFile(obj) {
	var obrID = $(obj).closest('.modal-footer').find('.btn-success').attr('obrID');
	var aneID = $(obj).closest('.modal-footer').find('.btn-success').attr('aneID');
	var fileName = $('#thumbnail'+aneID).attr('fileName');
	var tipo = $('#thumbnail'+aneID).attr('tipo');
	// Instacia AzulSender
	var _azSender7845 = new AzulSender;
	// Seta a ação em Payload
	_azSender7845.setPayload({
		module: 'app',
		action:'downloadFile'
	});
	// Prepara o envelope
	_azSender7845.setEnvelope({
		extra:
		{
			fileName : fileName,
			obrID : obrID,
			aneID : aneID,
			tipo : tipo
		}
	});
	// Seta a função pós-processamento com sucesso
	_azSender7845.callback = function(objRet) {
		// Url do arquivo downloader, que retornará o arquivo propriamente dito
		var urlDOwnloader = _SYS_FRAMEWORK_URL+'downloader.php';
		// Abri a janela de download do navegador já com o arquivo em stream
		var iframe = $("<iframe/>").attr({
								id: 'iFrameDownloader',
								src: urlDOwnloader,
								style: "visibility:hidden;display:none"
							}).appendTo('body');

		// Reseta sa.sender
		_azSender7845.reset();
	}
	// Envia pacote
	_azSender7845.send();
}

/*
 @description Mostra um popup com a imagem ou nome do arquivo a ser enviado
*/
// Campo de upload da foto capa da obra
$('#uploader').on('change', function() {
	handleFiles(this.files);
});
function abreJanelaDeArquivos(tipo){
	$('#uploader').attr('tipo',tipo);	
	if(tipo=='fotos') {
		$('#uploader').attr('accept',"image/*");	
	}else {
		$('#uploader').attr('accept',"*");
	}
	$('#uploader').trigger('click');
}
/*
	Manipulação de arquivos com evento drop na tela(soltar)
*/
dropbox = document.getElementById("dropbox");
dropbox.addEventListener("dragenter", dragenter, false);
dropbox.addEventListener("dragover", dragover, false);
dropbox.addEventListener("drop", drop, false);
// Função ao entrar na região de upload
function dragenter(e) {
  e.stopPropagation();
  e.preventDefault();
}
// Função ao sair da região de upload
function dragover(e) {
  e.stopPropagation();
  e.preventDefault();
}
// Função ao soltar na região de upload
function drop(e) {
  e.stopPropagation();
  e.preventDefault();
  const dt = e.dataTransfer;
  const files = dt.files;
  handleFiles(files);
}
// Função para manipular o(s) arquivo(s) recebido(s)
function handleFiles(files) {
  $('#uploader')[0].files = files; 
  const preview = $('#popup-manage-file .upload-thumbnail')[0];
  for (var i = 0; i < files.length; i++) {
    const file = files[i];
    if (file.type.startsWith('image/')){
		gl_imagem = true;
	} else {
		gl_imagem = false;	
	}
	gl_fileName = file.name;
    //preview.appendChild(img);     
    const reader = new FileReader();
    reader.onload = (function( obj ) { 
		return function(e) { 
			if(gl_imagem) {
				$( obj ).css('background-image','url('+e.target.result+')'); 
			}else {
				$( obj ).css('background-image','url()');
				$( obj ).html('<h3>Arquivo: '+gl_fileName) 
			}
			// Mostra os botões ocultos
			$('#popup-manage-file').find('.btn-info').addClass('hide');
			$('#popup-manage-file').find('.btn-danger').addClass('hide');
			// Prepara o popup antes de exibir
			$('#popup-manage-file').find('.comentario').val('');
			$('#popup-manage-file').find('.btn-success').attr('action','receiveFile');
			$('#popup-manage-file').find('.btn-success').attr('aneID','');
			// Exibe os popup já preparado para exibir a foto/arquivo e alterar a descrição,
			// fazer download ou deletar
			$('#popup-manage-file').modal('show');
		}; 
	})(preview);
    reader.readAsDataURL( file );
  }
}

// Executa funções após Azul Framework estar completamente arregado
_azAzul.afterLoad.push(function() {
	buscarMinhasObras();
});
</script>
</body>
</html>