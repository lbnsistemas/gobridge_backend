<?php
	// Define o título da página
	define("_title", "WebAdmin");
	
	// Configurações da View
	$view->config(array(
		'ID'			=> 0, 		// ID inicial para buscar os dados dos formularios
		'editMode'	=> false,	// Prepara o container principal apenas para editar determinado cadastro
		'normalMode'	=> false,	// Carrega todos containers e funções da View (CRUD)
	));
?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<?php require SYS_SRC_APP.'views/pieces/common_header.php' ?>
</head>  
<body class="theme-blue">
<style>
body{
    background: #f1f1f1;
   /* background-image: url(<?=SYS_APP_URL.'../etc/images/fundo.jpg'?>);*/
    background-size: 100% 100% !important;
    background-repeat: no-repeat;	
	min-height: 100%;
	display: block;
	background-attachment: fixed;
}
</style>
<script>
/**
 * Função para efetuar login no sistema
 */
function doLogin() {
	// Realiza validação do formulario de login
	if(_azValidation.exec('Main')) {
		// Seta a action
		_azSender7845.setPayload({
			action: "doLogin"
		});
		// Prepara o envelope
		_azSender7845.setEnvelope({ 
			action : 'efetuarLogin', 
			fields : [
				{value : $('#txtusuario').val(), column : 'USER'},
				{value : $('#txtsenha').val(), column : 'PASS'}
			]
		});
		// Seta a função pós-processamento com sucesso
		_azSender7845.callback = function(objRet) { 
			<?php if(DEBUG_MODE) :?>_DEBUGGER = objRet;<?php endif;?>
			
			setTimeout("<?=(_view=='' || _view=='login' ? "window.location='".SYS_DOMAIN."webadmin/'" : 'window.location.reload();')?>",1000);
			
			// Reseta sa.sender
			_azSender7845.reset();
		}
		// Envia pacote
		_azSender7845.send();
	}
}
</script>
<style>
  .dialog {
	  float: none;
	  margin: 1.5em auto 0;
	  width: <?=$view->isPortable() ? '90%' : '30%'?>;
  }
</style>
    <!--TOPO-->
	<?php //include(SYS_SRC_APP.'etc/common/common_top.php')?>
    
    
    <div class="dialog">
        <img src="<?=SYS_APP_URL.'../etc/images/sem_imagem.jpg'?>" style=" display: block;
    margin-left: auto;
    margin-right: auto;
    width: 60%; margin-bottom:10px;"/>
        <div class="panel panel-default">
            <p class="panel-heading no-collapse">
             	<b><i class="fa fa-angle-right " aria-hidden="true"></i></b>&nbsp; Entre com seu <b>Login</b> e <b>Senha</b>                                  
            </p>
            <div class="panel-body">
                <form action="javascript:doLogin()" class="az-form main">
                    <div class="form-group">
                        <label  class="control-label"  for="txtusuario"><strong>Login</strong></label>
                        <input type="text" id="txtusuario" placeholder="CPF/CNPJ ou Email" class="form-control span12 az-field"  az-dataType="texto" az-required="true"><span class="help-block"></span>
                    </div>
                    <div class="form-group">
                    <label class="control-label"  for="txtsenha"><strong>Senha</strong></label>
                        <input type="password" id="txtsenha" class="form-control span12 az-field" az-dataType="senha" az-required="true">
                        <span class="help-block"></span>
                    </div>
                   	<a href="<?=SYS_DOMAIN?>lbncursos/recuperarSenha" target="_blank">Esqueceu a senha? </a>
                    <button type="submit" class="btn btn-primary pull-right">Entrar</button>
                    <div class="clearfix"></div>
                </form>
                 <p class="pull-right small cinza" style="text-align:right; margin:0;">
                 	<br> versão <?=SYS_VERSION?> 
                    <!--
                     <br><img src="https://www.lbnsistemas.com.br/imagem/logos/lbn_escuro.png" height="40">
                     -->
                 </p> 
            </div>
           
        </div>
        
    </div>
<?php
	// Common piece bottom
	include(SYS_SRC_APP.'views/pieces/common_bottom.php')
?>
</body>
</html>