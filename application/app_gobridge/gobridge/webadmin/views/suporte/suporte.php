<?php
	// Define o título da página
	define("_title", "Suporte - MasterBusiness");
	
	// Configurações da View
	$view->config(array(
		'ID'			=> 0, 		// ID inicial para buscar os dados dos formularios
		'editMode'	=> false,	// Prepara o container principal apenas para editar determinado cadastro
		'normalMode'	=> false,	// Carrega todos containers e funções da View (CRUD)
	));
?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<?php require SYS_SRC_APP.'views/pieces/common_header.php' ?>
</head>  
<body >
<style>
  .dialog {
	  float: none;
	  margin: 1.5em auto 0;
	  width: 70%;
  }
</style>
    <!--TOPO-->
	<?php include(SYS_SRC_APP.'views/pieces/common_top.php')?>
    
    
    <div class="dialog">
   	   <div class="panel panel-default" >
            <p class="panel-heading no-collapse"></p>
            <div class="panel-body">
                <div class="row">
                  <div class="col-xs-12 col-md-12">
                    <h2> Suporte </h2>
                    <img src="<?=SYS_APP_URL?>/etc/logo.jpg" width="150" style="left:50%; margin-left:-100px; position:relative; margin-top:15px;" />
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-md-12"><br />
                    Master Business 2017 - Versão <?=SYS_VERSION?><br />
                    Criado em em: 17/06/2017<br />
                    Última revisão em: 17/07/2017<br /><br />
                    Desenvolvido por <a href="http://www.lbnsistemas.com.br" target="_blank"><b>Lucas Nunes (LBN Sistemas)</b></a><br /> 
                    Email suporte: <a href='mailto:suporte@lbnsistemas.com.br'>suporte@lbnsistemas.com.br</a><br />
                    Contato comercial: <a href='#' onClick="window.open('https://api.whatsapp.com/send?phone=+5515974033594', '_system')"><i class="fa fa-whatsapp"></i>15 97403-3594</a><br /><br />
                   <i>Créditos ao idealizador, empreendedor e amigo: <br /><b>Jefferson Lunardon Ruiz.</b></i>
                 </div>
               </div>
            </div>
        </div>
    </div>
<?php
	// Common piece bottom
	include(SYS_SRC_APP.'views/pieces/common_bottom.php')
?>
</body>
</head>
</html>