﻿<?php
	// Seta as configurações da view
	$view->config(array("normalMode"=>false));
	
	// Define o título da página
	define("_title", "goBridge");
	
	// Inicia classe para manipular datas
	$objData = new Datatempo();
	
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php require SYS_SRC_APP.'views/pieces/common_header.php' ?>
    <script type='text/javascript' src='<?=SYS_DOMAIN.'_azul/plugins/chart.min.js'?>' ></script>
</head>
<body class="theme-blue">
<style>
.sa-form {
	margin-top:15px;
	padding:10px;
	border-radius: 3px;
}
 * {
	font-family: calibri;        
}
.box-chart {
	width: 98%;
	padding: 5px;
}

.photo_moldura {
     background-repeat: no-repeat !important;
     background-position: center !important;
     background-size: cover !important;
     height: 70px;
     width: 70px;
	 border-radius: 50%;		
}
.event {
	width:100%;
	min-height:100px;
	border-bottom:1px solid #ccc;	
	margin-bottom: 5px;
	padding:10px;
	border-radius:5px;
	background-color: white;
	float:left;
}

.event .profile{
	height:100%; width:15%; float:left	
}
.event .body{
	 width:85%; float:left;
}
.feed_brickbe {
	font-size: 16px;	
}
.feed_brickbe .quando{
	font-size: 14px;
	color: #999;
}
.feed_brickbe .usuario{
	font-weight:bold;	
}
@media screen and (max-width:600px) {
	.event .profile{
		width:16% !important;
	}
	.event .body{
		width:84% !important;
	}
	.photo_moldura{
		width: 40px !important;
		height: 40px !important;	
	}
}
</style>
<script>
/**
* Cria a nova demanda(solicitação) de documentos
*/
function concluir() {
	$('#mdConfirmacao').modal('hide');
	var _azSender7845 = new AzulSender();
	// Seta payload:
	_azSender7845.setPayload({
	  action:'criaNovaDemanda'
	});
	// Seta envelope:
	_azSender7845.setEnvelope({
		extra : {
			area : $('#listareas').val(),
			tipo : $('#listtipos').val(),
			filtros : $('#lisfiltros').val(),
			cliente : $('#listclientes').val(),
			email : $('#txtemail').val(),
			documentos : $('#listdocumentos').val()
		}
	});
	// Seta win callback
	_azSender7845.callback = function(objRet){
		// Link para compartilhamento
		$('#txtlink').val(objRet);
		
		// Monta mensagem para envio de email
		var cliente = $('#listclientes option:selected').text();
		var celular = $('#listclientes option:selected').attr('celular');
		var caso = $('#listcasos').val();
		var mensagem = 'Olá '+cliente+', \r\n';
        mensagem +='   Felipe, do Demarest, lhe enviou uma solicitação de documentos referente à '+caso+'. Por favor, clique no botão abaixo para responder.';
		$('#txtmensagem').val(mensagem);
		
		// Link para envio ao whatsapp
		$('#linkWhatsApp').attr('href',"https://api.whatsapp.com/send?phone="+celular+"&text="+mensagem);
		
		// Abre modal para compartilhamento
		$('#mdEnvioEMail').modal();
		_azSender7845.reset();
	}
	// Envia o pacote
	_azSender7845.send(false);
}

/**
*  Envia o link diretamente ao cliente
*/
function enviaEmail() {
	$('#mdEnvioEMail').modal('hide');
	var _azSender7845 = new AzulSender();
	// Seta payload:
	_azSender7845.setPayload({
	  action:'enviaLinkEmail'
	});
	// Seta envelope:
	_azSender7845.setEnvelope({
		extra : {
			nome : $('#listclientes option:selected').text(),
			email : $('#txtemail').val(),
			mensagem : $('#txtmensagem').val(),
			link : $('#txtlink').val(),
		}
	});
	// Seta win callback
	_azSender7845.callback = function(objRet){
		_azSender7845.reset();
	}
	// Envia o pacote
	_azSender7845.send(false);
}   
	   
// Função para carrega os Jobs para determinadas áreas
function carregaJobs(area) {
	// Trabalhista e Sindical
	if(area=='Trabalhista e Sindical') {
		$('#listtipos').html("<option value='Contestação'>Contestação</option>");
	// Due Diligence	
	}else if(area=='Due Diligence') {
		$('#listtipos').html("<option value='Mapa de Certidões'>Mapa de Certidões</option>");
	// Qualquer outra área	
	}else {
		$('#listtipos').html('');
	}
	
	// Mostra os filtros
	mostraFiltros($('#listtipos').val());
	
}

// Mostra os filtros relacionados a determinados jobs
function mostraFiltros(job) {
	// JOB Contestação 
	if(job=='Contestação' ){
		opcoes = ['HORAS EXTRAS','INSALUBRIDADE/PERICULOSIDADE','DOENÇA OCUPACIONAL', 'ACIDENTE DE TRABALHO', 'ACÚMULO, DESVIO DE FUNÇÃO/EQUIPARAÇÃO SALARIAL','HORAS INTINERE','MULTAS 477/467', 'DEVOLUÇÃO DE DESCONTOS', 'NULIDADE JUSTA CAUSA'];	
	// JOB Mapa de Certidões
	}else if(job=='Mapa de Certidões') {
		opcoes = ['Aguardando'];		
	}
	
	// Exibe os filtros
	$('#listfiltros').html('');
	for(var i in opcoes) {
		var opcao = '<option value="'+opcoes[i]+'">'+opcoes[i]+'</option>';
		$('#listfiltros').append(opcao);	
	}
}

// Mostra os documentos relecionados a detemrinado filtro/job
function mostraDocumentos(filtros) {
	console.log(filtros);
	var documentos = $('#listdocumentos')
	// Mostra os serviços com base nos filtros selecionados
	$('#listdocumentos').val(null).trigger('change');
	arDocSel = []; // Array com os documentos selecionados
	for(var i=0; i<filtros.length;i++ ) {
		var filtro = filtros[i];
		$('#listdocumentos').find("option[filtro='"+filtro+"']").each(function(index) {
			arDocSel.push(this.value);
		});
	}
	$('#listdocumentos').val(arDocSel).trigger('change');
}

// Função para mostrar o status da solicitação
function mostrarStatusSolicitacao(i,HashID) {
	var documentos = gl_solicitacoes[i];
	$('#tbRecebimentos tbody').html('');
	for(var e in  documentos) {
		var doc = documentos[e];
		// Se estiver RECEBIDO, mostra o botão para download do documento
		if(doc.Status=='RECEBIDO' || doc.Status=='REJEITADO' || doc.Status=='APROVADO') {
			var html = '<a href="'+doc.Link_Download+'" target="_blank"><button class="btn btn-info"><i class="glyphicon glyphicon-download" ></i></button></a> ';
			
			// Somente se estiver como RECEBIDO ou Rejeitado, permite aprovar
			if(doc.Status=='RECEBIDO' || doc.Status=='REJEITADO'){
				html += '<button class="btn btn-success"><i class="glyphicon glyphicon-thumbs-up"></i></button>';
			}
			
			// Somente se estiver como RECEBIDO, permite rejeitar
			if(doc.Status=='RECEBIDO') {
				html += '<button class="btn btn-danger"><i class="glyphicon glyphicon-thumbs-down"></i></button>';	
			}
			
			
		// Mostra apenas o status	
		}else {
			var html = doc.Status;
		}
		var html = '<tr><td>'+doc.ENV_ID+'</td><td>'+doc.Titulo+'</td><td>'+html+'</td></tr>'	;
		$('#tbRecebimentos tbody').append(html);
	}
	$('#mdDocumentosRecebidos .modal-title span').html(documentos[0].DEM_ID+' - <a href="https://link.gobridge.com.br/'+HashID+'" target="_blank">acessar link</a>');
	$('#mdDocumentosRecebidos').modal();
} 	

// Função para requisitar as solicitações em ABERTO
gl_solicitacoes = [];
function requestData() {
	var _azSender7845 = new AzulSender();
	// Seta payload:
	_azSender7845.setPayload({
	  action:'requestData'
	});
	// Seta os dados do envelope
	_azSender7845.setEnvelope({
		limit:50,
		init:0,
		orderBy:'DataHora_criacao'
	});
	// Seta win callback
	_azSender7845.callback = function(objRet){
		$('#tbSolicitacoes tbody').html('');
		gl_documentos = [];
		for(var i in objRet.ret) {
			var demanda = objRet.ret[i];
			gl_solicitacoes.push(demanda.solicitacoes);
			var htmlLinha = '<tr><td>'+demanda.DEM_ID+'</td>'+
							'<td>'+demanda.Nome_Razao+'</td>'+
							'<td>'+demanda.Titulo+'</td>'+
							'<td>'+demanda.Qtd_resposta+'/'+demanda.Qtd_requisicao+'</td>'+
							'<td>'+demanda.DataHora_criacao+'</td>'+
							'<td><button class="btn btn-info" onclick="mostrarStatusSolicitacao('+i+',\''+demanda.HashID+'\')">ver</button></td></tr>';
			$('#tbSolicitacoes tbody').append(htmlLinha);
		}
		_azSender7845.reset();
	}
	// Envia o pacote
	_azSender7845.send(true);
}


// Função para copiar para área de transferência (CTRL + C)
function copyToClipboard(elem) {
	  // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);
    
    // copy the selection
    var succeed;
    try {
    	  succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    
    return succeed;
}
</script>	
<!--Topo-->
<?php include(SYS_SRC_APP.'views/pieces/common_top.php')?>
<!--Conteúdo -->
<div class="content" >
	<!-- Estatisticas -->
    <div class="row" >
    	<div class="col-md-12">
        	<button class="btn btn-success float-right" onClick="$('#mdSolicitacao').modal()">PEDIR DOCUMENTOS</button>
        </div>
    	<div class="col-md-8 col-xs-12">
            <div class="feed_brickbe" id="feed_brickbe">
            	<table class="table" id="tbSolicitacoes">
            		<thead>
                    	<tr>
                        	<th>#</th>
                            <th>Cliente</th>
                            <th>Tipo</th>
                            <th>Qtd</th>
                            <th>Data envio</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>
    <!-- /Estatísticas -->
</div>
<!-- /Conteúdo -->

<!-- Janelas Modals-->

<!-- Modal para ver os ítens recebidos -->
<div class="modal fade" id="mdDocumentosRecebidos"  role="dialog" aria-labelledby="mdDocumentosRecebidos" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Solicitação NR. <span></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<table class="table" id="tbRecebimentos">
			<thead>
            	<tr>
                	<th>#</th>
                    <th>Documento</th>
                    <th>Status</th>
                </tr>
            </thead> 
            <tbody>
            	
            </tbody>       
        </table>
      </div>
    </div>
  </div>
</div>


<!-- Modal para realizar o envio do email para o cliente com o link da solicitação-->
<div class="modal fade" id="mdEnvioEMail"  role="dialog" aria-labelledby="mdSolicitacao" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">ENVIO DO LINK</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	 <form>
          <div class="form-group">
          	 <label for="listarea" class="col-form-label"><i class="glyphicon glyphicon-copy" onClick="copyToClipboard($('#txtlink')[0])"></i> Link: </label> 
             <input type="text" class="form-control" id="txtlink" onClick="copyToClipboard(this)" value="" />
             
          </div>
           <div class="form-group">
          	 <label for="listarea" class="col-form-label">Mensagem email:</label>
             <textarea rows="8" class="form-control" id="txtmensagem" >
             </textarea>
          </div>
        </form>
      </div>
       <div class="modal-footer">
       	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <a href='' target="_blank" id="linkWhatsApp">
        <button type="button" class="btn btn-success">Enviar por WhatsApp</button>
        </a>
        
        <button type="button" class="btn btn-primary" onClick="enviaEmail()">Enviar por email</button>
      </div>
    </div>
  </div>
</div>
     
<!-- Modal para realizar nova solicitação -->
<div class="modal fade" id="mdSolicitacao"  role="dialog" aria-labelledby="mdSolicitacao" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">CRIAR SOLICITAÇÃO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="listareas" class="col-form-label">Área:</label>
            <select class="form-control select2" name="listareas" id="listareas" onChange="carregaJobs(this.value)">
            	<option  value=""></option>
            	<option  value="Ambiental">Ambiental</option>
				<option  value="Aviação">Aviação</option>
                <option  value="Bancos e serviços financeiros">Bancos e serviços financeiros</option>
                <option  value="Compliance e ética corporativa">Compliance e ética corporativa</option> 
                <option  value="Contencioso e Arbitragem">Contencioso e Arbitragem</option> 
                <option  value="Direito Concorrencial">Direito Concorrencial</option>
                <option  value="Direito Empresarial">Direito Empresarial</option>  
                <option  value="Direitos Humanos">Direitos Humanos</option>  
                <option  value="Direito Penal">Direito Penal</option> 
                <option  value="Direito Público">Direito Público</option> 
                <option  value="Direito Previdenciário">Direito Previdenciário</option> 
                <option value="Due Diligence">Due Diligence</option>  
                <option  value="Família e Sucessões">Família e Sucessões</option>  
                <option  value="Fundos de investimento">Fundos de investimento</option> 
                <option  value="Gestão Patrimonial">Gestão Patrimonial</option>  
                <option  value="Infraestrutura e energia">Infraestrutura e energia</option>  
                <option  value="Mercado de capitais">Mercado de capitais</option>  
                <option  value="Negócios Imobiliários">Negócios Imobiliários</option>  
                <option  value="Organizações da sociedade civil">Organizações da sociedade civil</option>  
                <option  value="Proteção de Dados e Cybersecurity">Proteção de Dados e Cybersecurity</option>  
                <option  value="Recuperação judicial">Recuperação judicial</option>  
                <option  value="Relações governamentais">Relações governamentais</option>  
                <option  value="Seguros e previdência privada">Seguros e previdência privada</option>  
                <option  value="Societário">Societário</option> 
                <option  value="Tecnologia">Tecnologia</option>  
                <option  value="Inovação e Negócios Digitais">Inovação e Negócios Digitais</option>  
                <option value="Trabalhista e Sindical">Trabalhista e Sindical</option>  
                <option  value="Tributário">Tributário</option>  
            </select>
          </div>
          <div class="form-group">
            <label for="listtipos" class="col-form-label">Job:</label>
            <select class="form-control select2"  id="listtipos" onChange="mostraFiltros(this.value)">             
            </select>
          </div>
          <div class="form-group">
            <label for="listfiltros" class="col-form-label">Filtros/Pedidos:</label>
            <select class="form-control select2" id="listfiltros" name="listfiltros[]" multiple="multiple" onChange="mostraDocumentos($('#listfiltros').val())">             
            </select>
          </div>
          <div class="form-group">
            <label for="listclientes" class="col-form-label">Cliente:</label>
            <select class="form-control select2" id="listclientes" onChange="$('#txtemail').val($(this).find('option:selected').attr('email'))" >
            	<option value=""></option>
            	<?php
					// SQL string para encontrar os clientes do escritorio logado
					$sql = "SELECT * FROM clientes WHERE ESC_ID='".$_SESSION[SES_USER_ID]."'";
					$r = $azul_DB->query($sql);
					while($row = $r->fetch_assoc()) {
						echo "<option value='".$row['CLI_ID']."' celular='".$row['Celular']."' email='".$row['Email']."'>".$row['Nome_Razao']."</option>";	
					}
				?>            
            </select>
          </div>
          <div class="form-group">
            <label for="listcasos" class="col-form-label">Caso:</label>
            <select class="form-control select2" id="listcasos" onChange="">
            	<option value="Reclamação trabalhista Dulce Aparecido">Reclamação trabalhista Dulce Aparecida</option>
            </select>
          </div>
          <div class="form-group">
            <label for="txtemail" class="col-form-label">Email:</label>
            <input type="text" class="form-control" id="txtemail" />
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary" onClick="$('#mdSolicitacao').modal('hide'); $('#mdConfirmacao').modal()">Avançar</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal para confirmar os documentos a serem solicitados -->
<div class="modal fade" id="mdConfirmacao" tabindex="-1" role="dialog" aria-labelledby="mdConfirmacao" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">CONFIRMAR DOCUMENTOS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="row" >
          	<select id="listdocumentos" name="listdocumentos[]" class="select2" multiple>
            	<option value=""></option>
                <?php
					$sql = " SELECT * FROM documentos ";
					$r = $azul_DB->query($sql);
					while($row = $r->fetch_assoc()) {
						echo "<option value='".$row['DOC_ID']."' filtro='".$row['Filtros']."'>".$row['Titulo']."</option>";	
					}
				?>
            </select>
          </div>
        </form>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-default" data-dismiss="modal" onClick="$('#mdSolicitacao').modal()">Voltar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-success" onClick="concluir()">Concluir</button>
      </div>
    </div>
  </div>
</div>

<?php 		
// Common piece bottom
include(SYS_SRC_APP.'views/pieces/common_bottom.php')
?>
<!-- /Janelas Modals-->
<script>
$(document).ready(function() {
	// Adiciona determinadas funções para serem executadas após o carregamento do Framework Azul
	_azAzul.afterLoad.push(function(){
		// Inicia select 2
		$('.select2').select2();
		// Requisita os dados da dashboard
		requestData();	
	});
});
</script>
</body>
</html>