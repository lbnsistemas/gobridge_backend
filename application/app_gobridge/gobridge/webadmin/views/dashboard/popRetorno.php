﻿<!--Modal para os filtros -->
<div class="modal fade" id="popRetorno" tabindex="-1" role="dialog" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<div class="modal-header">
             <button type="button" class="close"  data-toggle="tooltip" data-placement="bottom" title="Para fechar esta janela é necessário registrar o feedback da última ligação.">&times;</button>      
       		 <h4 class="modal-title">Tabulação de retorno de chamada</h4>
         </div>
          <!-- Corpo-->
          <div class="modal-body">
          		<div class="row">
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <label for="txtcontato">Contato:</label>
                            <input type="text"  sa-defaultValue="" sa-tipoDados="texto" 
                            id="txtcontato" class="form-control"  size="25" >
                         </div>
                         <div class="form-group col-sm-4">
                            <label for="txtnumero">Número:</label>
                            <input type="text"  sa-defaultValue="" id="txtnumero" class="form-control" 
                            value="" a-tipoDados="texto"  disabled size="25" >
                         </div>
                          <div class="form-group col-sm-4">
                            <label for="liststatus">Status:</label>
                            <select id="liststatus" class="form-control" a-tipoDados="texto"  >
                            	<option value="RETURNED">RETORNADO</option>
                                <option value="UNSUCCESSFUL">INSUCESSO</option>
                                
                                <option value="ABANDONED">ABANDONADO</option>
                            </select>
                         </div>
                     </div>
                     <div class="row">
                         <div class="form-group col-sm-12">
                            <label for="txtdesc">Descrição:</label>
                            <textarea id="txtdesc" class="form-control" a-tipoDados="texto" ></textarea>
                         </div>
                     </div>
                 </div>
          </div>
          <!--Rodape-->
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" onClick="salvarRetornoChamada()">Salvar</button>
          </div>
    </div>
  </div>
</div>