﻿<?php
	//Uso do webservice: GET/POST {dominio}/central/{serviceName}	
	//Inicia o controller da view
	require(SYS_SRC_CONTROLLERS.'/cWebServices/cWebServices.php');
	$con = new cWebServices($httpResponse=false);	
	/** 
	 * @description: Decadastrar-se dos emails BrickBe
	 * @method: GET
	 * @fields: {codigo,email}
	 *
	**/
	if($con->serviceName=='descadastrar-email') {
		$ret = $con->decadastrarEmail();
		if($ret->type=='win'){
			echo "<h2> Seu email já foi descadastrado da nossa base de envio, desculpe pelo transtorno. </h2>";	
		}else {
			echo "<h2 style='color:red'>".$ret->message."</h2>";	
		}
	/** 
	 * @description: Retorna os documentos a serem enviados pelo cliente
	 * @method: GET
	 * @fields: {HashID}
	 *
	**/
	}else if($con->serviceName=='documentRequest') {
		$ret = $con->documentRequest();
	
	/** 
	 * @description: Recebe o documento e a hash da demanda, salva o arquivo no servidor provisóriamente e atualiza
	 * as informações da demanda
	 * @method: GET
	 * @fields: {files, ENV_ID}
	 *
	**/
	}else if($con->serviceName=='documentUpload') {
		$ret = $con->documentUpload();
		
		
	/** 
	 * @description: Recebe o link do upload feito para uma outra plataforma (dropbox, drive, ...)
	 * @method: GET
	 * @fields: {url, ENV_ID}
	 *
	**/
	}else if($con->serviceName=='documentURL') {
		$ret = $con->documentURL();
	/** 
	 * @description: Enviar proposta (fornecedor)
	 * @method: GET
	 * @fields: {codigo,email}
	 *
	**/
	}else if($con->serviceName=='responder-solicitacao') {
		$ret = $con->registrar_callToAction();
		echo "<script>window.location='https://brickbe.com.br/solicitacao/?id=".$_GET['id']."&codigo=".$_GET['codigo']."'</script>";	
	/** 
	 * @description: Visualizar Proposta do Fornecedor
	 * @method: GET
	 * @fields: {codigo,email}
	 *
	**/
	}else if($con->serviceName=='visualizar-proposta') {
		$ret = $con->registrar_callToAction();
		echo "<script>window.location='https://brickbe.com.br/resposta-solicitacao/?id=".$_GET['id']."&codigo=".$_GET['codigo']."'</script>";
	/** 
	 * @description: Acessar a área do parceiro
	 * @method: GET
	 * @fields: {codigo,email}
	 *
	**/
	}else if($con->serviceName=='acessar-area-parceiro') {
		$ret = $con->registrar_callToAction();
		echo "<script>window.location='https://brickbe.com.br/webadmin/?codigo=".$_GET['codigo']."'</script>";
	/** 
	 * @description: Acessar aplicativo
	 * @method: GET
	 * @fields: {codigo,email}
	 *
	**/
	}else if($con->serviceName=='acessar-aplicativo') {
		$ret = $con->registrar_callToAction();
		echo "<script>window.location='https://brickbe.com.br/app/?codigo=".$_GET['codigo']."'</script>";	
		
	/** 
	 * @description: Cadastro de cliente (convite recebido por email)
	 * @method: GET
	 * @fields: {codigo,email}
	 *
	**/
	}else if($con->serviceName=='cadastrar-cliente') {
		$ret = $con->registrar_callToAction();
		echo "<script>window.location='https://brickbe.com.br/cadastro-cliente/?codigo=".$_GET['codigo']."'</script>";	
		
	//Nome de serviço desconhecido
	}else {
		die('Serviço inválido.');	
	}
	
	
	
?>