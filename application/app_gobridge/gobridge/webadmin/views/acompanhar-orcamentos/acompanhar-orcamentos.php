<?php	
	// Incorpora todas as abas/formularios/campos pelo arquivo {nome_view}.xml na pasta /templates
	$view->incorporateXML(true);
	
	// Configurações da View
	$view->config(array(
		'ID'			=> 0, 		// ID inicial para buscar os dados dos formularios
		'editMode'	=> false,	// Prepara o container principal apenas para editar determinado cadastro
		'normalMode'	=> true,	// Carrega todos containers e funções da View (CRUD)
	));
	
	// Define o título da página
	define("_title", "Orçamentos");
	
	// Define o título da página
	define("_columnID", "ORC_ID");
	
	// Define o controller
	define("_controller", "cMain");

	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php require SYS_SRC_APP.'views/pieces/common_header.php' ?>
</head>
<body class="theme-blue">
<style>
.modal-content .row .col-xs-12 {
  white-space: nowrap;                
  overflow: hidden;              /* "overflow" value must be different from "visible" */ 
  text-overflow:    ellipsis;	
}
</style>
<script>
/**
 * Paginação no navegador(JS) até limite máximo estabelecido ($maxRows), acima disso é por ajax(PHP)
 * @param int init indice de inicio para mostrar x linhas a partir deste ponto
 */
function user_prepareContainer(action,ID,objRet){
	
	var orcamento = objRet.orcamentos[0];
	var itens = objRet.orcamentos_itens;
	var envios = objRet.orcamentos_envios;
	var respostas = objRet.orcamentos_respostas;
	
	// Dados da solicitação
	$('#popup-detalhe').find('.numero').html(orcamento.ORC_ID);
	$('#popup-detalhe').find('.obra').html(orcamento.OBR_TITULO);
	$('#popup-detalhe').find('.proprietario').html(orcamento.OBR_PROPRIETARIO);
	$('#popup-detalhe').find('.emissao').html(orcamento.ORC_DATAEMISSAO);
	$('#popup-detalhe').find('.quantidade').html(orcamento.ORC_QTD);
	$('#popup-detalhe').find('.status').html(orcamento.ORC_STATUS);
	
	// Itens
	$('#popup-detalhe .itens').html('');
	for(var i in itens) {
		var iten = itens[i];
		var html = '<div class="row">'	
					  +'<div class="col-xs-3 col-md-2 col-lg-">'
							+iten.ORI_QTD
						 +'</div>'								
						 +'<div class="col-xs-9 col-md-3 col-lg-">'
							  +iten.ORI_DESCRICAO
						 +'</div>'
					 +'</div>';
		$('#popup-detalhe .itens').append(html);
	}
	
	// Envios
	$('#popup-detalhe .envios').html('');
	for(var i in envios) {
		var envio = envios[i];
		var html = '<div class="row">'	
					  +'<div class="col-xs-4 col-md-3 "><small>'
							+_azDataManipulation.invertDate(envio.ORE_DHENVIO)
						 +'</small></div>'								
						 +'<div class="col-xs-4 col-md-3 ">'
							  +envio.ORE_EMPRESA
						 +'</div>';
		if(!_IS_PORTABLE)
		html += '<div class="col-xs-12 col-md-4 ">' +envio.ORE_EMAIL+'</div>'
			
		html			 += '<div class="col-xs-4 col-md-2 ">'
							  +envio.ORE_RESPONSAVEL
						 +'</div>'
					 +'</div>';
		$('#popup-detalhe .envios').append(html);
	}
	
	// Respostas
	$('#popup-detalhe .respostas').html('');
	for(var i in respostas) {
		var resposta = respostas[i];
		var html = '<div class="row">'	
					  +'<div class="col-xs-4 col-md-3 "><small>'
							+_azDataManipulation.invertDate(resposta.AZ_DHREGISTER)
						 +'</small></div>'								
						 +'<div class="col-xs-4 col-md-2 ">'
							  +resposta.ORE_EMPRESA
						 +'</div>';
		if(!_IS_PORTABLE)
		html += '<div class="col-xs-4 col-md-2 ">' +resposta.ORR_QTD+'</div>'
						 +'<div class="col-xs-12 col-md-3 ">'
							  +'R$ '+_azDataManipulation.floatToCurrency(resposta.ORR_TOTALLIQUIDO)
						 +'</div>';
						
		html			 +='<div class="col-xs-4 col-md-2 ">'
							  +'<a href="<?=SYS_DOMAIN?>central/visualizar-proposta/?id='+resposta.ORR_ORC_ID+'&codigo='+resposta.ORR_TOKEN+'" class="btn btn-info" target="_blank">Abrir</a>'
						 +'</div>'
					 +'</div>';
		$('#popup-detalhe .respostas').append(html);
	}
	// Abre a modal com todos os dados da solicitação de orçamento
	$('#popup-detalhe').modal('show');
}

/**
 * Paginação no navegador(JS) até limite máximo estabelecido ($maxRows), acima disso é por ajax(PHP)
 * @param int init indice de inicio para mostrar x linhas a partir deste ponto
 */
function showData(init){
	// Altera a global que armazena a pagina atual
	//_PAGINA_ATUAL = init;
	// Limpa as linhas já existentes na listagem
	$('#tbData tbody').html('');
	// Loop master
	for ( i=init; i < ( init + _azListingObject.maxRowsPage ) & i < _azListingObject.data.length; i++ ) {
		// Linha com todas as colunas disponibilizadas em objListagem.data
		objRow = _azListingObject.data[i];
		ID = objRow['<?=_columnID?>'];
		
		//var infoAdc = "Registrado por: " + objRow['AZ_USREGISTER'] + " \r\n Data/Hora: "+objRow['AZ_DHREGISTER'];
		var infoAdc = '';
		var linha  = "<tr data-toggle='tooltip' data-placement='top' title='"+infoAdc+"' onclick=\"$(this).toggleClass('row_select')\" id='rowList"+ID+"' az-ID='"+ID+"'>";
		
		// Preenche todas as colunas visiveis da linha retornada pelo WS
		var contColVis = 0;// Contagem de colunas vísiveis exibidas
		for (c in _azListingColumnsArray) {
			var infoAdc = '';
			coluna = _azListingColumnsArray[c];
			// Se for  dispositivo portabil : (REVER ESTA LOGICA)
			//colspan = (_IS_PORTABLE==true ? '2' : '');
			colspan = '';
			
			// Se for mobile device e não for a coluna NR, Data ou Status
			if(_IS_PORTABLE==true && coluna!="ORC_ID" && coluna!="ORC_DATAEMISSAO" && coluna!="ORC_STATUS") continue;
			// Exibi coluna com possiveis informações adc. na primeira coluna (caso seja movel) ou na segunda coluna em PC/Notebook
			if(((contColVis==1 && _IS_PORTABLE==false) || (contColVis==0 && _IS_PORTABLE==true)) 
			&& objRow['AZ_ARCHIVED']==true) {
				infoAdc = objRow['AZ_USARCHIVED']+" - "+objRow['AZ_DHARCHIVED'];
				linha += "<td colspan='"+colspan+"'>"+objRow[coluna]+" <span class='label label-default'>Arquivado por "+infoAdc+"</span></td>";		
				
			// Exibe o conteudo da coluna
			} else {
				// Tratamento da data de emissão do orçamento
				if(coluna=='ORC_DATAEMISSAO'){
					conteudoColuna='<small>'+_azDataManipulation.invertDate(objRow[coluna])+'</small>';
		
				// Demais campos	
				}else {
					conteudoColuna=objRow[coluna];	
				}
				
				// Se estiver bloqueado, exibe botão laranja
				if(contColVis==1 && objRow['ORC_STATUS']==="Enviado") {
					infoAdc = " <span class='label label-danger'>Enviado</span>";
				}
				
				// Incrementa uma coluna na linha
				linha += "<td colspan='"+colspan+"'>"+conteudoColuna+infoAdc+"</td>";
			}
			
			// Incrementa contador de colunas visiveis
			contColVis++;
		}	
		// Botao alterar
		//btAlterar = "<span class='glyphicon glyphicon-pencil handCursor'  data-toggle='tooltip' data-placement='top'"
					//+" title='Alterar cadastro' onclick='fillOutForms("+ID+")'></span> &nbsp; &nbsp;";
		btVisualizar = '<i class="fa fa-info-circle handCursor" style="font-size:18px" onclick="fillOutForms('+ID+', this)"></i> &nbsp; &nbsp;';
		btnEnviar = '<i class="fa fa-arrow-right handCursor" onclick="carregaFornecedores('+ID+',\''+objRow['OBR_ID']+'\')" style="font-size:18px"></i>';
		
		// Se estiver arquivado, não permite nenhuma edição
		if(objRow['AZ_ARCHIVED']==true) {
			btVisualizar='';	btnEnviar='';
		}
		
		// Coluna funções (alterar e remover cadastro) (apenas pc e notebooks)
		linha += "<td align='right'>"+btVisualizar+" "+btnEnviar+"</td>";
		
		// Fecha linha
		linha += "</tr>";
		
		// Incrementa linha no corpo da tabela
		$('#tbData tbody').append(linha);
			
		// Inicia todas as tooltips (pop-up informativo)
		$('[data-toggle="tooltip"]').tooltip(); 
	}
	// Atualiza paginação
	nrPaginas = Math.round((_azListingObject.data.length/_azListingObject.maxRowsPage));
	html = '';
	for(i=0; i < nrPaginas;i++ ) {
		html += '<li><a href="#" onclick="showData('+(i*_azListingObject.maxRowsPage)+')">'+(i+1)+'</a></li>';	
	}
	$('ul.pagination').html(html);
}


// Carrega e preenche o list dos fornecedores (trazendo no topo os que já vem sendo cotado para a obra selecionada)
function carregaFornecedores(orcID, obrID) {
	var az = new AzulSender();
	az.setPayload({
		module: 'app',
		controller: 'cMain',
		action:'carregaParceiros',
		view: 'solicitar-orcamento'
	});
	az.setEnvelope({
		extra : {
			ORC_OBR_ID : obrID	
		}
	});
	az.callback = function(objRet) {
		var parceiros = objRet.parceiros;
		if(parceiros.length>0){
			$('#list_dest_fornecedor').html('');
			//var htmlOption = '<option value=""  ></option>';
			//$('#list_compra_obra').append(htmlOption);
			for(var i in parceiros){
			  var row = parceiros[i];
			  var htmlOption = '<option value="" par_id="'+row.PAR_ID+'" full_email="'+row.EMAIL+'" email="'+row.EMAIL_RESUMIDO+'" responsavel="'+row.RESPONSAVEL+'" orc_id="'+row.ORC_ID+'" >'+row.FORNECEDOR+'</option>';
			  $('#list_dest_fornecedor').append(htmlOption);
			}
			// Dispara evento Change para carrega 
			$('#list_dest_fornecedor').trigger('change');
			
			// Reinicia select 2
			$('#list_dest_fornecedor').select2('destroy');
			$('#list_dest_fornecedor').select2({tags:true});
			
			// ID do orçamento
			$('#popup-encaminhamento').find('.btn-success').attr('orcID', orcID);
			
			// Se o formulario estiver oculto, mostra-o
			$('#popup-encaminhamento').modal('show');
		 }	
		// Reseta sa.sender
		az.reset();
	};
	az.send(true);	
}

// Função para preencher o email, email resumido e nome do responsavel
function preencheCamposDest(obj){
	var option = $(obj).find('option:selected');
	// Preenche o email
	$('#txt_dest_email').val($(option).attr('email'));
	// Preenche o email completo
	$('#txt_dest_email').attr('full_email',$(option).attr('full_email'));
	// Preenche o responsavel
	$('#txt_dest_responsavel').val($(option).attr('responsavel'));
	// Se já for email cadastrado na base BrickBe, bloqueia o campo email e responsavel
	// e mostra parcialmente o email
	if($(option).attr('orc_id')>0 || $(option).attr('par_id')>0) {
		$('#txt_dest_email').attr('disabled',true);
		$('#txt_dest_responsavel').attr('disabled',true);
	}else {
		$('#txt_dest_email').attr('disabled',false);
		$('#txt_dest_responsavel').attr('disabled',false);
	}
}


// Incluir novo destinario
// Incluir novo destinario
function incluirNovoDestinatario() {
	if($('#txt_dest_email').val()=='') {
		return(false);	
	}
	var nrDest = $('#formDestinatarios .row:not(:first)').length+1;
	var empresa = $('#list_dest_fornecedor option:selected').text();
	var parID = $('#list_dest_fornecedor option:selected').attr('par_id');
	if(parID==undefined) parID = 0;
	var email = $('#txt_dest_email').val();
	var responsavel = $('#txt_dest_responsavel').val();
	
	// Duplica a primeira linha
	var objLinha = $('#formDestinatarios .row:first').clone();
	
	// Cria campo input com nome da empresa
	$(objLinha).find('.col-xs-12:eq(0) .form-group').html("<input type='text' par_id='"+parID+"' value='"+empresa+"' class='fornecedor form-control az-field' id='list_dest_fornecedor' disabled=true />");
		
	// Tira o botão mais e coloca apenas o botão 'x'
	$(objLinha).find('.col-xs-1').html('<span class="glyphicon glyphicon-remove handCursor"  title="" onclick="removeLinha(this)" style="margin-top:8px"></span>');
	
	// Remove todos os labels
	$(objLinha).find('label').remove();
	
	// Trava todos os campos para evitar alteração (somente exclusão)
	$('#formDestinatarios .row:not(:first) .az-field').attr('disabled',true);
	
	// Exibe a nova linha
	$('#listformDestinatarios').append($(objLinha)[0].outerHTML);
	
	// Acerta todos os IDs
	$('#listformDestinatarios .row:last').find('input').each(function() {
		//console.log(this);
		var oldID = $(this).attr('id');
		$(this).attr('id', oldID + nrDest);
		$(this).attr('value', $('#'+oldID).val());
		$(this).val($('#'+oldID).val());
		if(oldID=='txt_dest_email'){
			$(this).attr('full_email', $('#'+oldID).attr('full_email'));
			
		}
		if(oldID=='list_dest_fornecedor'){
			$(this).val($('#'+oldID).find('option:selected').text());
		}
		
	});
	// Reseta os campos
	$('#txt_dest_email').attr('full_email','');
	$('#txt_dest_email').val('');
	$('#txt_dest_responsavel').val('');

}

// Remover linha (item ou destinatario)
function removeLinha(obj) {
	$(obj).closest('.row').remove();
}


// Função para encaminhar ou reencaminhar a solicitação de orçamento
function  encaminharSolicitacao(btn) {
	// Dados da solicitação
	var orcID = $(btn).attr('orcID');
	// Verifica se tem no mínimo 1 destinatário
	var nrDestinatarios = $('#formDestinatarios .row').length-1;
	if(nrDestinatarios=='') {
		alert('Você precisa informar ao menos um fornecedor e clicar no "+".');
		return(false);	
	}
	// Destinatários
	var dest = {};
	var destinatarios = [];
	for(var i=1; i<=nrDestinatarios; i++) {
		dest.parID = $('#list_dest_fornecedor'+i+' option:selected').attr('par_id');
		dest.empresa = $('#list_dest_fornecedor'+i).val();
		dest.email = $('#txt_dest_email'+i).attr('full_email');
		dest.responsavel = $('#txt_dest_responsavel'+i).val();
		// Incrementa array com o ítem
		destinatarios.push(dest);
	}
	// Instacia AzulSender
	var az = new AzulSender();
	az.setPayload({
		module: 'app',
		controller: 'cOrcamentos',
		action:'encaminharSolicitacaoOrcamento',
		view: 'acompanhar-orcamento'
	});
	az.setEnvelope({
		extra : {
			orcID : orcID,
			destinatarios : destinatarios
		}
	});
	az.callback = function(objRet) {
		// Reseta o formulario
		$('#listformDestinatarios').html('');
		// Fecha modal
		$('#popup-encaminhamento').modal('hide');
		// Reseta sa.sender
		az.reset();
	};
	az.send(false);	
}



</script>
<style>
.table-striped > tbody > tr:nth-of-type(odd) {
    background-color: #f5f5f5 !important;
	color: black !important;
}
#ctListing .col-sm-6 .btn-new-register{
	display:none;	
}

</style>
<!--TOPO-->
<?php include(SYS_SRC_APP.'views/pieces/common_top.php')?>
<!--MENU de navegação do sistema-->
<?php include(SYS_SRC_APP.'views/pieces/common_menu.php')?>
<!--CONTEUDO DA VIEW -->
<div class="content" style="margin-left:240px;">
	<div class="main-content"> 
    	<!-- TITULO DA VIEW -->
        <?=$view->printTitleForm(_title,$view->isPortable())?>
        <!------------------------------------------------------------------
        --- Container ctPrincipal: Formulários de cadastro/informação
        ------------------------------------------------------------------->
        <div class="destaqueShadow bordaCinza hide" id="ctMain">
            <!-- ABAS -->
            <ul class="nav nav-tabs" role="tablist">
            	<?php $view->printTabs($Tabs->tabs); ?>
            </ul>
            <!-- FORMULARIOS (DE CADA ABA) -->
            <div class="tab-content" style="margin-top:15px;">
           		<?php $view->printForms($Tabs->tabs)?>
            </div>
        </div>
        <?php
			// Se for Modo de edição, não mostra a listagem, apenas um cadastro especifico
			if($view->config()->editMode==false) {
				$view->printListing($view->config());
			}
		?>
     </div>
</div>


<!--Popup para detalhamento do orcamento -->
<div class="modal fade" id="popup-detalhe" role="dialog"  >
  <div class="modal-dialog" role="document" >
    <div class="modal-content" >
      <div class="modal-body" id="popup-detalhe-body" style="overflow-y:scroll; height:500px">
      	 <form onSubmit="return(false)">
              <div class="row" >	
                 <div class="col-xs-2 col-md-1 col-lg-">
                      <!--CAMPO Número-->
                      <div class="form-group  initial" >
                        <label for="">Nr.</label> 	
                        <div class="label_detalhe numero">0001</div>
                      </div>
                 </div>								
                 <div class="col-xs-4 col-md-2 col-lg-">
                      <!--CAMPO Obra-->
                      <div class="form-group  initial">
                        <label for="">Obra</label> 	
                        <div class="label_detalhe obra">Obra 0001</div>
                      </div>
                  </div>
                  <div class="col-xs-6 col-md-3 col-lg-">
                      <!--CAMPO Cliente-->
                      <div class="form-group  initial">
                        <label for="">Proprietário</label> 										
                        <div class="label_detalhe proprietario">Lucas Nunes</div>
                      </div>
                  </div>
                  <div class="col-xs-4 col-md-3 col-lg-">
                      <!--CAMPO Data Emissão-->
                      <div class="form-group  initial">
                        <label for="">Emissão</label> 										
                        <div class="label_detalhe emissao">05/07/2019</div>
                      </div>
                  </div>
                  <div class="col-xs-2 col-md-1 col-lg-">
                      <!--CAMPO Data Emissão-->
                      <div class="form-group  initial">
                        <label for="">Qtd</label> 										
                        <div class="label_detalhe quantidade">30</div>
                      </div>
                  </div>
                  <div class="col-xs-6 col-md-2 col-lg-">
                      <!--CAMPO Data Emissão-->
                      <div class="form-group  initial">
                        <label for="">Status</label> 										
                        <div class="label_detalhe estatus"><strong>Enviado</strong></div>
                      </div>
                  </div>
             </div>
             
             <!-- Ítens da solicitação -->
             <h3>Ítens da solicitação</h3>
             <div class="row">	
                <div class="col-xs-3 col-md-2 col-lg-">
                      <!--CAMPO Qtd-->
                      <label for="list_dest_fornecedor">Qtd</label> 
                 </div>								
                 <div class="col-xs-9 col-md-3 col-lg-">
                      <!--CAMPO Obra-->
                      <label for="txt_dest_email">Descrição</label> 
                  </div>
             </div>
             <div class="itens"></div>
             
             <!-- Envios -->
             <h3>Envios</h3>
             <div class="row">	
                  <div class="col-xs-4 col-md-3 ">
                      <label >Data</label> 	
                  </div>	
                  <div class="col-xs-4 col-md-3 ">
                      <label >Fornecedor</label> 
                  </div>
                  <div class="col-xs-12 col-md-4 hidden-xs ">
                      <label >Email</label> 
                  </div>
                  <div class="col-xs-4 col-md-2 ">
                      <label >Responsável</label> 
                  </div>
             </div>
             <div class="envios"></div>
             
             <!-- Respostas -->
             <h3>Respostas de fornecedores</h3>
             <div class="row">	
                  <div class="col-xs-4 col-md-3">
                      <label >Data</label> 	
                  </div>								
                  <div class="col-xs-4 col-md-2">
                      <label >Fornecedor</label> 
                  </div>
                  <div class="col-xs-12 col-md-2  hidden-xs">
                      <label >Itens</label> 
                  </div>
                  <div class="col-xs-2 col-md-3">
                      <label >Valor</label> 
                  </div>
                  <div class="col-xs-2 col-md-2">
                      
                  </div>
             </div>
             <div class="respostas"></div>
             
        </form>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<script>
function imprimir(el){
	var htmlRestore = $(document).find('body');
	$(document).find('body').html($(el).html());
	window.print();
	$(document).find('body').html(htmlRestore);	
}
</script>

<!--Popup para encaminhamento -->
<div class="modal fade" id="popup-encaminhamento" role="dialog" >
  <div class="modal-dialog" role="document">
    <div class="modal-content" >
      <div class="modal-body">
      	 <form id="formDestinatarios" onSubmit="return(false)">
          <div class="row">	
             <div class="col-xs-12 col-md-4 col-lg-">
                  <!--CAMPO Fornecedor-->
                  <div class="form-group  initial" >
                    <label for="list_dest_fornecedor">Fornecedor </label> 										
                   <select onChange="preencheCamposDest(this)" name="list_dest_fornecedor" id="list_dest_fornecedor" az-default="" az-required="true" az-condition="" class="form-control az-field az-select" az-datatype="text" az-datatype-default="" az-title-default="" az-column-db="ORE_EMPRESA" placeholder="" title="Fornecedor" az-fieldtype="select" az-id="">
                    
                    </select>
                    <div class="help-block with-errors"></div>
                  </div>
             </div>								
             <div class="col-xs-12 col-md-4 col-lg-">
                  <!--CAMPO Email-->
                  <div class="form-group  initial">
                    <label for="txt_dest_email">Email </label> 	
                    <input type="input" name="txt_dest_email" id="txt_dest_email" maxlength="100" value="" az-default="" az-required="true" az-condition="" class="form-control az-field az-input" az-datatype="email" az-datatype-default="" az-title-default="" az-column-db="ORE_EMAIL" placeholder="" az-fieldtype="input" az-id="" disabled="disabled">
                    <div class="help-block with-errors"></div>
                  </div>
              </div>
              <div class="col-xs-12 col-md-3 col-lg-">
                  <!--CAMPO Responsável-->
                  <div class="form-group  initial">
                    <label for="txt_dest_responsavel">Responsavel </label> 										
                    <input type="input" name="txt_dest_responsavel" id="txt_dest_responsavel" maxlength="20" 
                    value="" az-default="" az-required="false" az-condition="" class="form-control az-field az-input"
                    az-datatype="text" az-datatype-default="" az-title-default="" az-column-db="ORE_RESPONSAVEL" 
                    placeholder="" az-fieldtype="input" az-id="" disabled="disabled">
                    <div class="help-block with-errors"></div>
                  </div>
              </div>
              <!-- Botão para adicionar destinatario -->
              <div class="col-xs-1 col-md-1 col-lg-1">
                    <div class="form-group">
                        <button class="btn btn-success margin-bottom" style="margin-top:24px; margin-left:-13px;" onclick="incluirNovoDestinatario()">
                        <b>+</b></button>
                    </div>
              </div> 
         </div>
         <div id="listformDestinatarios"></div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-success" onClick="encaminharSolicitacao(this)" >Enviar</button>
      </div>
    </div>
  </div>
</div>


<!--Janelas Modals-->
<?php 		
//Modal filtros de busca
include(SYS_SRC_APP.'views/pieces/common_modalSearch.php');

//Modal funções de listagem
include(SYS_SRC_APP.'views/pieces/common_modalSearch.php');

// Common piece bottom
include(SYS_SRC_APP.'views/pieces/common_bottom.php')
?>

<script>
	_azAzul.afterLoad.push(function() {
		$('#list_dest_fornecedor').select2({tags:true});
		
		// Função para abrir o detalhe de um orçamento, caso o ID seja informado na querystring
		<?php
			if($_GET['id']>0) {
				echo "fillOutForms(".$_GET['id'].")";	
			}
		?>
	})
	
</script>
</body>
</html>