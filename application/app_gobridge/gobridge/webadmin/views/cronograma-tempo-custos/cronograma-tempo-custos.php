<?php
	// Incorpora todas as abas/formularios/campos pelo arquivo {nome_view}.xml na pasta /templates
	$view->incorporateXML(true);
	
	// Busca todos cronogramas cadastrados no BD
	$sql = "SELECT COUNT(*) total FROM cronograma_tempo_custos_subetapas";
	$r = $azul_DB->query($sql);
	$total = mysqli_fetch_assoc($r);
	$total = $total['total'];
	$view->config(array('generalTotal'=>$total));
	
	// Define o título da página
	define("_title", "Cronograma de tempo e custos");
	
	// Define o título da página
	define("_columnID", "CRO_ID");
	
	// Define o controller
	define("_controller", "cCronogramaTempoCustos");

	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php require SYS_SRC_APP.'views/pieces/common_header.php' ?>
</head>
<body class="theme-blue">
<script>
/*
 * Preenche o list das categorias de obras
 */
function preencheSelectCategorias(objRet) {
	$('#listcategorias').html('');
	for(var i=0; i<objRet.length; i++) {
		var o = objRet[i];
		var htmlOption = '<option value="'+o.CAO_ID+'">'+o.CAO_TITULO+'</option>';
		$('#listcategorias').append(htmlOption);
	}
}
/*
 * Preenche o list das subetapas de obra
 */
function preencheSelectSubetapas(objRet) {
	$('#listsubetapas').html('');
	for(var i=0; i<objRet.length; i++) {
		var o = objRet[i];
		var htmlOption = '<option value="'+o.SUB_ID+'">'+o.SUB_TITULO+'</option>';
		$('#listsubetapas').append(htmlOption);
	}
}
/**
 * Paginação no navegador(JS) até limite máximo estabelecido ($maxRows), acima disso é por ajax(PHP)
 * @param int init indice de inicio para mostrar x linhas a partir deste ponto
 */
function showData(init){
	// Altera a global que armazena a pagina atual
	//_PAGINA_ATUAL = init;
	// Limpa as linhas já existentes na listagem
	$('#tbData tbody').html('');
	// Loop master
	for ( i=init; i < ( init + _azListingObject.maxRowsPage ) & i < _azListingObject.data.length; i++ ) {
		// Linha com todas as colunas disponibilizadas em objListagem.data
		objRow = _azListingObject.data[i];
		ID = objRow['<?=_columnID?>'];
		
		// Se for cadastro arquivado, exibe nome e hora do arquivamento
		if(objRow['AZ_ARCHIVED']==true) {
			var infoAdc = "Usuário : " + objRow['AZ_USARCHIVED']+" \r\n Data/Hora : "+objRow['AZ_DHARCHIVED'];
			var linha  = '<tr  data-toggle="tooltip" data-placement="top" title="'+infoAdc+'" az-ID="'+ID+'">';
		// Cadastro ativo :
		}else {
			var linha  = "<tr onclick=\"$(this).toggleClass('row_select')\" id='rowList"+ID+"' az-ID='"+ID+"'>";
		}
		// Preenche todas as colunas visiveis da linha retornada pelo WS
		var contColVis = 0;// Contagem de colunas vísiveis exibidas
		for (c in _azListingColumnsArray) {
			var infoAdc = '';
			coluna = _azListingColumnsArray[c];
			// Se for  dispositivo portabil : (REVER ESTA LOGICA)
			colspan = (_IS_PORTABLE==true ? '2' : '');
			
			// Exibi coluna com possiveis informações adc. na primeira coluna (caso seja movel) ou na segunda coluna em PC/Notebook
			if(((contColVis==1 && _IS_PORTABLE==false) || (contColVis==0 && _IS_PORTABLE==true)) && objRow['AZ_ARCHIVED']==true) {
				infoAdc = objRow['AZ_USARCHIVED']+" - "+objRow['AZ_DHARCHIVED'];
				linha += "<td colspan='"+colspan+"'>"+objRow[coluna]+" <span class='label label-default'>Arquivado por "+infoAdc+"</span></td>";		
				
			// Exibe o conteudo da coluna
			} else {
				// Se for imagem da capa, mostra o thumbnail :
				if(coluna=='CAT_IMAGE' && objRow[coluna]!='') {
					conteudoColuna = '<a href="'+objRow['URL']+'" target="_blank"><img src="'+objRow['THUMBNAIL']+'" height=50 /></a>';
				
				// Conteúdo padrão (apenas texto)	
				}else {
					conteudoColuna=objRow[coluna];
				}
				
				// Se estiver bloqueado, exibe botão laranja
				if(contColVis==1 && objRow['USE_STATUS']==="0") {
					infoAdc = " <span class='label label-danger'>BLOQUEADO</span>";
				}
	
				// Incrementa uma coluna na linha
				linha += "<td colspan='"+colspan+"'>"+conteudoColuna+infoAdc+"</td>";
			}
			
			// Incrementa contador de colunas visiveis
			contColVis++;
		}
		
		// Botao alterar
		btAlterar = "<span class='glyphicon glyphicon-pencil handCursor'  data-toggle='tooltip' data-placement='top'"
					+" title='Alterar cadastro' onclick='fillOutForms("+ID+")'></span> &nbsp; &nbsp;";
		// Botao Deletar
		btExcluir = " <span class='glyphicon glyphicon-remove handCursor'  data-toggle='tooltip' data-placement='top'"
					+" title='Excluir cadastro' onclick=\"archiveSelectedRegisters()\"></span> ";		
					
		// Se estiver arquivado, não permite nenhuma edição
		if(objRow['AZ_ARCHIVED']==true) {
			btAlterar='';	btExcluir='';
		}
		
		// Coluna funções (alterar e remover cadastro) (apenas pc e notebooks)
		<?php if(!$view->isPortable()) :?>
		linha += "<td align='right'>"+btAlterar+" "+btExcluir+"</td>";
		<?php else :?>
		linha += "<td align='right'>"+btAlterar+" </td>";
		<?php endif; ?>
		
		// Fecha linha
		linha += "</tr>";
		// Incrementa linha no corpo da tabela
		$('#tbData tbody').append(linha);
		
		
		// Inicia todas as tooltips (pop-up informativo)
		$('[data-toggle="tooltip"]').tooltip(); 

	}
	
	// Atualiza paginação
	nrPaginas = Math.round((_azListingObject.data.length/_azListingObject.maxRowsPage));
	html = '';
	for(i=0; i < nrPaginas;i++ ) {
		html += '<li><a href="#" onclick="showData('+(i*_azListingObject.maxRowsPage)+')">'+(i+1)+'</a></li>';	
	}
	$('ul.pagination').html(html);
	
	
}

</script>
<?php
	//var_dump($Abas->abas); exit();
	
?>
<!--TOPO-->
<?php include(SYS_SRC_APP.'views/pieces/common_top.php')?>
<!--MENU de navegação do sistema-->
<?php include(SYS_SRC_APP.'views/pieces/common_menu.php')?>
<!--CONTEUDO DA VIEW -->
<div class="content" style="margin-left:240px;">
	<div class="main-content"> 
    	<!-- TITULO DA VIEW -->
        <?=$view->printTitleForm(_title,$view->isPortable())?>
        <!------------------------------------------------------------------
        --- Container ctPrincipal: Formulários de cadastro/informação
        ------------------------------------------------------------------->
        <div class="destaqueShadow bordaCinza hide" id="ctMain">
            <!-- ABAS -->
            <ul class="nav nav-tabs" role="tablist">
            	<?php $view->printTabs($Tabs->tabs); ?>
            </ul>
            <!-- FORMULARIOS (DE CADA ABA) -->
            <div class="tab-content" style="margin-top:15px;">
           		<?php $view->printForms($Tabs->tabs)?>
            </div>
        </div>
        <?php
			// Se for Modo de edição, não mostra a listagem, apenas um cadastro especifico
			if($view->config()->editMode==false) {
				$view->printListing($view->config());
			}
		?>
     </div>
</div>
<!--Janelas Modals-->
<?php 		
//Modal filtros de busca
include(SYS_SRC_APP.'views/pieces/common_modalSearch.php');

//Modal funções de listagem
include(SYS_SRC_APP.'views/pieces/common_modalFunctions.php');

// Common piece bottom
include(SYS_SRC_APP.'views/pieces/common_bottom.php')
?>
</body>
</html>