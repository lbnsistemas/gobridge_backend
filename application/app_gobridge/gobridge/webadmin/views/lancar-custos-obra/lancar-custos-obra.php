<?php	
	// Incorpora todas as abas/formularios/campos pelo arquivo {nome_view}.xml na pasta /templates
	$view->incorporateXML(true);
	
	// Define o título da página
	define("_title", "Custos da Obra");
	
	// Define o título da página
	define("_columnID", "VND_ID");
	
	// Define o controller
	define("_controller", "cMain");


	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php require SYS_SRC_APP.'views/pieces/common_header.php' ?>
</head>
<body class="theme-blue">
<script language="javascript">
/**
 *	Função do usuário para preencher todos os formularios da view de forma dinamica
 *
 */
function user_fillOutForms(ID) {
	// Prepara os os cabeçalhos de 'payload':
	_azSender7845.setPayload({
		module: 'app',
		action:'requestData',
		view: 'lancar-custos-obra'
	});
	// Prepara o envelope
	_azSender7845.setEnvelope( {
		ID		: ID
	});
	// Seta a função pós-processamento com sucesso
	_azSender7845.callback = function(objRet) {
	   var r = objRet;
		// Carrega os documentos anexos
		showUploadFramesContent(objRet.uploadFramesContent);
		
		// Preenche o list com as obras
		if(objRet.VND_OBR_ID==0){
			var opcaoEmBranco = [{OBR_ID:'',OBR_TITULO:''}];
			objRet.obras = opcaoEmBranco.concat(objRet.obras);
		}
		preencheListObras(objRet.obras);
		
		// Preenche o list com as etapas
		if(objRet.VND_ETU_ID==0){
			var opcaoEmBranco = [{ETU_ID:'',ETU_TITULO:''}];
			objRet.etapas_obra_usuario = opcaoEmBranco.concat(objRet.etapas_obra_usuario);
		}
		preencheListEtapas(objRet.etapas_obra_usuario);
		
		// Preenche o list com as subetapas
		if(objRet.VND_SEO_ID==0){
			var opcaoEmBranco = [{SEO_ID:'',SEO_TITULO:''}];
			objRet.subetapas_obra_usuario = opcaoEmBranco.concat(objRet.subetapas_obra_usuario);
		}
		preencheListSubetapas(objRet.subetapas_obra_usuario);
		
		// Preenche o list com os parceiros
		if(objRet.VND_PAR_ID==0 && objRet.VND_EMPRESA==''){
			var opcaoEmBranco = [{PAR_ID:'999999',PAR_FANTASIA:''}];
			objRet.parceiros = opcaoEmBranco.concat(objRet.parceiros);
		}
		preencheListParceiros(objRet.parceiros);
		
		// Documento
		$('#txt_compra_documento').val(objRet.VND_DOCUMENTO);
		// Histórico / descrição da compra
		$('#txt_compra_descricao').val(objRet.VND_DESCRICAO);
		// Valor da compra
		$('#txt_compra_valor').val(_azDataManipulation.floatToCurrency(objRet.VND_TOTALBRUTO));
		// Data da compra
		$('#txt_compra_dtcompra').val(_azDataManipulation.invertDate(objRet.VND_DATACOMPRA));
		// Forma de pagamento
		$('#txt_compra_formapgto').val(objRet.VND_FORMAPGTO);
		// Número de parcelas
		$('#txt_compra_parcelas').val(objRet.VND_PARCELAS);
		// Forma pagamento
		$('#list_compra_formapgto option').attr('selected',false);
		$('#list_compra_formapgto').val(objRet.VND_FORMAPGTO);
		// Status
		$('#list_compra_status option').attr('selected',false);
		$('#list_compra_status').val(objRet.VND_STATUS);
		
		// Executa validação do formulario principal
		_azValidation.exec('formDadosdaCompraPrincipal');
		
		// Trata as parcelas
		for(var i=0; i<objRet.vendas_parcelas.length;i++) {
			var p = objRet.vendas_parcelas[i];
			objRet.vendas_parcelas[i].PAR_VALOR = _azDataManipulation.floatToCurrency(p.PAR_VALOR);
			objRet.vendas_parcelas[i].PAR_VENCIMENTO = _azDataManipulation.invertDate(p.PAR_VENCIMENTO);
			
		}	
		// Preenche o formulario dinamico com as parcelas da compra
		showDynamicFormDataList('formDadosdaCompraParcelas',objRet.vendas_parcelas);
		
		// Prepara container
		prepareContainer("update",ID);
		// Reseta sa.sender
		_azSender7845.reset();
	}
	// Envia pacote
	_azSender7845.send();	
}

/**
 * Função do usuário para preparar o container para incluir ou editar um cadastro
 */
function user_prepareContainer(action, ID) {
	var _azSender7845 = new AzulSender();
	if(action=='insert') {
		// Padroniza o botao de confirmação
		$('#btnConfirmation').attr('az-action','insert');
		$('#btnConfirmation').html('Concluir cadastro');

		// Bloqueia um possível botao de impressão
		$('button[az-action="print"').attr('disabled',true);

		// Prepara o envelope para a ação preparaContainer
		_azSender7845.setPayload({
			module: "app",
			controller: "cMain",
			action: "prepareContainer",
			view: "lancar-custos-obra" 
		});

		// Seta a função pós-processamento com sucesso
		_azSender7845.callback = function(objRet) {
			// Registra a PK reservada na global
			_RESERVED_ID = objRet.ID;
	
			// Mostra os arquivos já enviados anteriormente (se houver)
			showUploadFramesContent(objRet.uploadFramesContent);
			
			
			// Preenche o list de obras, etapas, subetapas e parceiros
			carregaSelects('carregaSelectsCompra', 
				function(objRet) { 
					preencheListObras(objRet.obras);
					preencheListEtapas(objRet.etapas_obra_usuario);
					preencheListSubetapas(objRet.subetapas_obra_usuario);
					var opcaoEmBranco = [{PAR_ID:'', PAR_FANTASIA:''}];
					objRet.parceiros = opcaoEmBranco.concat(objRet.parceiros);
					preencheListParceiros(objRet.parceiros);
				}
			)
			
			// Registra o ID reservado no attr az-ID do botão de confirmação
			$('#btnConfirmation').attr('az-ID',_RESERVED_ID);
			
			// Libera os campos 
			$('#list_compra_obra').attr('disabled',false);
			$('#list_compra_parceiro').attr('disabled',false);
			$('#txt_compra_valor').attr('disabled',false);
			$('#txt_compra_documento').attr('disabled',false);
			
			// Mostra a primeira linha do formulario dinamico para  ocnseguir adicionar parc.
			$($('#formDadosdaCompraParcelas').find('.row')[0]).removeClass('hide');
			
			// Mostra formulario e esconde listagem
			showHideListing(action,_RESERVED_ID);

			// Reseta sa.sender
			_azSender7845.reset();

		}
		// Envia pacote
		_azSender7845.send(true);
	}else if(action=='update') {
		// Registra a PK reservada na global
		_RESERVED_ID = ID;
		
		// Padroniza o botao de confirmação
		$('#btnConfirmation').attr('az-action','update');
		$('#btnConfirmation').html('Concluir alteração');
		
		// Oculta a primeira linha do formulario dinamico para não ocnseguir adicionar parc.
		$($('#formDadosdaCompraParcelas').find('.row')[0]).addClass('hide');
		
		// Bloqueia os campos que não podem mais ser alterados
		$('#list_compra_obra').attr('disabled',true);
		if($('#list_compra_status')!='REGISTRADO') {
			$('#list_compra_parceiro').attr('disabled',true);
			$('#txt_compra_valor').attr('disabled',true);
			$('#txt_compra_documento').attr('disabled',true);
		}else {
			$('#list_compra_parceiro').attr('disabled',false);
			$('#txt_compra_valor').attr('disabled',false);
			$('#txt_compra_documento').attr('disabled',false);
		}

		// Libera um possível botao de impressão (se existir)
		$('button[az-action="print"]').attr('disabled',false);

		// Mostra formulario e esconde listagem
		showHideListing(action, ID);

		// Registra o ID reservado no attr az-ID do botão de confirmação
		$('#btnConfirmation').attr('az-ID',_RESERVED_ID);
	}
}	


// Adiciona o Parceiro no select com o atributo 'az-salvar-item=true'
/*function incluirNovoParceiro(salvar) {
	// Pega o conteúdo digitado no campo de busca do select2
	var titulo = $('#list_compra_parceiro').data('select2').dropdown.$search.val();
	// Verifica se este titulo já existe na listagem (id)
	var pesquisa = $('#list_compra_parceiro option[value="'+titulo+'"]').length;
	if(pesquisa==0) {
		// Cria um novo option e adiciona ao select
		var option = new Option(titulo, titulo);
		option.selected = true;
		$("#list_compra_parceiro").append(option);
		// Se clicou no botão 'Salvar e incluir', prepara o ítem para ser salvo no BD
		if(salvar) {
			// Adiciona o atributo 'az-salvar-item=true' no select
			$("#list_compra_parceiro option:selected").attr('az-salvar-item',true);
			$("#list_compra_parceiro option:selected").append(' [Novo]');
		}
		//Reinicia o select 2
		$("#list_compra_parceiro").select2();
	}else {
		_azGeneral.alert('Esta opção já existe na lista, por favor selecione com um clique.');	
	}
}*/

// Função para recalcular a qtd, valor e vencimento de cada parcela
function recalculaParcelamento() {
	var nrParc = parseInt($('#txt_compra_parcelas').val());
	// Só prossegue se houver no mínimo 1 parcela informada no campo 'Qtd. Parcelas'
	if(isNaN(nrParc) || nrParc==0) 
		return(false);
	var vrTotalVenda = _azDataManipulation.currencyToFloat($('#txt_compra_valor').val());
	// Só prossegue se o valor for superior a 0
	if(vrTotalVenda==0) 
		return(false);
}

/**
 * Função do usuário para deletar um cadastro
 */
function user_archiveRegister(form, ID, objToRemove){
	var _azSender7845 = new AzulSender();
	// Se estiver excluindo um parcela e for modo de inclusao, apenas exclui a linha
	if(form=='formDadosdaCompraParcelas' && $('#btnConfirmation').attr('az-action')=='insert'){
		var nrParc = parseInt($('#txt_compra_parcelas').val());
		nrParc--;
		$('#txt_compra_parcelas').val(nrParc);
		// Apaga linha/coluna do cadastro deletado
		$(objToRemove).remove();
		return(false);
	// Se estiver excluindo uma parcela e for modo de edição, exclui do BD e depois a linha
	}else if(form=='formDadosdaCompraParcelas' && $('#btnConfirmation').attr('az-action')=='update'){
		var action = 'deletaParcelaVenda';
	// Se estiver tentando deletar a venda em si
	}else {
		var action = 'archive_Main';
	}
	// Confirmação
	bootbox.confirm("Tem certeza que deseja deletar este cadastro?", function(result) {
	  if(result) {
		  	// Seta a ação em Payload
			_azSender7845.setPayload({
				module:'app',
				controller:'cMain',
				action:action,
				view: 'lancar-custos-obra'
			});
			// Prepara o envelope
			_azSender7845.setEnvelope( { ID : ID } );
			// Seta a função pós-processamento com sucesso
			_azSender7845.callback = function(objRet) {
				// Apaga linha/coluna do cadastro deletado
				$(objToRemove).remove();
				// Reseta sa.sender
				_azSender7845.reset();
			}
			// Envia pacote
			_azSender7845.send();
	  }else {
		   return(-1);
	  }
	});
}

/**
 * Função do usuário inserir ou editar um cadastro
 */
function user_insertUpdateRegister(form, dynamic, action, ID) {	
	var _azSender7845 = new AzulSender();
	var action = $('#btnConfirmation').attr('az-action');
	// Se for modo de inclusão e formulario dinamico, nada faz
	if(dynamic==true && action=='insert') {
		if($('#txt_valor_parcela').val()=='') 
			return(true);
		var nrParc = parseInt($('#txt_compra_parcelas').val());
		// Primeira parcela
		if(isNaN(nrParc) || nrParc==0) 
		 	nrParc = 1;
		else
			nrParc++;
		// Incrementa uma linha no formulario dinamico com as parcelas da venda
		linesObject=[{}];
		linesObject[0].novoID = nrParc;
		linesObject[0].PAR_VALOR = $('#txt_valor_parcela').val();
		linesObject[0].PAR_VENCIMENTO = $('#txt_data_parcela').val();
		linesObject[0].PAR_STATUS = $('#chk_parcela').val();
		showDynamicFormDataList(form, linesObject);
		// Atualiza a quantidade total de parcelas
		$('#txt_compra_parcelas').val(nrParc);
		// Sai da função
		return(true);
	}
	// Valida o formulario
	var retValidation = _azValidation.exec(form, dynamic, ID);
	// Valida o(s) formulario(s) antes de enviar
	if(!retValidation) {
		_azGeneral.alert('Atenção','Algum campo não foi preenchido corretamente.');
		return(false);
	}
	// Campos do formulario
	fields = [];
	// Edição de dados da parcela
	if(dynamic==true && action=='update' ) {
		fields.push({column:'PAR_VALOR', value:_azDataManipulation.checkIn('#txt_valor_parcela'+ID)});
		fields.push({column:'PAR_VENCIMENTO', value:_azDataManipulation.checkIn('#txt_data_parcela'+ID)});
		fields.push({column:'PAR_STATUS', value:$('#chk_parcela'+ID).val()});
		action = 'editarParcelaCompra';
		
	// Formulario principal com os dados da compra	
	}else {
		var ID  = action=='update' ? $('#btnConfirmation').attr('az-id') : 0;
		fields.push({column:'list_compra_obra', value:$('#list_compra_obra').val()});
		fields.push({column:'list_compra_etapa', value:$('#list_compra_etapa').val()});
		fields.push({column:'list_compra_subetapa', value:$('#list_compra_subetapa').val()});
		fields.push({column:'txt_compra_documento', value:$('#txt_compra_documento').val()});
		fields.push({column:'txt_compra_descricao', value:$('#txt_compra_descricao').val()});
		fields.push({column:'txt_compra_valor', value:_azDataManipulation.checkIn('#txt_compra_valor')});
		fields.push({column:'txt_compra_dtcompra', value:_azDataManipulation.checkIn('#txt_compra_dtcompra')});
		fields.push({column:'list_compra_formapgto', value:$('#list_compra_formapgto').val()});
		fields.push({column:'txt_compra_parcelas', value:$('#txt_compra_parcelas').val()});
		fields.push({column:'list_compra_status', value:$('#list_compra_status').val()});
		//fields.push({column:'seo_titulo', value:$('#list_compra_subetapa').attr('seo_titulo')});
		if($.isNumeric( $('#list_compra_parceiro').val() ) == false || $('#list_compra_parceiro').val()== '999999') {
			fields.push({column:'list_compra_parceiro', value:'999999'});
			fields.push({column:'fornecedorAvulso', value:$('#list_compra_parceiro option:selected').text()});
		}else {
			fields.push({column:'list_compra_parceiro', value:$('#list_compra_parceiro').val()});
		}
		// Parcelas
		if(action=='insert') {
			var nr = parseInt($('#txt_compra_parcelas').val());
			for(var i=1; i <=nr; i++) {
				var valor = _azDataManipulation.checkIn('#txt_valor_parcela'+nr);
				fields.push({column:'txt_valor_parcela'+nr, value:valor});
				
				var vencimento = _azDataManipulation.checkIn('#txt_data_parcela'+nr);
				fields.push({column:'txt_data_parcela'+nr, value:vencimento});
				
				var status = $('#chk_parcela'+nr).val();
				fields.push({column:'chk_parcela'+nr, value:status});	
			}
		}
		// Ação para incluir a compra
		action = action=='update'? 'editarCompra' : 'incluirNovaCompra';
	}
	// Instancia AzulSender
	var az = new AzulSender();
	// Seta a ação
	az.setPayload({
		action:  action, 
		module: 'app',
		controller: 'cMain',
		view : 'lancar-custos-obra',
	});
	// Prepara o envelope
	az.setEnvelope( {ID : ID, fields : fields } );
	// Seta a função pós-processamento com sucesso
	az.callback = function(objRet) {
		if(dynamic==false ){
			// Reseta os formularios
			resetForms();
			
			// Fecha formularios de cadastro e exibe listagem
			showHideListing();
	
			// Reseta global de reserva de PK para cadastro
			_RESERVED_ID = '';
		}
		// Reseta sa.sender
		az.reset();
	}
	// Envia pacote
	az.send(silentMode=dynamic);
}	

 
/**
 * Paginação no navegador(JS) até limite máximo estabelecido ($maxRows), acima disso é por ajax(PHP)
 * @param int init indice de inicio para mostrar x linhas a partir deste ponto
 */
function showData(init){
	// Altera a global que armazena a pagina atual
	//_PAGINA_ATUAL = init;
	// Limpa as linhas já existentes na listagem
	$('#tbData tbody').html('');
	// Loop master
	for ( i=init; i < ( init + _azListingObject.maxRowsPage ) & i < _azListingObject.data.length; i++ ) {
		// Linha com todas as colunas disponibilizadas em objListagem.data
		objRow = _azListingObject.data[i];
		ID = objRow['<?=_columnID?>'];
		
		// Se for cadastro arquivado, exibe nome e hora do arquivamento
		if(objRow['AZ_ARCHIVED']==true) {
			var infoAdc = "Usuário : " + objRow['AZ_USARCHIVED']+" \r\n Data/Hora : "+objRow['AZ_DHARCHIVED'];
			var linha  = '<tr  data-toggle="tooltip" data-placement="top" title="'+infoAdc+'" az-ID="'+ID+'">';
		// Cadastro ativo :
		}else {
			var linha  = "<tr onclick=\"$(this).toggleClass('row_select')\" compartilhada='"+objRow['compartilhada']+"' id='rowList"+ID+"' az-ID='"+ID+"'>";
		}
		// Preenche todas as colunas visiveis da linha retornada pelo WS
		var contColVis = 0;// Contagem de colunas vísiveis exibidas
		for (c in _azListingColumnsArray) {
			var infoAdc = '';
			coluna = _azListingColumnsArray[c];
			// Se for  dispositivo portabil : (REVER ESTA LOGICA)
			colspan = (_IS_PORTABLE==true ? '2' : '');
			
			// Exibi coluna com possiveis informações adc. na primeira coluna (caso seja movel) ou na segunda coluna em PC/Notebook
			if(((contColVis==1 && _IS_PORTABLE==false) || (contColVis==0 && _IS_PORTABLE==true)) && objRow['AZ_ARCHIVED']==true) {
				infoAdc = objRow['AZ_USARCHIVED']+" - "+objRow['AZ_DHARCHIVED'];
				linha += "<td colspan='"+colspan+"'>"+objRow[coluna]+" <span class='label label-default'>Arquivado por "+infoAdc+"</span></td>";		
				
			// Exibe o conteudo da coluna
			} else {
				// Se a coluna da foto do perfil, mostra o thumbnail:
				if(coluna=='OBR_FOTOCAPA' && objRow['thumbnail']!='' && objRow['thumbnail']!=undefined) {
					conteudoColuna = '<a href="'+objRow['url']+'" target="_blank"><div style="background-image:url('+objRow['thumbnail']+')" class="capa"></div></a>';
					
				// Conteúdo padrão (apenas texto)	
				}else {
					conteudoColuna=objRow[coluna];
				}
				// Incrementa uma coluna na linha
				linha += "<td colspan='"+colspan+"'>"+conteudoColuna+infoAdc+"</td>";
			}
			
			// Incrementa contador de colunas visiveis
			contColVis++;
		}
		
		// Botao alterar
		btAlterar = "<span class='glyphicon glyphicon-pencil handCursor'  data-toggle='tooltip' data-placement='top'"
					+" title='Alterar cadastro' onclick='fillOutForms("+ID+")'></span> &nbsp; &nbsp;";
		// Botao Deletar
		btExcluir = " <span class='glyphicon glyphicon-remove handCursor'  data-toggle='tooltip' data-placement='top'"
					+" title='Excluir cadastro' onclick=\"archiveRegister('Main', "+ID+", $(this).closest('tr'))\"></span> ";		
					
		// Se estiver arquivado, não permite nenhuma edição
		if(objRow['AZ_ARCHIVED']==true) {
			btAlterar='';	btExcluir='';
		}
		
		// Coluna funções (alterar e remover cadastro) (apenas pc e notebooks)
		<?php if(!$view->isPortable()) :?>
		linha += "<td align='right'>"+btAlterar+" "+btExcluir+"</td>";
		<?php else :?>
		linha += "<td align='right'>"+btAlterar+" </td>";
		<?php endif; ?>
		
		// Fecha linha
		linha += "</tr>";
		// Incrementa linha no corpo da tabela
		$('#tbData tbody').append(linha);
	
		// Inicia todas as tooltips (pop-up informativo)
		$('[data-toggle="tooltip"]').tooltip(); 
	}
	
	// Atualiza paginação
	nrPaginas = Math.round((_azListingObject.data.length/_azListingObject.maxRowsPage));
	html = '';
	for(i=0; i < nrPaginas;i++ ) {
		html += '<li><a href="#" onclick="showData('+(i*_azListingObject.maxRowsPage)+')">'+(i+1)+'</a></li>';	
	}
	$('ul.pagination').html(html);
}

// Função do campo list_compra_obras
// Carrega as obras, etapas, subetapas e parceiros
function carregaEPreencheSelectsCompra(obj) {
	carregaSelects('carregaSelectsCompra', 
		function(objRet) { 
			preencheListEtapas(objRet.etapas_obra_usuario);
			preencheListSubetapas(objRet.subetapas_obra_usuario);
			preencheListParceiros(objRet.parceiros);
		}, 
		{ ID : $(obj).val() }
	);
}

// Carrega as sub-etapas
function carregaEPreencheSubetapasObra(obj) {
	carregaSelects('carregaSubEtapasObra', 
		function(objRet) { 
			preencheListSubetapas(objRet.subetapas_obra_usuario) 
		}, 
		{
			extra: {
				ETU_ID : $(obj).val()
			}
		}
	);
}

// Função para preencher o list das obras
function preencheListObras(obras){
	if(obras.length>0){
		$('#list_compra_obra').html('');
		//var htmlOption = '<option value=""  ></option>';
		//$('#list_compra_obra').append(htmlOption);
		for(var i in obras){
		  var row = obras[i];
		  var htmlOption = '<option value="'+row.OBR_ID+'" etu_id="'+row.ETU_ID+'"  >'+row.OBR_TITULO+'</option>';
		  $('#list_compra_obra').append(htmlOption);
		}
	 }	
}

// Função para preencher o list das etapas
function preencheListEtapas(etapas_obra_usuario) {
	 $('#list_compra_etapa').html('');
	  //Recarrega o list de subetapas
	  if(etapas_obra_usuario.length>0){
		$('#list_compra_etapa').html('');
		/*
		if(etapas_obra_usuario[0].VND_ID=='') {
			var htmlOption = '<option value=""  ></option>';
			$('#list_compra_etapa').append(htmlOption);
		}*/
		for(var i in etapas_obra_usuario){
		  var row = etapas_obra_usuario[i];
		  var htmlOption = '<option value="'+row.ETU_ID+'" >'+row.ETU_TITULO+'</option>';
		  $('#list_compra_etapa').append(htmlOption);
		}
	  }		
}

// Função para preencher o list das subetapas
function preencheListSubetapas(subetapas_obra_usuario) {
	//Recarrega o list de subetapas
	if(subetapas_obra_usuario.length>0){
		$('#list_compra_subetapa').html('');
		//var htmlOption = '<option value=""  ></option>';
		//$('#list_compra_subetapa').append(htmlOption);
		for(var i in subetapas_obra_usuario){
		  var row = subetapas_obra_usuario[i];
		  var htmlOption = '<option value="'+row.SEO_ID+'" >'+row.SEO_TITULO+'</option>';
		  $('#list_compra_subetapa').append(htmlOption);
		}
	 }		
}

// Função para preencher o list dos parceiros
function preencheListParceiros(parceiros){
	//Recarrega o list de parceiros 
	  if(parceiros.length>0){
		$('#list_compra_parceiro').html('');
		//var htmlOption = '<option value=""  ></option>';
		//$('#list_compra_parceiro').append(htmlOption);
		for(var i in parceiros){
		  var row = parceiros[i];
		  var htmlOption = '<option value="'+row.PAR_ID+'" >'+row.PAR_FANTASIA+'</option>';
		  $('#list_compra_parceiro').append(htmlOption);
		}
	  }
}


// Função para incluir nova subetapa
function incluirNovaSubetapa() {
	var subetapa = $('#popup-registrar-compra .input-with-value').val();
	$('#list_compra_subetapa').append('<option value="0">'+subetapa+'</option>');
	$('#list_compra_subetapa option').prop('selected',false);
	$('#list_compra_subetapa option:last').prop('selected',true);
	$('#list_compra_subetapa').closest('a').find('.item-content .item-after').html(subetapa);
	app.smartSelect.close('.list_compra_subetapa');
}

// Função para selecionar o fornecedor mesmo que ele não seja um parceiro brickbe e permitir o registro da venda
function selecionaFornecedorAvulso() {
	var fornecedor = $('#popup-registrar-compra .input-with-value').val();
	$('#list_compra_parceiro option').prop('selected',false);
	var option = '<option value="0">'+fornecedor+"</option>";
	$('#list_compra_parceiro').append(option);
	$('#list_compra_parceiro option:last').prop('selected',true);
	$('#list_compra_parceiro').closest('a').find('.item-content .item-after').html(fornecedor);
	app.smartSelect.close('.list_compra_parceiro');
}

// Função para carregar os selects da view
// Carrega os selects para o registro da compra (via ajax)
function carregaSelects(action, callback, envelope={}){
  var _azSender7845 = new AzulSender();
  // Seta a action
  _azSender7845.setPayload({
	  module: 'app',
      action: action,
      view:'lancar-custos-obra'
  });
  // Seta o envelope
  _azSender7845.setEnvelope(envelope);
  // Seta a função pós-processamento com sucesso
  _azSender7845.callback = function(objRet) {
	  //Executa o callback para inicio dos smarts selects e abertura do popup
	  callback(objRet);
      // Reseta sa.sender
      _azSender7845.reset();
  }
  // Envia pacote
  _azSender7845.send();
}


</script>
<style>
/* Optional: Makes the sample page fill the window. */
html, body {
height: 100%;
margin: 0;
padding: 0;
}

/* Tabela/Listagem */
#tbData th:nth-child(1){
	width:7%;	
}
#tbData th:nth-child(2){
	width:20%;	
}
#tbData th:nth-child(3){
	width:25%;	
}
#tbData th:nth-child(4){
	width:20%;	
}
#tbData th:nth-child(5){
	width:10%;	
}
#tbData th:nth-child(6){
	width:10%;	
}
#tbData th:nth-child(7){
	width:10%;	
}
</style>
<!--TOPO-->
<?php include(SYS_SRC_APP.'views/pieces/common_top.php')?>
<!--MENU de navegação do sistema-->
<?php include(SYS_SRC_APP.'views/pieces/common_menu.php')?>
<!--CONTEUDO DA VIEW -->
<div class="content" style="margin-left:240px;">
	<div class="main-content"> 
    	<!-- TITULO DA VIEW -->
        <?=$view->printTitleForm(_title,$view->isPortable())?>
        <!------------------------------------------------------------------
        --- Container ctPrincipal: Formulários de cadastro/informação
        ------------------------------------------------------------------->
        <div class="destaqueShadow bordaCinza hide" id="ctMain">
            <!-- ABAS -->
            <ul class="nav nav-tabs" role="tablist">
            	<?php $view->printTabs($Tabs->tabs); ?>
            </ul>
            <!-- FORMULARIOS (DE CADA ABA) -->
            <div class="tab-content" style="margin-top:15px;">
           		<?php $view->printForms($Tabs->tabs)?>
            </div>
        </div>
        <?php
			// Se for Modo de edição, não mostra a listagem, apenas um cadastro especifico
			if($view->config()->editMode==false) {
				$view->printListing($view->config());
			}
		?>
     </div>
</div>

<!-- Janela modal geolocation da obra -->
<div class="modal fade" id="popup-geolocation" tabindex="-1" role="dialog" >
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="height: 500px;">
      <div class="modal-body" style="padding:0">
      	 	<div class="pac-card" id="pac-card">
              <div id="pac-container">
                <input id="pac-input" type="text" placeholder="Digite o local">
                <i class="searchbar-icon"></i>
              </div>
            </div>
            <div id="map"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Concluir</button>
      </div>
    </div>
  </div>
</div>
<script>
//$('.destaqueShadow').find('.col-sm-6:nth-child(2)').remove();
</script>
<!--Janelas Modals-->
<?php 		
//Modal filtros de busca
include(SYS_SRC_APP.'views/pieces/common_modalSearch.php');

//Modal funções de listagem
include(SYS_SRC_APP.'views/pieces/common_modalFunctions.php');

// Common piece bottom
include(SYS_SRC_APP.'views/pieces/common_bottom.php')
?>
</body>
</html>