<?php
	// Seta as configurações da view
	$view->config(array("normalMode"=>true));
	
	// Incorpora todas as abas/formularios/campos pelo arquivo {nome_view}.xml na pasta /templates
	$view->incorporateXML(true);
	
	// Define o título da página
	define("_title", "Parceiros");
	
	// Define o título da página
	define("_columnID", "PAR_ID");
	
	// Define o controller
	define("_controller", "cParceiros");

	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php require SYS_SRC_APP.'views/pieces/common_header.php' ?>
</head>
<body class="theme-blue">
<script>
/**
 * Paginação no navegador(JS) até limite máximo estabelecido ($maxRows), acima disso é por ajax(PHP)
 * @param int init indice de inicio para mostrar x linhas a partir deste ponto
 */
function showData(init){
	// Altera a global que armazena a pagina atual
	//_PAGINA_ATUAL = init;
	// Limpa as linhas já existentes na listagem
	$('#tbData tbody').html('');
	// Loop master
	for ( i=init; i < ( init + _azListingObject.maxRowsPage ) & i < _azListingObject.data.length; i++ ) {
		// Linha com todas as colunas disponibilizadas em objListagem.data
		objRow = _azListingObject.data[i];
		ID = objRow['<?=_columnID?>'];
		
		// Se for cadastro arquivado, exibe nome e hora do arquivamento
		if(objRow['AZ_ARCHIVED']==true) {
			var infoAdc = "Usuário : " + objRow['AZ_USARCHIVED']+" \r\n Data/Hora : "+objRow['AZ_DHARCHIVED'];
			var linha  = '<tr  data-toggle="tooltip" data-placement="top" title="'+infoAdc+'" az-ID="'+ID+'">';
		// Cadastro ativo :
		}else {
			var linha  = "<tr onclick=\"$(this).toggleClass('row_select')\" id='rowList"+ID+"' az-ID='"+ID+"'>";
		}
		// Preenche todas as colunas visiveis da linha retornada pelo WS
		var contColVis = 0;// Contagem de colunas vísiveis exibidas
		for (c in _azListingColumnsArray) {
			var infoAdc = '';
			coluna = _azListingColumnsArray[c];
			// Se for  dispositivo portabil : (REVER ESTA LOGICA)
			colspan = (_IS_PORTABLE==true ? '2' : '');
			
			// Exibi coluna com possiveis informações adc. na primeira coluna (caso seja movel) ou na segunda coluna em PC/Notebook
			if(((contColVis==1 && _IS_PORTABLE==false) || (contColVis==0 && _IS_PORTABLE==true)) && objRow['AZ_ARCHIVED']==true) {
				infoAdc = objRow['AZ_USARCHIVED']+" - "+objRow['AZ_DHARCHIVED'];
				linha += "<td colspan='"+colspan+"'>"+objRow[coluna]+" <span class='label label-default'>Arquivado por "+infoAdc+"</span></td>";		
				
			// Exibe o conteudo da coluna
			} else {
				// Se a coluna da foto do perfil, mostra o thumbnail:
				if(coluna=='PAR_LOGO' && objRow['THUMBNAIL']!='' && objRow['THUMBNAIL']!=undefined) {
					conteudoColuna = '<a href="'+objRow['URL']+'" target="_blank"><img src="'+objRow['THUMBNAIL']+'" height=50 /></a>';
			
				// Conteúdo padrão (apenas texto)	
				}else {
					conteudoColuna=objRow[coluna];
				}
				
				/* Se estiver bloqueado, exibe botão laranja
				if(contColVis==1 && objRow['PAR_STATUS']=='') {
					infoAdc = " <span class='label label-danger'>BLOQUEADO</span>";
				}*/
	
				// Incrementa uma coluna na linha
				linha += "<td colspan='"+colspan+"'>"+conteudoColuna+infoAdc+"</td>";
			}
			
			// Incrementa contador de colunas visiveis
			contColVis++;
		}
		
		// Botao alterar
		btAlterar = "<span class='glyphicon glyphicon-pencil handCursor'  data-toggle='tooltip' data-placement='top'"
					+" title='Alterar cadastro' onclick='fillOutForms("+ID+")'></span> &nbsp; &nbsp;";
		// Botao Deletar
		btExcluir = " <span class='glyphicon glyphicon-remove handCursor'  data-toggle='tooltip' data-placement='top'"
					+" title='Excluir cadastro' onclick=\"archiveSelectedRegisters()\"></span> ";		
					
		// Se estiver arquivado, não permite nenhuma edição
		if(objRow['AZ_ARCHIVED']==true) {
			btAlterar='';	btExcluir='';
		}
		
		// Coluna funções (alterar e remover cadastro) (apenas pc e notebooks)
		<?php if(!$view->isPortable()) :?>
		linha += "<td align='right'>"+btAlterar+" "+btExcluir+"</td>";
		<?php else :?>
		linha += "<td align='right'>"+btAlterar+" </td>";
		<?php endif; ?>
		
		// Fecha linha
		linha += "</tr>";
		// Incrementa linha no corpo da tabela
		$('#tbData tbody').append(linha);
		
		
		// Inicia todas as tooltips (pop-up informativo)
		$('[data-toggle="tooltip"]').tooltip(); 

	}
	
	// Atualiza paginação
	nrPaginas = Math.round((_azListingObject.data.length/_azListingObject.maxRowsPage));
	html = '';
	for(i=0; i < nrPaginas;i++ ) {
		html += '<li><a href="#" onclick="showData('+(i*_azListingObject.maxRowsPage)+')">'+(i+1)+'</a></li>';	
	}
	$('ul.pagination').html(html);
	
	
}
/**
 * Preenche o list de categorias na ordem retornada pelo WS (cadastro)
 * Obs: está função é executada quando fillOutForms() ou prepareContainer() for chamada
 * @param estados:array -> array com os objetos de cada estado retorno pelo WS
 */
function preencheListCategorias(categorias){
	// Reseta todas options do list  categorias
	$('#listcategoria').html('');
	// Exibe os estados encontradas
	for(var c in categorias) {
		var opt = categorias[c];
		var htmlOption = "<option value='"+opt.PAC_ID+"' extra=''>"+opt.PAC_TITULO+"</option>";	
		$('#listcategoria').append(htmlOption);
	}		
};
/**
 * Preenche o list de segmentos na ordem retornada pelo WS (cadastro)
 * Obs: está função é executada quando fillOutForms() ou prepareContainer() for chamada
 * @param estados:array -> array com os objetos de cada estado retorno pelo WS
 */
function preencheListSegmentos(segmentos){
	// Reseta todas options do list  categorias
	$('#listsegmentos').html('');
	// Exibe os estados encontradas
	for(var c in segmentos) {
		var opt = segmentos[c];
		var htmlOption = "<option value='"+opt.SEG_ID+"' extra=''>"+opt.SEG_TITULO+"</option>";	
		$('#listsegmentos').append(htmlOption);
	}	
};
/**
 * Preenche o list de segmentos na ordem retornada pelo WS (cadastro)
 * Obs: está função é executada quando fillOutForms() ou prepareContainer() for chamada
 * @param estados:array -> array com os objetos de cada estado retorno pelo WS
 */
function preencheListEtapasObras(etapas_obras){
	// Reseta todas options do list  categorias
	$('#listetapas').html('');
	// Exibe os estados encontradas
	for(var c in etapas_obras) {
		var opt = etapas_obras[c];
		var htmlOption = "<option value='"+opt.ETA_ID+"' extra=''>"+opt.ETA_TITULO+"</option>";	
		$('#listetapas').append(htmlOption);
	}	
};
/**
 * Preenche o list de estados, conservando a ordem retornada pelo WS (cadastro)
 * Obs: está função é executada quando fillOutForms() ou prepareContainer() for chamada
 * @param estados:array -> todos os estados, com o estado cadastrado na primeira posição
 * @param cidades:array -> todas as cidades, com a cidade de cadastro na primeira posição
 */
function preencheListEstados(estados, cidades){
	// Reseta todas options do list  categorias
	$('#listestados').html('');
	// Exibe os estados encontradas
	for(var c in estados) {
		var opt = estados[c];
		var htmlOption = "<option value='"+opt.EST_ID+"' extra=''>"+opt.EST_SIGLA+"</option>";	
		$('#listestados').append(htmlOption);
	}
	
	// Carrega as cidades (com base no cadastro);
	showCities('listcidades', cidades);
}

/**
 * @desc 	Função para carregar o lista de cidades ao selecionar um estado
 * @param 	string UF : estado
 * @param 	string listcidade : list de cidades
 * @param	string selectID : value do option a ser selecionado após carregamento
 */
function loadCities(UF, list, selectID='') {
	if(UF.trim()=='' || UF==undefined) return(false);
	azSender = new AzulSender();
	// Preapra os os cabeçalhos de 'payload':
	azSender.setPayload({
		controller:'cMain',
		action:'carregarCidades'
	});
	// Prepara o envelope:
	azSender.setEnvelope(
		{
			orderBy : 'CID_NOME DESC',
			filters : [{
				value1	:UF,
				rule	:'=',
				columns	:['CID_ID_ESTADO']
			}]
		}
	);
	// Seta a função pós-processamento com sucesso
	azSender.callback = function(objRet) {
	    // Se estiver em modo de debugação salva e imprime o retorno no console
		if(_DEBUG_MODE) _azGeneral.debug(objRet);

		// Preenche o list cidades
		showCities(list, objRet.ret, selectID);

		// Reseta sa.sender
		azSender.reset();
	}
	// Envia pacote
	azSender.send(true);
}

/**
 * @desc	Preenche as cidades de determinado estado no list informado
 * @param	string list : atrib. id do list de cidades a ser carregado
 * @param	string cidades : array com os objetos das cidades
 */
function showCities(list, cities, selectID='') {
	// Limpa o list cidades
	$('#'+list).html('<option></option>');
	// Preenche o list cidades com todas informações adicionais
	var option = '';
	for (var i in cities) {
		var cid = cities[i];
		selected = selectID==cid.CID_ID ? 'selected' : '';
		// Adiciona um option para cada cidade encontrada
		option = '<option value="'+cid.CID_ID+'" az-codIBGE="'+cid.CID_COD_IBGE+'" '+selected+'>'+cid.CID_NOME+'</option>';
		$('#'+list).append(option);
	}
}
</script>
<?php
	//var_dump($Abas->abas); exit();
	
?>
<!--TOPO-->
<?php include(SYS_SRC_APP.'views/pieces/common_top.php')?>
<!--MENU de navegação do sistema-->
<?php include(SYS_SRC_APP.'views/pieces/common_menu.php')?>
<!--CONTEUDO DA VIEW -->
<div class="content" style="margin-left:240px;">
	<div class="main-content"> 
    	<!-- TITULO DA VIEW -->
        <?=$view->printTitleForm(_title,$view->isPortable())?>
        <!------------------------------------------------------------------
        --- Container ctPrincipal: Formulários de cadastro/informação
        ------------------------------------------------------------------->
        <div class="destaqueShadow bordaCinza hide" id="ctMain">
            <!-- ABAS -->
            <ul class="nav nav-tabs" role="tablist">
            	<?php $view->printTabs($Tabs->tabs); ?>
            </ul>
            <!-- FORMULARIOS (DE CADA ABA) -->
            <div class="tab-content" style="margin-top:15px;">
           		<?php $view->printForms($Tabs->tabs)?>
            </div>
        </div>
        <?php
			// Se for Modo de edição, não mostra a listagem, apenas um cadastro especifico
			if($view->config()->editMode==false) {
				$view->printListing($view->config());
			}
		?>
     </div>
</div>
<!--Janelas Modals-->
<?php 		
//Modal filtros de busca
include(SYS_SRC_APP.'views/pieces/common_modalSearch.php');

//Modal funções de listagem
include(SYS_SRC_APP.'views/pieces/common_modalFunctions.php');

// Common piece bottom
include(SYS_SRC_APP.'views/pieces/common_bottom.php')
?>
</body>
</html>