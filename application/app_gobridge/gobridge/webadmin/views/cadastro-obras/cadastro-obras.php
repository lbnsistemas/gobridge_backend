<?php	
	// Incorpora todas as abas/formularios/campos pelo arquivo {nome_view}.xml na pasta /templates
	$view->incorporateXML(true);
	
	// Define o título da página
	define("_title", "Obras");
	
	// Define o título da página
	define("_columnID", "OBR_ID");
	
	// Define o controller
	define("_controller", "cMain");
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php require SYS_SRC_APP.'views/pieces/common_header.php' ?>
</head>
<body class="theme-blue">
<script>
/**
 *	Função do usuário para preencher todos os formularios da view de forma dinamica
 *
 */
function user_fillOutForms(ID) {
	// Prepara os os cabeçalhos de 'payload':
	_azSender7845.setPayload({
		module: 'app',
		action:'requestData',
		controller: 'cMain',
		view: 'cadastro-obras'
	});
	// Prepara o envelope
	_azSender7845.setEnvelope( {
		ID		: ID
	});
	// Seta a função pós-processamento com sucesso
	_azSender7845.callback = function(objRet) {
	   var r = objRet;
		// Carrega a capa da obra
		$('#btupl_obra_capa').val(r.OBR_FOTOCAPA);
		if(r.filename!='' && r.OBR_FOTOCAPA!='') {
			objRet.uploadFramesContent = [{
				nameID:'uploadCapaObra',
				files:[{name:r.filename, extension:r.extension, thumbnail:r.thumbnail}]
			}];
			showUploadFramesContent(objRet.uploadFramesContent);
		}
		// Seleciona o tipo da obra
		$('#list_obra_tipo option').prop('selected',false);
		$('#list_obra_tipo option[value="'+r.OBR_TIPO+'"]').prop('selected',true);
		$('#list_obra_tipo').attr('disabled',true);
		// Informa a categoria
		$('#list_obra_categoria').html('<option value="'+r.OBR_CAO_ID+'" selected=true>'+r.CAO_TITULO+'</option>');
		$('#list_obra_categoria').attr('disabled',true);
		// Informa a etapa inicial
		$('#list_obra_etapa').html('<option value="'+r.OBR_ETU_ID+'" selected=true>'+r.ETU_TITULO+'</option>');
		$('#list_obra_etapa').attr('disabled',true);
		// Etapas selecionadas para reforma
		$('#list_obra_etapas_reforma').html('');
		if(r.OBR_TIPO=='Reforma') {
			var etapas = r.OBR_CAE_IDS.split(',');
			for(var i=0; i<etapas.length; i++) {
				etapa = etapas[i].split('#');
				if(etapa.length==2) {
					var html = '<option value="'+etapa[0]+'" selected=true>'+etapa[1]+'</option>';
					$('#list_obra_etapas_reforma').append(html);
				}
			}
		}
		$('#list_obra_etapas_reforma').attr('disabled',true);
		// Preenche o campo titulo
		$('#txt_obra_titulo').val(r.OBR_TITULO);
		// Preenche o campo localização
		$('#txt_obra_endereco').val(r.OBR_ENDERECO);
		$('#txt_obra_endereco').attr('lat',r.OBR_LATLNG.substring(0, r.OBR_LATLNG.indexOf(',')));
		$('#txt_obra_endereco').attr('lng',r.OBR_LATLNG.substring(r.OBR_LATLNG.indexOf(',')+1));
		// Preenche o campo area construída
		$('#txt_obra_area').val(r.OBR_AREACONSTRUIDA);
		// Preenche o campo meta de custo por m²
		$('#txt_meta_custom2').val(_azDataManipulation.floatToCurrency(r.OBR_META_CUSTO_M2));
		// Preenche o campo Dt inicio da obra
		$('#txt_obra_dtinicio').val(_azDataManipulation.invertDate(r.OBR_DTINICIO));
		// Preenche o campo Dt para conclusão da obra
		$('#txt_obra_dtfim').val(_azDataManipulation.invertDate(r.OBR_DTFIM));
		// Dados do proprietário
		$('#txtnomeproprietario').val(r.OBR_PROPRIETARIO_NOME);
		$('#txtemailproprietario').val(r.OBR_PROPRIETARIO_EMAIL);
		$('#txttelproprietario').val(r.OBR_PROPRIETARIO_FONE);
		// Carrega o campo slide com o andamento geral atual
		//app.range.get('#view-popup-cadastrar-obra .range-slider').setValue(r.OBR_ANDAMENTO); 
		$('#txt_obra_andamento').val(r.OBR_ANDAMENTO);	
		// Prepara container
		prepareContainer("update",ID);
		// Reseta sa.sender
		_azSender7845.reset();
	}
	// Envia pacote
	_azSender7845.send();	
}
/**
 * Função do usuário para preparar o container para incluir ou editar uma obra
 */
function user_prepareContainer(action, ID) {
	if(action=='insert') {
		// Padroniza o botao de confirmação
		$('#btnConfirmation').attr('az-action','insert');
		$('#btnConfirmation').html('Concluir cadastro');

		// Bloqueia um possível botao de impressão
		$('button[az-action="print"').attr('disabled',true);

		// Prepara o envelope para a ação preparaContainer
		_azSender7845.setPayload({
			module: "app",
			controller: "cMain",
			action: "prepareContainer",
			view: "cadastro-obras" 
		});

		// Seta a função pós-processamento com sucesso
		_azSender7845.callback = function(objRet) {
			// Registra a PK reservada na global
			_RESERVED_ID = objRet.ID;

			// Carrega o list de categorias de obras
			$('#list_obra_categoria').html('');
			for(var i=0; i<objRet.categorias_obras.length; i++) {
				var categoria = objRet.categorias_obras[i];
				var htmlOption = '<option value="'+categoria.CAO_ID+'"  >'+categoria.CAO_TITULO+'</option>';
			 	$('#list_obra_categoria').append(htmlOption);
			}
			
			// Mostra os arquivos já enviados anteriormente (se houver)
			showUploadFramesContent(objRet.uploadFramesContent);
			
			// Carrega o list de etapas relacionadas com o primeiro tipo e categoria já carregados 
			carregaListEtapas(function() {
				$('#list_obra_etapa').attr('disabled',false)
			});
			
			// Registra o ID reservado no attr az-ID do botão de confirmação
			$('#btnConfirmation').attr('az-ID',_RESERVED_ID);
			
			// Preenche o campo endereço com o valor atual carregado no googlemaps
			$('#txt_obra_endereco').val($('#pac-input').val());
			
			// Libera os campos 
			$('#list_obra_tipo').attr('disabled',false);
			$('#list_obra_categoria').attr('disabled',false);
			$('#list_obra_etapas_reforma').attr('disabled',false);
	
			// Mostra formulario e esconde listagem
			showHideListing(action,_RESERVED_ID);

			// Reseta sa.sender
			_azSender7845.reset();

		}
		// Envia pacote
		_azSender7845.send(true);
	}else if(action=='update') {
		// Registra a PK reservada na global
		_RESERVED_ID = ID;
		
		// Chama função para atualizar as estimativas
		calculaEstimativaCustoPrazo();
		
		// Padroniza o botao de confirmação
		$('#btnConfirmation').attr('az-action','update');
		$('#btnConfirmation').html('Concluir alteração');

		// Libera um possível botao de impressão (se existir)
		$('button[az-action="print"]').attr('disabled',false);

		// Mostra formulario e esconde listagem
		showHideListing(action, ID);

		// Registra o ID reservado no attr az-ID do botão de confirmação
		$('#btnConfirmation').attr('az-ID',_RESERVED_ID);
	}
}	


/**
 * Função do usuário inserir ou editar um cadastro
 */
function user_insertUpdateRegister(form, dynamic, action, ID) {	
	var action = $('#btnConfirmation').attr('az-action');
	var ID  = action=='update' ? $('#btnConfirmation').attr('az-id') : 0;
	// Valida o(s) formulario(s) antes de enviar
	if(!_azValidation.exec(form, dynamic, ID)) {
		_azGeneral.alert('Atenção','Algum campo não está correto.');
		return(false);
	}
	
	if($('#uploadCapaObra').attr('value')=='') {
		_azGeneral.alert('Atenção','Envie a imagem capa da obra.');
		return(false);
	}
	
	// Campos do formulario
	fields = [];
	fields.push({column:'list_obra_tipo', value:$('#list_obra_tipo').val()});
	fields.push({column:'list_obra_categoria', value:$('#list_obra_categoria').val()});
	fields.push({column:'list_obra_etapa', value:$('#list_obra_etapa').val()});
	fields.push({column:'list_obra_etapas_reforma', value:$('#list_obra_etapas_reforma').val()});
	fields.push({column:'txt_obra_titulo', value:$('#txt_obra_titulo').val()});
	fields.push({column:'txt_obra_dtinicio', value:_azDataManipulation.invertDate($('#txt_obra_dtinicio').val())});
	fields.push({column:'txt_obra_dtfim', value:_azDataManipulation.invertDate($('#txt_obra_dtfim').val())});
	fields.push({column:'txt_obra_area', value:$('#txt_obra_area').val()});
	fields.push({column:'txt_meta_custom2', value:_azDataManipulation.currencyToFloat($('#txt_meta_custom2').val())});
	fields.push({column:'txtprevisaofim', value:_azDataManipulation.invertDate($('#txtprevisaofim').val())});
	fields.push({column:'txt_obra_andamento', value:$('#txt_obra_andamento').val()});
	fields.push({column:'txt_obra_endereco', value:$('#txt_obra_endereco').val()});
	fields.push({column:'txtnomeproprietario', value:$('#txtnomeproprietario').val()});
	fields.push({column:'txtemailproprietario', value:$('#txtemailproprietario').val()});
	fields.push({column:'txttelproprietario', value:$('#txttelproprietario').val()});
	fields.push({column:'btupl_obra_capa', value:$('#uploadCapaObra').attr('value')});
	fields.push({column:'prx_obr_id', value: _RESERVED_ID});
	// Pega o número de sequencia da etapa atual selecionada pelo usuário
	var cae_sequencia = $('#list_obra_etapa option:selected').attr('cae_sequencia');
	fields.push({column:'cae_sequencia', value:cae_sequencia});
	// Pega a latitude e longitude do endereço
	var lat = parseFloat($('#txt_obra_endereco').attr('lat')).toFixed(5);
	var lng = parseFloat($('#txt_obra_endereco').attr('lng')).toFixed(5);
	fields.push({column:'obr_latlng', value: (lat+','+lng)});
	
	// Instancia AzulSender
	var az = new AzulSender();
	// Seta a ação
	az.setPayload({
		action:  'incluirOuEditarObra', 
		module: 'app',
		controller: 'cMain',
		view : 'cadastro-obras',
	});
	// Prepara o envelope
	az.setEnvelope( {ID : ID, fields : fields } );
	// Seta a função pós-processamento com sucesso
	az.callback = function(objRet) {
		// Reseta os formularios
		resetForms();

		// Fecha formularios de cadastro e exibe listagem
		showHideListing();

		// Reseta global de reserva de PK para cadastro
		_RESERVED_ID = '';
		
		// Reseta sa.sender
		az.reset();
	}
	// Envia pacote
	az.send(silentMode=false);
}	

/**
 * Função do usuário para requisitar dados paginados
 */
function user_requestData(init,orderBy){
	// Fecha janela modal com os filtros de pesquisa
	$('#mdFiltroBusca').modal('hide');
	// Instancia AzulSender
	var az =  new AzulSender(); 
	// Seta a action
	az.setPayload({
		controller: "cMinhasObras",
		action: "verificaMinhasObras",
		module: "app"
	});
	// Prepara o envelope
	//az.setEnvelope();
	// Define a função pós-processamento com sucesso
	az.callback = function(objRet) { 
		// Atualiza _azListingObject com as linhas recebidas
		_azListingObject.data 		 = objRet.obras;
		_azListingObject.dataDefault  = objRet.obras;
		showData(0);
	}
	// Envia pacote
	az.send(silentMode=true);
}

/*
 * Função do botão arquivar obra
 */
function user_archiveRegister(form, ID, objToRemove) {
	var obrID = ID;
	var compartilhada = $(objToRemove).closest('tr').attr('compartilhada')=='true';
	// Confirmação
	bootbox.confirm("Tem certeza que deseja deletar esta obra?", function(result) {
		 if(result) {
			// Seta a ação para arquivar o cadastro
			_azSender7845.setPayload({
				action: "deletarObra",
				module: "app",
				controller: 'cMinhasObras',
				view:'cadastro-obras'
			});
			// Prepara o envelope
			_azSender7845.setEnvelope( {  ID : obrID, extra : {compartilhada:compartilhada}} );
			// Seta a função pós-processamento com sucesso
			_azSender7845.callback = function(objRet) {
				// Exclui a li da obra recém excluída
				$(objToRemove).remove();
				// Reseta sa.sender
				_azSender7845.reset();
			}
			// Envia pacote
			_azSender7845.send(silentMode=false);
		 }
	});
}
 
/**
 * Paginação no navegador(JS) até limite máximo estabelecido ($maxRows), acima disso é por ajax(PHP)
 * @param int init indice de inicio para mostrar x linhas a partir deste ponto
 */
function showData(init){
	// Altera a global que armazena a pagina atual
	//_PAGINA_ATUAL = init;
	// Limpa as linhas já existentes na listagem
	$('#tbData tbody').html('');
	// Loop master
	for ( i=init; i < ( init + _azListingObject.maxRowsPage ) & i < _azListingObject.data.length; i++ ) {
		// Linha com todas as colunas disponibilizadas em objListagem.data
		objRow = _azListingObject.data[i];
		ID = objRow['<?=_columnID?>'];
		
		// Se for cadastro arquivado, exibe nome e hora do arquivamento
		if(objRow['AZ_ARCHIVED']==true) {
			var infoAdc = "Usuário : " + objRow['AZ_USARCHIVED']+" \r\n Data/Hora : "+objRow['AZ_DHARCHIVED'];
			var linha  = '<tr  data-toggle="tooltip" data-placement="top" title="'+infoAdc+'" az-ID="'+ID+'">';
		// Cadastro ativo :
		}else {
			var linha  = "<tr onclick=\"$(this).toggleClass('row_select')\" compartilhada='"+objRow['compartilhada']+"' id='rowList"+ID+"' az-ID='"+ID+"'>";
		}
		// Preenche todas as colunas visiveis da linha retornada pelo WS
		var contColVis = 0;// Contagem de colunas vísiveis exibidas
		for (c in _azListingColumnsArray) {
			var infoAdc = '';
			coluna = _azListingColumnsArray[c];
			// Se for  dispositivo portabil : (REVER ESTA LOGICA)
			//colspan = (_IS_PORTABLE==true ? '2' : '');
			colspan = '';
			
			// Se for a coluna do titulo, pula
			if(_IS_PORTABLE==true && coluna=="OBR_TITULO") continue;
			
			// Exibi coluna com possiveis informações adc. na primeira coluna (caso seja movel) ou na segunda coluna em PC/Notebook
			if(((contColVis==1 && _IS_PORTABLE==false) || (contColVis==0 && _IS_PORTABLE==true)) && objRow['AZ_ARCHIVED']==true) {
				infoAdc = objRow['AZ_USARCHIVED']+" - "+objRow['AZ_DHARCHIVED'];
				linha += "<td colspan='"+colspan+"'>"+objRow[coluna]+" <span class='label label-default'>Arquivado por "+infoAdc+"</span></td>";		
				
			// Exibe o conteudo da coluna
			} else {
				// Se a coluna da foto do perfil, mostra o thumbnail:
				if(coluna=='OBR_FOTOCAPA' && objRow['thumbnail']!='' && objRow['thumbnail']!=undefined) {
					conteudoColuna = '<a href="'+objRow['url']+'" target="_blank"><div style="background-image:url(\''+objRow['thumbnail']+'\')" class="capa"></div></a>';
					
				}else if(coluna=='OBR_FOTOCAPA') {
					conteudoColuna = '<div style="background-image:url(<?=SYS_APP_URL?>../etc/images/sem_imagem.jpg)" class="capa"></div>';
					
				// Se for a coluna do andamento:
				}else if(coluna=='OBR_ANDAMENTO' ) {
					conteudoColuna = montaHTMLAndamento(objRow);
				
				// Coluna do ID da obra
				}else if(coluna=='OBR_ID') {
					conteudoColuna = objRow['OBR_ID'];
					// Verifica se é uma obra compartilhada pelo proprietário e mostra ícone
					if(objRow['compartilhada']) {
						conteudoColuna += ' <BR /><i class="compartilhada fa fa-share-alt"></i>';
					}
				// Conteúdo padrão (apenas texto)	
				}else {
					conteudoColuna=objRow[coluna];
				}
				// Incrementa uma coluna na linha
				linha += "<td colspan='"+colspan+"'>"+conteudoColuna+infoAdc+"</td>";
			}
			
			// Incrementa contador de colunas visiveis
			contColVis++;
		}
		
		// Botao alterar
		btAlterar = "<span class='glyphicon glyphicon-pencil handCursor'  data-toggle='tooltip' data-placement='top'"
					+" title='Alterar cadastro' onclick='fillOutForms("+ID+")'></span> &nbsp; &nbsp;";
		// Botao Deletar
		btExcluir = " <span class='glyphicon glyphicon-remove handCursor'  data-toggle='tooltip' data-placement='top'"
					+" title='Excluir cadastro' onclick=\"archiveRegister('Main', "+ID+", $(this).closest('tr'))\"></span> ";		
					
		// Se estiver arquivado, não permite nenhuma edição
		if(objRow['AZ_ARCHIVED']==true) {
			btAlterar='';	btExcluir='';
		}
		
		// Coluna funções (alterar e remover cadastro) (apenas pc e notebooks)
		<?php if(!$view->isPortable()) :?>
		linha += "<td align='right'>"+btAlterar+" "+btExcluir+"</td>";
		<?php else :?>
		linha += "<td align='right'>"+btAlterar+" </td>";
		<?php endif; ?>
		
		// Fecha linha
		linha += "</tr>";
		// Incrementa linha no corpo da tabela
		$('#tbData tbody').append(linha);
	
		// Inicia todas as tooltips (pop-up informativo)
		$('[data-toggle="tooltip"]').tooltip(); 
	}
	
	// Atualiza paginação
	nrPaginas = Math.round((_azListingObject.data.length/_azListingObject.maxRowsPage));
	html = '';
	for(i=0; i < nrPaginas;i++ ) {
		html += '<li><a href="#" onclick="showData('+(i*_azListingObject.maxRowsPage)+')">'+(i+1)+'</a></li>';	
	}
	$('ul.pagination').html(html);
}

/**
 * Função para preencher/atualizar os lists dinamicamente 
 * @param string listNameID: propriedade ID do list a ser preenchido
 * @param array objRet: dados retornados pelo WS e repassados no preparo do container 
 */
function montaHTMLAndamento(obra){
	var html = '<div  class="etapas">';
	for(var e in obra.etapas_obra_usuario) {
		etapa = obra.etapas_obra_usuario[e];
		html += '<div class="etapa '+etapa.status+'" style="background-image:url(\'<?=SYS_DOMAIN?>app/img/icones/'+etapa.ETA_ICONE+'\')"></div>';
	}
	html += '</div>';
	html += '<div class="progress">';
	  html += '<div class="andamento '+obra.status+' progress-bar" role="progressbar" style="width: '+obra.OBR_ANDAMENTO+'%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">'+obra.OBR_ANDAMENTO+'%</div>';
	html += '</div>';
	html += '<div class="status '+obra.status+'"> '+obra.msg+'</div>';
	return(html);
}


/*
 * Consulta o cadastro de cronograma de prazo e custos (no webadmin), para informar o usuário qual a previsão
 * do custo do M² e o prazo médio para conclusão da obra
 */
function calculaEstimativaCustoPrazo() {
  var areaConstruida = $('#txt_obra_area').val();
  // Se a área construída estiver em branco, zera as previsões e sai da função
  if(areaConstruida.trim()==0){
		$('#txt_meta_custom2').html('0,00');  
		$('#txtprevisaofim').html('');
		return(false);
  }
  // Seta a action
  _azSender7845.setPayload({
    action: 'calculaEstimativaCustoPrazo',
    module: 'app',
	controller: 'cMain',
	view:'cadastro-obras'
  });
  // Seta o envelope
  _azSender7845.setEnvelope({
	 extra: {
		 areaConstruida:areaConstruida,
		tipo:$('#list_obra_tipo').val(),
		categoria:$('#list_obra_categoria').val(),
		andamento:$('#txt_obra_andamento').val(),
		dtInicio:_azDataManipulation.invertDate($('#txt_obra_dtinicio').val()),
	 }
  });
  // Seta a função pós-processamento com sucesso
  _azSender7845.callback = function(objRet) { 
  	 // Mostra previsão do custo por m²
	 $('#txt_meta_custom2').val(_azDataManipulation.floatToCurrency(objRet.prevMetaCustoM2.valor));
	 // Fonte do custo médio do m2
	// $('#popup-cadastrar-obra .infoFonteM2').attr('msg',objRet.prevMetaCustoM2.fonte );
	 // Mostra a previsão da data de conclusão
	 //$('#popup-cadastrar-obra .prevDtConclusao').html(_azDataManipulation.invertDate(objRet.prevDtConclusao));
	 $('#txtprevisaofim').val(_azDataManipulation.invertDate(objRet.prevDtConclusao));
	 $('#txt_obra_dtfim').val( $('#txtprevisaofim').val() );
  
      // Reseta sa.sender
      _azSender7845.reset();
  }

  // Envia pacote
  _azSender7845.send(silentMode=true);	
}


/**
 * Função para somar e calcular o andamento geral da obra
 */
function calculaAndamentoGeral() {
	// Soma todas as porcentagens das etapas anteriores
	var porcAnd = 0; // % do andamento geral da obra
	var etapasObra = $('#list_obra_etapa').find('option');
	for(var i=0;i<etapasObra.length;i++) {
		if($($(etapasObra)[i]).prop('selected')) break;
		var cro_porc = $($(etapasObra)[i]).attr('cro_porc');
		if(cro_porc>0){
			porcAnd += parseInt(cro_porc);	
		}
	}
	$('#txt_obra_andamento').val(porcAnd);	
}

/*
 * Carrega o select das etapas no cadastro de obra
 * OBS: quando for reforma, é o usuário que determina quais etapas fazem parte da reforma dele
 */
function carregaListEtapas( callback='') {
   if($('#list_obra_categoria').val()=='' || $('#list_obra_tipo').val()=='') return(false);
   // Seta a action
  _azSender7845.setPayload({
	controller: 'cMain',
	module: 'app',
    action: 'carregaListEtapasRelacionadas',
    view:'cadastro-obras'
  });
  // Seta o envelope
  _azSender7845.setEnvelope({
	 fields: [
	 	{column:'categoria', value:$('#list_obra_categoria').val()},
		{column:'tipo', value:$('#list_obra_tipo').val()}
	 ] 
  });
  // Seta a função pós-processamento com sucesso
  _azSender7845.callback = function(objRet) {
      if(_DEBUG_MODE) _DEBUGGER = objRet;
	  
	  // Se for reforma, preenche e mostra o list com as possíveis etapas da reforma do cliente
	  if($('#list_obra_tipo').val()=='Reforma') {
		  // Desbloqueia o select2 das etapas para reforma
		  $('#list_obra_etapas_reforma').attr('disabled',false)
		  // Carrega as etapas para reforma da obra
		  $('#list_obra_etapas_reforma').html('');
		  $('#list_obra_etapa').html('');
		  if(objRet.categorias_obras_etapas.length>0){
			for(var i in objRet.categorias_obras_etapas){
			  var row = objRet.categorias_obras_etapas[i];
			  var htmlOption = '<option value="'+row.CAE_ID+'" eta_id="'+row.ETA_ID+'" '
			  +' cae_sequencia="'+row.CAE_SEQUENCIA+'" cro_porc="'+parseInt(row.CRO_PORC)+'" >'+row.ETA_TITULO+'</option>';
			  $('#list_obra_etapas_reforma').append(htmlOption);
			  $('#list_obra_etapa').append(htmlOption);
			}
		  }
		  $('#list_obra_etapas_reforma').select2();

		  
	  // Obra Nova: o sistema já determina quais as etapas de acordo a categoria de obra selecionada previamente
	  }else {	
	  	  // Bloqueia o select2 das etapas para reforma
		  $('#list_obra_etapas_reforma').attr('disabled',true)
		  
		  // Carrega o list para escolher a etapa inicial da obra
		  $('#list_obra_etapa').html('');
		  if(objRet.categorias_obras_etapas.length>0){
			for(var i in objRet.categorias_obras_etapas){
			  var row = objRet.categorias_obras_etapas[i];
			  var htmlOption = '<option value="'+row.CAE_ID+'" eta_id="'+row.ETA_ID+'" '
			  +' cae_sequencia="'+row.CAE_SEQUENCIA+'" cro_porc="'+parseInt(row.CRO_PORC)+'" >'+row.ETA_TITULO+'</option>';
			  $('#list_obra_etapa').append(htmlOption);
			}
		  }
	  }
	  
	  // Callback function
	  if(callback!='') callback();
	  
      // Reseta sa.sender
      _azSender7845.reset();
  }

  // Envia pacote
  _azSender7845.send(true);
}

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
function initMap() {
	//<BUG> framework7, não aparece a listagem do autocompletar google
	$(document).on({
		'DOMNodeInserted': function() {
			$('.pac-item, .pac-item span', this).addClass('no-fastclick');
		}
	}, '.pac-container');
	var input = document.getElementById('pac-input');
	//</BUG> 
	
	// Mostra inicialmente o marcado no local atual do usuário
	if($('#txt_obra_endereco').val()!='') {
		var latlng = $('#txt_obra_endereco').val().split(',');
		var lat = latlng[0];
		var lng = latlng[1];
		var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
	}else {
		var latlng = {lat: _GL_LATITUDE, lng: _GL_LONGITUDE};
	}
	
	// Google Map
	var map = new google.maps.Map(document.getElementById('map'), {
	  center: latlng,
	  zoom: 17
	});
	
	// objeto para reverse geocoding (encontra o endereço de uma coordenada)
	var geocoder = new google.maps.Geocoder;
	
	// objeto auto complete
	var autocomplete = new google.maps.places.Autocomplete(input);
		
	// Bind the map's bounds (viewport) property to the autocomplete object,
	// so that the autocomplete requests use the current map bounds for the
	// bounds option in the request.
	autocomplete.bindTo('bounds', map);
	
	// Set the data fields to return when the user selects a place.
	autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
	
	// Cria e inicia o marcador com a posição atual do usuário
	var marker = new google.maps.Marker({
	  map: map,
	  anchorPoint: new google.maps.Point(0, -29),
	  draggable: true,
	  position: latlng
	});	
	
	
	geocoder.geocode({'location': latlng}, function(results, status) {
	  if (status === 'OK') {
		if (results[0]) {
		  $('#txt_obra_endereco').attr('lat', latlng.lat);
		  $('#txt_obra_endereco').attr('lng', latlng.lng);	
		  $('#txt_obra_endereco').val(results[0].formatted_address);
		  $('#pac-input').val(results[0].formatted_address); 
		} else {
		  console.log('No results found.');
		}
	  } else {
		 console.log('Geocoder failed due to: ' + status);
	  }
	});
	
	//Atualiza as coordenadas ao movimentar o marcador
	google.maps.event.addListener(marker, 'dragend', function (event) {
		var lat = this.getPosition().lat();
		var lng = this.getPosition().lng();
		// Encontra o endereço para a nova geolocalização
		var latlng = {lat: lat, lng: lng};
		geocoder.geocode({'location': latlng}, function(results, status) {
		  //console.log(results);
		  if (status === 'OK') {
			if (results[0]) {
			  $('#txt_obra_endereco').val(results[0].formatted_address);
			  $('#txt_obra_endereco').attr('lat',lat);
			  $('#txt_obra_endereco').attr('lng',lng);
			  $('#pac-input').val(results[0].formatted_address);
			} else {
			  console.log('No results found.');
			}
		  } else {
			 console.log('Geocoder failed due to: ' + status);
		  }
		});
	});
	
	// Ao escolher um novo endereço da lista, muda o marcador e reposiciona o mapa
	autocomplete.addListener('place_changed', function() {
	  marker.setVisible(false);
	  place = autocomplete.getPlace();
	  if (!place.geometry) {
		// User entered the name of a Place that was not suggested and
		// pressed the Enter key, or the Place Details request failed.
		_azGeneral.alert("Detalhes indisponíveis para o endereço: '" + place.name + "'");
		return;
	  }
	  
	  // If the place has a geometry, then present it on a map.
	  if (place.geometry.viewport) {
		map.fitBounds(place.geometry.viewport);
	  } else {
		map.setCenter(place.geometry.location);
		map.setZoom(17);  // Why 17? Because it looks good.
	  }
	  //console.log(place.geometry.location);
	  marker.setPosition(place.geometry.location);
	  marker.setVisible(true);
	
	  var address = '';
	  if (place.address_components) {
		address = [
		  (place.address_components[0] && place.address_components[0].short_name || ''),
		  (place.address_components[1] && place.address_components[1].short_name || ''),
		  (place.address_components[2] && place.address_components[2].short_name || '')
		].join(' ');
	  }
	  
	  //Salva o endereço e as coordenadas no campo txt_obra_endereco
	  var lat = place.geometry.location.lat();
	  var lng = place.geometry.location.lng();
	  $('#txt_obra_endereco').val(address);
	  $('#txt_obra_endereco').attr('lat',lat);
	  $('#txt_obra_endereco').attr('lng',lng); 
	  $('#pac-input').val(address);  
	});	
}
// Seleciona o campo ao receber o focus
$('#pac-input').on('click',function() {
	 $('#pac-input')[0].setSelectionRange(0, $('#pac-input').val().length);	
	 $('#pac-input').focus();
});

// Carrega e executa API da google
function abreMapaGoogle() {
	if(window.google==undefined) 
		$.getScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyDFBtU-m944g8eI373JVAk6gWEr0s2oQ1E&callback=initMap&libraries=places');
		
	// Abre o popup/modal para o usuario selecionar a posição no mapa
	$("#popup-geolocation").modal('show');
}
</script>
<style>
/* 
* Always set the map height explicitly to define the size of the div
* element that contains the map. 
*/
#map {
height: 415px;
}
/* Optional: Makes the sample page fill the window. */
html, body {
height: 100%;
margin: 0;
padding: 0;
}
.pac-container {
  z-index:100000;	  
}
.pac-card {
border-radius: 2px 0 0 2px;
box-sizing: border-box;
-moz-box-sizing: border-box;
outline: none;
box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
background-color: #fff;
font-family: Roboto;
position: absolute;
z-index: 1;
top:60px;
width:95%;
left: 2.5%;
}
#pac-input {
font-size: 15px;
font-weight: bold;
text-overflow: ellipsis;
width: 100%;
background-color: #ff8a52;
padding:15px;
padding-left: 25px;
}
#popup-geocoding-cadastrar-obra .searchbar-icon {
width: 13px;
height: 13px;
position: absolute;
top: 50%;
margin-top: -6px;
background-image: url(data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%…5'%20x1%3D'12.6'%20y1%3D'12.6'%20x2%3D'8.2'%20y2%3D'8.2'%2F%3E%3C%2Fsvg%3E);
background-size: 13px 13px;
z-index: 40;
left: 8px;
}

/* Tabela/Listagem */
#tbData th:nth-child(1){
	width:10%;	
}
#tbData th:nth-child(2){
	width:15%;	
}
#tbData th:nth-child(3){
	width:35%;	
}
#tbData th:nth-child(4){
	width:30%;	
}
#tbData th:nth-child(5){
	width:10%;	
}
/* CSS para o grafico de andamento por etapa */
/* foto capa da obra */
.capa{
   width: 100px;
   height: 100px;
   background-repeat: no-repeat !important;
   background-position: center !important;
   background-size: cover !important;
}
/* div das etapas */
.etapas{
	display: inline-block; 
	text-align: center	;
	width:100%;
}

/* icones das etapas */
@media screen and (max-width:600px) {
	.etapa {
		width: 70px !important;
		height:70px !important;
	}
}
.etapa {
   width:30px !important;
   height:30px !important;
   background:#fff;
   border-radius: 50%;
   border: 2px solid #ccc;
   float:left;
   margin-left:14px;
   background-repeat: no-repeat !important;
   background-position: center !important;
   background-size: contain !important;
  
}
.etapa:after {
  content: '';
   display: inline-block;
   width: 10px;
   height: 10px;
   -moz-border-radius: 7.5px;
   -webkit-border-radius: 7.5px;
   border-radius: 7.5px;
   background-color: #ccc;
   margin-top: 33px;
}
.etapa.ok:after,  .etapa.ok{
   background-color: green;
   border-color: green !important;
}
.etapa.atraso:after, .etapa.atraso{
   background-color: red;
   border-color: red !important;
}
 .etapa.atrasando:after, .etapa.atrasando{
   background-color: orange;
   border-color: orange !important;
}

#tbData td:nth-child(4) {
	background-color:inherit !important;	
}
/* Barra de andamento geral da obra */
.andamento.ok {background-color: green !important;}
.andamento.atraso  {background-color: red !important;}
.andamento.atrasando  {background-color: orange !important;}

/* Porcentagem do andamento e status da obra */
.porcentagem.ok, .status.ok{ color: green ;}
.porcentagem.atraso, .status.atraso{color: red ;}
.porcentagem.atrasando, .status.atrasando{color: orange ;}

/* Icone de obra compartilha */
i.compartilhada{
  color: blue;
  font-size:20px;
}
</style>
<!--TOPO-->
<?php include(SYS_SRC_APP.'views/pieces/common_top.php')?>
<!--MENU de navegação do sistema-->
<?php include(SYS_SRC_APP.'views/pieces/common_menu.php')?>
<!--CONTEUDO DA VIEW -->
<div class="content" style="margin-left:240px;">
	<div class="main-content"> 
    	<!-- TITULO DA VIEW -->
        <?=$view->printTitleForm(_title,$view->isPortable())?>
        <!------------------------------------------------------------------
        --- Container ctPrincipal: Formulários de cadastro/informação
        ------------------------------------------------------------------->
        <div class="destaqueShadow bordaCinza hide" id="ctMain">
            <!-- ABAS -->
            <ul class="nav nav-tabs" role="tablist">
            	<?php $view->printTabs($Tabs->tabs); ?>
            </ul>
            <!-- FORMULARIOS (DE CADA ABA) -->
            <div class="tab-content" style="margin-top:15px;">
           		<?php $view->printForms($Tabs->tabs)?>
            </div>
        </div>
        <?php
			// Se for Modo de edição, não mostra a listagem, apenas um cadastro especifico
			if($view->config()->editMode==false) {
				$view->printListing($view->config());
			}
		?>
     </div>
</div>

<!-- Janela modal geolocation da obra -->
<div class="modal fade" id="popup-geolocation" tabindex="-1" role="dialog" >
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="height: 500px;">
      <div class="modal-body" style="padding:0">
      	 	<div class="pac-card" id="pac-card">
              <div id="pac-container">
                <input id="pac-input" type="text" placeholder="Digite o local">
                <i class="searchbar-icon"></i>
              </div>
            </div>
            <div id="map"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Concluir</button>
      </div>
    </div>
  </div>
</div>
<script>
$('.destaqueShadow').find('.col-sm-6:nth-child(2)').remove();
</script>


<!--Janelas Modals-->
<?php 		
//Modal filtros de busca
include(SYS_SRC_APP.'views/pieces/common_modalSearch.php');

//Modal funções de listagem
include(SYS_SRC_APP.'views/pieces/common_modalFunctions.php');

// Common piece bottom
include(SYS_SRC_APP.'views/pieces/common_bottom.php')
?>
</body>
</html>