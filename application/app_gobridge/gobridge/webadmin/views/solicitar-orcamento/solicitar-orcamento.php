<?php	
	// Incorpora todas as abas/formularios/campos pelo arquivo {nome_view}.xml na pasta /templates
	$view->incorporateXML(false);
	
	// Configurações da View
	$view->config(array(
		'ID'			=> 0, 		// ID inicial para buscar os dados dos formularios
		'editMode'	=> false,	// Prepara o container principal apenas para editar determinado cadastro
		'normalMode'	=> false,	// Carrega todos containers e funções da View (CRUD)
	));
	
	// Define o título da página
	define("_title", "Orçamentos");
	
	// Define o título da página
	define("_columnID", "ORC_ID");
	
	// Define o controller
	define("_controller", "cOrcamentos");

	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php require SYS_SRC_APP.'views/pieces/common_header.php' ?>
</head>
<body class="theme-blue">
<script>
// Carrega e preenche o list com as obras (do usuario e compartilhadas com ele)
function carregaObras() {
	var az = new AzulSender();
	az.setPayload({
		module: 'app',
		controller: 'cMain',
		action:'carregaObras',
		view: 'solicitar-orcamento'
	});
	az.callback = function(objRet) {
		var obras = objRet.obras;
		if(obras.length>0){
			$('#list_orcamento_obra').html('');
			//var htmlOption = '<option value=""  ></option>';
			//$('#list_compra_obra').append(htmlOption);
			for(var i in obras){
			  var row = obras[i];
			  var htmlOption = '<option value="'+row.OBR_ID+'"   >'+row.OBR_TITULO+'</option>';
			  $('#list_orcamento_obra').append(htmlOption);
			}
		 }	
		// Reseta sa.sender
		az.reset();
		// Carrega os fornecedores
		carregaFornecedores(obras[0].OBR_ID);
	};
	az.send();
}

// Carrega e preenche o list dos fornecedores (trazendo no topo os que já vem sendo cotado para a obra selecionada)
function carregaFornecedores(obrID) {
	var az = new AzulSender();
	az.setPayload({
		module: 'app',
		controller: 'cMain',
		action:'carregaParceiros',
		view: 'solicitar-orcamento'
	});
	az.setEnvelope({
		extra : {
			ORC_OBR_ID : obrID	
		}
	});
	az.callback = function(objRet) {
		var parceiros = objRet.parceiros;
		if(parceiros.length>0){
			$('#list_dest_fornecedor').html('');
			//var htmlOption = '<option value=""  ></option>';
			//$('#list_compra_obra').append(htmlOption);
			for(var i in parceiros){
			  var row = parceiros[i];
			  var htmlOption = '<option value="" par_id="'+row.PAR_ID+'" full_email="'+row.EMAIL+'" email="'+row.EMAIL_RESUMIDO+'" responsavel="'+row.RESPONSAVEL+'" orc_id="'+row.ORC_ID+'" >'+row.FORNECEDOR+'</option>';
			  $('#list_dest_fornecedor').append(htmlOption);
			}
			// Dispara evento Change para carrega 
			$('#list_dest_fornecedor').trigger('change');
			// Se o formulario estiver oculto, mostra-o
			if($('#formDestinatarios').hasClass('hide')) {
				$('#btnAbrirFormDest').trigger('click');
			}
		 }	
		// Reseta sa.sender
		az.reset();
	};
	az.send(true);	
}

// Função para preencher o email, email resumido e nome do responsavel
function preencheCamposDest(obj){
	var option = $(obj).find('option:selected');
	// Preenche o email
	$('#txt_dest_email').val($(option).attr('email'));
	// Preenche o email completo
	$('#txt_dest_email').attr('full_email',$(option).attr('full_email'));
	// Preenche o responsavel
	$('#txt_dest_responsavel').val($(option).attr('responsavel'));
	// Se já for email cadastrado na base BrickBe, bloqueia o campo email e responsavel
	// e mostra parcialmente o email
	if($(option).attr('orc_id')>0 || $(option).attr('par_id')>0) {
		$('#txt_dest_email').attr('disabled',true);
		$('#txt_dest_responsavel').attr('disabled',true);
	}else {
		$('#txt_dest_email').attr('disabled',false);
		$('#txt_dest_responsavel').attr('disabled',false);
	}
}

// Criar e enviar solicitação
function criarEnviarSolicitacao() {
	/*
	 	VALIDAÇÕES:
	*/
	// Valida o(s) formulario(s) antes de enviar
	if(!_azValidation.exec('Main')) {
		_azGeneral.alert('Atenção','Informe a obra.');
		return(false);
	}
	// Verifica se tem no mínimo 1 ítem
	var nrItens = $('#formItensSolicitados .row:not(:first)').length;
	if(nrItens==0) {
		_azGeneral.alert('Atenção','Você precisa adicionar no mínimo um ítem na solicitação.');
		if($('#formItensSolicitados').hasClass('hide')){
			$('#btnAbrirFormItens').trigger('click');	
		}
		return(false);	
	}
	// Valida cada item da solicitação
	for(var i = 1; i<=nrItens; i++) {
		if(!_azValidation.exec('formItensSolicitados',true,i)) {
			if($('#formItensSolicitados').hasClass('hide')){
				$('#btnAbrirFormItens').trigger('click');	
			}
			_azGeneral.alert('Atenção','É obrigatório informar a quantidade e a descrição em todos os ítens.');
			return(false);
		}
	}
	
	// Verifica se tem no mínimo 1 destinatário
	var nrDestinatarios = $('#formDestinatarios .row').length-1;
	/*if(nrItens==0) {
		_azGeneral.alert('Atenção','Você precisa adicionar no mínimo um destinatário para a solicitação.');
		return(false);	
	}*/
	/*
	 	MONTAGEM DOS CAMPOS PARA ENVIO:
	*/
	var extraObj = {};
	extraObj.itens = [];
	extraObj.destinatarios = [];
	// Dados gerais
	extraObj.list_orcamento_obra = $('#list_orcamento_obra').val();
	extraObj.txt_orcamento_observacao = $('#txt_orcamento_observacao').val();
	extraObj.txt_orcamento_dtprevista = _azDataManipulation.checkIn('#txt_orcamento_dtprevista');
	extraObj.qtdGeral = 0;
	// Itens da solicitação
	for(var i=1; i<=nrItens; i++) {
		var iten = {};
		iten.qtd = parseInt($('#txt_item_qtd'+i).val());
		iten.descricao = $('#txt_item_descricao'+i).val();
		// Incrementa array com o ítem
		extraObj.itens.push(iten);
		// Soma total de ítens sendo solicitados
		extraObj.qtdGeral += iten.qtd;
	}
	// Destinatários
	for(var i=1; i<=nrDestinatarios; i++) {
		var dest = {};
		dest.parID = $('#list_dest_fornecedor'+i).attr('par_id')
		dest.empresa = $('#list_dest_fornecedor'+i).val()
		// Se o email estiver mascarado, pega o email completo
		if($('#txt_dest_email'+i).attr('full_email')!=''){
			dest.email =  $('#txt_dest_email'+i).attr('full_email');
			
		// Pega o email digitado normalmente
		}else {
			dest.email = $('#txt_dest_email'+i).val();
			
		}
		dest.responsavel = $('#txt_dest_responsavel'+i).val();
		// Incrementa array com o ítem
		extraObj.destinatarios.push(dest);
	}
	console.log(extraObj);
	/*
		ENVIO:	
	*/
	var az = new AzulSender();
	az.setPayload({
		module: 'app',
		controller: 'cOrcamentos',
		action:'criarEnviarSolicitacaoOrcamento',
		view: 'solicitar-orcamento'
	});
	az.setEnvelope({
		extra : extraObj
	});
	az.callback = function(objRet) {
		// Reseta os formularios
		resetForms();
		// Reseta sa.sender
		az.reset();
	};
	az.send();
}

// Incluir novo ítem
function incluirNovoItem() {
	if($('#txt_item_qtd').val()=='') {
		return(false);	
	}
	var nrItens = $('#formItensSolicitados .row:not(:first)').length+1;
	// Cria um objeto para duplicar o ítem adicionado
	linesObject=[{}];
	linesObject[0].ORI_ID = nrItens;
	linesObject[0].ORI_QTD = $('#txt_item_qtd').val();
	linesObject[0].ORI_DESCRICAO = $('#txt_item_descricao').val();
	// Exibe a linha duplicada
	showDynamicFormDataList('formItensSolicitados',linesObject)
	// Muda a função onblur de todos os campos
	$('#formItensSolicitados .row:not(:first) .az-field').attr('onBlur',"if($(this).parent().hasClass('has-error')) _azValidation.exec('formItensSolicitados',true, "+nrItens+"); ");
	// Muda a função do botão deletar
	$('#formItensSolicitados .row:not(:first) .glyphicon-remove').attr('onClick','removeLinha(this)');
}

// Incluir novo destinario
function incluirNovoDestinatario() {
	if($('#txt_dest_email').val()=='') {
		return(false);	
	}
	var nrDest = $('#formDestinatarios .row:not(:first)').length+1;
	var empresa = $('#list_dest_fornecedor option:selected').text();
	var parID = $('#list_dest_fornecedor option:selected').attr('par_id');
	if(parID==undefined) parID = 0;
	var email = $('#txt_dest_email').val();
	var responsavel = $('#txt_dest_responsavel').val();
	
	// Duplica a primeira linha
	var objLinha = $('#formDestinatarios .row:first').clone();
	
	// Cria campo input com nome da empresa
	$(objLinha).find('.col-xs-12:eq(0) .form-group').html("<input type='text' par_id='"+parID+"' value='"+empresa+"' class='fornecedor form-control az-field' id='list_dest_fornecedor' disabled=true />");
		
	// Tira o botão mais e coloca apenas o botão 'x'
	$(objLinha).find('.col-xs-1').html('<span class="glyphicon glyphicon-remove handCursor"  title="" onclick="removeLinha(this)" style="margin-top:8px"></span>');
	
	// Remove todos os labels
	$(objLinha).find('label').remove();
	
	// Trava todos os campos para evitar alteração (somente exclusão)
	$('#formDestinatarios .row:not(:first) .az-field').attr('disabled',true);
	
	// Exibe a nova linha
	$('#listformDestinatarios').append($(objLinha)[0].outerHTML);
	
	// Acerta todos os IDs
	$('#listformDestinatarios .row:last').find('input').each(function() {
		console.log(this);
		var oldID = $(this).attr('id');
		$(this).attr('id', oldID + nrDest);
		$(this).attr('value', $('#'+oldID).val());
		$(this).val($('#'+oldID).val());
		if(oldID=='txt_dest_email'){
			$(this).attr('full_email', $('#'+oldID).attr('full_email'));
			
		}
		if(oldID=='list_dest_fornecedor'){
			$(this).val($('#'+oldID).find('option:selected').text());
		}
		
	});
	
	
	// Reseta os campos
	$('#txt_dest_email').attr('full_email','');
	$('#txt_dest_email').val('');
	$('#txt_dest_responsavel').val('');

}
// Remover linha (item ou destinatario)
function removeLinha(obj) {
	$(obj).closest('.row').remove();
}


</script>
<!--TOPO-->
<?php include(SYS_SRC_APP.'views/pieces/common_top.php')?>
<!--MENU de navegação do sistema-->
<?php include(SYS_SRC_APP.'views/pieces/common_menu.php')?>
<!--CONTEUDO DA VIEW -->
<div class="content" style="margin-left:240px;">
	<div class="main-content"> 
    	<div class="destaqueShadow bordaCinza" id="ctMain">
            <!-- ABAS -->
            <ul class="nav nav-tabs" role="tablist">
            	<li role="presentation" class="active">
                	<a href="#pntabDadosdoOrcamento" aria-controls="pntabDadosdoOrcamento" role="tab" data-toggle="tab">Dados da solicitação</a>
                </li>	
            </ul>
            <!-- Formulario da Aba principal -->
            <div class="tab-content" style="margin-top:15px;">
           		<!--Aba Principal-->
                <div role="tabpanel" class="tab-pane active" id="pntabDadosdoOrcamento">         
                      <!--FORMULARIO : Principal -->
					  <form class="az-form   main" id="formDadosdoOrcamentoPrincipal" onsubmit="return(false)">
                     	 <fieldset>
						 	<div class="row">	
								 <div class="col-xs-12 col-md-3 col-lg-">
								      <!--CAMPO Obra-->
                                      <div class="form-group  initial">
                                        <label for="list_orcamento_obra">Obra </label> 										
                                        <select name="list_orcamento_obra" id="list_orcamento_obra" az-required="true" az-condition="" class="form-control az-field az-select" az-datatype="text" az-datatype-default="text" az-title-default="Obra" az-column-db="ORC_OBR_ID"  placeholder="" onBlur="carregaFornecedores(this.value)" title="" az-fieldtype="select" az-id="">
            
                                            <option value=""></option>
            
                                        </select>
                                        <div class="help-block with-errors"></div>
                                      </div>
							      </div>								
                                 <div class="col-xs-12 col-md-6 col-lg-">
								      <!--CAMPO Observação-->
                                      <div class="form-group  initial">
                                        <label for="txt_orcamento_observacao">Observação </label> 									
							<input type="input" name="txt_orcamento_observacao" id="txt_orcamento_observacao" maxlength="" value="" az-default="" az-required="false" az-condition="" class="form-control az-field az-input" az-datatype="text" az-datatype-default="text" az-title-default="Titulo" az-column-db="ORC_OBSERVACAO" placeholder="" title="" az-fieldtype="input" az-id="">
                                        <div class="help-block with-errors"></div>
                                      </div>
							       </div>								
                                 <div class="col-xs-12 col-md-3 col-lg-">
								   <!--CAMPO Dt. prevista para realização da compra-->
                                      <div class="form-group  initial">
                                        <label for="txt_orcamento_dtprevista">Dt. prevista p/ compra</label> 							
                                        <div class="input-group date" id="dhtxt_orcamento_dtprevista">
                                        <input type="input" name="txt_orcamento_dtprevista" id="txt_orcamento_dtprevista" maxlength="10" value="" az-default="" az-required="true" az-condition="" class="form-control az-field az-input" az-datatype="date" az-datatype-default="date" az-title-default="Dt. prevista p/ compra" az-column-db="ORC_DATAPREVISTA" placeholder="" title="Ex. : 01/01/2020" az-fieldtype="input" az-id="" data-format="dd/MM/yyyy" date-format="DD/MM/YYYY" onkeypress="return(_azDataManipulation.formatField('##/##/####','9',this,event))">
             
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>	
                                         <script type="text/javascript">
                                            // Campo data hora 1
                                            $("#dhtxt_orcamento_dtprevista").datetimepicker({
                                                locale : "pt-br",
                                                format : "DD/MM/YYYY"
                                            });
                                         </script>
				                         <div class="help-block with-errors"></div>
                                      </div>
							    	</div>								
						    </div>
                         </fieldset>
                      </form> 
                      
                      <!-- Ítens da solicitação de orçamento -->   
                      <div class="row ">
                        <div class="col-xs-12">
                        	<br>
                            <label class="bold handCursor" id="btnAbrirFormItens" onclick="showHideDynamicForm('formItensSolicitados',this)">
                            <i>+ Ítens solicitados</i> 
                            </label>                      
                        </div>
                      </div>
                      <form class="az-form   dynamic hide" id="formItensSolicitados" onsubmit="return(false)">
                     	 <fieldset>
						    <div class="row">	
								<div class="col-xs-12 col-md-2">
								      <!--CAMPO Qtd-->
                                      <div class="form-group  initial">
                                        <label for="txt_item_qtd">Qtd </label> 										
										<input type="input" name="txt_item_qtd" id="txt_item_qtd" maxlength="9" value="" az-default="" az-required="true" az-condition="" class="form-control az-field" az-datatype="number" az-datatype-default="" az-title-default="" az-column-db="ORI_QTD" placeholder=""  az-fieldtype="input" az-id="">
                                        <div class="help-block with-errors"></div>
                                      </div>
							      </div>								
                                  <div class="col-xs-12 col-md-8">
								      <!--CAMPO Descrição-->
                                      <div class="form-group  initial">
                                        <label for="txt_item_descricao">Descrição </label> 		
										<input type="input" name="txt_item_descricao" id="txt_item_descricao" maxlength="250" value="" az-default="" az-required="true" az-condition="" class="form-control az-field az-input" az-datatype="text" az-datatype-default="" az-title-default="" az-column-db="ORI_DESCRICAO" placeholder="" az-fieldtype="input" az-id="" >
 									 	<div class="help-block with-errors"></div>
                                      </div>
							      </div>						
                                  <div class="col-xs-1 col-md-1">
                                        <!-- Botão adicionar novo item -->
                                        <div class="form-group">
                                        <button class="btn btn-success margin-bottom" style="margin-top:24px;" 
                                        onclick="incluirNovoItem()">
                                            <b>+</b>
                                        </button>
                                        </div>
                                  </div>
                             </div>
						     <!--LISTAGEM DE itens-->
                             <div class="" id="listformItensSolicitados"></div>
                         </fieldset>
                      </form>
                      
                      <!-- Encaminhamento para destinatários -->
                      <div class="row ">
                        <div class="col-xs-12">
                        	<br>
                            <label class="bold handCursor"  id="btnAbrirFormDest" onclick="showHideDynamicForm('formDestinatarios',this)">
                            <i>+ Fornecedores / Destinatários</i> 
                            </label> 
                            <!--<i class="glyphicon glyphicon-info-sign handCursor" onclick="_azGeneral.alert('Informações','Parcelas da venda')"></i>    -->                     
                        </div>
                      </div>
                      <form class="az-form  hide dynamic" id="formDestinatarios" onsubmit="return(false)">
                     	 <fieldset>
						 	<div class="row">	
								 <div class="col-xs-12 col-md-3 col-lg-">
								      <!--CAMPO Fornecedor-->
                                      <div class="form-group  initial">
                                        <label for="list_dest_fornecedor">Fornecedor </label> 										
										<select onChange="preencheCamposDest(this)" name="list_dest_fornecedor" id="list_dest_fornecedor" az-default="" az-required="true" az-condition="" class="fornecedor form-control az-field az-select" az-datatype="text" az-datatype-default="" az-title-default="" az-column-db="ORE_EMPRESA" placeholder="" title="Fornecedor" az-fieldtype="select" az-id="">
                                        	<option value=""></option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                      </div>
							     </div>								
                                 <div class="col-xs-12 col-md-4 col-lg-">
								      <!--CAMPO Email-->
                                      <div class="form-group  initial">
                                        <label for="txt_dest_email">Email </label> 	
										<input type="input" name="txt_dest_email" id="txt_dest_email" maxlength="100" value="" az-default="" az-required="true" az-condition="" class="email form-control az-field az-input" az-datatype="email" az-datatype-default="" az-title-default="" onBlur="$(this).attr('full_email',this.value)" az-column-db="ORE_EMAIL" placeholder=""  az-fieldtype="input" az-id="" >
 									    <div class="help-block with-errors"></div>
                                      </div>
                                  </div>
							      <div class="col-xs-12 col-md-3 col-lg-">
								      <!--CAMPO Responsável-->
                                      <div class="form-group  initial">
                                        <label for="txt_dest_responsavel">Responsavel </label> 										
										<input type="input" name="txt_dest_responsavel" id="txt_dest_responsavel" maxlength="20" value="" az-default="" az-required="false" az-condition="" class="responsavel form-control az-field az-input" az-datatype="text" az-datatype-default="" az-title-default="" az-column-db="ORE_RESPONSAVEL" placeholder=""  az-fieldtype="input" az-id="" >
                                        <div class="help-block with-errors"></div>
                                      </div>
                                  </div>
                                  <!-- Botão para adicionar destinatario -->
							      <div class="col-xs-1 col-md-1 col-lg-1">
                                        <div class="form-group">
                                        	<button class="btn btn-success margin-bottom" style="margin-top:24px;" 
                                            onclick="incluirNovoDestinatario()">
                                            <b>+</b></button>
                                        </div>
                                  </div> 
 							 </div>
                             <!--LISTAGEM de Encaminhamentos-->
                             <div class="" id="listformDestinatarios"></div>
                          </fieldset>
                      </form>
                      
				      <!-- Botão enviar solicitação -->
                      <br>
                      <div class="row">
                            <div class="col-xs-12 col-md-12 col-lg-12">
                                <button type="button" class="btn btn-success" id="btnConfirmation" 
                                az-action="insert" az-id="203" 
                                onclick="criarEnviarSolicitacao()">Criar/Enviar solicitação</button>    
                            </div>
                      </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>


<!--Janelas Modals-->
<?php 		
//Modal funções de listagem
include(SYS_SRC_APP.'views/pieces/common_modalSearch.php');

// Common piece bottom
include(SYS_SRC_APP.'views/pieces/common_bottom.php')
?>

<script>
	_azAzul.afterLoad.push(function() {
		carregaObras();	
		_azValidation.init('Main');
		_azValidation.init('#formItensSolicitados', true);
		$('#list_dest_fornecedor').select2({tags:true});
	})
</script>
</body>
</html>