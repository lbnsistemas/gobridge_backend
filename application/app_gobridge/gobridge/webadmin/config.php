<?php
// Identify base URL
///$protocol = (strpos($_SERVER['SERVER_PROTOCOL'],'https')!==false ? 'https://' : 'http://');
$protocol = ($_SERVER['HTTPS']=='on' ? 'https://' : 'http://');
$domain = $protocol.$_SERVER['HTTP_HOST'].str_replace('index.php','',$_SERVER['PHP_SELF']);
// URL and Sources paths ---------------------------------------------------------------------------------------//
define('SYS_HOST',					str_replace('www.', '', $_SERVER['HTTP_HOST']));
define('SYS_DOMAIN', 				$domain);
define('SYS_FRAMEWORK_URL', 		SYS_DOMAIN."_azul/");
define('SYS_APP_URL', 				SYS_DOMAIN."application/"._application."/"._client."/"._module."/");
define('SYS_SRC_ROOT', 				getcwd()."/");
define('SYS_SRC_FRAMEWORK', 		SYS_SRC_ROOT.'_azul/');
define('SYS_SRC_APP', 				SYS_SRC_ROOT.'application/'._application.'/'._client."/"._module."/");
define('SYS_SRC_LIBS', 				SYS_SRC_FRAMEWORK.'libs/');
define('SYS_SRC_CONTROLLERS', 		SYS_SRC_APP.'controllers/');
define('SYS_SRC_MODELS', 			SYS_SRC_APP.'models/');
define('SYS_FILE_LOG_ERROR_PHP',	SYS_SRC_APP.'logs/phperrors.log');
define('SYS_FILE_LOG_ERROR_MYSQL',	SYS_SRC_APP.'logs/mysqlerrors.log');
define('SYS_FILE_LOG_MYSQL',		SYS_SRC_APP.'logs/mysql.log');
define('_DYNAMIC_FORM_YES',			TRUE);	// Indica que é um formulario dinamico
define('_DYNAMIC_FORM_NO',			FALSE);	// Indica que não é um formulario dinamico
define('_OPEN_FORM_YES',			TRUE);	// Formulário aberto por padrão na view
define('_OPEN_FORM_NO',				FALSE);	// Formulário fechado por padrão na view

// Loads config.xml in aplication/{_application}/{_module}/config.xml
// In this file is possible edit all informations system through web interface
$srcFile = SYS_SRC_APP."config.xml";
$dom = new DOMDocument;
$dom->loadXML(file_get_contents($srcFile));	
$nodes = $dom->getElementsByTagName('system')->item(0);

// System constants --------------------------------------------------------------------------------------------//
// Modes 
define('PRODUCTION_MODE',			$nodes->getElementsByTagName('PRODUCTION_MODE')->item(0)->nodeValue=='TRUE');
define('DEBUG_MODE',				$nodes->getElementsByTagName('DEBUG_MODE')->item(0)->nodeValue=='TRUE');
// Database constants
define('DB_ALIAS',					$nodes->getElementsByTagName('DB_ALIAS')->item(0)->nodeValue);
define('DB_HOST', 					$nodes->getElementsByTagName('DB_HOST')->item(0)->nodeValue);
define('DB_USER', 					$nodes->getElementsByTagName('DB_USER')->item(0)->nodeValue);
define('DB_PASS', 					$nodes->getElementsByTagName('DB_PASS')->item(0)->nodeValue);
if(DB_ALIAS!='') {
	define('DB_NAME', 					DB_ALIAS.'_'.$nodes->getElementsByTagName('DB_NAME')->item(0)->nodeValue);
}else {
	define('DB_NAME', 					$nodes->getElementsByTagName('DB_NAME')->item(0)->nodeValue);
}
// System information  
define('SYS_SUPPORT_MAIL', 			$nodes->getElementsByTagName('SYS_SUPPORT_MAIL')->item(0)->nodeValue);
define('SYS_NAME', 					$nodes->getElementsByTagName('SYS_NAME')->item(0)->nodeValue);
define('SYS_VERSION', 				$nodes->getElementsByTagName('SYS_VERSION')->item(0)->nodeValue);
define('SYS_LATEST_VERSION', 	    $nodes->getElementsByTagName('SYS_LATEST_VERSION')->item(0)->nodeValue);

// Constantes de sessões criptogradas ----------------------------------------------------------------------------//
define('SES_LOGIN',					md5('LOGIN'));			// Index da sessão de login 
define('SES_TOKEN',					md5('TOKEN'));			// Index da sessão token
define('SES_USER_LEVEL',			md5('USER_NIVEL'));	  	// Index da sessão nivel usr
define('SES_USER_NAME',				md5('USER_USUARIO'));  	// Index da sessão usuario
define('SES_USER_ID',				md5('USER_ID'));  	  	// Index da sessão user_id
define('SES_USER_EXTENSION',		md5('USER_EXTENSION'));  	// Index da sessão user_id
define('SES_COMPANY_NAME',			md5('EMPRESA'));		// Index da sessão empresa
define('SES_ACC_ID',				md5('SES_ACC_ID'));		// Index da sessão do ID de acesso

// Dados da empresa ---------------------------------------------------------------------------------------------//
$nodes = $dom->getElementsByTagName('empresa')->item(0);
define("EMP_RAZAO",					$nodes->getElementsByTagName('razao')->item(0)->nodeValue);
define("EMP_FANTASIA",				$nodes->getElementsByTagName('fantasia')->item(0)->nodeValue);
define("EMP_CNPJ",					$nodes->getElementsByTagName('cnpj')->item(0)->nodeValue);
define("EMP_IE",					$nodes->getElementsByTagName('ie')->item(0)->nodeValue);
define("EMP_LOGRADOURO",			$nodes->getElementsByTagName('logradouro')->item(0)->nodeValue);
define("EMP_NUMERO",				$nodes->getElementsByTagName('numero')->item(0)->nodeValue);
define("EMP_BAIRRO",				$nodes->getElementsByTagName('bairro')->item(0)->nodeValue);
define("EMP_COMPLEMENTO",			$nodes->getElementsByTagName('complemento')->item(0)->nodeValue);
define("EMP_CIDADE",				$nodes->getElementsByTagName('cidade')->item(0)->nodeValue);
define("EMP_CIDADE_IBGE",			$nodes->getElementsByTagName('cidadeIbge')->item(0)->nodeValue);
define("EMP_UF",					$nodes->getElementsByTagName('uf')->item(0)->nodeValue);
define("EMP_UF_IBGE",				$nodes->getElementsByTagName('ufIbge')->item(0)->nodeValue);
define("EMP_CEP",					$nodes->getElementsByTagName('cep')->item(0)->nodeValue);
define("EMP_FONE",					$nodes->getElementsByTagName('fone')->item(0)->nodeValue);
define("EMP_EMAIL",					$nodes->getElementsByTagName('email')->item(0)->nodeValue);

// Configurações para o site -----------------------------------------------------------------------------------//
$nodes = $dom->getElementsByTagName('site')->item(0);
define("SITE_LOGO_CLARO",			$nodes->getElementsByTagName('logoClaro')->item(0)->nodeValue);
define("SITE_LOGO_ESCURO",			$nodes->getElementsByTagName('logoEscuro')->item(0)->nodeValue);
define("SITE_UA_ANALYTICS",			$nodes->getElementsByTagName('uaAnalitycs')->item(0)->nodeValue);
define("SITE_GOOGLE_VERIFICATION",	$nodes->getElementsByTagName('googleVerification')->item(0)->nodeValue);
define("SITE_ID_GOOGLE",			$nodes->getElementsByTagName('idGoogle')->item(0)->nodeValue); // Chave/credencial p/ API do google
define("SITE_ID_FACEBOOK",			$nodes->getElementsByTagName('idFacebook')->item(0)->nodeValue);// ID de aplicativo do facebook
define("SITE_LINK_GOOGLEPLUS",		$nodes->getElementsByTagName('linkGPlus')->item(0)->nodeValue);
define("SITE_LINK_YOUTUBE",			$nodes->getElementsByTagName('linkYoutube')->item(0)->nodeValue);
define("SITE_LINK_TWITTER",			$nodes->getElementsByTagName('linkTwitter')->item(0)->nodeValue);
define("SITE_LINK_FACEBOOK",		$nodes->getElementsByTagName('linkFacebook')->item(0)->nodeValue);
define("SITE_LINK_INSTAGRAM",		$nodes->getElementsByTagName('linkInstagram')->item(0)->nodeValue);
define("SITE_LINK_LINKEDIN",		$nodes->getElementsByTagName('linkLinkedin')->item(0)->nodeValue);
define("SITE_SKYPE",				$nodes->getElementsByTagName('skype')->item(0)->nodeValue);
define("SITE_WHATSAPP",				$nodes->getElementsByTagName('whatsapp')->item(0)->nodeValue);
define("SITE_GMAPS_X",				$nodes->getElementsByTagName('gMapsX')->item(0)->nodeValue); //Coordenada X Gmaps
define("SITE_GMAPS_Y",				$nodes->getElementsByTagName('gMapsY')->item(0)->nodeValue); //Coordenada Y Gmaps
define("SITE_TEXTO_RODAPE",			$nodes->getElementsByTagName('textoRodape')->item(0)->nodeValue); 

// Configurações dos emails  ----------------------------------------------------------------------------------//
$nodes = $dom->getElementsByTagName('mail')->item(0);
define("MAIL_HOST",					$nodes->getElementsByTagName('host')->item(0)->nodeValue); // Host : mail.dominio
define("MAIL_PORT",					$nodes->getElementsByTagName('port')->item(0)->nodeValue); // Padrão 587 (saida)
define("MAIL_SECURE",				$nodes->getElementsByTagName('secure')->item(0)->nodeValue); // Padrão tls
define("MAIL_AUTH_SMTP",			$nodes->getElementsByTagName('auth')->item(0)->nodeValue); // Autentica ou não
define("MAIL_USER",					$nodes->getElementsByTagName('user')->item(0)->nodeValue); // Usuario
define("MAIL_PASS",					$nodes->getElementsByTagName('pass')->item(0)->nodeValue); // Senha

/*/Informações bancárias ----------------------------------------------------------------------------------------//
define("EMP_AG",						'');
define("EMP_CC",						'');
define("EMP_CC_DV",					'');
//*/

// IP do cliente (checagem a cada requisição) -----------------------------------------------------------------//
if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
{
  //ip from share internet
  $ip=$_SERVER['HTTP_CLIENT_IP'];
}
elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
{
  //ip is pass from proxy
  $ip=$_SERVER['HTTP_X_FORWARDED_FOR']; 
}
else
{
  $ip=$_SERVER['REMOTE_ADDR'];
}
define('SYS_USER_IP',				$ip);
/*
	# ARRAY DE CONFIGURACAO DE PLUGINS JS,CSS
*/
$CONFIG_PLUGIN = array (
	//JQUERY
	'JQUERY'=>SYS_FRAMEWORK_URL.'plugins/jquery 1.11.3/jquery-1.11.3.min.js',
	//BOOTSTRAP
	'BOOTSTRAP'=>SYS_FRAMEWORK_URL.'plugins/bootstrap 3.3.5/js/bootstrap.js',
	'BOOTSTRAP_CSS'=>SYS_FRAMEWORK_URL.'plugins/bootstrap 3.3.5/css/bootstrap.css',
	//BOOTBOX
	'BOOTBOX'=>SYS_FRAMEWORK_URL.'plugins/bootbox/bootbox.js',
	//BOOSTRAP DATETIMEPICKER
	'MOMENT'=>SYS_FRAMEWORK_URL.'plugins/datetimepicker/js/moment.js',
	'DATETIMEPICKER'=>SYS_FRAMEWORK_URL.'plugins/datetimepicker/js/datetimepicker.js',
	'DATETIMEPICKER_CSS'=>SYS_FRAMEWORK_URL.'plugins/datetimepicker/css/datetimepicker.css',
	//DATERANGE PICKER
	'DATERANGE'=>SYS_FRAMEWORK_URL.'plugins/daterangepicker2.1.24/daterangepicker.js',
	'DATERANGE_CSS'=>SYS_FRAMEWORK_URL.'plugins/daterangepicker2.1.24/daterangepicker.css',
	//JQUERY FILE UPLOAD
	'FILE_UPLOAD_JQUERY_WIDGET'=>SYS_FRAMEWORK_URL.'plugins/jquery file upload 9.13.1/js/vendor/jquery.ui.widget.js',
	'FILE_UPLOAD_iTRANSPORT'=>SYS_FRAMEWORK_URL.'plugins/jquery file upload 9.13.1/js/jquery.iframe-transport.js',
	'FILE_UPLOAD'=>SYS_FRAMEWORK_URL.'plugins/jquery file upload 9.13.1/js/jquery.fileupload.js',
	'FILE_UPLOAD_CSS'=>SYS_FRAMEWORK_URL.'plugins/jquery file upload 9.13.1/css/jquery.fileupload.css',
	//SUMMER NOTE (EDITOR HTML)
	'SUMMER_JS'=>SYS_FRAMEWORK_URL.'plugins/summernote-0.8.2-dist/summernote.js',
	'SUMMER_CSS'=>SYS_FRAMEWORK_URL.'plugins/summernote-0.8.2-dist/summernote.css',
	//SELECT 2
	//'SELECT2_JS'=>SYS_FRAMEWORK_URL.'plugins/select2-4.0.3/select2.full.js',
	//'SELECT2_CSS'=>SYS_FRAMEWORK_URL.'plugins/select2-4.0.3/select2.css',
	'SELECT2_JS'=>SYS_FRAMEWORK_URL.'plugins/select2-4.0.7/js/select2.full.js',
	'SELECT2_CSS'=>SYS_FRAMEWORK_URL.'plugins/select2-4.0.7/css/select2.css',
    //FONTE AWESOME
	'AWESOME'=>SYS_FRAMEWORK_URL.'plugins/font-awesome4.7.0/css/font-awesome.css',
	//TEMA BRICKBE
	//TEMA AZUL
	//TEMA
	'THEMA'=>SYS_FRAMEWORK_URL.'plugins/themes/bootstrapBlack.css',
	//CSS GERAL
	'CSS_GERAL'=>SYS_FRAMEWORK_URL.'plugins/general.css'
);	




?>
