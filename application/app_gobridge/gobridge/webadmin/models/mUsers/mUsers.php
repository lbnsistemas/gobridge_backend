<?php
require_once(SYS_SRC_LIBS.'Database.php');
/**
 * Model mUsuarios : Funções específicas para a tabela de usuários
 * 
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, Lucas Nunes
 * @version 1.0.0
 */ 
class mUsers extends Database { 
	/**
	 * Verifica se usuario informado no cadastro já existe
	 * @param string $username
	 * @return array ou false
	 */
	public function checksUserName($username) {
		$objRet = new StdClass();
		$objRet->ret = false;
		if($username!='') {
			Database::connect();
			// Executa ação no BD do sistema
			$objRet->ret = Database::select("az_users","WHERE USE_USERNAME='".$username."'");
			// Trata o retorno
			if($objRet->ret===false && MODO_DEBUG==true) {
				$objRet->stringSQL = Database::get_stringSQL();
				$objRet->errorSQL  = Database::get_errorSQL();
			}else {
				if(Database::rows() > 0) {
					$objRet->ret = array("exists" => true);
				}else {
					$objRet->ret = array("exists" => false);
				}
			}
			Database::disconnect();
		}
		return($objRet);
	}
	
	/**
	 * Cadastrar permissões
	 * @param string $username
	 * @return array ou false
	 */
	
	
	
	
}