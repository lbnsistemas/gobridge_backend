<?php
require_once(SYS_SRC_LIBS.'Database.php');
/**
 * Model principal : Fornecer dados do sistema como permissões, login,...
 * 
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, Lucas Nunes
 * @version 1.5.0
 */ 
class mMain extends Database { 
	/**
	 * Executa uma transction com uma ou mais instruções SQL
	 * @param array $arSQLs: instruções SQL a serem executadas
	 * @return boolean
	 */
	public function mTransaction($arSQLs){
		$objRet = new StdClass();
		// Conecta ao BD
		Database::connect();
		// Executa todas as querys/triggers (se uma falhar todas são invalidadas e o erro é registrado no log do sistema
		$objRet->ret = Database::transaction($arSQLs);
		if($objRet->ret===false  || DEBUG_MODE==true) {
			$objRet->stringSQL = Database::get_stringSQL();
			$objRet->errorSQL  = Database::get_errorSQL();
		}
		// Desconecta do BD
		Database::disconnect();
		return($objRet);
	}
	/**
	 * Retorna os dados de um determinado modelo de email
	 * @param  string $finalidade: Busca pela finalidade do modelo de email
	 * @return stdClass objeto : {ret(false ou linhas), stringSQL, errorSQL}
	 */
	public function email_model($finalidade) {
		if(trim($finalidade)=='') return(false);
		$objRet = new StdClass();
		// Conecta ao BD
		Database::connect();
		// Executa o select
		$ret = Database::select('az_email_models',"WHERE MOE_FINALIDADE='".$finalidade."' LIMIT 1");
		// Trata o retorno
		$objRet->ret = $ret;
		if($objRet->ret===false || DEBUG_MODE==true) {
			$objRet->stringSQL = Database::get_stringSQL();
			$objRet->errorSQL  = Database::get_errorSQL();
		}
		// Desconecta do BD
		Database::disconnect();
		// Retorna o objeto
		return($objRet);
	}
	/**
	 * Reserva uma PK na tabela temp_ids do BD (somente se a mesma não estiver reservada)
	 * @param String $tabela, String $col_id coluna da PK
	 * @return stdClass objeto : {ret(false ou ID reservado), stringSQL, errorSQL}
	 */
	public function reserveID($table, $col_id) {
		$objRet = new StdClass();
		$ret = false;
		if(trim($table)=='') trigger_error('Argumento $table vazio.',E_USER_NOTICE);
		if(trim($col_id)=='') trigger_error('Argumento $col_id vazio.',E_USER_NOTICE);
		// Conecta ao BD
		Database::connect();
		$mysqli = Database::getInstanceMysqli();
		// String SQL para pegar a proxima PK
		$sql = "
		SELECT VAR, MAX(PRXID) MAX
		FROM
		(
		(select '' VAR, ".$col_id." AS PRXID  from ".$table."  ORDER BY ".$col_id." DESC LIMIT 1)
		UNION ALL
		(select '' VAR, TMP_RESERVED AS PRXID FROM az_temp WHERE TMP_TABLE='".$table."' ORDER BY TMP_RESERVED DESC LIMIT 1)
		) VIRTUAL GROUP BY VAR";
		$r = $mysqli->query($sql,MYSQLI_USE_RESULT);
		$row = $r->fetch_assoc();
		$next_id = $row['MAX']+1;
		$r->close();
		//Registra o ultimo ID encontrado no inicio desta funcao na tabela buffer
		$sql = "INSERT INTO az_temp SET TMP_PKSTRING='".$table.$next_id."', TMP_RESERVED='".$next_id."',
		 TMP_TABLE='".$table."', TMP_USE_ID=".$_SESSION[SES_USER_ID];
		$r = $mysqli->query($sql,MYSQLI_USE_RESULT);
		if($mysqli->errno) {
			$errorString = $mysqli->error." <b> String :</b> ".$sql;
			$objRet->stringSQL = $sql;
			$objRet->errorSQL  = $mysqli->error;
			Database::writeLog($stringMysql,true);//Registra erro no log errosmysql
			trigger_error($errorString ,E_USER_WARNING);	
		}else if($mysqli->affected_rows>0) {
			$ret = true;
		}
		Database::disconnect();
		$objRet  = $ret==true ? array('ID'=>$next_id ): false;
		return($objRet);
	}
	
	/**
	 * Retorna um ID (PK) reservado na tabela temp_ids do BD 
	 * @param String $tabela, String $col_id coluna da PK
	 * @return stdClass objeto : {ret(false ou ID reservado), stringSQL, errorSQL}
	 */
	public function reservedID($table, $col_id) {
		// Inicia objeto para retorno da função
		$objRet = new StdClass();
		$ret = false;
		// Checa os parametros
		if(trim($table)=='') trigger_error('Argumento $table vazio.',E_USER_NOTICE);
		if(trim($col_id)=='') trigger_error('Argumento $col_id vazio.',E_USER_NOTICE);
		// Conecta ao BD
		Database::connect();
		$mysqli = Database::getInstanceMysqli();
		// String SQL para pegar a proxima PK
		$sql = "SELECT TMP_RESERVED FROM az_temp WHERE TMP_USE_ID=".$_SESSION[SES_USER_ID]." AND TMP_TABLE='".$table."'";
		$r = $mysqli->query($sql);
		if($mysqli->errno) {
			$errorString = $mysqli->error." <b> String :</b> ".$sql;
			$objRet->stringSQL = $sql;
			$objRet->errorSQL  = $mysqli->error;
			Database::writeLog($stringMysql,true);//Registra erro no log errosmysql
			trigger_error($errorString ,E_USER_WARNING);
			
		// Verifica se encontrou um ID reservado para a tabela e o usuário logado
		}else if($r->num_rows>0) {
			$row = $r->fetch_assoc();
			$r->close();
			// Verifica se o ID já existe na tabela informada
			$sql = "SELECT ".$col_id." FROM ".$table." WHERE ".$col_id."=".$row['TMP_RESERVED'];
			$r = $mysqli->query($sql);
			// Se não existe, o ID ainda é válido
			if($r->num_rows==0) {
				$ret = true;
			// Caso contrário, reserva um novo ID e retorna
			}else {
				// Fecha result
				$r->close();
				// Desconecta do BD
				Database::disconnect();
				
				// Reserva e retorna um novo ID
				return($this->reserveID($table,$col_id));	
			}
		// Id ainda não reservado
		}else {
			// Fecha result
			$r->close();
			// Desconecta do BD
			Database::disconnect();

			// Reserva e retorna um novo ID
			return($this->reserveID($table,$col_id));	
		}
		
		// Fecha result
		$r->close();
		// Desconecta do BD
		Database::disconnect();
		// Estabelece o retorno
		$objRet  = $ret==true ? array('ID'=>$row['TMP_RESERVED'] ) : false;
		// Retorna o objeto
		return($objRet);
	}
	
	
	
	/**
	 * Utilizando o comentario da tabela do proprio mysql (phpmyadmin), o sistema irá verificar se a tabela é publica ou não
	 * @param String $table  nome da tabela que se deseja saber se é publica ou privada
	 * @return bool true: public, false : private
	 */
	protected function isPublicTable($table) {
		if(trim($table)=='') trigger_error('Argumento $table vazio.',E_USER_NOTICE);
		// Conecta ao BD
		Database::connect();
		$ret = Database::select('INFORMATION_SCHEMA.TABLES','WHERE table_name="'.$table.'"','table_comment');
		$ret = (Database::rows() > 0 && $ret[0]['table_comment']=='public' ? true  : false);
		Database::disconnect();
		return($ret);
	}
	/**
	 * Busca uma linha com usuário e senha informados na tabela usuarios
	 * @param String $usuario, String $senha
	 * @return bool true ou false
	 */
	public function verifyLogin($user, $password) {
		$ret = false;
		if($user!='' && $password!='') {
			Database::connect();
			$mysqli = Database::getInstanceMysqli();
			// Tratamento anti sql-injection
			$user = $mysqli->escape_string($user);
			//$password = md5($mysqli->escape_string($password).$user);
			// Checa as credenciais (Usuário e senha criptgrafa)
			$linhas = Database::select(DB_NAME.'.escritorios','WHERE 
			(Email="'.$user.'" || CPF_CNPJ="'.$user.'") AND  Senha="'.$password.'"');
			//die(Database::get_stringSQL());
			Database::disconnect();
			$ret = (count($linhas) >0  ? $linhas : false);
		}
		return($ret);
	}
	/**
	 * Valida o token recebido e após retorna os demais dados do usuário informado
	 * @param String $user, String $token
	 * @return object stdclass ou false
	 */
	public function verifyLoginByToken($user, $token) {
		$ret = false;
		if($user!='' && strlen($token)==32) {
			
			Database::connect();
			$mysqli = Database::getInstanceMysqli();
			// Tratamento anti sql-injection
			$user = $mysqli->escape_string($user);
			$token = $mysqli->escape_string($token);		
			// Checa as credenciais (Usuário e senha criptgrafa)
			$ret = Database::select(DB_NAME.'.az_users','
			LEFT JOIN az_access ON ACC_TOKEN="'.$token.'"
			WHERE USE_USERNAME="'.$user.'" AND USE_STATUS="ATIVO"');
			// Verifica se encontrou o cadastro do usuário
			if(count($ret) >0 && is_array($ret) ) {
				// Verifica se o token já foi utilizado 
				if($ret[0]['ACC_TOKEN']!=$token) {
					/*
						Alteração dia 24/02/2015 - Analista Lucas Nunes
						Obs: Devido a muito problema de acesso pelo desintrosamento dos relógios, foi adicionado esta nova lógica
							 que considera o minuto anterior e o posterior para validar o token de acesso.
					*/
					$dataformatada_minAnt = date('d/m/y:H:i', strtotime('-1 minute'));//Data com minuto anterior
					$dataformatada=date('d/m/y:H:i');//Data hora e minuto atuais
					$dataformatada_minPost = date('d/m/y:H:i', strtotime('+1 minute'));//Data com minuto posterior
					//Hashs
					$hashVerify_minAnt = md5($user.$ret[0]['USE_PASSWORD'].$dataformatada_minAnt);//Hash do minuto anterior
					$hashVerify= md5($user.$ret[0]['USE_PASSWORD'].$dataformatada);//Hash do minuto atual no servidor
					$hashVerify_minPost= md5($user.$ret[0]['USE_PASSWORD'].$dataformatada_minPost);//Hash do minuto posterior
					//Validação da hash informada, 3 possibilidades aceitas
					if($token!=$hashVerify_minAnt && $token!=$hashVerify && $token!=$hashVerify_minPost) {
						$ret=false; // Token inválido (intruso, problema com relogóio, conexao...)
					}
				}else {
					$ret=false; // Token já utilizado e salvo na tabela az_access	
				}
			}
			Database::disconnect();	
		}
		
		return($ret);
	}
	/**
	 * Verifica as permissões de um det. usuario em uma det. view
	 * @param String $view  nome do arquivo da view, int $userID 
	 * @return Array com as permissões encontradas ou false para permissão negada
	 */
	public function checksPermission($view,$userID) {
		return(array(true));
		$ret = false;
		// Lança erros se a tabela ou array campos estiver vazio
		if(trim($view)=='') trigger_error('Argumento $view vazio.',E_USER_NOTICE);
		if(trim($userID)=='') trigger_error('Argumento $userID vazio.',E_USER_NOTICE);
		// Conecta ao BD
		Database::connect();
		$linhas = Database::select(DB_NAME.'.az_permissions','WHERE PER_VIEW="'.$view.'" AND PER_USE_ID='.$userID);
		Database::disconnect();
		$ret = (count($linhas) > 0 ? $linhas : false);
		return($ret);
	}
	
	/**
	 * Seleciona, através da model, uma listagem de veículos a serem monitorados pelo usuario
	 * @param stdClass $args stdClass : tabela, inicio, limit, orderBy, filtros(value1, value2, colunas, regra), colunas, complSQL
	 * @return stdClass objeto : {ret(linhas encontradas), stringSQL, errorSQL}
	 */
	public function mSelect($args) {
		$objRet = new StdClass();
		$compl = '';
		Database::connect();
		$mysqli = Database::getInstanceMysqli();
		$compl = $args->complSQL;
		// Verifica se $filtros é um array como esperado
		if(is_array($args->filters)) {
			// Recebe todos os possíveis filtros de pesquisa recebidos do controller
			foreach($args->filters as $filter) {
				$complFilters = '';
				// Verifica se é regra LIKE '%'
				if($filter->rule == '%' && $filter->value1!='') {
					// Abre sintaxe do MySQL LIKE
					$complFilters .= ' AND (';
					
					// Incrementa as colunas do filtro
					foreach($filter->columns as $column) {
						$complFilters .= $mysqli->escape_string($column).' LIKE "%'.$mysqli->escape_string($filter->value1).'%" OR ' ;	
					}
					// Retira o ultimo 'OR '
					$complFilters = substr($complFilters,0,-4);
					// Fecha a sintaxe
					$complFilters .= ')';
				}
				// Verifica se é regra = 
				if($filter->rule == '=' && $filter->value1!='') {
					// Abre sintaxe do MySQL LIKE
					$complFilters .= ' AND (';
					// Incrementa as colunas do filtro
					foreach($filter->columns as $column) {
						$complFilters .= $mysqli->escape_string($column).' = "'.$mysqli->escape_string($filter->value1).'" OR ' ;	
					}
					// Retira o ultimo 'OR '
					$complFilters = substr($complFilters,0,-4);
					// Fecha a sintaxe
					$complFilters .= ')';
				}	
				// Verifica se é regra _B (beetween)
				if($filter->rule =='_B' && $filter->value1!='' && $filter->value2!='') {
					// Abre sintaxe do MySQL Beetween
					$complFilters .= ' AND ('.$mysqli->escape_string($filter->columns[0]).' BETWEEN ';
					// Value 1
					$complFilters .= '"'.$mysqli->escape_string($filter->value1).'" AND ';
					// Value 2
					$complFilters .= '"'.$mysqli->escape_string($filter->value2).'"';
					// Fecha a sintaxe
					$complFilters .= ')';
				}
				$compl .= (strpos($compl,'WHERE')!==false  ? $complFilters : ' WHERE 1=1 '.$complFilters);
			}
		}
		// Group By
		if(trim($args->groupBy)!='') {
			$compl .= ' GROUP BY '.$mysqli->escape_string($args->groupBy);
		}	
		// Order by
		if(trim($args->orderBy)!='') {
			$compl .= ' ORDER BY '.$mysqli->escape_string($args->orderBy);
		}	
		// Limit
		if($args->limit>0 && $args->init>=0) {
			$compl .= ' LIMIT '.$mysqli->escape_string($args->init).','.$mysqli->escape_string($args->limit);
		}
		// Executa o select
		$ret = Database::select($args->table,$compl,$args->columns);
		// Trata o retorno
		$objRet->ret = $ret;
		if($objRet->ret===false || DEBUG_MODE==true) {
			$objRet->stringSQL = Database::get_stringSQL();
			$objRet->errorSQL  = Database::get_errorSQL();
		}
		// Desconecta do BD
		Database::disconnect();
		//Retorno objeto com as informações da consulta
		return($objRet);
	}
	/**
	 * Inclui novo cadastro no BD
	 * @param stdClass $args stdClass : tabela, campos(coluna, value, tipoDados),writeLog
	 * @return stdClass objeto : {ret[ID criado], stringSQL, errorSQL}
	 */
	public function mInsert($args) {
		$objRet = new StdClass();
		$complSQLfields = ''; // Complemento SQL com a sintaxe dos campos a serem incluidos
		// Lança erros se a tabela ou array campos estiver vazio
		if(trim($args->table)=='') trigger_error('Argumento $args->table vazio.',E_USER_NOTICE);
		if(count($args->fields)==0) trigger_error('Argumento $args->fields vazio.',E_USER_NOTICE);
		// Conecta ao BD
		Database::connect();
		// Pega Instancia de Mysqli
		$mysqli = Database::getInstanceMysqli();
		// Monta todos os campos na query
		foreach($args->fields as $field) {
			if($field->column!='') 
			$complSQLfields .= $mysqli->escape_string($field->column)."='".$mysqli->escape_string($field->value)."',";
		}
		// Retira a ultima virgula
		$complSQLfields = substr($complSQLfields,0,-1);
		// Executa o insert query no BD
		$ret = Database::insert($args->table,$complSQLfields,$args->writeLog===false);
		// Trata o retorno
		$objRet->ret = ($ret===true ? array("ID"=>Database::id()) : false);// ID criado se executou o insert com sucesso
		if($objRet->ret===false  || DEBUG_MODE==true) {
			$objRet->stringSQL = Database::get_stringSQL();
			$objRet->errorSQL  = Database::get_errorSQL();
		}
		// Excluí o ID Reservado da tabela az_temp
		$sql = "DELETE FROM az_temp WHERE TMP_USE_ID=".$_SESSION[SES_USER_ID]." AND TMP_TABLE='".$args->table."'";
		$ret = $mysqli->query($sql);
		if($ret==false) {
			$objRet->stringSQL = Database::get_stringSQL();
			$objRet->errorSQL  = Database::get_errorSQL();	
		}
		// Desconecta do BD
		Database::disconnect();
		// Retorno
		return($objRet);
	}
	/**
	 * Alterar um cadastro já inserido no BD
	 * @param stdClass $args stdClass : tabela, campos(coluna, value, default, tipoDados),writeLog
	 * @return stdClass objeto : {ret->rows[linhas afetadas ou false], stringSQL, errorSQL}
	 */
	public function mUpdate($args) {
		$objRet = new StdClass();
		$complSQLfields = $args->complSQL; // Complemento SQL com a sintaxe dos campos a serem incluidos
		// Lança erros se a tabela ou array campos estiver vazio
		if(trim($args->table)=='') trigger_error('Argumento $args->table vazio.',E_USER_NOTICE);
		if(count($args->fields)==0 && trim($complSQLfields)=='') trigger_error('Argumento $args->campos e $args->complSQL vazios.',E_USER_NOTICE);
		if(trim($args->colID)=='') trigger_error('Argumento $args->colID vazio.',E_USER_NOTICE);
		if($args->ID==0 || $args->ID=='' || count($args->ID)==0) trigger_error('Argumento $args->ID vazio.',E_USER_NOTICE);
		// Conecta ao BD
		Database::connect();
		// Pega Instancia de Mysqli
		$mysqli = Database::getInstanceMysqli();
		// Verifica se $campos é um array como esperado
		if(is_array($args->fields)) {
			foreach($args->fields as $field) {
				if($field->column!='') 
				$complSQLfields .= $mysqli->escape_string($field->column)."='".$mysqli->escape_string($field->value)."',";
			}
			// Retira a ultima virgula
			$complSQLfields = substr($complSQLfields,0,-1);
		}
		// Executa o insert query no BD
		$ret = Database::update($args->table, $complSQLfields, $args->colID,  $args->ID, $args->writeLog===false);
		// Trata o retorno
		$objRet->ret = ($ret===true ? array("rows"=>Database::rows()) : false);
		if($objRet->ret===false  || DEBUG_MODE==true) {
			$objRet->stringSQL = Database::get_stringSQL();
			$objRet->errorSQL  = Database::get_errorSQL();
		}
		// Desconecta do BD
		Database::disconnect();
		// Retorno
		return($objRet);
	}
	/**
	 * Deletar um cadastro já inserido no BD (somente rascunhos)
	 * @param string $tabela nome da tabela no BD,int $ID PK
	 * @return bool
	 */
	public function mDelete($args) {
		$objRet = new StdClass();
		// Lança erros se a tabela ou array campos estiver vazio
		if(trim($args->table)=='') trigger_error('Argumento $args->table vazio.',E_USER_NOTICE);
		//if(trim($args->ID)=='') trigger_error('Argumento $args->ID vazio.',E_USER_NOTICE);
		if(trim($args->colID)=='') trigger_error('Argumento $args->colID vazio.',E_USER_NOTICE);
		// Conecta ao BD 
		Database::connect();
		// Deleta permanente a linha da tabela
		$ret = Database::delete($args->table,$args->colID,$args->ID);
		// Trata o retorno
		$objRet->ret = ($ret===true ? array("rows"=>Database::rows()) : false);
		if($objRet->ret===false  || DEBUG_MODE==true) {
			$objRet->stringSQL = Database::get_stringSQL();
			$objRet->errorSQL  = Database::get_errorSQL();
		}
		// Desconecta do BD
		Database::disconnect();
		// Retorno
		return($objRet);
	}
	/**
	 * Se o retorno indica 'sucesso' (pressuõe que a ação tenha sido efetuada)
	 * ATENÇÃO : toda ação deve ao final fazer chamada a está função(direta ou indiretamente)
	 * @return string json total cadastrado ou falha
	 */
	public function registerAction() {
		$objRet = new StdClass();
		Database::connect();
		$mysqli = Database::getInstanceMysqli();
		$sql = "INSERT INTO ".DB_NAME.".az_effected_actions SET ACT_USERNAME='".$_SESSION[SES_USER_NAME]."', 
		ACT_USE_ID='".$_SESSION[SES_USER_ID]."', ACT_MODULE='".$_POST['module']."', ACT_ACTION='".$_POST['action']."', 
		ACT_VIEW='".$_POST['view']."', ACT_ENVELOPE='".$_POST['envelope']."', ACT_DATETIME=NOW(), 
		ACT_ACCESSTOKEN='".$_SESSION[SES_TOKEN]."', ACT_ACC_ID=".$_SESSION[SES_ACC_ID];
		$ret = $mysqli->query($sql);
		// Trata o retorno
		$objRet->ret = ($ret===true ? array("ID"=>Database::id()) : false);// ID criado se executou o insert com sucesso
		if($ret==false) {
			$objRet->stringSQL = Database::get_stringSQL();
			$objRet->errorSQL  = Database::get_errorSQL();	
		}
		// Desconecta do BD
		Database::disconnect();
		// Retorno
		return($objRet);
	}
	
}

