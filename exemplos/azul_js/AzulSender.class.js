/**
  @class AzulSender
  @author Lucas Nunes <devmaster@lbnsistemas.com.br>
  @description Objeto para enviar as ações da view e receber o retorno do controller
 */
var AzulSender = function () {
    // Carga valiosa a ser implementada e enviada ao WS
    this.payload = {
        client: _SYS_CLIENT,
        application: _SYS_APPLICATION,
        module: _SYS_MODULE,
        controller: _SYS_CONTROLLER,
        view: _SYS_VIEW,
        action: '',
        url: _SYS_DOMAIN  + _SYS_CLIENT + '/webservice',
        method: 'POST',
		files:[], // [{nameID, files},..]
        envelope: {
            // dataTypes : date, time, dateTime, number, currency, text, cep, cpf, cnpj, ie, password
            fields: [{ value: '', column: '', dataType: '' }],
            // Pode ser um array (varias colunas) ou string (unica coluna)
            columns: [],
            init: 0,
            limit: 1,
            orderBy: '',
			extra: '',
            // PK da tabela principal ou da tabela do formulario dinamico
            // Pode ser um array (varios IDs selecionados) ou string (unico ID)
            ID: 0,
            // Regras : =, %(like), _B(beetween)
            filters: [{ value1: '', value2: '', columns: [], rule: '=' }],
        },
        envelopeJSON : ''
    };
    // Cópia inicial de payload
    this.payloadInitialCopy = jQuery.extend(true, {}, this.payload);
    // Callback a ser implementado pela View
    this.callback = function(){};
	 // Callback de falha a ser implementado pela View
    this.callbackFail = function(){};
	// Variavel que indica se é um formulario com arquivos para upload ou não
	this.isUploadForm = false;
};
/**
  Seta as propriedades de payload.envelope{}
  @return void
*/
AzulSender.prototype.setPayload = function (p) {
    if (p.client)
        this.payload.client = p.client;
    if (p.application)
        this.payload.application = p.application;
    if (p.module)
        this.payload.module = p.module;
    if (p.controller)
        this.payload.controller = p.controller;
    if (p.view)
        this.payload.view = p.view;
    if (p.action)
        this.payload.action = p.action;
	if (p.url)
        this.payload.url = p.url;
	if (p.files)
        this.payload.files = p.files;
};
/**
  Seta as propriedades de pauload.envelope{}
  @return void
*/
AzulSender.prototype.setEnvelope = function (p) {
    if (p.fields){
		var fields = [];
		this.payload.files = [];
        for(var i in p.fields) {
			var field = p.fields[i];	
			if(field.files!=undefined) {
				this.payload.files.push({nameID:field.nameID, files:field.files});
				field.files = null;
			}
			fields.push(field);	
		}
		this.payload.envelope.fields = fields;
	}
    if (p.columns)
        this.payload.envelope.columns = p.columns;
    if (p.ID > Array.isArray(p.ID))
        this.payload.envelope.ID = p.ID;
    if (p.init)
        this.payload.envelope.init = p.init;
    if (p.limit)
        this.payload.envelope.limit = p.limit;
    if (p.orderBy)
        this.payload.envelope.orderBy = p.orderBy;
	if (p.extra)
		this.payload.envelope.extra = p.extra;
    if (Array(p.filters).length > 0)
        this.payload.envelope.filters = p.filters;
};
/**
  Retorna um objeto FormData para envio ao WS
  @return string
*/
AzulSender.prototype.setFormData = function () {
	this.payload.envelopeJSON = JSON.stringify(this.payload.envelope);
	var self = this;
	self.isUploadForm = false;
	/*
	var retString = "client="+this.payload.client+"&application="+this.payload.application+"&module="+this.payload.module+
	"&controller="+this.payload.controller+"&view="+this.payload.view+"&action="+this.payload.action+"&envelopeJSON="+
	this.payload.envelopeJSON;
	return(retString);
	*/
	var formData = new FormData();
	formData.append('client', this.payload.client);
	formData.append('application', this.payload.application);
	formData.append('module', this.payload.module);
	formData.append('controller', this.payload.controller);
	formData.append('view', this.payload.view);
	formData.append('action', this.payload.action);
	formData.append('azismobile',1);
	
	// Prepara os arquivos para upload junto com os campos do formulario
	for(var i in this.payload.files) {
		var objFile = this.payload.files[i];
		$.each(objFile.files,function(i,file) {
			formData.append(objFile.nameID+i,file);
			self.isUploadForm = true;	
		});
	}
	formData.append('envelopeJSON',this.payload.envelopeJSON);
	return(formData);
}
/**
  Envia payload para: {domain}/webservice (indexphp -> home/{user}/_azul/webservice.php)
  Se receber uma mensagem JSON informando sucesso, executa a function callback()
  definida pelo usuário
  @param silentMode:boolean => se TRUE, faz o enviar sem exibir a div loading ...
  @param uploadForm:boolean=> se TRUE prepara a requisição para envio multipart, caso contrario mantém padrão ajax
  @return void
*/
AzulSender.prototype.send = function (silentMode=false) {
  // Proxy para a função callback ser executada pela função ajax do jQuery
  var self = this;

  // Utiliza função do Jquery para envio dinamico (AJAX)
  $.ajax({
   type: self.payload.method,
   url:  self.payload.url,
   data: self.setFormData(),
   contentType: false,
   processData: false,
   cache: false,
   error: function(jqXHR){
      if(jqXHR.status==0) {
          _azGeneral.alert("Erro","Falha de conexão, verifique se você possui acesso a internet.");
      }else {
          _azGeneral.alert("Erro","Falha no envio do dados, verifique sua conexão e tente novamente.");
      }
	  if(silentMode==false) {
	    // Oculta o preloader
	   // app.preloader.hide();
	  }
   },
   beforeSend: function(jqXHR, settings) {
     jqXHR.setRequestHeader('sessionId', _azSessionId);
	 jqXHR.setRequestHeader('azFrameworkVersion', _SYS_FRAMEWORK );
     if(silentMode==false) {
	   // Abre o preloader
	   //app.preloader.show();
	 }
   },
   success: function(data){

     // Oculta o preloader
	// app.preloader.hide();

     // Determina se o retorno é uma String JSON válida:
     if(_azGeneral.testJSON(data)!==false) {
         objRetJSON = JSON.parse(data);
		 // Se estiver em modo de debugação salva e imprime o retorno no console
    	 if(_DEBUG_MODE) _azGeneral.debug(objRetJSON);
		 var redirect;
         // Se retornar mensagem de erro/info
         if(objRetJSON.type=='fail' || objRetJSON.type=='info') {
		   // Função callback para falha/insucesso
		   self.callbackFail(objRetJSON.data);
		   // Exibe mensagem
		   _azGeneral.alert('Ops!', objRetJSON.message, objRetJSON.type,objRetJSON.data.redirect);
		   
		   /*
				Tratamento de login inativo 
		   */
		   if(objRetJSON.data=='login_inativo') {
				openPopup('popup-login');   
		   }
		   
		   // Reseta o sa.sender
		   self.reset();
         // Caso a requisição tenha sido processada com sucesso
         }else if(objRetJSON.type == 'win'){
           // Se houver alguma mensagem de sucesso, exibe um modal
           if(objRetJSON.message.trim()!='') {
             _azGeneral.alert('Ação concluída', objRetJSON.message,'win',objRetJSON.data.redirect);
           }

           // Executa a função callback
          self.callback(objRetJSON.data);
         }
     // Caso não seja uma string JSON válida :
     }else {
		 // Se estiver em modo de debugação salva e imprime o retorno no console
    	 if(_DEBUG_MODE) _azGeneral.debug(data);
         _azGeneral.alert('Desculpe-nos!', 'Algo estranho ocorreu no servidor, informe o suporte técnico.','fail');
         // Reseta o sa.sender
         self.reset();
     }
   }
  });
};
/**
  Reseta payload com as configurações iniciais
  @return void
*/
AzulSender.prototype.reset = function () {
  this.payload = this.payloadInitialCopy;
};

// Cria nova instância para AzulGeneral
console.log('file "AzulSender.class.js" is ready!');
_azSender7845 = new AzulSender(); // Sufixo '7845', pois para cada thread será chamada uma nova instância, previnindo conflitos
