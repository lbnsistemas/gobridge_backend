/**
  @description Principais classes JS do framework Azul
  @author Lucas Nunes <devmaster@lbnsistemas.com.br>
  @version 2.8.15
  @since  22/09/2016
  @update 09/07/2019
*/

// Variaveis Globais do MVC Azul:
var _DEBUG=[];
var _DEBUG_MODE=true;
var _SYS_DOMAIN='http://gobridge.com.br/backend/';
var _SYS_FRAMEWORK_URL='http://gobridge.com.br/backend/_azul/';

//var _SYS_DOMAIN='http://192.168.25.185/lbn/azul-project/';
//var _SYS_FRAMEWORK_URL='http://192.168.25.185/lbn/azul-project/_azul/';

//var _SYS_DOMAIN='http://www.lbnsistemas.com.br/azul-project/';
//var _SYS_FRAMEWORK_URL='http://www.lbnsistemas.com.br/azul-project/_azul/';

//var _SYS_DOMAIN='http://localhost/';
//var _SYS_FRAMEWORK_URL='http://localhost/_azul/';

var _SYS_CLIENT = 'gobridge';
var _SYS_APPLICATION = 'app_gobridge';
var _SYS_MODULE = 'webadmin';
var _SYS_CONTROLLER = 'cMain';
var _SYS_VIEW = '';
var _SYS_DATA = {};
var _SYS_INDEX_LIMIT = 10;
var _RESERVED_ID=''; // Reserva de um ID para um novo cadastro
var _VIEW_FUNCTION='';
var _SYS_APP_URL = _SYS_DOMAIN+'/'+_SYS_APPLICATION+'/'+_SYS_CLIENT+'/'+_SYS_MODULE+'/';
var _USER_ID; // ID do usuário logado
var _USER_NAME; // Nome do usuário logado
var _USER_NICKNAME; // NickName do usuário logado
var _USER_PROFILEPIC='../../app/img/no_profile_picture.jpg'; // NickName do usuário logado
var _GL_APP_THEME = _GL_APP_THEME==undefined ? 'auto' : _GL_APP_THEME; // Tema do aplicativo
var _SYS_FRAMEWORK = '@MVC Azul Framework v2.8.15';
console.log( _SYS_FRAMEWORK );

// Salva um novo UUID para sessão ativo e salva no armazenamento local do dispositivo
var _azSessionId = localStorage.getItem('myCookieName');
// if there was no localStorage for the session id
// the application is being run for the first time
// the session id must be created
if (!_azSessionId) {
    _azSessionId = generateUUID();
    localStorage.setItem('myCookieName', _azSessionId);
}
/*Envia o id da sessão  sempre que uma função ajax for chamada
$.ajaxPrefilter(function(options, originalOptions, jqXHR) {
    // if there is data being sent
    // add the sessionId to it
    if (options.data) {
        options.data += '&sessionId=' + _azSessionId;
    }
    // if there is no data being sent
    // create the data and add the sessionId
    else {
        options.data = 'sessionId=' + _azSessionId;
    }
});*/

// Cria um UUID
function generateUUID () { // Public Domain/MIT
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
        d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}
// Classe com as principais funções JS
$.getScript("azul_js/AzulGeneral.class.js", function()
{
	console.log('file "AzulGeneral.class.js" is ready!');
	_azGeneral = new AzulGeneral();
	// Classe com funções JS para validação de formulários
	$.getScript("azul_js/AzulValidation.class.js", function()
	{
		console.log('file "AzulValidation.class.js" is ready!');
		_azValidation = new AzulValidation();
		// Classe com funções JS para tratamento de dados
		$.getScript("azul_js/AzulDataManipulation.class.js", function()
		{
			console.log('file "AzulDataManipulation.class.js" is ready!');
			_azDataManipulation = new AzulDataManipulation();
			// Classe com funções JS para envelopamento e envio de pacotes de dados para o webservice
			$.getScript("azul_js/AzulSender.class.js", function()
			{
				console.log('file "AzulSender.class.js" is ready!');
				_azSender7845 = new AzulSender(); // Sufixo '7845', pois para cada thread será chamada uma nova instância, revinindo conflitos

				// ....


			});
		});
	});
});
