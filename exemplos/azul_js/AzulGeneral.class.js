/**
  @class AzulGeneral
  @author Lucas Nunes <devmaster@lbnsistemas.com.br>
  @description Funções de utilizade geral
 */
var AzulGeneral = function () {
  // Construtor
};
/**
   Isola o valor e um parametro em uma dada URL.
   @param  {var} nome do parametro
   @return {var} url(optional)
 */
AzulGeneral.prototype.urlParam = function(name, url) {
    if (!url) {
     url = window.location.href;
    }
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(url);
    if (!results) {
        return undefined;
    }
    return results[1] || undefined;
}
/**
   Salva ocorrencia na global _DEBUG e imprime no console de depuração do navegador
   @param  {var} ocorrencia
   @return {void}
 */
AzulGeneral.prototype.debug = function (ocorrencia) {
   _DEBUG.push(ocorrencia);
   console.log(ocorrencia);
}
/**
  Função para testar um string e determinar se é um JSON valido
  @param {string} jsonString
  @return {object} json or {false}
*/
AzulGeneral.prototype.testJSON = function(jsonString) {
  try {
      var o = JSON.parse(jsonString);
      if (o && typeof o === "object" && o !== null) return(o);
  }
  catch (e) { }
  return(false);
};
/**
    Função para exibir a data de hoje, no padrão BR ou US
    @param {string} titulo
    @param {string} mensagem
    @param {string} titpo 'erro' ou 'sucesso'
*/
AzulGeneral.prototype.today = function(padrao='BR'){
  var local = new Date();
  local.setMinutes(local.getMinutes() - local.getTimezoneOffset());
  var today =  local.toJSON().slice(0,10);
  if(padrao=='BR') {
  	today = _azDataManipulation.invertDate(today);
  }
  return(today);
  
}
/**
    Função para exibir mensagens de erro ou sucesso
    @param {string} titulo
    @param {string} mensagem
    @param {string} titpo 'erro' ou 'sucesso'
 */
AzulGeneral.prototype.alert = function(title, message, type='', redir=''){
  // Se estiver em modo de debugação salva e imprime o retorno no console
  if(_DEBUG_MODE) this.debug(message);
  if(message!='') {
	  alert(message);
	  /*app.dialog.alert(message,title,function(){
			if(redir!='') {
				window.location = redir;	
			}
	  });*/
  }else if(redir!='') {
	  window.location = redir;
  }
}
/**
  Função para determinar se um dador valor/array está vazio
  @param {var} value
  @return {boolean}
*/
AzulGeneral.prototype.isEmpty = function (value) {
	var retorno = true;
	// Verificação de array ou objeto
	if(typeof value=='array' || typeof value=='object') {
		for(var a in value) {
			// Verifica se o valor é uma string
			if(typeof value[a]=='string') {
				var teste =  value[a].trim();
				if(teste!='') return(false);
			// Verifica se o valor é um numero ou boolean
			}else if(typeof value[a]=='number' || typeof value[a]=='boolean') {
				return(false);
			// Se for objeto, utiliza recursividade
			}else if(typeof value[a]=='object') {
				retorno = this.isEmpty(value[a]);
			}
		}	
	// Verificação de string
	}else if (typeof value=='string') {
		var teste = value.trim();
		if(teste!='' ) return(false);
	// Verifica se o valor é um numero ou boolean
	}else if (typeof value=='number' || typeof value=='boolean') {
		return(false);
	}
	// Retorna a variavel de retorno
	return(retorno);
}
/**
   @description Pega os campos úteis (az-field) do formulario solicitado ou do pacote main
   @param {string} form : nameID do formulario ou a palavra "Main" (todos formularios do pacote main)
   @param {bool} dynamic : true (formulario dinamico) ou false
   @param {integer} ID : PK do cadastro main ou do cadastro dinamico relacionado
 */
AzulGeneral.prototype.getFields = function (form, dynamic=false, ID='') {
  var arFields=false;
  // Se for informado uma string em form :
  if(typeof form == 'string') {
    // Pega todos os campos dos formularios do pacote main
    if(form=='Main') {
      arFields =  $(document).find('form.main').find('.az-field');
      return(arFields);
    // Se foi informado um ID ou uma classe HTML
    } else if(form.indexOf(".")===0 || form.indexOf("#")===0) {
      // form = form;
    // Default : Busca os campos pelo ID
    }else {
      //arFields = $('#'+form).find('.az-field');
      form = '#'+form;
    }
    /* Verifica se é um formulario dinamico do sistema LBN SIge
    // (pega apenas os campos assinados com o mesmo ID do cad. dinamico relacionado)
    if(dynamic=true && ID>0) {
      arFields =	$(form).find('.az-field[az-ID="'+ID+'"]');
    // Se for um formulario comum :
    }else {
      arFields = $(form).find('.az-field[az-ID=""]');
    }*/
	var arFields = [];
	$(form).each(function() {
		var f =    $(this).find('.az-field[az-ID="'+ID+'"]');
		for(var i=0; i<f.length; i++) { 
			arFields.push(f[i]);
		}
	});
  // Se for um object
  }else if (typeof form == 'object') {
    arFields = $(form).find('.az-field');
  // Erro
  }else {
    this.alert("Desculpe-nos !","Houve um erro no carregamento do(s) formulário(s), atualize a página","fail");
    return(false);
  }
  // Retorna os campos encontrados
  return(arFields);
}
/**
 * @description   Incluí um arquivo js da pasta plugins/
 * @param     {string} pluginName : nome do Plugin JS a ser carregado dinamicamente
 * @param		  {function} callBack : função a ser executada após carregamento do plugin
 */
AzulGeneral.prototype.addPlugin = function (pluginName, callBack = '') {
  $.getScript(_SYS_FRAMEWORK_URL+"plugins/"+pluginName)
    .done(callBack)
    .fail(function( jqxhr, settings, exception ) {
    this.alert('Desculpe-nos!','Erro no carregamento do pluggin : '+pluginName
    +', atualize a página ou contate o suporte.','fail');
  });
},
/**
   @description Retorna todos os campos que sofreram alteração na View, enviando estes para o WS.
   utilizar a mesma estrutura de campos do Bootstrap, porem com os atributos :
   sa-default (value ou status default, para determinar que o campo foi alterado)
   @param {string} form : nameID do formulario ou a palavra "Main" (todos formularios do pacote main)
   @param {bool} dinamico : true (formulario dinamico) ou false
   @param {integer} ID : PK do cadastro main ou do cadastro dinamico relacionado
   @return array with updated fields
 */
AzulGeneral.prototype.getUpdatedFields = function(form, dynamic=false, ID='') {
  arUpdatedFields = [];
  // Pega os campos do formulario(s) ou do pacote main
  var arFields =  this.getFields(form, dynamic, ID);

  if(_DEBUG_MODE) _azGeneral.debug(arFields);

  if(typeof arFields == "undefined") return(false);

  for(var i=0; i < arFields.length; i++) {
    obj = arFields[i];
    // Nome do nó (input, textarea,select,..)
    objNodeName = $(obj).prop('nodeName').toLowerCase();
    // Tipo do campo (text,password, radio, checkbox)
    objType = $(obj).attr('type') != undefined ? $(obj).attr('type').toLowerCase() : '';
    // Tipo de dados deste campo
    objDataType = $(obj).attr('az-dataType').trim();
	
    // Se for um input (text, password) ou textarea
    if( objNodeName=='input' && (objType=='range' || objType=='number' || objType=='date' || objType=='tel' || objType=='email' || objType=='input' || objType=='text' || objType=='password' || objType=='hidden')
	|| objNodeName=='textarea') {
	  if($(obj).val() != $(obj).attr('az-default') || $(obj).attr('az-alwaysSending')=="true") {
		objUpdatedField = {
			'column' : $(obj).attr('az-column-db'),
			'value'	 : _azDataManipulation.checkIn(obj),
			'default': $(obj).attr('az-default'),
			'nameID' : $(obj).attr('id')
		}
		arUpdatedFields.push(objUpdatedField);
	  }
	// Se for um input (radio, checkbox)
  	}else if( objNodeName=='input' &&  objType=='checkbox' ) {
	  // Se for um array/object, transforma em string para comparar com o value default
	  if(typeof $(obj).val() == 'object' && $(obj).val() !=null) {
		var compare = $(obj).val().toString();
	  // Pega a string/number para comparação
	  }else if(typeof $(obj).val() == 'string') {
		var compare = $(obj).val();
	  }else {
		var compare = '';
	  }
	  // Foi marcado ou desmarcado agora!
	  if(compare != $(obj).attr('az-default') || $(obj).attr('az-alwaysSending')=="true" ) {
		objUpdatedField = {
			'column' : $(obj).attr('az-column-db'),
			'value'	 : _azDataManipulation.checkIn(obj),
			'default': $(obj).attr('az-default'),
			'nameID' : $(obj).attr('id')
		}
		arUpdatedFields.push(objUpdatedField);
	  }
	// Se for um select
 	}else if( objNodeName=='select' ) {
	  // Se for um array/object, transforma em string para comparar com o value default
	  if(typeof $(obj).val() == 'object' && $(obj).val() !=null) {
		var compare = $(obj).val().toString();
	  // Pega a string/number para comparação
	  }else if(typeof $(obj).val() == 'string') {
		var compare = $(obj).val();
	  }else {
		var compare = '';
	  }
	  if(compare != $(obj).attr('az-default') || $(obj).attr('az-alwaysSending')=="true" ) {
		objUpdatedField = {
		  'column' : $(obj).attr('az-column-db') || '',
		  'value'  : _azDataManipulation.checkIn(obj),
		  'default': $(obj).attr('az-default') || '',
		  'nameID' : $(obj).attr('id')
		}
		arUpdatedFields.push(objUpdatedField);
	  }
	// Se for uma div (exclusiva para HTML), sempre envia ao WS
	}else if( objNodeName=='div'  ){
	  objUpdatedField = {
		  'column' : $(obj).attr('az-column-db')  || '',
		  'value'	 : $(obj).summernote('code'),
		  'nameID' : $(obj).attr('id'),
		  
	  }
	  arUpdatedFields.push(objUpdatedField);
	// Se for um input file (upload de arquivo)
	}else if( objNodeName=='input' && (objType=='file') ) {
	  objUpdatedField = {
		  'column' : $(obj).attr('az-column-db')  ||'',
		  'value'  : _azDataManipulation.checkIn(obj),
		  'nameID' : $(obj).attr('id'),
		  'files'  : $(obj)[0].files
	  }
	  arUpdatedFields.push(objUpdatedField);
	}
  }
  // Retorna os campos alterados para serem enviados ao WS
  return(arUpdatedFields);
}
/**
   @function      initCheckboxes
   @description   Inicia um ou mais checkbox, criando um hidden para armazenar o conteudo de cada chk marcado
   @param         {string} nameID : propriedade nameID do objeto (php) campo do tipo checkbox
 */
AzulGeneral.prototype.initCheckbox = function(nameID) {
  // Busca todos os chkbox do grupo
  var chkboxes = $($.find('.'+nameID)).find('input[type="checkbox"]');
  // Lista todos chkboxes encontrados
  for(var ck=0; ck<chkboxes.length; ck++ ){
    var obj = $(chkboxes[ck]);
    obj.click(function(e) {
      // Se estiver desmarcando, retira o value do hidden
      if(!this.checked) {
        var pattern = this.value+"|,"+this.value;
        // Retira todas ocorrências de value em hidden (com ou sem o separador ',')
        var value = $('#'+nameID).val().replace(new RegExp(pattern,'g'), '');
              $('#'+nameID).val(value);
      // Se estiver marcando, acrescenta o value no hidden
      }else {
        // Se for o primeiro valor acrescentado, não utilizado o separado ','
        if($('#'+nameID).val().trim()=='') {
          $('#'+nameID).val($('#'+nameID).val()+this.value);
        // Acrescenta value em hidden com o separador ','
        }else {
          $('#'+nameID).val($('#'+nameID).val()+','+this.value);
        }
      }
    });
  }
}

// Cria nova instância para AzulGeneral
console.log('file "AzulGeneral.class.js" is ready!');
_azGeneral = new AzulGeneral();
