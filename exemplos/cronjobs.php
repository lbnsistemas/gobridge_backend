﻿<?php
	/**
	 * MasterBusiness
	 * Azul Framework - suporte@lbnsistemas.com.br
	 *
	 * Envia um pacote formatado ao webservice (WS) da aplicação contendo a action "updateAppLevels"
	 * Está ação irá atualizar todos os levels dos associados, aumentando ou reduzindo seus níveis
	 * com base na configuração de pontuação por (indicação, negócio e visita)
	 *
	 */
	// Define as principais constantes
	define('_application', 	'redesocial');
	define('_client', 		'masterbusiness');
	define('_module',		'webadmin');
	define('_view',			'cronjob');
	define('_controller',	'cAppLevelsRules');
	define('_action',		'updateAppLevels');
	define('SYS_DOMAIN',	'https://www.projetoazul.com.br/masterbusiness/');
	
	// Configura os parametros de envio
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, SYS_DOMAIN.'webservice');  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 15);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_POST, 1);
	// Define o pacote de envio contendo a action updateAppLevels
	$dataToPost = "application="._application
	."&module="._module
	."&client="._client
	."&controller="._controller
	."&view="._view
	."&action="._action
	."&envelopeJSON={\"ID\":0}";
	curl_setopt($ch, CURLOPT_POSTFIELDS, $dataToPost);
	// Envia o pacote e salva o resultado
	$ret = trim(curl_exec($ch));
	
	
	//var_dump($ret);
	//exit();
	// Verifica se o WS retornou uma string json valida
	$objRet=json_decode($ret);
	if($objRet!==false) {
		// Verifica	se processou com sucesso a solicitação
		if($objRet->type=='win') {
			$winMessage = date('Y-m-d H:i')." - ".$objRet->message."\r\n";
		// Falha no processamento 	
		}else {
			// Monta a mensagem de erro para o log
			$errorMessage = date('Y-m-d H:i')." - Falha de processamento remoto: ".$objRet->message."\r\n";
		}
	}else {
		// Monta a mensagem de erro para o log
		$errorMessage = date('Y-m-d H:i')." - O WS não retornou uma string JSON: '".$ret."'\r\n";	
	}
	
	
	if($errorMessage!='') {
		error_log($errorMessage,3, "cron_fail.log");
		error_log($errorMessage,1,"suporte@lbnsistemas.com.br");
	}else if($winMessage) {
		error_log($winMessage,3, "cron_win.log");
	}
	curl_close($ch);
?>