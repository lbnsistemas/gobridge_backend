<?php
	//Get all errors except NOTICE, STRICT e WARNINGS
	error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT ^ E_WARNING);
	
	/***
	 * Starts a session with a specific timeout and a specific GC probability.
	 * @param int $timeout The number of seconds until it should time out.
	 * @param int $probability The probablity, in int percentage, that the garbage
	 *        collection routine will be triggered right now.
	 * @param strint $cookie_domain The domain path for the cookie.
	 */
	function session_start_timeout($timeout=5, $probability=100, $cookie_domain='/') {
		// Set the max lifetime
		ini_set("session.gc_maxlifetime", $timeout);

		// Set the session cookie to timout
		ini_set("session.cookie_lifetime", $timeout);

		// Change the save path. Sessions stored in teh same path
		// all share the same lifetime; the lowest lifetime will be
		// used for all. Therefore, for this to work, the session
		// must be stored in a directory where only sessions sharing
		// it's lifetime are. Best to just dynamically create on.
		$seperator = strstr(strtoupper(substr(PHP_OS, 0, 3)), "WIN") ? "\\" : "/";
		$path = $_SERVER['DOCUMENT_ROOT'] . $seperator . '..' . $seperator . "session_" . $timeout . "sec";
		if(!file_exists($path)) {
			if(!mkdir($path, 750)) {
				trigger_error("Failed to create session save path directory '$path'. Check permissions.", E_USER_ERROR);
			}
		}
		ini_set("session.save_path", $path);


		// Set the chance to trigger the garbage collection.
		ini_set("session.gc_probability", $probability);
		ini_set("session.gc_divisor", 100); // Should always be 100

		// Start the session!
		session_start();

		// Renew the time left until this session times out.
		// If you skip this, the session will time out based
		// on the time when it was created, rather than when
		// it was last used.
		if(isset($_COOKIE[session_name()])) {
			setcookie(session_name(), $_COOKIE[session_name()], time() + $timeout, $cookie_domain);
		}
	}
	// Iinicia/reinicia a sessão com x segundos de duração
	session_start_timeout(60*60*24*30);//30 dias
	
	/**
	 * Cria as Statics obrigatórias para o carregamento da página (template)
	 * @param string $pagina : nome da página  a ser carregada pela View
	 * @param string $cliente : define o arquivo config.php e diretorios exclusivos
	 * @return void
	 */ 
	function defineConstants($routesFile) {
		// Check if routes.xml exists
		if(!file_exists($routesFile)) die("Routes file (routes.xml) not exists.");
		// Check if it not received POST data 
		if(!isset($_POST['client'])) {
			$routesXML = new DOMDocument;
			// Load routes file
			$routesXML->loadXML(file_get_contents($routesFile));
			// Load clients
			$clients = $routesXML->getElementsByTagName("client");
			// Get a portion of URL string
			$pURL = parse_url($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
			// Get just path of url string
			$pURL = $pURL['path'];
			// Reads the clients
			foreach($clients as $client) {
				$rule1 = $client->getAttribute("rule");// Property of <client>
				
				if(preg_match($rule1,$pURL,$matches)) {
					// Defines client constant
					$client_nameID = $client->getAttribute("nameID");// Property of <client>
					define("_client", $client_nameID);	
					// Defines aplication constant
					$client_application = $client->getAttribute("application");// Property of <client>
					define( "_application", $client_application );	
					
					// Load routes in parent <client>
					$routes = $client->getElementsByTagName("route");
					// Reads the routes found
					foreach( $routes as $route ) {
						$rule2 = $route->getElementsByTagName("rule");
						$rule2 = $rule2->item(0)->nodeValue;
						if( preg_match( $rule2, $pURL, $matches ) ) {
							// Module
							$dom = $route->getElementsByTagName("module");
							$name = $dom->item(0)->nodeValue;
							define( "_module", $name );	
							// View
							$dom = $route->getElementsByTagName("view");
							$value = $dom->item(0)->nodeValue;
							$name = (is_numeric($value)===true ? $matches[$value] : $value);
							define( "_view", $name );
							// Access type
							$dom = $route->getElementsByTagName("access");
							$name = $dom->item(0)->nodeValue;
							define( "_access", $name );
							break;
						}
					}
				}
			}
		// Defines constants throug POST variables
		}else {
			define('_application',	$_POST['application']);
			define('_client',		$_POST['client']);
			define('_module',		$_POST['module']);
			define('_view',			$_POST['view']);
		}
		// Check the constants: _client, _application e _module 
		if(!defined('_client') || !defined('_application') || !defined('_module')) 
			die("_client, _application ou _module n&atildeo reconhecido(s).");
		// Check the constants content defined above
		if(_client=='' || _application=='' || _module=='')
			die("_client, _application ou _module est&atildeo vazio(s).");
	}
	
	// Defines the system constants based on routes file 
	defineConstants('routes.xml');	

	// Master plataform config file 
	require_once 'application/'._application.'/'._client.'/'._module.'/config.php';
	
	// Calls  the custom error functions
	require_once SYS_SRC_LIBS."error.php";
	
	// Loads and starts class Controller
	// Controller file loads Controller.classes.php
	require_once SYS_SRC_LIBS.'Controller.php';
	$controller = new Controller();
	
	// Starts a new instance of Database class
	$azul_DB = $controller->getMysqli();
	
	// Loads and starts class View
	require_once SYS_SRC_LIBS.'View.php';
	$view = new View();
	
	/*Checks 'www'
	if(strpos($view->urlVirtual(),'www.')!==false) {
		$newURL = str_replace("www.","",$view->urlVirtual());
		$view->printLocation($newURL);
	}*/	
	

	// Valid request for Controller
	if($controller->isControllerRequest()) {
		
		require(SYS_SRC_FRAMEWORK."webservice.php");
		
		
	// Others requests
	} else {
		// Alteração dia 11/08/2017 - Lucas Nunes
		// Realiza acesso via token (acesso direto de outros dispositivos)
		$ret = $controller->doLoginByToken();
		//var_dump($ret);
		//exit();
		
		// Checks access type
		if(_access!='public') {
			// Verifica se a view necessita de validação de sessão, caso necessite : 
			// verifica se o usuário está logado em uma sessão ativa e tem permissão de visualização sobre a View
			//$ret = $controller->checksPermission('V',_view);
			$ret = ($controller->validSession()==false ? -1 : $controller->validSession());
			//var_dump($_SESSION);
		 	//exit();	
		}else $ret=true;	
			
		// Se houver permissão e login ativo:
		if($ret===true) {
			// Mostra a view requerida
			require SYS_SRC_APP.'views/'._view.'/'._view.'.php';
			
		// Usuário não logado :
		}else if($ret==-1){
			// Mostra view de login
			require SYS_SRC_APP.'views/login/login.php';
			
		// Sem permissão de visualização (V) para a view requerida :
		}else {
			$msgErro = "Permiss&atilde;o negada. Verifique o cadastro de usu&aacute;rios.";
			require SYS_SRC_APP.'views/errorPages/general.php';
			
		}	
	}	
	
?>	